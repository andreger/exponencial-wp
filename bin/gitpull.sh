#!/bin/bash

if [ -z "$1" ]
  then
    echo "Erro. Necessario informar o branch de deploy"
    exit
fi

printf "Script de execucao de git pull iniciado.\n"

projetos=(exponencial-wp exponencial-tema exponencial-notafiscal exponencial-questoes exponencial-coaching)
for p in "${projetos[@]}"
do
    echo "Atualizando projeto: $p"
    cd ../../$p && git checkout $1 && git pull && cd -
done

printf "Script de execucao de git pull finalizado.\n"