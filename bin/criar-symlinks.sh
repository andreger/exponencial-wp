#!/bin/bash

folder="src"

if [ "$1" != "src" ] && [ "$1" != "dist" ] ;  then
  echo "Erro. Executar comando no seguinte formato: ./deploy.sh [src|dist] [develop|master]"
  exit
fi

folder=$1

printf "Script de criacao de symlinks iniciado.\n"

function removerSymlinks()
{
  basedir="../$folder/"

  projetos=(notafiscal painel-coaching/uploads painel-coaching questoes/uploads questoes
  core wp-content/themes/academy wp-content/temp wp-content/uploads ../www  )

  for p in "${projetos[@]}"
  do
    symlink=$basedir$p
    printf "Excluindo symlink: %s => " "$symlink"
    rm "$symlink" 2> /dev/null
    printf "Ok\n"
  done
}

function criarSymlinks()
{
  printf "Criando symlinks... "
  cd ../$folder && ln -s ../../exponencial-notafiscal/$folder notafiscal && cd -
  cd ../$folder && ln -s ../../exponencial-questoes/$folder questoes && cd -
  cd ../$folder && ln -s ../../exponencial-coaching/$folder painel-coaching && cd -
  cd ../$folder && ln -s ../../exponencial-tema/$folder/core core && cd -
  cd ../$folder/wp-content/themes && ln -s ../../../../exponencial-tema/$folder academy && cd -

  cd ../$folder/wp-content && ln -s ../../../../files/exponencial/uploads uploads && cd -
  cd ../$folder/wp-content && ln -s ../../../../files/exponencial/temp temp && cd -
  cd ../../exponencial-coaching/$folder && ln -s ../../../files/exponencial-coaching/uploads uploads && cd -
  cd ../../exponencial-questoes/$folder && ln -s ../../../files/exponencial-questoes/uploads uploads && cd -

  cd .. && ln -s $folder www && cd -
  printf "Ok\n"
}

removerSymlinks

criarSymlinks

printf "Script de criacao de symlinks finalizado.\n\n\n"