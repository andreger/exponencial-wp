#!/bin/bash

function rodarGulp()
{
  projetos=(exponencial-wp exponencial-coaching exponencial-notafiscal exponencial-questoes exponencial-tema)
  for p in "${projetos[@]}"
  do
    printf "Rodando gulp em : %s" "$p"
    cd ../../$p && npm install && npm run gulp && cd -
    printf "Ok\n"
  done
}

function criarSymlinks()
{
  ./criar-symlinks.sh $1
}

function rodarGit()
{
  ./gitpull.sh $1
}

function cacheFlush()
{
  cd ../www && wp cache flush --allow-root && cd -
}

function rodarComposer()
{
  projetos=(exponencial-questoes)
  for p in "${projetos[@]}"
  do
    printf "Rodando composer em : %s" "$p"
    cd ../../$p/dist && composer install -q && cd -
    printf "Ok\n"
  done
}

if [ "$1" != "src" ] && [ "$1" != "dist" ] ;  then
  echo "Erro. Executar comando no seguinte formato: ./deploy.sh [src|dist] [develop|master]"
  exit
fi

if [ "$2" != "master" ] && [ "$2" != "develop" ] && [ "$2" != "m5" ] ;  then
  echo "Erro. Executar comando no seguinte formato: ./deploy.sh [src|dist] [develop|master]"
  exit
fi

printf "Deploy iniciado.\n"

rodarGit $2

rodarGulp

rodarComposer

criarSymlinks $1

cacheFlush

printf "Deploy finalizado.\n\n\n"