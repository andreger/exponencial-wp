var gulp = require('gulp'),
    clean = require('gulp-clean'),
    browserSync = require('browser-sync');

gulp.task('clean', function() {
    return gulp.src('dist', {allowEmpty: true})
        .pipe(clean());
});

gulp.task('copy', function() {
    return gulp.src(['src/**/*', '!src/kadmin/**', '!src/core/**', '!src/questoes/**', '!src/notafiscal/**', '!src/painel-coaching/**',
            '!src/wp-content/themes/academy/**', '!src/wp-content/temp/**', '!src/wp-content/uploads/**'], { dot: true,  allowEmpty: true, buffer:false })
        .pipe(gulp.dest('dist'));
});

gulp.task('server', async function() {
    browserSync.init({
        open: 'external',
        host: 'exponencial',
        proxy: 'exponencial'
    });

    gulp.watch('www/**/*.*').on('change', browserSync.reload);
});

gulp.task('default', gulp.series('clean', 'copy'));