<?php
require_once 'item_coaching.php';
require_once '../wp-load.php';

$emails = array();
foreach(get_alunos_ativos_podio() as $aluno) {
	array_push($emails, $aluno['email']);
	echo "Adicionando e-mail: " . $aluno['email'] . "<br>";
}

$cupons = ItemCoaching::listar_cupons();

echo "Atualizando " . count($cupons) . "cupons";
foreach ($cupons as $cupom) {
	echo "Atualizando cupom: " . $cupom['ite_slug'];
	
	$post = get_cupom_by_post_name($cupom['ite_slug']);
	update_post_meta($post->ID, 'customer_email', $emails);
}