<?php
require_once 'database.php';

class UsuarioLucro {
	
	static function init($usuarioLucro)
	{
		$sql = "INSERT INTO `usuarios_lucro`(`usu_id`, `ul_valor`, `ul_data_inicio`) VALUES ('{$usuarioLucro['usu_id']}', '{$usuarioLucro['ul_valor']}', '{$usuarioLucro['ul_data_inicio']}')";
		$GLOBALS['link_corp']->query($sql);
	}
	
	static function salvar($usuarioLucro)
	{
		$sql = "UPDATE `usuarios_lucro` SET `ul_data_fim` = NOW() WHERE `usu_id` = " . $usuarioLucro['usu_id'] . " AND `ul_data_fim` IS NULL";
		$GLOBALS['link_corp']->query($sql);
		
		$sql = "INSERT INTO `usuarios_lucro`(`usu_id`, `ul_valor`, `ul_data_inicio`) VALUES ('{$usuarioLucro['usu_id']}', '{$usuarioLucro['ul_valor']}', NOW())";
		$GLOBALS['link_corp']->query($sql);
	}
	
	static function get_valor_atual($usuario_id)
	{
		$sql = "SELECT * FROM `usuarios_lucro` WHERE `usu_id` = " . $usuario_id . " AND `ul_data_fim` IS NULL";
		return $GLOBALS['link_corp']->get_row($sql, ARRAY_A);
	}
	
	static function get_lucro_por_data($usuario_id, $data)
	{
		$data_formatada = date('Y-m-d', strtotime(str_replace('/','-', $data)));
		$sql = "SELECT * FROM `usuarios_lucro` WHERE ((`ul_data_inicio` <= '{$data_formatada}' AND `ul_data_fim` > '{$data_formatada}') OR  ".
				"(`ul_data_inicio` <= '{$data_formatada}' AND `ul_data_fim` IS NULL)) AND `usu_id` = " . $usuario_id;
	
		return $GLOBALS['link_corp']->get_row($sql, ARRAY_A);
	}
	
}

?>