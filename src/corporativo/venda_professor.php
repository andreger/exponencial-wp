<?php
require_once 'database.php';

class VendaProfessor {
	
	static function init($professor_id, $data_venda, $valor_final, $pacote, $curso) {
		return array('vef_professor_id' => $professor_id, 'vef_data_venda' => $data_venda, 'vef_valor_final' => $valor_final, 'vef_pacote' => $pacote, 'vef_curso' => $curso);
	}
	
	static function salvar($venda) {
		$sql = "INSERT INTO `vendas_professores`(`vef_professor_id`,`vef_data_venda`,`vef_valor_final`,`vef_pacote`, `vef_curso`) VALUES ({$venda['vef_professor_id']}, '{$venda['vef_data_venda']}', {$venda['vef_valor_final']}, {$venda['vef_pacote']}, '{$venda['vef_curso']}')";
		$GLOBALS['link_corp']->query($sql);
	}
	
	static function get_by_professor($user_id) {
		$sql = "SELECT * FROM `vendas_professores` WHERE `vef_professor_id` = $user_id";
		return $GLOBALS['link_corp']->get_results($sql);
	}
	
}

// $venda = VendaProfessor::init(123, '2014-05-05', 98, 0, 'Curso doido');
// VendaProfessor::salvar($venda);
?>
