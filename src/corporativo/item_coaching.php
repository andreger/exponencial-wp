<?php
require_once 'database.php';

class ItemCoaching {
	
	static function salvar($item)
	{
		$sql = "INSERT INTO `items_coaching`(`ite_slug`, `ite_tipo`) VALUES ('{$item['ite_slug']}','{$item['ite_tipo']}')";
		$GLOBALS['link_corp']->query($sql);
	}
	
	static function listar_todos()
	{
		$sql = "SELECT * FROM `items_coaching` ORDER BY ite_slug";
		return $GLOBALS['link_corp']->get_results($sql, ARRAY_A);
	}
	
	static function listar_cupons()
	{
		$sql = "SELECT * FROM `items_coaching` WHERE ite_tipo = 1 ORDER BY ite_slug";
		return $GLOBALS['link_corp']->get_results($sql, ARRAY_A);
	}
	
	static function excluir($slug)
	{
		$sql = "DELETE FROM `items_coaching` WHERE ite_slug = '$slug'";
		$GLOBALS['link_corp']->query($sql);
	}
	
}

?>