<?php
require_once 'database.php';

class Imposto {
	
	static function salvar($imposto)
	{
		$data_inicio = converter_para_yyyymmdd($imposto['imp_data_inicio']);
		
		$sql = "UPDATE `impostos` SET `imp_data_fim` = '{$data_inicio}' WHERE `imp_data_fim` IS NULL";
		$GLOBALS['link_corp']->query($sql);
		
		$sql = "INSERT INTO `impostos`(`imp_valor`, `imp_data_inicio`) VALUES ('{$imposto['imp_valor']}', '{$data_inicio}')";
		$GLOBALS['link_corp']->query($sql);
	}
	
	static function get_imposto_atual()
	{
		return self::get_imposto_por_data(date('Y-m-d'));
	}
	
	static function get_imposto_por_data($data)
	{
		$data_formatada = date('Y-m-d', strtotime(str_replace('/','-', $data)));
		$sql = "SELECT * FROM `impostos` WHERE (`imp_data_inicio` <= '{$data_formatada}' AND `imp_data_fim` > '{$data_formatada}') OR  ".
					"(`imp_data_inicio` <= '{$data_formatada}' AND `imp_data_fim` IS NULL)";

		return $GLOBALS['link_corp']->get_row($sql, ARRAY_A);
	}
	
	static function listar_impostos()
	{
		$sql = "SELECT * FROM impostos";
		return $GLOBALS['link_corp']->get_results($sql, ARRAY_A);
	}
}

?>