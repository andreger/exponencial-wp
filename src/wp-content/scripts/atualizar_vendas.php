<?php
include_once '../../wp-load.php';

//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

$force_pagseguro = isset($_GET['force_pagseguro']) && $_GET['force_pagseguro'] ? true : false;
$num = isset($_GET['num']) && $_GET['num'] ? $_GET['num'] : 50 ;

$pedidos = processar_pedidos_nao_processados($force_pagseguro, $num);
?>
<html>
<head>
<meta http-equiv="Refresh" content="1">
</head>
<body>
	Ainda faltam <?= contar_pedidos_nao_processados() ?> pedidos para serem processados.

	<?php foreach ($pedidos as $pedido) : ?>
	<div>	
		Pedido: #<?= $pedido->get_id(); ?> 
		Data: <?= $pedido->get_date_created()->date('d/m/Y H:i:s'); ?>
	</div>
	<?php endforeach ?>
	
</body>
</html>