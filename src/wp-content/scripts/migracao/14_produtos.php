<?php
require_once '../../../wp-load.php';

KLoader::api('ProdutoApi');

$ultimoId = isset($_GET['id']) ? $_GET['id'] : 0;

$novoUltimoId  = ProdutoApi::sincronizar($ultimoId);

if($novoUltimoId > $ultimoId) {
    echo "Executando...";
    echo "<script>window.location.href = window.location.href.split('?')[0] + '?id={$novoUltimoId}'</script>";
}
else {
    echo "Concluído.";
}