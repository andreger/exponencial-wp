<?php
$links = [
        '1_rodape_links' => '1 - Rodapé Links',
        '2_avisos' => '2 - Avisos',
        '3_saidas' => '3 - Saídas',
        '4_materias' => '4 - Matérias',
        '5_concursos' => '5 - Concursos',
        '6_depoimentos' => '6 - Depoimentos',
        '7_usuarios' => '7 - Usuários',
        '8_blogs' => '8 - Blogs',
        '9_paginas' => '9 - Páginas',
        '10_landing_pages' => '10 - Landing Pages',
        '11_perguntas' => '11 - Perguntas',
        '12_slides' => '12 - Slides',
        '13_areas' => '13 - Áreas',
        '14_produtos' => '14 - Produtos',
        '15_pedidos' => '15 - Pedidos',
        '16_forums' => '16 - Fórum',
        '17_topicos' => '17 - Tópicos',
        '18_respostas' => '18 - Respostas',
];
?>
<html>
    <body>
        <ul>
            <?php foreach ($links as $url => $texto) : ?>
            <li>
                <a href="/wp-content/scripts/migracao/<?= $url ?>.php"><?= $texto ?></a>
            </li>
            <?php endforeach; ?>
        </ul>
    </body>
</html>