<?php
include_once '../../wp-load.php';

$produtos = get_todos_produtos();

foreach($produtos as $produto) {
	echo "Verificando produto " . $produto->id . "<br>";
	
	$aulas = get_post_meta($produto->id, 'aulas_nome', true);
	$datas = get_post_meta($produto->id, 'aulas_data', true);
	$arquivos = get_post_meta($produto->id, 'aulas_arquivos', true);
	
	if(!$datas) continue;
	
	foreach ($datas as $i => $data) {
		if(hoje_yyyymmdd() == converter_para_yyyymmdd($data)) {
			if(!isset($arquivos[$i]) && !$arquivos[$i]) {
				$autores = get_autores($produto->post);
				$aula_nome = $aulas[$i];
				
				foreach ($autores as $autor) {
					$u = get_usuario_array($autor);
					
					echo "<h3>Enviando e-mail para: {$u['email']}</h3><br>";
					
					$mensagem = get_template_email('notificacao-aula-professor.php', array(
							'nome' => $u['nome_completo'],
							'aula_nome' => $aulas[$i],
							'curso_nome' => $produto->post->post_title
					));
					enviar_email('leonardo.coelho@exponencialconcursos.com.br', "Lembrete liberação de aula - {$produto->post->post_title} - {$aulas[$i]}", $mensagem);
					// enviar_email($u['email'], "Lembrete liberação de aula - {$produto->post->post_title} - {$aulas[$i]}", $mensagem);
					
				}
			}
		}
	}
		
	echo "*******************************<br>";
}