<?php
require_once '../../wp-load.php';

// não executa em PRD
if(is_producao()) exit;

$o = $_GET['o'] ?: 1;

$usuarios = [];
for($i = 132370; $i < 132390; $i++) {
    $usuarios[] = $i;
}

$produtos = [ 389589, 389462, 389465, 389315, 389269, 388928, 388919, 386804, 386647, 386645, 386643, 386640, 386636, 386633, 386630, 386351, 386355];

$u = array_rand($usuarios);
$p = array_rand($produtos);

$address = array(
    'first_name' => 'Usuário',
    'last_name'  => 'Carga ' . $usuarios[$u],
    'company'    => 'Teste',
    'email'      => 'teste@gmail.com',
    'phone'      => '760-555-1212',
    'address_1'  => 'Rua ZBD',
    'address_2'  => '104',
    'city'       => 'San Diego',
    'state'      => 'Ca',
    'postcode'   => '92121',
    'country'    => 'US'
);

// Now we create the order
$order = wc_create_order(['customer_id' => $usuarios[$u]]);

// The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
$order->add_product( get_product( $produtos[$p] ), 1 ); // This is an existing SIMPLE product
$order->set_address( $address, 'billing' );
//
$order->calculate_totals();
$order->update_status("completed", 'Teste de carga', TRUE);

$total = 20000;
$o++;

if($o >= $total) {
    echo "Script concluído";
}
else {
    echo "Processados {$o} itens de {$total} ";
    
    echo "<script>window.location.href='/wp-content/scripts/povoar_base_vendas.php?o={$o}';</script>";
}