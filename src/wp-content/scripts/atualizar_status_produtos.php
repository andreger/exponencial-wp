<?php
include_once '../../wp-load.php';

mudar_status_produtos_expirados_para_pendente();
mudar_status_produtos_expirados_para_rascunho();
enviar_email_cursos_expirando();

/*************************************************************
 * Enviar e-mail avisando sobre expiracao
 *************************************************************/
function enviar_email_cursos_expirando()
{
	$data_expiracao = date('d/m/Y', strtotime('+15 days'));
	
	// Trata produto SEM data de "Produto Disponível Até:"
	$produtos = get_produtos_expirando(PRODUTO_VENDAS_ATE_POST_META, 'now -5 months -15 days');
	foreach ($produtos as $produto) {
		$post = $produto->post;
		
		if(get_post_meta($post->id, PRODUTO_DISPONIVEL_ATE_POST_META, true)) continue;
		
		$usuarios = get_usuarios_que_compraram($produto->id);
		foreach ($usuarios as $usuario) {
			$usuario_array = get_usuario_array($usuario->ID);
			
			$mensagem = get_template_email('curso-expirando.php', array(
					'nome' => $usuario_array['nome_completo'],
					'curso_nome' => $post->post_title,
					'data_expiracao' => $data_expiracao
			));
			
			enviar_email($usuario_array['email'], "Curso Expirando - {$post->post_title}", $mensagem);
		}
	}
	
	// Trata produto COM data de "Produto Disponível Até:"
	$produtos = get_produtos_expirando(PRODUTO_DISPONIVEL_ATE_POST_META, 'now +15 days');
	foreach ($produtos as $produto) {
		$post = $produto->post;
	
		$usuarios = get_usuarios_que_compraram($produto->id);
		foreach ($usuarios as $usuario) {
			$usuario_array = get_usuario_array($usuario->ID);
				
			$mensagem = get_template_email('curso-expirando.php', array(
					'nome' => $usuario_array['nome_completo'],
					'nome_curso' => $post->post_title,
					'data_expiracao' => $data_expiracao
			));
				
			enviar_email($usuario_array['email'], "Curso Expirando - {$post->post_title}", $mensagem);
		}
	}
}

/*************************************************************
 * Muda status de produtos para pendente
 *************************************************************/
function mudar_status_produtos_expirados_para_pendente()
{
	$produtos = get_produtos_expirando(PRODUTO_VENDAS_ATE_POST_META);

	foreach ($produtos as $produto) {
		$post = $produto->post;
		$post->post_status = STATUS_POST_REVISAO_PENDENTE;
		wp_update_post($post);
	}
}

/*************************************************************
 * Muda status de produtos para rascunho
 *************************************************************/
function mudar_status_produtos_expirados_para_rascunho()
{
	// Trata produto SEM data de "Produto Disponível Até:"
	$produtos = get_produtos_expirando(PRODUTO_VENDAS_ATE_POST_META, '+6 months');

	foreach ($produtos as $produto) {
		$post = $produto->post;
		
		if(get_post_meta($post->id, PRODUTO_DISPONIVEL_ATE_POST_META, true)) continue;
		
		$post->post_status = STATUS_POST_RASCUNHO;
		wp_update_post($post);
	}
	
	// Trata produto COM data de "Produto Disponível Até:"
	$produtos = get_produtos_expirando(PRODUTO_DISPONIVEL_ATE_POST_META);
	
	foreach ($produtos as $produto) {
		$post = $produto->post;
		$post->post_status = STATUS_POST_RASCUNHO;
		wp_update_post($post);
	}
}


/*************************************************************
 * Recupera produtos que estão expirando
 *************************************************************/
function get_produtos_expirando($meta_key, $str_dias = 'now')
{
	global $wpdb;
	
	$data_expiracao = date('d/m/Y', strtotime($str_dias));
	
	$query = "SELECT post_id FROM wp_postmeta WHERE meta_key = '{$meta_key}' AND meta_value = '{$data_expiracao}'";
	$result = $wpdb->get_results($query, ARRAY_A);
	
	$produtos = array();
	foreach ($result as $item) {
		array_push($produtos, get_produto_by_id($item['post_id']));
	}
	
	return $produtos;
}
