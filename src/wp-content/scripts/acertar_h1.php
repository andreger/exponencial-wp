<?php
require_once '../../wp-load.php';

set_time_limit(300);

KLoader::helper("SeoH1Helper");
KLoader::model("SeoH1Model");

$limit = 1;
$offset = $_GET['o'] ?: 0;

$posts = listar_posts_por_tipo([TIPO_POST_PAGINA, TIPO_POST_CONCURSO, TIPO_POST_POST, TIPO_POST_PRODUTO], $limit, $offset);
$total = contar_posts_por_tipo([TIPO_POST_PAGINA, TIPO_POST_CONCURSO, TIPO_POST_POST, TIPO_POST_PRODUTO]);


foreach ($posts as $post) {
    if($dados = SeoH1Helper::parse_html($post->ID)) {
        SeoH1Model::salvar($dados);
    }    
}

$offset += $limit;
if($offset > $total) {
	echo "Script concluído";
}
else {
	echo "Processados {$offset} itens de {$total} ";

	echo "<script>setTimeout(function() { window.location.href='/wp-content/scripts/acertar_h1.php?o={$offset}'; }, 5000); </script>";
}