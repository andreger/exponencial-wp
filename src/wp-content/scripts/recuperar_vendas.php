<?php 
include_once '../../wp-load.php';

$pedidos = listar_pedidos_recuperacao_vendas(); 

log_wp('debug', "[Recuperacao de Vendas] " . count($pedidos) . " pedido(s) encontrado(s)");

foreach ($pedidos as $pedido) {

	log_wp('debug', "[Recuperacao de Vendas] Processando pedido: {$pedido->id}");

	if(listar_pedidos_por_data_por_usuario($pedido->order_date, $pedido->get_user_id(), array($pedido->id))) {

		log_wp('debug', "[Recuperacao de Vendas] Existe outro pedido no dia: {$pedido->id}, data: {$pedido->order_date}");
		update_post_meta($pedido->id, 'recuperacao_enviada', "outro pedido no dia");
		continue;
	}

	$cursos_a = [];
	foreach ($pedido->get_items() as $item) {
		array_push($cursos_a, "<strong>" . $item['name'] . "</strong>");
	}

	switch (count($cursos_a)) {
		case 0: {
			$cursos_frag = "";
			break;
		}
		case 1: {
			$cursos_frag = "do produto " . $cursos_a[0];
			break;
		}
		case 2: {
			$cursos_frag = "dos produtos " . $cursos_a[0] . ' e ' . $cursos_a[1];
			break;
		}
		case 3: {
			$cursos_frag = "dos produtos " . $cursos_a[0] . ', ' . $cursos_a[1] . ' e ' . $cursos_a[2];
			break;
		}
		default: {
			$cursos_frag = "dos produtos " . implode(', ', array_slice($cursos_a, 0, 3)) . ' e outros';
			break;
		}
	}

	$usuario = get_usuario_array($pedido->get_user_id());

	$titulo = "Conclua seu pedido";
	$mensagem = get_template_email ('recuperar_venda.php', array (
		'nome' => $usuario['nome_completo'],
		'cursos' => $cursos_frag,
	) );

	// $usuario['email'] = 'leonardo.coelho@exponencialconcursos.com.br';
	$bcc = array('contato@exponencialconcursos.com.br', 'leonardo.coelho@exponencialconcursos.com.br');

	log_wp('debug', "[Recuperacao de Vendas] Enviando e-mail: {$usuario['email']} , pedido: {$pedido->id}");
	enviar_email($usuario['email'], $titulo, $mensagem, NULL, $bcc);

	update_post_meta($pedido->id, 'recuperacao_enviada', date('Y-m-d H:i:s'));
}
