<?php
include_once '../../wp-load.php';

global $wpdb;

$sql = "SELECT ID FROM wp_posts WHERE post_status = 'wc-completed' AND post_type = 'shop_order' AND ID NOT IN (SELECT post_id FROM wp_postmeta WHERE meta_key = 'valores-produtos') ";

$order_ids = $wpdb->get_col($sql);

foreach ($order_ids as $id) {
	set_time_limit(300);

	$order = new WC_Order($id);

	$valores_produtos = array();
	foreach ($order->get_items() as $item) {
		

		if(is_item_raiz_de_pacote($item)) {
			$last_bundle_id = $item['product_id'];
			$last_bundle_valor = $item->get_total();
			$item_raiz_pacote = $item;
		}

		if(is_item_de_pacote($item)) {
			$percentual = get_valor_percentual_item_pacote_no_pedido($order->get_id(), $item['product_id'], $item_raiz_pacote);
			$valor_item = $last_bundle_valor * $percentual;		    		
		}
		else {
			$valor_item = $item->get_total();
		}

		$valores_produtos[$item->get_id()] = $valor_item;
	}

	update_post_meta($order->id, 'valores-produtos', $valores_produtos);
}