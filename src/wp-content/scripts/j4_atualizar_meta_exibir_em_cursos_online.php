<?php
include_once '../../wp-load.php';

global $wpdb;

$produtos = get_todos_produtos_posts(false);

log_listar_cursos_online("Atualizando " . count($produtos) . " produtos");

$i = 1;
foreach ($produtos as $produto) {
	set_time_limit(120);

	$id = $produto->ID;
	log_listar_cursos_online("[$i] Processando produto: {$id}");
	
	definir_produto_listagem_status($id);

	$i++;
}