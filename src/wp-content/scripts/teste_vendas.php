<?php
include_once '../../wp-load.php';

global $wpdb;
// $query = "SELECT MAX(ven_order_id) FROM wp_vendas";
// $ultima_venda = $wpdb->get_var($query); 

// $query = "SELECT * FROM wp_posts WHERE ID > $ultima_venda AND post_type = 'shop_order' ORDER BY ID ASC LIMIT 100";
// $result = $wpdb->get_results($query);

// $ids = array();
// foreach ($result as $item) {
// 	array_push($ids, $item->ID);
// }

// $args = array(
// 		'post_type' => 'shop_order',
// 		'post_status' => 'publish',
// 		'posts_per_page' => '100',
// 		'order' => 'ASC',
// 		'post__in' => $ids
// );

// $my_query = new WP_Query($args);
// $customer_orders = $my_query->posts;

// foreach ($customer_orders as $customer_order) {
	$order = new WC_Order(14550); 
// 	$order->populate($customer_order);
	$orderdata = (array) $order;
	
	if($order->status != 'completed') continue;
	
	$parcelas = get_post_meta($order->id, 'Installments', TRUE);
	$parcelas = empty($parcelas) ? get_post_meta($order->id, 'Parcelas', TRUE) : $parcelas;
	
	$metodo = get_post_meta($order->id, 'Payment method', TRUE);
	$metodo = empty($metodo) ? get_post_meta($order->id, 'Método de pagamento', TRUE) : $metodo;
	
	$total_desconto = $order->get_total_discount();
	$total = $order->get_total()+$order->get_total_discount();
	
	if($total >  0) {
			
		$taxas_pagseguro = get_post_meta($order->id, 'PagSeguro Total Taxes', TRUE);
		if(empty($taxas_pagseguro)) {
			$pagseguro_id = get_post_meta($order->id, 'PagSeguro Transaction ID', TRUE);
			$pagseguro_id = empty($pagseguro_id) ? get_post_meta($order->id, 'PagSeguro: ID da transação', TRUE) : '';
	
			$base = "https://ws.pagseguro.uol.com.br/v3/transactions/". $pagseguro_id ;
			$data = "email=leonardo.coelho@exponencialconcursos.com.br&token=67E47689AEB74EF685A9B47B2FCB1E4A";
	
			$url = $base . '?' . $data;
	
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_URL, $url );
			$return = curl_exec($ch);
			curl_close($ch);
	
			$xml= str_get_html($return);
	
			$installmentFeeAmout = $xml->find('installmentFeeAmount', 0)->plaintext;
			$intermediationRateAmount = $xml->find('intermediationRateAmount', 0)->plaintext;
			$intermediationFeeAmount = 	$xml->find('intermediationFeeAmount', 0)->plaintext;
			$taxas_pagseguro = 	$installmentFeeAmout + $intermediationFeeAmount + $intermediationRateAmount;
	
			update_post_meta($order->id, 'PagSeguro Total Taxes', $taxas_pagseguro);
			update_post_meta($order->id, 'PagSeguro Installment Fee Amount', $installmentFeeAmout);
			update_post_meta($order->id, 'PagSeguro Intermediation Rate Amount', $intermediationRateAmount);
			update_post_meta($order->id, 'PagSeguro Intermediation Fee Amount', $intermediationFeeAmount);
		}
	}
	
	$last_bundle_id = null;
	$last_bundle_valor_venda = null;
	$last_bundle_desconto = null;
	$last_bundle_pagseguro = null;
	
	foreach ($order->get_items() as $item) {
		$product = new WC_Product($item['product_id']);
		$post = $product->post;
	
		$percentual_item = ($total > 0) ? $item['line_subtotal'] / $total : 0;
	
		$venda = array();
		$venda['ven_order_id'] = $order->id;
		$venda['ven_aluno_id'] = $order->user_id;
		$venda['ven_data'] = $orderdata['order_date'];
		$venda['ven_curso_id'] = $item['product_id'];
		$venda['ven_valor_venda'] = $item['line_subtotal'];
		$venda['ven_desconto'] = $total_desconto * $percentual_item;
		$venda['ven_pagseguro'] = $taxas_pagseguro * $percentual_item;
		$venda['ven_forma_pgto'] = $metodo;
		$venda['ven_parcelamento'] = $parcelas;
		$venda['ven_aliquota'] = get_imposto_por_data($orderdata['order_date']);
		
		// Item Raiz de Pacote
		if(is_item_raiz_de_pacote($item)) {
			$venda['ven_item_tipo'] = 1;
			$venda['ven_item_perc'] = 100;
			$last_bundle_id = $item['product_id'];
			$last_bundle_valor_venda = $venda['ven_valor_venda'];
			$last_bundle_desconto = $venda['ven_desconto'];
			$last_bundle_pagseguro = $venda['ven_pagseguro'];
		}
		// Item Não-Raiz de Pacote
		elseif(is_item_de_pacote($item)) {
			$venda['ven_item_tipo'] = 2;
			$venda['ven_item_perc'] = get_valor_percentual_item_pacote($last_bundle_id, $item['product_id']);
			$venda['ven_valor_venda'] = $last_bundle_valor_venda * $venda['ven_item_perc'];
			$venda['ven_desconto'] = $last_bundle_desconto * $venda['ven_item_perc'];
			$venda['ven_pagseguro'] = $last_bundle_pagseguro * $venda['ven_item_perc'];
		}
		// item Não-Pacote
		else {
			$venda['ven_item_tipo'] = 0;
			$venda['ven_item_perc'] = 100;
		}
		
		// Salva em wp_vendas
// 		$wpdb->insert('wp_vendas', $venda);		
// 		$venda_id = $wpdb->insert_id;
		
		$autores = get_autores($post);
		$venda_professor = array();
		foreach ($autores as $autor) {
// 			$item = array(
// 				'ven_id' 				=> $venda_id,
// 				'vep_professor_id' 		=> $autor->ID,
// 				'vep_professor_perc' 	=> get_participacao_autor($item['product_id'], $autor->ID)
// 			);
			
			// Salva em wp_vendas
// 			$wpdb->insert('wp_vendas_professores', $item);
			
// 			array_push($venda_professor, $item);
			$participacao = get_participacao_autor($item['product_id'], $autor->ID);
			$percentual_professor = get_lucro_por_data($autor->ID, $venda['ven_data']) * ($participacao / 100);
		}
		
		echo "<pre>";
		print_r($venda);
// 		print_r($venda_professor);
	}
	
	
// }
?>
<html>
<head>
<meta http-equiv="Refresh" content="30">
</head>
<body></body>
</html>