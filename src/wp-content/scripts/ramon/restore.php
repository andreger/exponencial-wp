<?php
require_once '../../../wp-load.php';

global $wpdb;

//$sql = "select post_id from wp_postmeta where meta_key = '_authors' and meta_value like '%53712%'";
$sql = "select * from produtosbak";

$rows = $wpdb->get_results($sql);

foreach ($rows as $row) {
    update_post_meta($row->post_id, PRODUTO_VENDAS_ATE_POST_META, $row->vendas_ate);
    update_post_meta($row->post_id, PRODUTO_DISPONIVEL_ATE_POST_META, $row->disponivel_ate);
    wp_update_post([
        'ID' => $row->post_id,
        'post_status' => $row->status,
    ]);
}