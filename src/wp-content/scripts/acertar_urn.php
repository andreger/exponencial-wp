<?php
require_once '../../wp-load.php';

KLoader::model("ProdutoModel");

global $wpdb;

set_time_limit(300);

$limit = 100;
$offset = $_GET['o'] ?: 0;

$total = $wpdb->get_var("SELECT COUNT(*) AS total FROM wp_posts WHERE post_type = 'product'");
$produtos_ids = $wpdb->get_col("SELECT ID FROM wp_posts WHERE post_type = 'product' ORDER BY ID LIMIT {$offset}, {$limit}");

foreach ($produtos_ids as $id) {
    
    try{
       ProdutoModel::atualizar_metadados_status_aulas($id);
       ProdutoModel::atualizar_metadados_aulas($id);
    }
    catch(Exception $e) {
        continue;
    }
}

$offset += $limit;
if($offset > $total) {
    echo "Script concluído";
}
else {
    echo "Processados {$offset} itens de {$total}";
    
    echo "<script>window.location.href='/wp-content/scripts/acertar_urn.php?o={$offset}';</script>";
}