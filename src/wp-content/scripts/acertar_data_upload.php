<?php
require_once '../../wp-load.php';

global $wpdb;

set_time_limit(300);

$limit = 100;
$offset = $_GET['o'] ?: 0;

$total = $wpdb->get_var("SELECT COUNT(*) AS total FROM wp_posts WHERE post_type = 'product'");
$produtos_ids = $wpdb->get_col("SELECT ID FROM wp_posts WHERE post_type = 'product' ORDER BY ID LIMIT {$offset}, {$limit}");

foreach ($produtos_ids as $post_id) {
    
    try{
        $aulas_vimeo = get_post_meta($post_id, 'aulas_vimeo', true);
        $aulas_caderno = get_post_meta($post_id, 'aulas_caderno', true);
        
        gravar_primeiro_upload($post_id, $aulas_vimeo, AULA_UPLOAD_TIPO_VIDEO);
        gravar_primeiro_upload($post_id, $aulas_caderno, AULA_UPLOAD_TIPO_CADERNO);
    }
    catch(Exception $e) {
        continue;
    }
}

$offset += $limit;
if($offset > $total) {
    echo "Script concluído";
}
else {
    echo "Processados {$offset} itens de {$total}";
    
    echo "<script>window.location.href='/wp-content/scripts/acertar_data_upload.php?o={$offset}';</script>";
}