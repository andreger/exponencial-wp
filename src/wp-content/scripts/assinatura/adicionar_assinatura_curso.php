<?php

require_once '../../../wp-load.php';


$produtos = listar_produtos_assinaturas();
adicionar_categoria($produtos);


function adicionar_categoria($produtos)
{
    foreach ($produtos as $produto) {
        echo "<br>" . $produto->ID;
        wp_set_object_terms( $produto->ID, TERM_ASSINATURA_CURSO, 'product_cat', true );
    }

}



function listar_produtos_assinaturas()
{
    global $wpdb;

    $sql = "select * 
            from wp_posts 
            where 
                post_title like 'Assinatura%' 
                and post_type = 'product' 
                and ID not in (711291,877143,449048,449054,121503,121508,121509,121510,135551)";

    return $wpdb->get_results($sql);
}
