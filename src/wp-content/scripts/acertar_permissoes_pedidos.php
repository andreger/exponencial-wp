<?php
require_once '../../wp-load.php';

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

global $wpdb;

set_time_limit(300);

if($_GET['gerar_tabela'] == 'sim'){

	echo date('Y-m-d H:i:s') . ": Gerando tabela auxiliar...<br>";

	$sql_tabela = 
		"create table temp_acertar_permissoes_pedidos " .
		" select p.ID as produto_id, p.post_title as produto, p.post_modified as produto_edicao, oi.order_id as pedido_id, o.post_date as data_pedido, 0 as status_script ".
		" from wp_posts p " .
		" 	inner join wp_woocommerce_order_itemmeta oim on (oim.meta_value = p.ID and oim.meta_key = '_product_id') " .
		" 	inner join wp_woocommerce_order_items oi on oim.order_item_id = oi.order_item_id " .
		" 	inner join wp_postmeta pm on (pm.post_id = p.ID and pm.meta_key = '_downloadable_files') " . #é a parte que gera os arquivos baixáveis
		" 	inner join wp_postmeta pm2 on (pm2.post_id = p.ID and pm2.meta_key = 'aulas_arquivo') " . #faz sentido apenas para produtos com arquivos
		" 	inner join wp_posts o on (o.ID = oi.order_id and o.post_status = 'wc-completed') " . #somente pedidos completos, estou ignorando o wc-refund em que o acesso não foi retirado
		" where p.post_modified >= '2020-06-01 00:00:00' " . #data que eu acredito que a alteração entrou em produção
		" 	and o.post_type = 'shop_order' " . #somente pedidos porque também estavam vindo reembolsos
		" 	and not exists(select 1 from wp_woocommerce_downloadable_product_permissions d where d.order_id = oi.order_id) " . #pedido não possui dados de downloads
		" 	and pm.meta_value not like '%zero.pdf%' " . #não é arquivo zero
		" 	and (pm2.meta_value like '%.pdf%' or pm2.meta_value like '%.mp3%') "; #possui algum arquivo no aulas_arquivo
	
	$result = $wpdb->query($sql_tabela);

	echo date('Y-m-d H:i:s') . ": Tabela criada? "; var_dump($result); echo "<br>";

}

$limit = 100;
$offset = $_GET['o'] ?: 0;

echo date('Y-m-d H:i:s') . ": Iniciou com Offset: {$offset}<br>";

$sql_count = " select count(*) from temp_acertar_permissoes_pedidos";
$total = $wpdb->get_var($sql_count);

echo date('Y-m-d H:i:s') . ": Terminou de contar, total: {$total}<br>";

$sql_busca = "select pedido_id from temp_acertar_permissoes_pedidos limit {$offset}, {$limit} ";

$pedidos_ids = $wpdb->get_col($sql_busca);

echo date('Y-m-d H:i:s') . ": Terminou a busca e vai executar as permissões para os encontrados<br>";

echo "Vai carregar WC_Data_Store...";

$data_store = WC_Data_Store::load( 'customer-download' );

echo "OK<br>";

foreach ($pedidos_ids as $id) {

	try{

		echo date('Y-m-d H:i:s') . ": ";
		echo "Vai deletar por order_id {$id}...";
		$data_store->delete_by_order_id( $id );
		echo "OK<br>";
		echo "Vai dar as permissões para {$id}...";
		wc_downloadable_product_permissions( $id, true );
		echo "OK<br>......................<br>";

	}
	catch(Exception $e) {
		echo "<BR>OCORREU UM ERRO: ";
		print_r($e);

		$wpdb->query("update temp_acertar_permissoes_pedidos set status_script = 1 where pedido_id = {$id}");
		continue;
	}
}

$offset += $limit;
if($offset > $total) {
	echo date('Y-m-d H:i:s') . ": ";
	echo "Script concluído.";
}
else {
	echo date('Y-m-d H:i:s') . ": ";
	echo "Processados {$offset} pedidos de {$total}";

	echo "<script>window.location.href='/wp-content/scripts/acertar_permissoes_pedidos.php?o={$offset}';</script>";
}