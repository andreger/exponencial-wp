<?php
require_once '../../../wp-load.php';

global $wpdb;

$ultimoId = isset($_GET['id']) ? $_GET['id'] : 0;

$sql = "select * from wp_postmeta where meta_key = '_transaction_id' and post_id > {$ultimoId} ORDER BY post_id LIMIT 100";

$resultado = $wpdb->get_results($sql);

if($resultado) {
    foreach ($resultado as $item) {

        if($item->meta_value) {

            $wpdb->update('wp_vendas',
                ['ven_transacao_id' => $item->meta_value],
                ['ven_order_id' => $item->post_id],
            );
        }

        $novoUltimoId = $item->post_id;
    }
}

if($novoUltimoId > $ultimoId) {
    echo "Executando...";
    echo "<script>window.location.href = window.location.href.split('?')[0] + '?id={$novoUltimoId}'</script>";
}
else {
    echo "Concluído.";
}