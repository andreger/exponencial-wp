<?php
require_once '../../../wp-load.php';

$inicio = isset($_GET['i']) ? $_GET['i'] : "2020-01-01";
$fim = isset($_GET['f']) ? $_GET['f'] : "2020-09-30";

$data_iterator = $inicio;
while($data_iterator <= $fim) {
    $data = converter_para_ddmmyyyy($data_iterator);
    $data_iterator = date("Y-m-d", strtotime($data_iterator . " +1 day"));

    $dados = recuperar_dados($data);

    imprimir_cabecalho($data);
    imprimir_cursos($dados);
}

function recuperar_dados($data) {
    $dados_relatorio_concurso = listar_vendas_agrupadas_por_concurso($data, $data);
    $somatorios = [];

    // conta quantas vezes cada curso é somado
    foreach ($dados_relatorio_concurso as $categoria_nome => $categoria_objeto ) {
        foreach ($categoria_objeto['cursos'] as $curso) {
            $somatorio_chave = $curso['nome'];

            // inicializa índice caso não exista
            if(array_key_exists($somatorio_chave, $somatorios) == false) {
                $somatorios[$somatorio_chave]['qtde'] = 0;
                $somatorios[$somatorio_chave]['categorias'] = [];
            }

            // incrementa índice
            if($curso['nao_somar'] == false && $curso['total'] > 0) {
                $somatorios[$somatorio_chave]['qtde']++;
                $somatorios[$somatorio_chave]['categorias'][] = $categoria_nome;
            }
        }
    }

    return $somatorios;
}

function imprimir_cabecalho($data) {
    echo "<br><br>------------------------<br>";
    echo " $data <br>";
    echo "------------------------<br><br>";
}

function imprimir_cursos($dados)
{
    arsort($dados);

    foreach ($dados as $nome => $dado) {

        if($dado['qtde'] > 1) {
            echo "[{$dado['qtde']}] $nome<br>";

            foreach ($dado['categorias'] as $categoria) {
                echo "-> {$categoria}<br>";
            }

            echo "<br>";
        }
    }
}