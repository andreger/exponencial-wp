<?php
include_once '../../wp-load.php';

global $phpmailer;
// (Re)create it, if it's gone missing
if ( !is_object( $phpmailer ) || !is_a( $phpmailer, 'PHPMailer' ) ) {
	require_once ABSPATH . WPINC . '/class-phpmailer.php';
	require_once ABSPATH . WPINC . '/class-smtp.php';
	$phpmailer = new PHPMailer( true );
}

$endereco = "andreger@gmail.com";
$titulo = "Titulo da mensagem";
$mensagem = "Corpo da mensagem";
$from = 'exponencial@exponencialconcursos.com.br';
$bcc = null;

$phpmailer = new PHPMailer(true);
$phpmailer->CharSet = 'UTF-8';
$phpmailer->isSendmail();
// $phpmailer->isSMTP();
// $phpmailer->SMTPAuth   = true;                  
// $phpmailer->Host       = "smtp.gmail.com";
// $phpmailer->Port       = 465;             
// $phpmailer->Username   = "contato@exponencialconcursos.com.br";
// $phpmailer->Password   = "expo2015"; 
// $phpmailer->SMTPSecure = "ssl";
$phpmailer->SMTPDebug  = 2;

switch($_SERVER['HTTP_HOST']) {
	case 'homol.exponencialconcursos.com.br' : {
		$titulo = '[HOMOLOGACAO] ' . $titulo;
		$bcc = 'leonardo.coelho@exponencialconcursos.com.br';
		$endereco = EMAIL_BCC_KEYDEA;
		break;
	}
	case 'localhost' :
	case 'expoconcursos' : {
		$titulo = '[DESENVOLVIMENTO] ' . $titulo;
		$bcc = null;
		$endereco = EMAIL_BCC_KEYDEA;
		break;
	}
}

$phpmailer->AddReplyTo($from, 'Exponencial Concursos');
$phpmailer->setFrom($from, 'Exponencial Concursos');
$phpmailer->addAddress($endereco);
$phpmailer->addBCC(EMAIL_BCC_KEYDEA);

if(!is_null($bcc)) { 
	$phpmailer->addBCC($bcc);
}

$titulo = sanitizar_string($titulo);
$phpmailer->Subject = '=?UTF-8?B?'.base64_encode($titulo).'?=';
$phpmailer->msgHTML($mensagem);
$phpmailer->AltBody = ' ';

if(!$phpmailer->Send()) {
	logger_error($phpmailer->ErrorInfo);
	exit;
}
else {
	logger_info("E-mail enviado com sucesso para {$endereco} (assunto: {$titulo})");
}