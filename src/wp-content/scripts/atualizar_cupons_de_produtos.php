<?php
include_once '../../wp-load.php';

$produtos = get_pacotes_por_categoria('pacote');

foreach ($produtos as $produto) {
	echo "Verificando pedidos do produto: {$produto->id}<br>";

	$pedidos = listar_pedidos_por_produto($produto->id);

	echo count($pedidos) . " pedido(s) encontrado(s)<br>";

	foreach ($pedidos as $pedido) {

		echo "Verificando itens do pedido: {$pedido->id}<br>";

		foreach ($pedido->get_items() as $item) {

			set_time_limit(180);
			
			$item_id = $item['product_id'];
			$post = get_post($item_id);

			echo "Verificando cupons do item (produto): {$item_id}<br>";

			verifica_cupom_produto($post, $pedido, FALSE);
		}
	}
	
}

echo "Script encerrado<br>";

