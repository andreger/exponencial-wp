<?php 
include_once '../../wp-load.php';

global $wpdb;

echo "Iniciando execução de script.<br>";

$sql = "SELECT ID FROM wp_posts WHERE post_type = \"product\" limit 0, 10000";
$resultado = $wpdb->get_results($sql);

foreach ($resultado as $item) {
	set_time_limit(30);

	$id = $item->ID;

	echo "Atualizando produto ID: " . $id . " ...";
	update_post_meta($id, '_virtual', 'yes');
	update_post_meta($id, '_downloadable', 'yes');
	echo "ok.<br>";
}

echo "Finalizando execução de script!<br>";
