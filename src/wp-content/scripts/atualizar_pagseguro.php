<?php
include_once '../../wp-load.php';

$order_id = $_GET['order_id'];

$pagseguro_id = get_post_meta($order_id, '_transaction_id', TRUE);

if(!$pagseguro_id) {
	$pagseguro_id = get_post_meta($order_id, 'PagSeguro Transaction ID', TRUE);
}

if(!$pagseguro_id) {
	$pagseguro_id = get_post_meta($order_id, 'PagSeguro: ID da transação', TRUE);	
}

if(!$pagseguro_id) {
	echo "Não foi possível identificar o ID da transação.";
	exit;
}

$base = "https://ws.pagseguro.uol.com.br/v3/transactions/". $pagseguro_id ;
$data = "email=leonardo.coelho@exponencialconcursos.com.br&token=67E47689AEB74EF685A9B47B2FCB1E4A";

$url = $base . '?' . $data;

$ch = curl_init();
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_URL, $url );
$return = curl_exec($ch);
curl_close($ch);

$xml= str_get_html($return);

$installmentFeeAmout = $xml->find('installmentFeeAmount', 0)->plaintext;
$intermediationRateAmount = $xml->find('intermediationRateAmount', 0)->plaintext;
$intermediationFeeAmount = 	$xml->find('intermediationFeeAmount', 0)->plaintext;
$taxas_pagseguro = 	$installmentFeeAmout + $intermediationFeeAmount + $intermediationRateAmount;

update_post_meta($order_id, 'PagSeguro Total Taxes', $taxas_pagseguro);
update_post_meta($order_id, 'PagSeguro Installment Fee Amount', $installmentFeeAmout);
update_post_meta($order_id, 'PagSeguro Intermediation Rate Amount', $intermediationRateAmount);
update_post_meta($order_id, 'PagSeguro Intermediation Fee Amount', $intermediationFeeAmount);