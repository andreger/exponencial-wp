<?php 
include_once '../../wp-load.php';

global $wpdb;

echo "Iniciando execução de script...<br>";

$sql = "UPDATE exponenc_corp.questoes que 
                    SET que.que_procedencia = 1 
                WHERE que.que_procedencia IS NULL 
                    AND que.que_id IN (SELECT q.que_id FROM exponenc_corp.questoes_provas q, exponenc_corp.provas p, exponenc_corp.bancas b WHERE b.ban_id = p.ban_id AND q.pro_id = p.pro_id AND b.ban_nome = 'Exponencial Concursos')";

$resultado = $wpdb->query($sql);

echo "O total de {$resultado} questões associadas à banca com nome 'Exponencial Concursos' foram marcadas como 'Inédita'.<br>Finalizando execução de script!";