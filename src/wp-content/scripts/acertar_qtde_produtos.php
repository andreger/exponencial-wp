<?php
require_once '../../wp-load.php';

global $wpdb;

KLoader::model("ProdutoModel");

set_time_limit(300);

$limit = 1000;
$offset = $_GET['o'] ?: 0;

$total = $wpdb->get_var("SELECT COUNT(*) AS total FROM concursos ");

$concursos = $wpdb->get_results("SELECT * FROM concursos LIMIT $limit, $offset");

if($concursos) {
    foreach($concursos as $item) {
     
        $qtde = ProdutoModel::contar_por_concurso($item->con_slug, false);
        
        $wpdb->update("concursos", ["con_qtde_cursos" => $qtde], ["con_id" => $item->con_id]);
    }
}

$offset += $limit;
if($offset > $total) {
    echo "Script concluído";
}
else {
    echo "Processados {$offset} itens de {$total} ";
    
    echo "<script>window.location.href='/wp-content/scripts/acertar_qtde_produtos.php?o={$offset}';</script>";
}