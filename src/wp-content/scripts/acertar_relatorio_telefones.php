<?php
require_once '../../wp-load.php';

KLoader::model("ProdutoModel");

global $wpdb;

set_time_limit(300);

$limit = 1000;
$offset = $_GET['o'] ?: 0;

// echo "SELECT ven_id, ven_aluno_id FROM wp_vendas ORDER BY ven_id LIMIT {$offset}, {$limit}";exit;

$total = $wpdb->get_var("SELECT COUNT(*) AS total FROM wp_vendas");
$rows = $wpdb->get_results("SELECT ven_id, ven_aluno_id FROM wp_vendas ORDER BY ven_id LIMIT {$offset}, {$limit}");

foreach ($rows as $row) {

    try{
        $telefone = get_user_meta($row->ven_aluno_id, 'telefone', true);
        $wpdb->update("wp_vendas", ["ven_aluno_telefone" => $telefone], ["ven_id" => $row->ven_id]);
        
    }
    catch(Exception $e) {
        continue;
    }
}

$offset += $limit;
if($offset > $total) {
    echo "Script concluído";
}
else {
    echo "Processados {$offset} itens de {$total}";
    
    echo "<script>window.location.href='/wp-content/scripts/acertar_relatorio_telefones.php?o={$offset}';</script>";
}