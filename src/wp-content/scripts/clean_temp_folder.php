<?php
limpar_pasta_temp();
limpar_pasta_logs();
limpar_pasta_logs_ci();
limpar_logs_wp();
criar_temp_index();

function limpar_pasta_temp() {
	$files = glob(dirname(__FILE__) . '/../temp/*'); // get all file names
	
	$num_files = remover_arquivos($files);

	$msg = date("[d/m/Y H:i:s]") . " [INFO] clean_temp_folder.php: " . $num_files . " arquivos temporários excluídos.\r\n";
}

function limpar_pasta_logs() {
	$files = glob(dirname(__FILE__) . '/../../logs/*'); 

	$num_files = remover_arquivos($files);

	$msg = date("[d/m/Y H:i:s]") . " [INFO] clean_temp_folder.php: " . $num_files . " arquivos de logs excluídos.\r\n";
}

function limpar_pasta_logs_ci() {
	$files = glob(dirname(__FILE__) . '/../../questoes/app-expo/logs/*'); 

	$num_files = remover_arquivos($files);

	$msg = date("[d/m/Y H:i:s]") . " [INFO] clean_temp_folder.php: " . $num_files . " arquivos de logs do CI excluídos.\r\n";
}

function limpar_logs_wp() {
	$files = [
		dirname(__FILE__) . '/../debug.log',
		dirname(__FILE__) . '/../../questoes/php_errorlog',
		dirname(__FILE__) . '/../../php_errorlog',
		dirname(__FILE__) . '/../themes/academy/ajax/php_errorlog',
	];

	$num_files = remover_arquivos($files);

	$msg = date("[d/m/Y H:i:s]") . " [INFO] clean_temp_folder.php: " . $num_files . " arquivos de logs do CI excluídos.\r\n";
}

function remover_arquivos($files)
{
	$num_files = 0;
	foreach($files as $file){ // iterate files
		if(is_file($file)) {
		    
		    // verifica se arquivo tem mais de 1 dia de vida
		    if (time() - filemtime($file) >= 3600 * 24) { 
    			echo "Removendo {$file}<br>";
    			unlink($file); // delete file
    			$num_files++;	
		    }
		}
	}

	return $num_files;
}

function criar_temp_index() {
    file_put_contents(dirname(__FILE__) . '/../temp/index.html', "403 - Forbidden Access");
}
