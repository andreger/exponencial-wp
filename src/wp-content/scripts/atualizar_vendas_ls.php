<?php
include_once '../../wp-load.php';

global $wpdb;

$query = "select * from wp_woocommerce_order_items where order_item_name like '%Usuario LS%'";
$result = $wpdb->get_results($query);

foreach ($result as $item) {
	set_time_limit(30);

	$order_id = $item->order_id;
	$order_item_id = $item->order_item_id;

	echo "Pedido: {$order_id}<br>";

	$total = get_post_meta($order_id, '_order_total', true);

	echo "Recuperar valor do pedido... {$total}<br>";

	$desconto = $total / 4;	

	echo "Recuperar valor do desconto... {$desconto}<br>";

	update_post_meta($order_id, '_cart_discount', $desconto);

	echo "Atualizar valor do desconto.<br>";

	$wpdb->update( 
		'table', 
		array( 
			'order_item_name' => 'aluno-ls',	// string
			'order_item_type' => 'coupon'	// integer (number) 
		),
		array( 'order_item_id' => $order_item_id )
	);

	echo "Atualizando tabela do woo <br>";

	excluir_vendas_por_pedido($order_id);

	echo "Excluir venda <br>";

	echo "Item finalizado **************** <br>";

}

processar_pedidos_nao_processados();

echo "Processar pedidos não processados <br>";
?>