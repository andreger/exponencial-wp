<?php
include_once '../../wp-load.php';

global $wpdb;

$sql = "SELECT p.post_id, pm.meta_value as tempo_acesso 
            FROM produtos p, wp_postmeta pm 
        WHERE p.post_id = pm.post_id 
            AND pm.meta_key = 'tempo_de_acesso' 
            AND pm.meta_value IS NOT NULL
            AND trim(pm.meta_value) != ''
        ";
$result = $wpdb->get_results($sql);
$count = 0;
foreach ($result as $item) {
    $update_sql = "UPDATE produtos SET pro_tempo_acesso = {$item->tempo_acesso} WHERE post_id = {$item->post_id}";
    //echo "<br>".$update_sql;
    if($wpdb->query($update_sql)){
        $count++;
    }
}

echo "<br>Encontrei " . count($result) . " produtos e atualizei {$count} (quando o valor é igual não conta como atualizado)<br>";
