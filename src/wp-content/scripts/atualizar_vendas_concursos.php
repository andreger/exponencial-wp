<?php
include_once '../../wp-load.php';

global $wpdb;

ini_set('default_socket_timeout', 1200);
set_time_limit(0);

KLoader::model("ConcursoModel");

if (ob_get_level() == 0) ob_start();

// inicializando todos os concursos com 1 vendas. Necessário para a busca de concursos da Home
$sql = "select * from wp_posts where post_type = 'concurso'";

$resultado = $wpdb->get_results($sql);

echo "Inicializando concursos...<br>";
foreach ($resultado as $item) {
	echo "Concurso ID {$item->ID}, titulo: {$item->post_title}<br>";
	update_post_meta($item->ID, 'vendas', 1);
}

echo "------------------<br>";

// Atualiza os valores de vendas de acordo com os últimos 3 meses
$inicio = date('Y-m-d', strtotime("-2 months"));
$fim = date('Y-m-d');

echo "Processando compras entre {$inicio} e {$fim}<br>";

$lines = listar_vendas_agrupadas_por_concurso($inicio, $fim, 1, 'slug');

echo "------------------<br>";

foreach ($lines as $key => $line) {
	$concurso = get_concurso_por_slug($key);

	if($concurso) {
		$vendas = $line['qtde_alunos'];
		update_post_meta($concurso->ID, 'vendas', $vendas);

		// se não tiver vendas não será mostrado no Home. torna-se desnecessário a verificação do menor preços
		if($vendas == 0) {
		    $menor = 0;
		}
		else {
		    $menor = ConcursoModel::get_menor_preco_concurso($concurso->ID);
		}

		$wpdb->update('concursos', ['con_vendas' => $vendas, 'con_menor_preco' => $menor], ['con_slug' => $key] );

		echo "Atualizando slug: {$key}, id: {$concurso->ID} com {$vendas} vendas<br>";
		
	}
	else {
		echo "<span style='color:ff0000'>Não existe concurso com slug: {$key}</span><br>";	
	}
	
	echo str_pad('',4096)."\n";   
	flush();
	ob_flush();
}

ob_end_flush();