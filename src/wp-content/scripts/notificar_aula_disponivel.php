<?php
include_once '../../wp-load.php';

$produtos = get_todos_produtos();

foreach($produtos as $produto) {
	echo "Verificando produto " . $produto->id . "<br>";
	
	// verifica se produto tem arquivo
	$files = listar_arquivos_de_produto($produto->id);
	
	if($files) {
		$aulas = get_post_meta($produto->id, 'aulas_nome', true);
		$arquivos = get_post_meta($produto->id, 'aulas_arquivo', true);
		$datas = get_post_meta($produto->id, 'aulas_data', true);
		$dias_liberacao = get_post_meta($produto->id, 'aulas_liberacao', true);
		$liberar_gradualmente = get_post_meta($produto->id, 'liberar_gradualmente', true) == 'yes'; 
		
		foreach ($files as $file) {
			// recupera o indice da aula
			echo "Procurando indice da aula " . $file['name'] . "<br>";
			
			$index = null;
			for($i = 0; $i < count($arquivos); $i++) {
				$aux = explode('/', $arquivos[$i][0]);
				$arq = $aux[count($aux) - 1];
				if(trim($file['name']) == trim($arq)) {
					$index = $i;
					echo "Encontrado indice da aula " . $file['name'] . "<br>";
					break;
				}
			}
		
			// verifica se data da aula eh menor ou igual a data atual 
			if(!is_null($index)) {
				echo "Verificando data da aula " . $file['name'] . "<br>";
				if($liberar_gradualmente){
					echo "<h2>Aula sendo liberada hoje (por LMS (".$dias_liberacao[$index]."): " . $datas[$index] . "</h2><br>";
					$usuarios = get_usuarios_que_compraram_por_dias_apos_compra($produto->id, $dias_liberacao[$index]);			
				}elseif(hoje_yyyymmdd() == converter_para_yyyymmdd($datas[$index])) {
					echo "<h2>Aula sendo liberada hoje (por data): " . $datas[$index] . "</h2><br>";
					$usuarios = get_usuarios_que_compraram($produto->id);			
				}

				if(!empty($usuarios)){
					
					foreach ($usuarios as $usuario) {
						$u = get_usuario_array($usuario->ID);
						echo "<h3>Enviando e-mail para: {$u['email']}</h3><br>";
						
						$mensagem = get_template_email('notificacao-nova-aula.php', array(
								'nome' => $u['nome_completo'],
								'aula' => $file['name'],
								'curso_nome' => $produto->post->post_title
						));
						enviar_email('leonardo.coelho@exponencialconcursos.com.br', "Nova aula disponível - {$produto->post->post_title} - {$file['name']}", $mensagem);
	// 						enviar_email($u['email'], "Nova aula disponível - {$produto->post->post_title} - {$file['name']}", $mensagem);	
					}

				}else {
					echo "Nao bateu a data da aula " . $datas[$index] . "<br>";
				}
			}
			else {
				echo "Não encontrei indice da aula " . $file['name'] . "<br>";
			}
		}
	}
	else {
		echo "Produto " . $produto->ID . " nao possui arquivos<br>";
	}
	echo "*******************************<br>";
}