<?php
include_once '../../wp-load.php';

if(!isset($_GET['dt_ini'])){
    $dt_ini = date('d-m-Y_h:i:sa');
}else{
    $dt_ini = $_GET['dt_ini'];
}

set_time_limit(300);

$limite = 1000;

$offset = $_GET['offset']?:0;

if(!isset($_GET['total'])){
    $sql = "SELECT COUNT(*) FROM pedido_produto WHERE ped_data_base IS NULL";

    if($usu_id = $_GET['usu_id']){
        $sql .= " AND usu_id = {$usu_id}";
    }

    $total = $wpdb->get_var($sql);
}else{
    $total = $_GET['total'];
}

$sql = "SELECT pedido_id FROM pedido_produto WHERE ped_data_base IS NULL";

if($usu_id = $_GET['usu_id']){
    $sql .= " AND usu_id = {$usu_id}";
}

$sql .= " ORDER BY pedido_id DESC LIMIT $limite";

$result = $wpdb->get_results($sql);

foreach ($result as $item) {
    try{
        $order = new WC_Order($item->pedido_id);
    }catch (Exception $e){
        continue;
    }
    $data_base = $order->get_date_completed();
    if(!$data_base) {
        $data_base = $order->get_date_created();
    }
    $data_base = converter_para_yyyymmdd($data_base);
    if($wpdb->query("UPDATE pedido_produto SET ped_data_base = '{$data_base}' WHERE pedido_id = {$item->pedido_id}")){
        $count++;
    }
}

$offset += $limite;

if($offset <= $total) : ?>
<html>
<head>
<meta http-equiv="Refresh" content="1">
</head>
<body>
	<?php echo("Atualizando PEDIDO_PRODUTO->ped_data_base COM OFFSET {$offset} PARA O TOTAL DE ".$total." PEDIDOS<br/>"); ?>
	<script>
        window.location.replace('/wp-content/scripts/atualizar_pedido_data_base.php?offset=<?= $offset ?>&total=<?= $total ?>&dt_ini=<?= $dt_ini ?><?= $usu_id?("&usu_id=".$usu_id):"" ?>');
     </script>
</body>
</html>
<?php else : ?>
Inicio: <?= $dt_ini ?><br>
Fim: <?= date('d-m-Y_h:i:sa'); ?><br>
Script finalizado.
<?php endif ?>
