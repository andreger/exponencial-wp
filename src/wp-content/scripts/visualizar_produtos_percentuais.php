<?php
require_once '../../wp-load.php';

global $wpdb;

set_time_limit(300);

// output headers so that the file is downloaded rather than displayed
header('Content-type: text/csv');
header('Content-Disposition: attachment; filename="percentuais.csv"');

// do not cache the file
header('Pragma: no-cache');
header('Expires: 0');

// create a file pointer connected to the output stream
$file = fopen('php://output', 'w');

$sql = "SELECT post_id, post_title, meta_value FROM wp_postmeta pm JOIN wp_posts p ON p.ID = pm.post_id
            WHERE meta_key = '_percentuais' AND post_status = 'publish' ";

$resultado = $wpdb->get_results($sql);

fputcsv($file, ["Produto ID", "Nome", "Percentual"]);

foreach ($resultado as $item) {
    
    $percentuais = unserialize($item->meta_value);
    
    $soma = 0.00;
    foreach ($percentuais as $percentual) {
        $percentual = str_replace(",", ".", $percentual);
        
        $soma += (float)$percentual;
    }

    $linha = [$item->post_id, $item->post_title, $soma];
    fputcsv($file, $linha);
}

exit;