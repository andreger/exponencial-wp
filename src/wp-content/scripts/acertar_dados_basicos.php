<?php
require_once '../../wp-load.php';

global $wpdb;

set_time_limit(300);

KLoader::model("ProdutoModel");

$limit = 100;
$offset = $_GET['o'] ?: 0;

$total = $wpdb->get_var("SELECT COUNT(*) AS total FROM produtos");	
$produtos_ids = $wpdb->get_col("SELECT post_id FROM produtos ORDER BY post_id DESC LIMIT {$offset}, {$limit}");

foreach ($produtos_ids as $id) {

	try{
		ProdutoModel::atualizar_metadados((int)$id, false);
	}
	catch(Exception $e) {
		continue;
	}
}

$offset += $limit;
if($offset > $total) {
	echo "Script concluído";
}
else {
	echo "Processados {$offset} itens de {$total}";

	echo "<script>window.location.href='/wp-content/scripts/acertar_dados_basicos.php?o={$offset}';</script>";
}