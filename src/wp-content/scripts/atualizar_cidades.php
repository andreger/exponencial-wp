<?php
include_once '../../wp-load.php';

global $wpdb;

$query = "SELECT COUNT(*) AS num_usuarios FROM wp_users u WHERE ID NOT IN (SELECT user_id from wp_usermeta where meta_key = 'cidade_atualizada')";
$num_usuarios =  $wpdb->get_var($query);

echo "Ainda existem {$num_usuarios} usuário para serem tratados...";

$query = "SELECT * FROM wp_users u WHERE ID NOT IN (SELECT user_id from wp_usermeta where meta_key = 'cidade_atualizada') LIMIT 10";
$usuarios = $wpdb->get_results($query, ARRAY_A);

foreach ($usuarios as $usuario) {
	$cidade_antiga = get_user_meta($usuario['ID'], 'billing_city', true);
	$estado = get_user_meta($usuario['ID'], 'billing_state', true);
	
	if($cidade_antiga && $estado) {
		$query = "SELECT * FROM cidades WHERE cid_nome = \"{$cidade_antiga}\" AND cid_uf = \"{$estado}\"";
		$cidade = $wpdb->get_row($query, ARRAY_A);
		
		if($cidade) {
			update_user_meta($usuario['ID'], 'cidade', $cidade['cid_codigo_municipio']);
			update_user_meta($usuario['ID'], 'cidade_atualizada', '1');
		}
		else {
			update_user_meta($usuario['ID'], 'cidade_atualizada', '2');
		}
	}
	else {
		update_user_meta($usuario['ID'], 'cidade_atualizada', '3');
	}
}
?>
<script>location.reload(true)</script>
	