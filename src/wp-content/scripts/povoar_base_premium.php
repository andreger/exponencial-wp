<?php
require_once '../../wp-load.php';

// não executa em PRD
if(is_producao()) exit;

$o = $_GET['o'] ?: 1;

$usuarios = [];
for($i = 132370; $i < 132390; $i++) {
    $usuarios[] = $i;
}

$produtos = [ 393163 ];

$u = array_rand($usuarios);
$p = array_rand($produtos);

$address = array(
    'first_name' => 'Usuário',
    'last_name'  => 'Carga ' . $usuarios[$u],
    'company'    => 'Teste',
    'email'      => 'teste@gmail.com',
    'phone'      => '760-555-1212',
    'address_1'  => 'Rua ZBD',
    'address_2'  => '104',
    'city'       => 'San Diego',
    'state'      => 'Ca',
    'postcode'   => '92121',
    'country'    => 'US'
);

// Now we create the order
$order = wc_create_order(['customer_id' => 1]);

// The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
$order->add_product( get_product( $produtos[$p] ), 1 ); // This is an existing SIMPLE product
$order->set_address( $address, 'billing' );
//
$order->calculate_totals();
$order->update_status("completed", 'Teste de premium', TRUE);

$total = 5;
$o++;

if($o >= $total) {
    echo "Script concluído";
}
else {
    echo "Processados {$o} itens de {$total} ";
    
    echo "<script>window.location.href='/wp-content/scripts/povoar_base_premium.php?o={$o}';</script>";
}