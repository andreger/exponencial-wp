<?php
include_once '../../wp-load.php';

KLoader::model("RelatorioVendasPorProfessorModel");

$data_ref = '-30 days';

if(isset($_GET['d'])) {
    $data_ref = $_GET['d'];
}

RelatorioVendasPorProfessorModel::calcular_acumulados($data_ref);