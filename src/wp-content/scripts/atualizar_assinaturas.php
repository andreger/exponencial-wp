<?php

include_once '../../wp-load.php';

$produtos = get_produtos_por_categoria('assinatura-sq', TRUE);

foreach ($produtos as $produto) {
	echo "Produto: {$produto->post_title}<br>";

	if($pedidos = listar_pedidos_por_produto($produto->ID)) {
		foreach ($pedidos as $pedido) {
			$usuario_id = $pedido->get_user_id();

			if(!get_assinatura_validade($usuario_id)) {

				echo "Data: {$pedido->order_date}<br>";
				echo "Pedido: {$pedido->id}<br>";
				echo "Usuario: " . $usuario_id . "<br>";
				echo "Validade: " . get_assinatura_validade($usuario_id) . "<br>";
				echo "---------------------------<br>";

				$adicionar_validade = 0;
				$categorias = listar_categorias($produto->ID);
		
				foreach ($categorias as $categoria) {
					
					if(is_categoria_assinatura_sq_mensal($categoria)) {
						log_wp('debug', 'É assinatura mensal');
						$adicionar_validade += 1;
					}
					elseif(is_categoria_assinatura_sq_trimestral($categoria)) {
						log_wp('debug', 'É assinatura trimenstral');
						$adicionar_validade += 3;
					}
					elseif(is_categoria_assinatura_sq_semestral($categoria)) {
						log_wp('debug', 'É assinatura semestral');
						$adicionar_validade += 6;
					}
					elseif(is_categoria_assinatura_sq_anual($categoria)) {
						log_wp('debug', 'É assinatura anual');
						$adicionar_validade += 12;
					}
			
				}

				if($adicionar_validade) {
					$nova_validade = date('Y-m-d', strtotime($pedido->order_date . " +" . $adicionar_validade . " months"));
					update_user_meta($usuario_id, 'assinante-sq-validade', $nova_validade);
				}
			}
		}
	}
	else {
		echo "Nao ha pedidos para esse produto<br>";
	}
}

