<?php
require_once '../../../wp-load.php';

global $wpdb;

if(!is_administrador()) {
    echo "Falha de autenticação.";
    exit;
}

set_time_limit(300);

// recupera cupons publicados
$cupons_ids = $wpdb->get_col("select ID from wp_posts where post_type = 'shop_coupon' and post_status = 'publish'");

$wpdb->query("update wp_posts set post_status = 'draft' where ID in 
    (select ID FROM (SELECT ID from wp_posts where post_type = 'shop_coupon' and post_status = 'publish') AS c) ");

update_option("cupons_ids_desativados", $cupons_ids, false);

echo "Script concluído";