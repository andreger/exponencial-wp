<?php
require_once '../../../wp-load.php';

global $wpdb;

if(!is_administrador()) {
    echo "Falha de autenticação.";
    exit;
}

set_time_limit(300);

// recupera cupons publicados
$cupons_ids = get_option("cupons_ids_desativados");

$cupons_ids = implode(",", $cupons_ids);

$sql = "update wp_posts set post_status = 'publish' where ID in
    ({$cupons_ids})";

$wpdb->query($sql);

// delete_option("cupons_ids_desativados");

echo "Script concluído";