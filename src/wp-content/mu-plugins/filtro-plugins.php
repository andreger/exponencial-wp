<?php
/**
 * @package mu-plugins
 * @version 1.0
 *
 * Plugin Name: Filtro de Plugins
 * Description: Plugin desenvolvido para filtrar os plugins que não são necessários em uma página
 * Author: João Paulo Ferreira <jpaulofms@gmail.com>
 * Version: 1.0
 * 
 * Obs.: Os plugins do tipo Must-Use são carregados em ordem alfabética
 * @see https://kinsta.com/blog/disable-wordpress-plugins-loading/#mu-plugins
 * @see https://wordpress.org/support/article/must-use-plugins/
 */


/**
 * Função genérica para verificar se uma URL bate com um padrão especificado
 * 
 * @since L1
 * 
 * @param string 		$padrao Padrão da URL a ser verificado.
 * @param string|NULL 	$url Url a ser consultada. Se não for informada, a URL atual será utilizada
 * 
 * @return bool
 */ 
function fplugin_is_url($padrao, $url = NULL)
{

    $debug = "PADRÃO: {$padrao}";

    $padrao_array = explode("[FP]", $padrao);

    $url_regex = $padrao_array[0];
    $query_text = count($padrao_array) > 1 ? $padrao_array[1] : "" ;

    $debug .= " | URL_REGEX: {$url_regex} | QUERY_TEXT: {$query_text}";

    $url = $url ?: $_SERVER['REQUEST_URI'];

    $debug .= " | URL: {$url}";
    
    $url_a = parse_url($url);
    
    $result = preg_match($url_regex, $url_a['path']);

    $debug .= " | URL_PATH: {$url_a['path']} (result: {$result})";

    $asd = false;
    if($result && $query_text)
    {
        $asd = true;
        $query_url = array_key_exists('query', $url_a) ? $url_a['query'] : "";

        $debug .= " | URL_QUERY: {$query_url}";

        $result = strpos($url_a['query'], $query_text) !== false;

        $debug .= " (result: {$result})";
    }

    if($result || $asd){
        log_fplugins("DEBUG", $debug);
    }

    return $result;
}

/**
 * Filtro dos plugins com base no especificado em ./_filtro-plugins-constantes.php
 * 
 * @see FiltroPluginsConstantes
 * 
 * @param array $plugins Lista de plugins que serão filtrados
 * 
 * @since L1
 */
add_filter( 'option_active_plugins', 'fplugin_filter_plugins', 100);

function fplugin_filter_plugins ( $plugins )
{
    
    if ( FPLUGIN_IS_ACTIVE )
    {
        
        $plugins_to_remove = array();
        
        //Verifica se está em algum subprojeto do sistema
        $has_matched_subproject = fplugin_find_what_remove($plugins, $plugins_to_remove, FiltroPluginsConstantes::$mu_filtro_plugins_sub_projects);
        
        //Se não está em um subprojeto então vai para as páginas específicas
        if ( !$has_matched_subproject )
        {
            
            $has_matched_page = fplugin_find_what_remove($plugins, $plugins_to_remove, FiltroPluginsConstantes::$mu_filtro_plugins_especificos_por_pagina);
            
            //Se não existir nenhuma definição específica então usa a padrão
            if ( !$has_matched_page )
            {
                $log = ", nada será adicionado pois FiltroPluginsConstantes::UNKNOWN_PAGE_GROUP é nulo";
                if ( !is_null( FiltroPluginsConstantes::UNKNOWN_PAGE_GROUP ) )
                {
                    $plugins_to_remove = array_diff( $plugins, FiltroPluginsConstantes::UNKNOWN_PAGE_GROUP );
                    $log = ", adicionando o padrão UNNKNOWN_PAGE_GROUP";
                }
                
                $request_uri = $_SERVER['REQUEST_URI'];
                $url_a = parse_url($request_uri);
                $qsLog = isset($url_a['query']) ? $url_a['query'] : "";
                $urlLog = isset($url_a['path']) ? $url_a['path'] : "";

                log_fplugins("DEBUG", "Não foi encontrado REGEX para a URL {$urlLog} com QueryString {$qsLog}{$log}");
            }

        }
        
        //Se houverem plugins específicos então remove os outros
        if( !empty( $plugins_to_remove ) )
        {
            
            $keys = array();
            foreach ($plugins as $key => $plugin)
            {
                if(in_array($plugin, $plugins_to_remove))
                {
                    array_push($keys, $key);
                }
            }
            
            foreach($keys as $key){
                unset($plugins[$key]);
            }
        
            //Sempre se adiciona os plugins padrão quando não se está em nenhum subprojeto e está removendo algo
            if ( !$has_matched_subproject )
            {
                $plugins = array_merge( $plugins, FiltroPluginsConstantes::$mu_filtro_plugins_default_por_pagina );
            }    
        }
        
    }
    
    return $plugins;
    
}

/**
 * Busca quais plugins devem ser removidos com base na definição regex -> plugin informada
 * 
 * @since L1
 * 
 * @param array $plugins
 * @param array $plugins_to_remove
 * @param array $regex_plugin_definition
 * @param string $debug
 * 
 * @return boolean TRUE se foi encontrada alguma definição para a página
 * 
 */
function fplugin_find_what_remove ( $plugins, &$plugins_to_remove, $regex_plugin_definition )
{
    $has_definition = FALSE;
    
    foreach ($regex_plugin_definition as $regex => $espec_plugins)
    {

        if ( fplugin_is_url($regex) )
        {
            
            if ( !is_null( $espec_plugins ) )
            {
                $plugins_to_remove = array_diff( $plugins, $espec_plugins );
            }
            
            $has_definition = TRUE;
            
            break;
        }
    }
    
    return $has_definition;
}

function log_fplugins($level, $mensagem)
{
    if(FPLUGIN_IS_DEBUG){
        $arquivo = $_SERVER['DOCUMENT_ROOT'] . '/../../../files/exponencial/logs/plugins.txt';
        $log = strtoupper($level) . ' - ' . date('Y-m-d H:i:s') . ' - ' . $mensagem . "\r\n";
        file_put_contents($arquivo, $log, FILE_APPEND);
    }
}