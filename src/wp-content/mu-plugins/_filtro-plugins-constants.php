<?php
/**
 * Filtro de Plugins
 * 
 * Constantes escalares para o funcionamento do filtro de plugins
 * 
 * @since L1
 */

/**
 * Regex de páginas específicas
 * @see https://exponencialconcursos.atlassian.net/wiki/spaces/EC/pages/119275665/Lista+de+P+ginas
 */
defined('AGRADECIMENTO_REGEX')          or define('AGRADECIMENTO_REGEX', "/\/checkout\/order-received(\/.*)?/");
defined('AREA_ALUNO_REGEX')             or define('AREA_ALUNO_REGEX', "/\/minha-conta(\/.*)?/");
defined('ARTIGOS_PROFESSOR_REGEX')      or define('ARTIGOS_PROFESSOR_REGEX', "/\/artigos-por-professor\/.+/");
defined('ASSINATURAS_REGEX')            or define('ASSINATURAS_REGEX', "/\/assinaturas(\/.*)?/");
defined('AUDIOBOOK_REGEX')              or define('AUDIOBOOK_REGEX', "/\/audiobooks-gratis(\/.*)?/");
defined('BLOG_REGEX')                   or define('BLOG_REGEX', "/\/blog-(.*)?/");
defined('CADASTRO_LOGIN_REGEX')         or define('CADASTRO_LOGIN_REGEX', "/\/cadastro-login(\/.*)/");
defined('CARRINHO_REGEX')               or define('CARRINHO_REGEX', "/\/carrinho(\/.*)?/");
defined('COACHING_REGEX')               or define('COACHING_REGEX', "/\/coaching(\/.*)?/");
defined('CHECKOUT_REGEX')               or define('CHECKOUT_REGEX', "/\/checkout(\/.*)?/");
defined('CHECKOUT_API_REGEX')           or define('CHECKOUT_API_REGEX', "/\/wc-api(\/.*)?/");
defined('CHECKOUT_QUERY_REGEX')         or define('CHECKOUT_QUERY_REGEX', "/^[\/]?$/[FP]wc-ajax=");//[FP] é usado como divisor do regex, a segunda parte verifica a query string da URL
defined('COMO_FUNCIONA_REGEX')          or define('COMO_FUNCIONA_REGEX', "/\/como-funciona(\/.*)?/");
defined('CONCURSO_REGEX')               or define('CONCURSO_REGEX', "/\/concurso\/.*/");
defined('CURSOS_GRATIS_REGEX')          or define('CURSOS_GRATIS_REGEX', "/\/cursos-gratis(\/.*)?/");
defined('CURSOS_POR_CONCURSO_REGEX')    or define('CURSOS_POR_CONCURSO_REGEX', "/\/cursos-por-concurso(\/.*)?/");
defined('CURSOS_POR_PROFESSOR_REGEX')   or define('CURSOS_POR_PROFESSOR_REGEX', "/\/cursos-por-professor(\/.*)?/");
defined('CURSOS_POR_MATERIA_REGEX')     or define('CURSOS_POR_MATERIA_REGEX', "/\/cursos-por-materia(\/.*)?/");
defined('DEPOIMENTOS_REGEX')            or define('DEPOIMENTOS_REGEX', "/\/depoimento(\/.*)?/");
defined('DESCONTOS_PROMOCOES_REGEX')    or define('DESCONTOS_PROMOCOES_REGEX', "/\/descontos-promocoes(\/.*)?/");
defined('ENTREVISTAS_REGEX')            or define('ENTREVISTAS_REGEX', "/\/entrevista(\/.*)?/");
defined('FALE_CONOSCO_REGEX')           or define('FALE_CONOSCO_REGEX', "/\/fale-conosco(\/.*)?/");
defined('FORUM_REGEX')                  or define('FORUM_REGEX', "/\/forum(.*)?/");
defined('FORUM_PENDING_REGEX')          or define('FORUM_PENDING_REGEX', "/^[\/]?$/[FP]post_type=topic");//[FP] é usado como divisor do regex, a segunda parte verifica a query string da URL
defined('HOME_REGEX')                   or define('HOME_REGEX', "/^[\/]?$/");
defined('KADMIN_SCRIPTS_REGEX')         or define('KADMIN_SCRIPTS_REGEX', "/\/wp-content\/kadmin(\/.*)?/");
defined('LANDING_PAGES_REGEX')          or define('LANDING_PAGES_REGEX', "/\/eventos\/.+/");
defined('MAPAS_MENTAIS_GRATIS_REGEX')   or define('MAPAS_MENTAIS_GRATIS_REGEX', "/\/mapas-mentais-gratis(\/.*)?/");
defined('METODOLOGIA_REGEX')            or define('METODOLOGIA_REGEX', "/\/metodologia-exponencial(\/.*)?/");
defined('PARCEIROS_REGEX')              or define('PARCEIROS_REGEX', "/\/parceiros(\/.*)?/");
defined('PERGUNTAS_FREQUENTES_REGEX')   or define('PERGUNTAS_FREQUENTES_REGEX', "/\/perguntas-frequentes-portal(\/.*)?/");
defined('POLITICA_PRIVACIDADE_REGEX')   or define('POLITICA_PRIVACIDADE_REGEX', "/\/politica-de-privacidade(\/.*)?/");
defined('PRODUTO_REGEX')                or define('PRODUTO_REGEX', "/\/produto\/.+/");
defined('PROFESSOR_REGEX')              or define('PROFESSOR_REGEX', "/\/professor(\/.*)?/");
defined('QUEM_SOMOS_REGEX')             or define('QUEM_SOMOS_REGEX', "/\/quem-somos(\/.*)?/");
defined('QUESTOES_COMENTADAS_REGEX')    or define('QUESTOES_COMENTADAS_REGEX', "/\/questoes-comentadas-gratis(\/.*)?/");
defined('RECUPERACAO_SENHA_REGEX')      or define('RECUPERACAO_SENHA_REGEX', "/\/recuperacao-de-senha(\/.*)?/");
defined('RELATORIO_REGEX')              or define('RELATORIO_REGEX', "/\/relatorio-(.*)?/");
defined('RESULTADOS_REGEX')             or define('RESULTADOS_REGEX', "/\/resultados(\/.*)?/");
defined('SIMULADOS_REGEX')              or define('SIMULADOS_REGEX', "/\/simulados(\/.*)?/");
defined('SIMULADOS_GRATIS_REGEX')       or define('SIMULADOS_GRATIS_REGEX', "/\/simulados-gratis(\/.*)?/");
defined('SQ_PAGINA_REGEX')              or define('SQ_PAGINA_REGEX', "/\/sistema-de-questoes(\/.*)?/");
defined('TERMOS_USO_REGEX')             or define('TERMOS_USO_REGEX', "/\/termos-de-uso(\/.*)?/");
defined('TODOS_CURSOS_REGEX')           or define('TODOS_CURSOS_REGEX', "/\/todos-cursos(\/.*)?/");


/** 
 * Regex de subprojetos
 */
defined('ADMIN_REGEX')              or define('ADMIN_REGEX', "/\/admin\/.+/");
defined('LOGOUT_REGEX')             or define('LOGOUT_REGEX', "/\/questoes\/login\/logout(\/.*)?/");//Está aqui porque é uma subpágina do SQ que precisa de um tratamento diferenciado
defined('NOTA_FISCAL_REGEX')        or define('NOTA_FISCAL_REGEX', "/\/notafiscal\/.+/");
defined('PAINEL_COACHING_REGEX')    or define('PAINEL_COACHING_REGEX', "/\/painel-coaching\/.+/");
defined('SQ_REGEX')                 or define('SQ_REGEX', "/\/questoes\/.+/");
defined('WP_ADMIN_REGEX')           or define('WP_ADMIN_REGEX', "/\/wp-admin(\/.*)?/");

/**
 * Plugins
 */
defined('FPLUGIN_ADVANCED_CUSTOM_FIELDS')                   or define('FPLUGIN_ADVANCED_CUSTOM_FIELDS', 'advanced-custom-fields/acf.php');
defined('FPLUGIN_BBPRESS_NOTIFY')                           or define('FPLUGIN_BBPRESS_NOTIFY', 'bbpress-notify/bbpress-notify.php');
defined('FPLUGIN_BBPRESS')                                  or define('FPLUGIN_BBPRESS', 'bbpress/bbpress.php');
defined('FPLUGIN_BBPRESS_MODERATION')                       or define('FPLUGIN_BBPRESS_MODERATION', 'bbpressmoderation/bbpressmoderation.php');
defined('FPLUGIN_CLASSIC_EDITOR')                           or define('FPLUGIN_CLASSIC_EDITOR', 'classic-editor/classic-editor.php');
defined('FPLUGIN_CHECKOUT_FEES')                            or define('FPLUGIN_CHECKOUT_FEES', 'checkout-fees-for-woocommerce/checkout-fees-for-woocommerce.php');
defined('FPLUGIN_EXPONENCIAL_CONCURSOS')                    or define('FPLUGIN_EXPONENCIAL_CONCURSOS', 'concursos/concursos.php');
defined('FPLUGIN_EXPONENCIAL_DEPOIMENTOS')                  or define('FPLUGIN_EXPONENCIAL_DEPOIMENTOS', 'exponencial-depoimentos/depoimentos.php');
defined('FPLUGIN_EXPONENCIAL_LANDING_PAGES')                or define('FPLUGIN_EXPONENCIAL_LANDING_PAGES', 'exponencial-landing-pages/landing_pages.php');
defined('FPLUGIN_QUERY_MONITOR')                            or define('FPLUGIN_QUERY_MONITOR', 'query-monitor/query-monitor.php');
defined('FPLUGIN_FAQ_MANAGER')                              or define('FPLUGIN_FAQ_MANAGER', 'wordpress-faq-manager/faq-manager.php');
defined('FPLUGIN_ONE_SIGNAL')                               or define('FPLUGIN_ONE_SIGNAL', 'onesignal-free-web-push-notifications/onesignal.php');
defined('FPLUGIN_POST_AFFILIATE')                           or define('FPLUGIN_POST_AFFILIATE', 'postaffiliatepro/postaffiliatepro.php');
defined('FPLUGIN_REVSLIDER')                                or define('FPLUGIN_REVSLIDER', 'revslider/revslider.php');
defined('FPLUGIN_SCROLL_BACK_TO_TOP')                       or define('FPLUGIN_SCROLL_BACK_TO_TOP', 'scroll-back-to-top/scroll-back-to-top.php');
defined('FPLUGIN_TAWKTO')                                   or define('FPLUGIN_TAWKTO', 'tawkto-live-chat/tawkto.php');
defined('FPLUGIN_TINYMCE')                                  or define('FPLUGIN_TINYMCE', 'tinymce-advanced/tinymce-advanced.php');
defined('FPLUGIN_USER_ROLE_EDITOR')                         or define('FPLUGIN_USER_ROLE_EDITOR', 'user-role-editor/user-role-editor.php');
defined('FPLUGIN_WOOCOMMERCE')                              or define('FPLUGIN_WOOCOMMERCE', 'woocommerce/woocommerce.php');
defined('FPLUGIN_WOOCOMMERCE_CHECKOUT_MANAGER')             or define('FPLUGIN_WOOCOMMERCE_CHECKOUT_MANAGER', 'woocommerce-checkout-manager/woocommerce-checkout-manager.php');
defined('FPLUGIN_WOOCOMMERCE_EXTRA_CHECKOUT_FIELDS_BRAZIL') or define('FPLUGIN_WOOCOMMERCE_EXTRA_CHECKOUT_FIELDS_BRAZIL', 'woocommerce-extra-checkout-fields-for-brazil/woocommerce-extra-checkout-fields-for-brazil.php');
defined('FPLUGIN_WOOCOMMERCE_PAGARME')                      or define('FPLUGIN_WOOCOMMERCE_PAGARME', 'woocommerce-pagarme/woocommerce-pagarme.php');
defined('FPLUGIN_WOOCOMMERCE_PAGSEGURO')                    or define('FPLUGIN_WOOCOMMERCE_PAGSEGURO', 'woocommerce-pagseguro/woocommerce-pagseguro.php');
defined('FPLUGIN_WOOCOMMERCE_PAGSEGURO_ASSINATURAS')        or define('FPLUGIN_WOOCOMMERCE_PAGSEGURO_ASSINATURAS', 'woocommerce-pagseguro-assinaturas/woocommerce-pagseguro-assinaturas.php');
defined('FPLUGIN_WOOCOMMERCE_PRODUCT_BUNDLES')              or define('FPLUGIN_WOOCOMMERCE_PRODUCT_BUNDLES', 'woocommerce-product-bundles/woocommerce-product-bundles.php');
defined('FPLUGIN_WOOCOMMERCE_PRODUCT_PAYMENTS')             or define('FPLUGIN_WOOCOMMERCE_PRODUCT_PAYMENTS', 'woocommerce-product-payments/woocommerce-payment-gateway-per-product.php');
defined('FPLUGIN_WOOCOMMERCE_SUBSCRIPTION')                 or define('FPLUGIN_WOOCOMMERCE_SUBSCRIPTION', 'woocommerce-subscriptions/woocommerce-subscriptions.php');
defined('FPLUGIN_WORDPRESS_FAQ_MANAGER')                    or define('FPLUGIN_WORDPRESS_FAQ_MANAGER', 'wordpress-faq-manager/faq-manager.php');
defined('FPLUGIN_WORDPRESS_IMPORTER')                       or define('FPLUGIN_WORDPRESS_IMPORTER', 'wordpress-importer/wordpress-importer.php');
defined('FPLUGIN_WORDPRESS_SEO')                            or define('FPLUGIN_WORDPRESS_SEO', 'wordpress-seo/wp-seo.php');
defined('FPLUGIN_WOOCOMMERCE_FINALE')                            or define('FPLUGIN_WOOCOMMERCE_FINALE', 'finale-woocommerce-sales-countdown-timer-discount-plugin/finale-woocommerce-sales-countdown-timer-discount-plugin.php');

//Debug
defined('FPLUGIN_IS_ACTIVE') or define('FPLUGIN_IS_ACTIVE', TRUE);
defined('FPLUGIN_IS_DEBUG')  or define('FPLUGIN_IS_DEBUG', FALSE);

/**
 * FiltroPluginsConstantes monta as constantes não escalares estáticas para o funcionamento do filtro de plugins
 * 
 * @since L1
 * 
 * @author João Paulo Ferreira <jpaulofms@gmail.com>
 *
 */
class FiltroPluginsConstantes {

    /**
     * Grupos de plugins a serem carregados nas páginas
     * @see https://exponencialconcursos.atlassian.net/wiki/spaces/EC/pages/119177233/Carregamento+Seletivo+de+Plugins+do+Wordpress
     * @see https://exponencialconcursos.atlassian.net/wiki/spaces/EC/pages/119275665/Lista+de+P+ginas
     */
    const GRUPO_A = [
        FPLUGIN_ONE_SIGNAL,
        FPLUGIN_POST_AFFILIATE,
        FPLUGIN_REVSLIDER,
        FPLUGIN_SCROLL_BACK_TO_TOP,
        FPLUGIN_TAWKTO,
    ];

    const GRUPO_B = [
        FPLUGIN_ONE_SIGNAL,
        FPLUGIN_POST_AFFILIATE,
        FPLUGIN_SCROLL_BACK_TO_TOP,
        FPLUGIN_TAWKTO,
    ];

    const GRUPO_C = [
        FPLUGIN_EXPONENCIAL_LANDING_PAGES
    ];

    const GRUPO_D = [
        FPLUGIN_ONE_SIGNAL,
        FPLUGIN_POST_AFFILIATE,
        FPLUGIN_REVSLIDER,
        FPLUGIN_SCROLL_BACK_TO_TOP,
        FPLUGIN_FAQ_MANAGER
    ];

    const GRUPO_E = [
        FPLUGIN_EXPONENCIAL_CONCURSOS,
        FPLUGIN_ONE_SIGNAL,
        FPLUGIN_POST_AFFILIATE,
        FPLUGIN_REVSLIDER,
        FPLUGIN_SCROLL_BACK_TO_TOP,
        FPLUGIN_WOOCOMMERCE_CHECKOUT_MANAGER,//Woocommerce adicionado para os preços funcionarem
        FPLUGIN_WOOCOMMERCE_EXTRA_CHECKOUT_FIELDS_BRAZIL,
        FPLUGIN_WOOCOMMERCE_PAGARME,
        FPLUGIN_WOOCOMMERCE_PAGSEGURO,
        FPLUGIN_WOOCOMMERCE_PAGSEGURO_ASSINATURAS,
        FPLUGIN_WOOCOMMERCE_PRODUCT_BUNDLES,
        FPLUGIN_WOOCOMMERCE_FINALE,
        FPLUGIN_WOOCOMMERCE_PRODUCT_PAYMENTS,
        FPLUGIN_WOOCOMMERCE_SUBSCRIPTION
    ];

    const GRUPO_F = [
        FPLUGIN_ONE_SIGNAL,
        FPLUGIN_POST_AFFILIATE,
        FPLUGIN_SCROLL_BACK_TO_TOP,
        FPLUGIN_TAWKTO,
        FPLUGIN_FAQ_MANAGER
    ];

    const GRUPO_G = [
        FPLUGIN_ADVANCED_CUSTOM_FIELDS,
        FPLUGIN_BBPRESS,
        FPLUGIN_BBPRESS_MODERATION,
        FPLUGIN_BBPRESS_NOTIFY,
        FPLUGIN_CHECKOUT_FEES,
        FPLUGIN_CLASSIC_EDITOR,
        FPLUGIN_ONE_SIGNAL,
        FPLUGIN_POST_AFFILIATE,
        FPLUGIN_SCROLL_BACK_TO_TOP,
        FPLUGIN_TAWKTO,
        FPLUGIN_WOOCOMMERCE_CHECKOUT_MANAGER,
        FPLUGIN_WOOCOMMERCE_EXTRA_CHECKOUT_FIELDS_BRAZIL,
        FPLUGIN_WOOCOMMERCE_PAGARME,
        FPLUGIN_WOOCOMMERCE_PAGSEGURO,
        FPLUGIN_WOOCOMMERCE_PAGSEGURO_ASSINATURAS,
        FPLUGIN_WOOCOMMERCE_PRODUCT_BUNDLES,
        FPLUGIN_WOOCOMMERCE_PRODUCT_PAYMENTS,
        FPLUGIN_WOOCOMMERCE_SUBSCRIPTION,
        FPLUGIN_WOOCOMMERCE_FINALE,
    ];

    const GRUPO_H = [
        FPLUGIN_SCROLL_BACK_TO_TOP,
        FPLUGIN_BBPRESS,
        FPLUGIN_BBPRESS_MODERATION,
        FPLUGIN_BBPRESS_NOTIFY
    ];

    const GRUPO_I = [
        FPLUGIN_ONE_SIGNAL,
        FPLUGIN_POST_AFFILIATE,
        FPLUGIN_SCROLL_BACK_TO_TOP,
        FPLUGIN_TAWKTO,
        FPLUGIN_BBPRESS,
        FPLUGIN_BBPRESS_MODERATION,
        FPLUGIN_BBPRESS_NOTIFY
    ];
    
    const GRUPO_J = [
        FPLUGIN_WOOCOMMERCE,
        FPLUGIN_WOOCOMMERCE_FINALE,
        FPLUGIN_WORDPRESS_SEO,
        FPLUGIN_QUERY_MONITOR,
    ];

    const GRUPO_K = [
        FPLUGIN_ADVANCED_CUSTOM_FIELDS,
        FPLUGIN_EXPONENCIAL_DEPOIMENTOS,
        FPLUGIN_SCROLL_BACK_TO_TOP,
        FPLUGIN_TAWKTO,
    ];

    //Para páginas não identificadas, em teoria um "Blog - Post Específico"
    const UNKNOWN_PAGE_GROUP = self::GRUPO_G;

    
    /**
     * Array contendo a definição dos subprojetos que, a princípio, não devem carregar nenhum plugin.
     * O valor de todas as entradas é vazio para que nenhum plugin seja carregado, opcionalmente pode
     * ser utilizado para tratar os casos que aparecerem durante a implantação deste plugin
     * 
     * @since L1
     * 
     * @var array
     */
    public static $mu_filtro_plugins_sub_projects = [
        WP_ADMIN_REGEX => NULL,
        LOGOUT_REGEX => NULL,//Tem que ser antes do SQ_REGEX, poderia ser só o Woocommerce mas não sei se o wp_logout() é eventualmente utilizado por algum outro plugin
        SQ_REGEX => self::GRUPO_J,
        ADMIN_REGEX => self::GRUPO_J,
        NOTA_FISCAL_REGEX => self::GRUPO_J,
        PAINEL_COACHING_REGEX => self::GRUPO_J,
    ];
    
    /**
     * Array contendo a definição do regex da página ligado a quais plugins devem ser carregados.
     * 
     * Se for a página do regex e os plugins específicos não forem nulos então verifica o que tem que ser removido.
     * Se os plugins específicos forem um array vazio então todos devem ser removidos.
     * Se os plugins específicos for nulo então é para manter todos os plugins.
     * 
     * A ordem do array importa pois após o primeiro 'match' a verificação acaba, por isso suburl's devem vir antes da mais genérica quando for o caso.
     * 
     * @since L1
     * 
     * @var array
     */
    public static $mu_filtro_plugins_especificos_por_pagina = [
        AGRADECIMENTO_REGEX => self::GRUPO_G,
        AREA_ALUNO_REGEX => self::GRUPO_G,
        ARTIGOS_PROFESSOR_REGEX => self::GRUPO_B,
        ASSINATURAS_REGEX => self::GRUPO_B,
        AUDIOBOOK_REGEX => self::GRUPO_B,
        BLOG_REGEX => self::GRUPO_B,
        CADASTRO_LOGIN_REGEX => self::GRUPO_B,
        CARRINHO_REGEX => self::GRUPO_G,
        COACHING_REGEX => self::GRUPO_D,
        CHECKOUT_REGEX => self::GRUPO_G,
        CHECKOUT_API_REGEX =>self::GRUPO_G,
        CHECKOUT_QUERY_REGEX => self::GRUPO_G,
        COMO_FUNCIONA_REGEX => self::GRUPO_B,
        CONCURSO_REGEX => self::GRUPO_E,
        CURSOS_GRATIS_REGEX => self::GRUPO_B,
        CURSOS_POR_CONCURSO_REGEX => self::GRUPO_B,
        CURSOS_POR_PROFESSOR_REGEX => self::GRUPO_B,
        CURSOS_POR_MATERIA_REGEX => self::GRUPO_B,
        DEPOIMENTOS_REGEX => self::GRUPO_K,
        DESCONTOS_PROMOCOES_REGEX => self::GRUPO_B,
        ENTREVISTAS_REGEX => self::GRUPO_K,
        FALE_CONOSCO_REGEX => self::GRUPO_B,
        FORUM_REGEX => self::GRUPO_I,
        FORUM_PENDING_REGEX => self::GRUPO_I,
        HOME_REGEX => self::GRUPO_A,
        KADMIN_SCRIPTS_REGEX => NULL,
        LANDING_PAGES_REGEX => self::GRUPO_C,
        MAPAS_MENTAIS_GRATIS_REGEX => self::GRUPO_B,
        METODOLOGIA_REGEX => self::GRUPO_A,
        PARCEIROS_REGEX => self::GRUPO_B,
        PERGUNTAS_FREQUENTES_REGEX => self::GRUPO_F,
        POLITICA_PRIVACIDADE_REGEX => self::GRUPO_B,
        PRODUTO_REGEX => self::GRUPO_G,
        PROFESSOR_REGEX => self::GRUPO_B,
        QUEM_SOMOS_REGEX => self::GRUPO_B,
        QUESTOES_COMENTADAS_REGEX => self::GRUPO_B,
        RECUPERACAO_SENHA_REGEX => self::GRUPO_B,
        RELATORIO_REGEX => self::GRUPO_H,
        RESULTADOS_REGEX => self::GRUPO_A,
        SIMULADOS_GRATIS_REGEX => self::GRUPO_B,//Tem que vir antes de "simulados" porque também começa com "simulados"
        SIMULADOS_REGEX => self::GRUPO_A,
        SQ_PAGINA_REGEX => self::GRUPO_A,
        TERMOS_USO_REGEX => self::GRUPO_B,
        TODOS_CURSOS_REGEX => self::GRUPO_E,
    ];
    
    /**
     * Array contendo a definição padrão que todas as páginas não afetadas pelo {@link FiltroPluginsConstantes::$mu_filtro_plugins_sub_projects}
     * 
     * @since L1
     * 
     * @var array
     */
    public static $mu_filtro_plugins_default_por_pagina = [
        FPLUGIN_WOOCOMMERCE,
        FPLUGIN_WOOCOMMERCE_PRODUCT_BUNDLES,
        FPLUGIN_WORDPRESS_SEO,
        FPLUGIN_WOOCOMMERCE_FINALE,
        FPLUGIN_QUERY_MONITOR,
    ];  
    
    /**
     * Escreve uma lista com os plugins ativos
     *
     * @since L1
     *
     */
    public static function plugins_ativos()
    {
        $active_plugins = get_option( 'active_plugins' );
        $plugins = "<ul><li>Nenhum plugin encontrado</li></ul>";
        if( count( $active_plugins ) > 0 ){
            sort($active_plugins);
            $plugins = "<ul>";
            foreach ( $active_plugins as $plugin ) {
                $plugins .= "<li>" . $plugin . "</li>";
            }
            $plugins .= "</ul>";
        }
        echo $plugins;
    }
    
}