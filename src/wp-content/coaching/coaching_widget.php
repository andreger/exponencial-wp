<?php
     /* Plugin Name: Coaching Widget
    Plugin URI: www.webacer.in
    Description: Coaching Front-end Widget for Homepage
    Version: 1.0
    Author: WebAcer Software
    Author URI: www.webacers.com
    */
    class coaching_widget extends WP_Widget {

              function coaching_widget() {
					$widget_ops = array(
                        'classname' => 'coaching_widget',
                        'description' => 'Coaching Widget'
             	 	);

              		$this->WP_Widget(
                        'coaching_widget',
                        'Coaching Widget',
                        $widget_ops
              		);
				} // func ends 

                 function widget() {
					global $coaching_db_version;
					$coaching_db_version = "1.0";
					global $wpdb;
					$table_name = $wpdb->prefix . "coaching";
					$sql = "SELECT * FROM ".$table_name." LIMIT 0,3" ;
					$result = $wpdb->get_results( $sql);
						$k = 1;
						if($wpdb->num_rows > 0) {
							echo '';
							echo '<div class="section_content">
								<div class="row">';
							foreach($result as $i => $value )
							{   
								if($k==3) $sc = 'one_third_last'; else $sc = 'one_third';
								echo do_shortcode('['.$sc.']<div class="c_img"><img src="'.$result[$i]->imageurl.'" /></div>
								<div class="c_head"><h3>'.$result[$i]->heading.'</h3></div>
								<div  class="c_desc">'.$result[$i]->description.'</div>[/'.$sc.']');
								$k++;		
							} // foreach ends 
							echo '</div></div>';
						}
					} // func ends 

    }  // class ends 
	
	add_action( 'widgets_init', 'load_coach_widgets' );

	function load_coach_widgets() {
		register_widget( 'coaching_widget' );
	}
?>