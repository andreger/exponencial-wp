<?php
/*
Plugin Name: Coaching Home Page Section 
Plugin URI: http://webacers.com
Description: This plugin will display the coaching section on homepage
Version: 1.0
Author: WebAcer Software
Author URI: http://webacers.com
*/
	global $coaching_db_version;
	$coaching_db_version = "1.0";
	include 'add_coaching_info.php';
	include 'coaching_widget.php';

	function coaching_install() {
		 
	   global $wpdb;
	   global $coaching_db_version;
	
	$table_name = $wpdb->prefix."coaching";
	$sql = "CREATE TABLE $table_name (
				id mediumint(9) NOT NULL AUTO_INCREMENT,
				heading varchar(100) NOT NULL,
				description text NOT NULL,
				imageurl varchar(255) NOT NULL,
				UNIQUE KEY id (id)
				);";
		
	   require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
	   dbDelta($sql);
	   add_option( "coaching_db_version", $coaching_db_version );
	}

		
	add_action( 'admin_menu', 'admin_coaching_menu' );


	function admin_coaching_menu() {
		add_object_page( 'Coaching', 'Coaching', 'manage_options', 'coaching', 'coaching' ); //getting side menu page
	 }

    register_activation_hook( __FILE__, 'coaching_install' );
?>