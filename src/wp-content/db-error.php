<?php
date_default_timezone_set('GMT');
header("HTTP/1.1 503 Service Temporarily Unavailable");
header("Status: 503 Service Temporarily Unavailable");
header("Retry-After: " . MINUTE_IN_SECONDS);
header("Retry-After: " . date("D, d M Y H:i:s e", strtotime(" +" . MINUTE_IN_SECONDS . " second")));
?>
<!DOCTYPE HTML>
<html>
<head>
<title>Aguarde um momento ... | Exponencial Concursos</title>
<META http-equiv="refresh" content="<?= MINUTE_IN_SECONDS ?>">
<style>
 body {
    margin: 40px 0;
    padding: 0;
    direction: ltr;
    font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 16px;
	font-style: normal;
	font-weight: bold;
	line-height: 1.4;
    text-align: center;
     color: #666;
}
</style>
</head>
<body>
    <img class="img-logo" src="/core/resources/img/layout/logo-header.png" width="313" height="84" alt="Exponencial Concursos" title="Exponencial Concursos"><br/>
    <span style="margin-top: 20px; display: inline-block">Aguarde um momento.<br>Estamos atualizando o sistema.</span>
</body>
</html>