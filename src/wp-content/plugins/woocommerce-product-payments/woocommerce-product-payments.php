<?php

/**
* Plugin Name: Dreamfox Media Payment gateway per Product for Woocommerce Premium
* Plugin URI: https://www.dreamfoxmedia.com/project/woocommerce-payment-gateway-per-product-premium/ 
* Version: 3.1.2
* Author: Dreamfox
* Author URI: www.dreamfoxmedia.com 
* Description: Extend Woocommerce plugin to add payments methods to a product
* Requires at least: 4.5
* Tested up to: 5.6.1
* WC requires at least: 4.0.0
* WC tested up to: 5.0.0
* Text Domain: dreamfoxmedia
* Domain Path: /languages
* @Developer : Anand Rathi (Softsdev) / Hoang Xuan Hao / Marco van Loghum Slaterus ( Dreamfoxmedia )
*/
if ( !defined( 'ABSPATH' ) ) {
    exit;
}

if ( function_exists( 'dfm_pgppfw_fs' ) ) {
    dfm_pgppfw_fs()->set_basename( false, __FILE__ );
} else {
    // DO NOT REMOVE THIS IF, IT IS ESSENTIAL FOR THE `function_exists` CALL ABOVE TO PROPERLY WORK.
    
    if ( !function_exists( 'dfm_pgppfw_fs' ) ) {
        // Create a helper function for easy SDK access.
        function dfm_pgppfw_fs()
        {
            global  $dfm_pgppfw_fs ;
            
            if ( !isset( $dfm_pgppfw_fs ) ) {
                // Activate multisite network integration.
                if ( !defined( 'WP_FS__PRODUCT_4167_MULTISITE' ) ) {
                    define( 'WP_FS__PRODUCT_4167_MULTISITE', true );
                }
                // Include Freemius SDK.
                require_once dirname( __FILE__ ) . '/freemius/start.php';
                $dfm_pgppfw_fs = fs_dynamic_init( array(
                    'id'              => '4167',
                    'slug'            => 'dfm-payment-gateway-per-product-for-woocommerce',
                    'type'            => 'plugin',
                    'public_key'      => 'pk_5a51c11bf6bf5275ffda4baf7fbaa',
                    'is_premium'      => false,
                    'premium_suffix'  => 'Premium',
                    'has_addons'      => false,
                    'has_paid_plans'  => true,
                    'has_affiliation' => 'all',
                    'menu'            => array(
                    'slug'   => 'dfm-pgppfw',
                    'parent' => array(
                    'slug' => 'woocommerce',
                ),
                ),
                    'is_live'         => true,
                ) );
            }
            
            return $dfm_pgppfw_fs;
        }
        
        // Init Freemius.
        dfm_pgppfw_fs();
        // Signal that SDK was initiated.
        do_action( 'dfm_pgppfw_fs_loaded' );
    }

}

/**
* For multi Network
*/
if ( !function_exists( 'is_plugin_active_for_network' ) ) {
    require_once ABSPATH . '/wp-admin/includes/plugin.php';
}
/**
* For multi Network
*/
if ( !function_exists( 'is_plugin_active_for_network' ) || !function_exists( 'is_plugin_active' ) ) {
    require_once ABSPATH . '/wp-admin/includes/plugin.php';
}
/**
* Check is free plugin is installed then we will deactivate free first
*/
if ( is_plugin_active( 'woocommerce-product-payments/woocommerce-payment-gateway-per-product.php' ) ) {
    deactivate_plugins( 'woocommerce-product-payments/woocommerce-payment-gateway-per-product.php' );
}
/**
* Check if WooCommerce is active
*/

if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && !function_exists( 'softsdev_product_payments_settings' ) || is_plugin_active_for_network( 'woocommerce/woocommerce.php' ) ) {
    /* ----------------------------------------------------- */
    if ( isset( $_GET['product-payment-ignore-notice'] ) && $_GET['product-payment-ignore-notice'] == 1 ) {
        update_option( 'product-payment-ignore-notice', '1' );
    }
    function product_payment_ignore_notice()
    {
        if ( isset( $_GET['product-payment-ignore-notice'] ) ) {
            update_option( 'product_payments_alert', 1 );
        }
    }
    
    add_action( 'admin_init', 'product_payment_ignore_notice' );
    // Submenu on woocommerce section
    add_action( 'admin_menu', 'softsdev_product_payments_submenu_page' );
    /* ----------------------------------------------------- */
    add_action( 'admin_enqueue_scripts', 'softsdev_product_payments_enqueue' );
    /* ----------------------------------------------------- */
    function softsdev_product_payments_submenu_page()
    {
        add_submenu_page(
            'woocommerce',
            __( 'Product Payment', 'softsdev' ),
            __( 'Product Payment', 'softsdev' ),
            'manage_options',
            'dfm-pgppfw',
            'softsdev_product_payments_settings'
        );
    }
    
    function softsdev_product_payments_enqueue()
    {
        wp_enqueue_style( 'softsdev_product_payments_enqueue', plugin_dir_url( __FILE__ ) . '/css/style.css' );
    }
    
    /**
     * 
     * @param string $text
     * @return string
     */
    function softsdev_product_payments_footer_text( $text )
    {
        if ( isset( $_GET['page'] ) && strpos( plugin_basename( wp_unslash( $_GET['page'] ) ), 'dfm-pgppfw' ) === 0 ) {
            $text = '<a href="https://www.dreamfoxmedia.com" target="_blank">www.dreamfoxmedia.com</a>';
        }
        return $text;
    }
    
    /**
     * 
     * @param string $text
     * @return string
     */
    function softsdev_product_payments_update_footer( $text )
    {
        if ( isset( $_GET['page'] ) && strpos( plugin_basename( wp_unslash( $_GET['page'] ) ), 'dfm-pgppfw' ) === 0 ) {
            $text = 'Version 1.4.3';
        }
        return $text;
    }
    
    /**
     * Type: updated,error,update-nag
     */
    if ( !function_exists( 'softsdev_notice' ) ) {
        function softsdev_notice( $message, $type )
        {
            $html = <<<EOD
        <div class="{$type} notice">
        <p>{$message}</p>
        </div>
EOD;
            echo  $html ;
        }
    
    }
    /**
     * 
     */
    /**
     * Setting form of product payment
     */
    add_action( 'add_meta_boxes', 'wpp_meta_box_add' );
    /**
     * 
     */
    function wpp_meta_box_add()
    {
        global  $post ;
        if ( isset( $post->ID ) && is_product_eligible( $post->ID ) ) {
            add_meta_box(
                'payments',
                'Payments',
                'wpp_payments_form',
                'product',
                'side',
                'core'
            );
        }
    }
    
    /**
     * 
     * @global type $post
     * @global WC_Payment_Gateways $woo
     */
    function wpp_payments_form()
    {
        global  $post, $woo ;
//        $productIds = get_option( 'woocommerce_product_apply', array() );

//        print_r($productIds);exit;
//        if ( is_array( $productIds ) ) {
//            foreach ( $productIds as $key => $product ) {
//                if ( !get_post( $product ) || get_post_meta( $product, 'payments', true ) && !count( get_post_meta( $product, 'payments', true ) ) ) {
//                    unset( $productIds[$key] );
//                }
//            }
//        }
//        update_option( 'woocommerce_product_apply', $productIds );
        $postPayments = ( get_post_meta( $post->ID, 'payments', true ) ? get_post_meta( $post->ID, 'payments', true ) : array() );
        $woo = new WC_Payment_Gateways();
        //$payments = $woo->get_available_payment_gateways();
        $payments = $woo->payment_gateways;
        foreach ( $payments as $pay ) {
            if ( apply_filters( 'softsdev_show_disabled_gateways', false ) || $pay->enabled === 'no' ) {
                continue;
            }
            $checked = '';
            if ( is_array( $postPayments ) && in_array( $pay->id, $postPayments ) ) {
                $checked = ' checked="yes" ';
            }
            ?>  
        <input type="checkbox" <?php 
            echo  $checked ;
            ?> value="<?php 
            echo  $pay->id ;
            ?>" name="pays[]" id="payment_<?php 
            echo  $pay->id ;
            ?>" />
        <label for="payment_<?php 
            echo  $pay->id ;
            ?>"><?php 
            echo  $pay->title ;
            ?></label>  
        <br />  
        <?php 
        }
    }
    
    add_action(
        'save_post',
        'wpp_meta_box_save',
        10,
        2
    );
    /**
     * 
     * @param type $post_id
     * @param type $post
     * @return type
     */
    function wpp_meta_box_save( $post_id, $post )
    {
        // Restrict to save for autosave
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE || isset( $_REQUEST['action'] ) && sanitize_title( $_REQUEST['action'] ) != 'editpost' ) {
            return $post_id;
        }
        // Restrict to save for revisions
        if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
            return $post_id;
        }
        
        if ( get_post_type() === 'product' && isset( $_POST['pays'] ) ) {
            $productIds = get_option( 'woocommerce_product_apply', array() );
            
            if ( is_array( $productIds ) && !in_array( $post_id, $productIds ) ) {
                $productIds[] = $post_id;
                update_option( 'woocommerce_product_apply', $productIds );
            }
            
            //delete_post_meta($post_id, 'payments');
            $payments = array();
            $post_payments = array_filter( array_map( 'sanitize_title', $_POST['pays'] ) );
            if ( $post_payments ) {
                foreach ( $post_payments as $pay ) {
                    $payments[] = $pay;
                }
            }
            update_post_meta( $post_id, 'payments', $payments );
        } elseif ( get_post_type() === 'product' ) {
            update_post_meta( $post_id, 'payments', array() );
        }
    
    }
    
    /**
     *
     * 
     * 
     * @global type $woocommerce
     * @param type $available_gateways
     * @return type
     */
    function wpppayment_gateway_disable_country( $available_gateways )
    {
        global  $woocommerce ;
        $arrayKeys = array_keys( $available_gateways );
        /**
         * default setting
         */
        $softsdev_wpp_plugin_settings = get_option( 'sdwpp_plugin_settings', array(
            'default_payment' => '',
        ) );
        $default_payment = unserialize( $softsdev_wpp_plugin_settings['default_payment'] );
        $is_default_pay_needed = false;
        /**
         * checking all cart products
         */
        
        if ( is_object( $woocommerce->cart ) ) {
            $items = $woocommerce->cart->cart_contents;
            $itemsPays = '';
            if ( is_array( $items ) ) {
                foreach ( $items as $item ) {
                    if ( !is_product_eligible( $item['product_id'] ) ) {
                        continue;
                    }
                    $itemsPays = get_post_meta( $item['product_id'], 'payments', true );
                    if ( is_array( $itemsPays ) && count( $itemsPays ) ) {
                        foreach ( $arrayKeys as $key ) {
                            if ( array_key_exists( $key, $available_gateways ) && !in_array( $available_gateways[$key]->id, $itemsPays ) ) {
                                
                                if ( $default_payment == $key ) {
                                    $is_default_pay_needed = true;
                                    $default_payment_obj = $available_gateways[$key];
                                    unset( $available_gateways[$key] );
                                } else {
                                    unset( $available_gateways[$key] );
                                }
                            
                            }
                        }
                    }
                }
            }
            /**
             * set default payment if there is none
             */
            if ( $is_default_pay_needed && count( $available_gateways ) == 0 ) {
                $available_gateways[$default_payment] = $default_payment_obj;
            }
        }
        
        return $available_gateways;
    }
    
    add_filter( 'woocommerce_available_payment_gateways', 'wpppayment_gateway_disable_country' );
    function softsdev_product_payments_settings()
    {
        wp_enqueue_style( 'softsdev_select2_css', plugins_url( '/vendor/select2/css/select2.min.css', __FILE__ ) );
        wp_enqueue_script( 'softsdev_select2_js', plugins_url( '/vendor/select2/js/select2.min.js', __FILE__ ) );
        $categories = get_terms( array(
            'taxonomy'   => 'product_cat',
            'hide_empty' => false,
        ) );
        $softsdev_wpp_plugin_settings = get_option( 'sdwpp_plugin_settings', array(
            'softsdev_selected_cats' => '',
        ) );
        $softsdev_selected_cats = unserialize( $softsdev_wpp_plugin_settings['softsdev_selected_cats'] );
        if ( !$softsdev_selected_cats ) {
            $softsdev_selected_cats = array();
        }
        ob_start();
        ?>

<select class="js-softsdev_selected_cats" name="sdwpp_setting[softsdev_selected_cats][]" multiple="multiple" style="width: 100%;">
    <?php 
        foreach ( $categories as $category ) {
            ?>
        <option value="<?php 
            echo  $category->term_id ;
            ?>"<?php 
            echo  ( in_array( $category->term_id, $softsdev_selected_cats ) ? ' selected="selected"' : '' ) ;
            ?>><?php 
            echo  $category->name ;
            ?></option>
    <?php 
        }
        ?>
</select>
<p>You can select any 2 categories for this functionality due to free plugin.</p>

<script>
    (function($) {
        var softsdev = {
            select2: function() {
                $('.js-softsdev_selected_cats').select2({
                    maximumSelectionLength: 2
                });
            },
        }

        $(document).ready(function() {
            for (var func in softsdev) {
                if (softsdev[func] instanceof Function) {
                    softsdev[func]();
                }
            }
        });
    })(jQuery);
</script>
<?php 
        $additional_html = ob_get_clean();
        softsdev_product_payments_settings_part( $additional_html );
    }
    
    add_action( 'init', 'softsdev_product_payments_save_settings' );
    function softsdev_product_payments_save_settings()
    {
        
        if ( isset( $_POST['sdwpp_setting'] ) ) {
            update_option( 'sdwpp_plugin_settings', array_filter( array_map( 'serialize', $_POST['sdwpp_setting'] ) ) );
            softsdev_notice( 'Woocommerce Payment Gateway per Product setting is updated.', 'updated' );
        }
    
    }
    
    function softsdev_product_payments_settings_part( $additional_html = '' )
    {
        wp_register_script( 'dd_horztab_script', plugins_url( '/js/dd_horizontal_tabs.js', __FILE__ ) );
        wp_enqueue_script( 'dd_horztab_script' );
        add_filter( 'admin_footer_text', 'softsdev_product_payments_footer_text' );
        add_filter( 'update_footer', 'softsdev_product_payments_update_footer' );
        echo  '<div class="wrap wrap-mc-paid"><div id="icon-tools" class="icon32"></div>' ;
        echo  '<h2 class="title">' . __( 'Woocommerce Product Payments', 'softsdev' ) . '</h2>' ;
        ?>

<div class="left-dd-paid ">
    <div class="left_box_container">
        <ul class="horz_tabs">
            <li <?php 
        if ( !isset( $_GET['tab'] ) ) {
            ?> class="active"  <?php 
        }
        ?> id="payment_information">
                <a href="javascript:;">Information</a>
            </li>
            <li id="payment_settings" <?php 
        if ( isset( $_GET['tab'] ) && $_GET['tab'] == 'payment_settings' ) {
            ?>class="active"  <?php 
        }
        ?>>
                <a href="javascript:;">Settings</a>
            </li>
            <li id="payment_newsletter" <?php 
        if ( isset( $_GET['tab'] ) && $_GET['tab'] == 'payment_newsletter' ) {
            ?>class="active"  <?php 
        }
        ?>>
                <a href="javascript:;">Newsletter</a>
            </li>
            <li id="payment_faq" >
                <a href="javascript:;">FAQ</a>
            </li>
            <li id="payment_support" <?php 
        if ( isset( $_GET['tab'] ) && $_GET['tab'] == 'payment_support' ) {
            ?>class="active"  <?php 
        }
        ?>>
                <a href="javascript:;">Support</a>
            </li>
            <li id="payment_dfmplugins">
                <a href="javascript:;">DFM Plugins</a>
            </li>
        </ul>
    </div>
</div>

<div class="right-dd-paid ">
  <div id="tab_payment_information" class="postbox <?php 
        if ( !isset( $_GET['tab'] ) ) {
            ?>active<?php 
        }
        ?>" style="padding: 10px; margin: 10px 0px;">
      <?php 
        add_filter( 'admin_footer_text', 'softsdev_product_payments_footer_text' );
        add_filter( 'update_footer', 'softsdev_product_payments_update_footer' );
        echo  '<div class="wrap wrap-mc-paid"><div id="icon-tools" class="icon32"></div></div>' ;
        echo  '<h2 class="title">' . __( 'Woocommerce Product Payments - Information', 'softsdev' ) . '</h2>' ;
        ?>
      <p>This plugin for WooCommerce Payment Gateway per Product lets you select the available payment method for each individual product.<br>
      This plugin will allow the admin to select the available payment gateway for each individual product.<br>
      Admin can select for each individual product the payment gateway that will be used by checkout. If no selection is made, then the default payment gateways are displayed.<br>
      If you for example only select paypal then only paypal will available for that product by checking out.</p>
  </div>

  <div id="tab_payment_settings" class="postbox <?php 
        if ( isset( $_GET['tab'] ) && $_GET['tab'] == 'payment_settings' ) {
            ?>active<?php 
        }
        ?>" style="padding: 10px; margin: 10px 0px;">
    <?php 
        add_filter( 'admin_footer_text', 'softsdev_product_payments_footer_text' );
        add_filter( 'update_footer', 'softsdev_product_payments_update_footer' );
        echo  '<div class="wrap wrap-mc-paid"><div id="icon-tools" class="icon32"></div></div>' ;
        echo  '<h2 class="title">' . __( 'Woocommerce Product Payments - Settings', 'softsdev' ) . '</h2>' ;
        ?>


    <?php 
        /**
         * Settings default
         */
        $softsdev_wpp_plugin_settings = get_option( 'sdwpp_plugin_settings', array(
            'default_payment' => '',
        ) );
        $default_payment = unserialize( $softsdev_wpp_plugin_settings['default_payment'] );
        ?>
    <form id="woo_sdwpp" action="<?php 
        echo  $_SERVER['PHP_SELF'] . '?page=dfm-pgppfw&tab=payment_settings' ;
        ?>" method="post">
        <div style="padding: 10px 0; margin: 10px 0px;">
          <?php 
        echo  $additional_html ;
        ?>
        


        <h3 class="hndle"><?php 
        echo  __( 'Default Payment option( If not match any.)', 'softsdev' ) ;
        ?></h3>
        <?php 
        $woo = new WC_Payment_Gateways();
        $payments = $woo->payment_gateways;
        ?>
        <select id="sdwpp_default_payment" name="sdwpp_setting[default_payment]">
            <option value="none" <?php 
        selected( $default_payment, 'none' );
        ?>>None</option>
            <?php 
        foreach ( $payments as $pay ) {
            /**
             *  skip if payment in disbled from admin
             */
            if ( $pay->enabled === 'no' ) {
                continue;
            }
            echo  "<option value = '" . $pay->id . "' " . selected( $default_payment, $pay->id ) . ">" . $pay->title . "</option>" ;
        }
        ?>
        </select>
        <br />
        <small><?php 
        echo  __( 'If in some case payment option not show then this will default one set', 'softsdev' ) ;
        ?></small>
        </div>
        <input class="button-large button-primary" type="submit" value="Save changes" />
    </form>
  </div>

  <div id="tab_payment_newsletter" class="postbox" style="padding: 10px; margin: 10px 0px;">
    <?php 
        add_filter( 'admin_footer_text', 'softsdev_product_payments_footer_text' );
        add_filter( 'update_footer', 'softsdev_product_payments_update_footer' );
        echo  '<div class="wrap wrap-mc-paid"><div id="icon-tools" class="icon32"></div></div>' ;
        echo  '<h2 class="title">' . __( 'Woocommerce Product Payments - Newsletter', 'softsdev' ) . '</h2>' ;
        ?>
    <!-- Begin Sendinblue Form -->
    <!-- START - We recommend to place the below code in head tag of your website html  -->
    <style>
        @font-face {
        font-display: block;
        font-family: Roboto;
        src: url(https://assets.sendinblue.com/font/Roboto/Latin/normal/normal/7529907e9eaf8ebb5220c5f9850e3811.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/normal/normal/25c678feafdc175a70922a116c9be3e7.woff) format("woff")
        }

        @font-face {
        font-display: fallback;
        font-family: Roboto;
        font-weight: 600;
        src: url(https://assets.sendinblue.com/font/Roboto/Latin/medium/normal/6e9caeeafb1f3491be3e32744bc30440.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/medium/normal/71501f0d8d5aa95960f6475d5487d4c2.woff) format("woff")
        }

        @font-face {
        font-display: fallback;
        font-family: Roboto;
        font-weight: 700;
        src: url(https://assets.sendinblue.com/font/Roboto/Latin/bold/normal/3ef7cf158f310cf752d5ad08cd0e7e60.woff2) format("woff2"), url(https://assets.sendinblue.com/font/Roboto/Latin/bold/normal/ece3a1d82f18b60bcce0211725c476aa.woff) format("woff")
        }

        #sib-container input:-ms-input-placeholder {
        text-align: left;
        font-family: "Helvetica", sans-serif;
        color: #c0ccda;
        border-width: px;
        }

        #sib-container input::placeholder {
        text-align: left;
        font-family: "Helvetica", sans-serif;
        color: #c0ccda;
        border-width: px;
        }
    </style>
    <link rel="stylesheet" href="https://assets.sendinblue.com/component/form/2ef8d8058c0694a305b0.css">
    <link rel="stylesheet" href="https://assets.sendinblue.com/component/clickable/b056d6397f4ba3108595.css">
    <link rel="stylesheet" href="https://assets.sendinblue.com/component/progress-indicator/f86d65a4a9331c5e2851.css">
    <link rel="stylesheet" href="https://sibforms.com/forms/end-form/build/sib-styles.css">
    <!--  END - We recommend to place the above code in head tag of your website html -->

    <!-- START - We recommend to place the below code where you want the form in your website html  -->
    <div class="sib-form" style="text-align: center;
    background-color: #EFF2F7;                                 ">
    <div id="sib-form-container" class="sib-form-container">
    <div id="error-message" class="sib-form-message-panel" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;max-width:540px; border-width:px;">
    <div class="sib-form-message-panel__text sib-form-message-panel__text--center">
    <svg viewBox="0 0 512 512" class="sib-icon sib-notification__icon">
    <path d="M256 40c118.621 0 216 96.075 216 216 0 119.291-96.61 216-216 216-119.244 0-216-96.562-216-216 0-119.203 96.602-216 216-216m0-32C119.043 8 8 119.083 8 256c0 136.997 111.043 248 248 248s248-111.003 248-248C504 119.083 392.957 8 256 8zm-11.49 120h22.979c6.823 0 12.274 5.682 11.99 12.5l-7 168c-.268 6.428-5.556 11.5-11.99 11.5h-8.979c-6.433 0-11.722-5.073-11.99-11.5l-7-168c-.283-6.818 5.167-12.5 11.99-12.5zM256 340c-15.464 0-28 12.536-28 28s12.536 28 28 28 28-12.536 28-28-12.536-28-28-28z"
    />
    </svg>
    <span class="sib-form-message-panel__inner-text">
    Your subscription could not be saved. Please try again.
    </span>
    </div>
    </div>
    <div></div>
    <div id="success-message" class="sib-form-message-panel" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#085229; background-color:#e7faf0; border-radius:3px; border-width:px; border-color:#13ce66;max-width:540px; border-width:px;">
    <div class="sib-form-message-panel__text sib-form-message-panel__text--center">
    <svg viewBox="0 0 512 512" class="sib-icon sib-notification__icon">
    <path d="M256 8C119.033 8 8 119.033 8 256s111.033 248 248 248 248-111.033 248-248S392.967 8 256 8zm0 464c-118.664 0-216-96.055-216-216 0-118.663 96.055-216 216-216 118.664 0 216 96.055 216 216 0 118.663-96.055 216-216 216zm141.63-274.961L217.15 376.071c-4.705 4.667-12.303 4.637-16.97-.068l-85.878-86.572c-4.667-4.705-4.637-12.303.068-16.97l8.52-8.451c4.705-4.667 12.303-4.637 16.97.068l68.976 69.533 163.441-162.13c4.705-4.667 12.303-4.637 16.97.068l8.451 8.52c4.668 4.705 4.637 12.303-.068 16.97z"
    />
    </svg>
    <span class="sib-form-message-panel__inner-text">
    Your subscription has been successful.
    </span>
    </div>
    </div>
    <div></div>
    <div id="sib-container" class="sib-container--large sib-container--vertical" style="text-align:center; background-color:rgba(255,255,255,1); max-width:540px; border-radius:3px; border-width:1px; border-color:#C0CCD9; border-style:solid;">
    <form id="sib-form" method="POST" action="https://sibforms.com/serve/MUIEAIVS9ttzxgUJhY8D9xc7wQ4kTQTq_JTs9RQdWMDiVMzzsObZnVLhVuQWlumv5W8QgJaFhvYGxP7dFAC7T2_TCi4wdiesKfF6ASgQXiy9ajJd9NfoItv4uZ963n6cNvfUYGQbf7eC4xaT9V2KZ0m0N2WJJSLK16Mi-kor1pqNH5z02h7Flv2iybiVI6sBLrQxW058GgqjAy8K"
    data-type="subscription">
    <div style="padding: 16px 0;">
    <div class="sib-form-block" style="font-size:32px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3C4858; background-color:transparent; border-width:px;">
    <p>Newsletter</p>
    </div>
    </div>
    <div style="padding: 16px 0;">
    <div class="sib-form-block" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#3C4858; background-color:transparent; border-width:px;">
    <div class="sib-text-form-block">
    <p>Subscribe to our newsletter and stay updated.</p>
    </div>
    </div>
    </div>
    <div style="padding: 16px 0;">
    <div class="sib-input sib-form-block">
    <div class="form__entry entry_block">
    <div class="form__label-row ">
    <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="FIRSTNAME" data-required="*">
    Enter your FIRSTNAME
    </label>

    <div class="entry__field">
    <input class="input" maxlength="200" type="text" id="FIRSTNAME" name="FIRSTNAME" autocomplete="off" placeholder="FIRSTNAME" data-required="true" required />
    </div>
    </div>

    <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
    </label>
    <label class="entry__specification" style="font-size:12px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#8390A4; border-width:px;">
    Customize this optional help text before publishing your form.
    </label>
    </div>
    </div>
    </div>
    <div style="padding: 16px 0;">
    <div class="sib-input sib-form-block">
    <div class="form__entry entry_block">
    <div class="form__label-row ">
    <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="EMAIL" data-required="*">
    Enter your email address to subscribe
    </label>

    <div class="entry__field">
    <input class="input" type="text" id="EMAIL" name="EMAIL" autocomplete="off" placeholder="EMAIL" data-required="true" required />
    </div>
    </div>

    <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
    </label>
    <label class="entry__specification" style="font-size:12px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#8390A4; border-width:px;">
    Provide your email address to subscribe. For e.g abc@xyz.com
    </label>
    </div>
    </div>
    </div>
    <div style="padding: 16px 0;">
    <div class="sib-radiobutton-group sib-form-block" data-required="true">
    <div class="form__entry entry_mcq">
    <div class="form__label-row ">
    <label class="entry__label" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#3c4858; border-width:px;" for="WC-PGPP-P" data-required="*">
    Support for this plugin:
    </label>
    <div>
    <div class="entry__choice">
    <label>
    <input /type="radio" name="WC-PGPP-P" class="input_replaced" value="1" required>
    <span class="radio-button"></span><span style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#3C4858; background-color:transparent; border-width:px;">WooCommerce Payment Gateway per product Premium</span>                      </label>
    </div>
    </div>
    </div>
    <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
    </label>
    </div>
    </div>
    </div>
    <div style="padding: 16px 0;">
    <div class="sib-optin sib-form-block">
    <div class="form__entry entry_mcq">
    <div class="form__label-row ">
    <div class="entry__choice">
    <label>
    <input type="checkbox" class="input_replaced" value="1" id="OPT_IN" name="OPT_IN" />
    <span class="checkbox checkbox_tick_positive"></span><span style="font-size:14px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#3C4858; background-color:transparent; border-width:px;"><p>I agree to receive your newsletters and to terms and conditions.</p></span>                    </label>
    </div>
    </div>
    <label class="entry__error entry__error--primary" style="font-size:16px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#661d1d; background-color:#ffeded; border-radius:3px; border-width:px; border-color:#ff4949;">
    </label>
    <label class="entry__specification" style="font-size:12px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#8390A4; border-width:px;">
    You may unsubscribe at any time using the link in our newsletter.
    </label>
    </div>
    </div>
    </div>
    <div style="padding: 16px 0;">
    <div class="sib-form__declaration">
    <div class="declaration-block-icon">
    <svg class="icon__SVG" width="0" height="0" version="1.1" xmlns="http://www.w3.org/2000/svg">
    <defs>
    <symbol id="svgIcon-sphere" viewBox="0 0 63 63">
    <path class="path1" d="M31.54 0l1.05 3.06 3.385-.01-2.735 1.897 1.05 3.042-2.748-1.886-2.738 1.886 1.044-3.05-2.745-1.897h3.393zm13.97 3.019L46.555 6.4l3.384.01-2.743 2.101 1.048 3.387-2.752-2.1-2.752 2.1 1.054-3.382-2.745-2.105h3.385zm9.998 10.056l1.039 3.382h3.38l-2.751 2.1 1.05 3.382-2.744-2.091-2.743 2.091 1.054-3.381-2.754-2.1h3.385zM58.58 27.1l1.04 3.372h3.379l-2.752 2.096 1.05 3.387-2.744-2.091-2.75 2.092 1.054-3.387-2.747-2.097h3.376zm-3.076 14.02l1.044 3.364h3.385l-2.743 2.09 1.05 3.392-2.744-2.097-2.743 2.097 1.052-3.377-2.752-2.117 3.385-.01zm-9.985 9.91l1.045 3.364h3.393l-2.752 2.09 1.05 3.393-2.745-2.097-2.743 2.097 1.05-3.383-2.751-2.1 3.384-.01zM31.45 55.01l1.044 3.043 3.393-.008-2.752 1.9L34.19 63l-2.744-1.895-2.748 1.891 1.054-3.05-2.743-1.9h3.384zm-13.934-3.98l1.036 3.364h3.402l-2.752 2.09 1.053 3.393-2.747-2.097-2.752 2.097 1.053-3.382-2.743-2.1 3.384-.01zm-9.981-9.91l1.045 3.364h3.398l-2.748 2.09 1.05 3.392-2.753-2.1-2.752 2.096 1.053-3.382-2.743-2.102 3.384-.009zM4.466 27.1l1.038 3.372H8.88l-2.752 2.097 1.053 3.387-2.743-2.09-2.748 2.09 1.053-3.387L0 30.472h3.385zm3.069-14.025l1.045 3.382h3.395L9.23 18.56l1.05 3.381-2.752-2.09-2.752 2.09 1.053-3.381-2.744-2.1h3.384zm9.99-10.056L18.57 6.4l3.393.01-2.743 2.1 1.05 3.373-2.754-2.092-2.751 2.092 1.053-3.382-2.744-2.1h3.384zm24.938 19.394l-10-4.22a2.48 2.48 0 00-1.921 0l-10 4.22A2.529 2.529 0 0019 24.75c0 10.47 5.964 17.705 11.537 20.057a2.48 2.48 0 001.921 0C36.921 42.924 44 36.421 44 24.75a2.532 2.532 0 00-1.537-2.336zm-2.46 6.023l-9.583 9.705a.83.83 0 01-1.177 0l-5.416-5.485a.855.855 0 010-1.192l1.177-1.192a.83.83 0 011.177 0l3.65 3.697 7.819-7.916a.83.83 0 011.177 0l1.177 1.191a.843.843 0 010 1.192z"
    fill="#0092FF"></path>
    </symbol>
    </defs>
    </svg>
    <svg class="svgIcon-sphere" style="width:63px; height:63px;">
    <use xlink:href="#svgIcon-sphere"></use>
    </svg>
    </div>
    <p style="font-size:14px; text-align:left; font-family:&quot;Helvetica&quot;, sans-serif; color:#687484; background-color:transparent; border-width:px;">
    We use Sendinblue as our marketing platform. By Clicking below to submit this form, you acknowledge that the information you provided will be transferred to Sendinblue for processing in accordance with their
    <a target="_blank" class="clickable_link"
    href="https://www.sendinblue.com/legal/termsofuse/">terms of use</a>
    </p>
    </div>

    </div>
    <div style="padding: 16px 0;">
    <div class="sib-form-block" style="text-align: left">
    <button class="sib-form-block__button sib-form-block__button-with-loader" style="font-size:16px; text-align:left; font-weight:700; font-family:&quot;Helvetica&quot;, sans-serif; color:#FFFFFF; background-color:#3E4857; border-radius:3px; border-width:0px;"
    form="sib-form" type="submit">
    <svg class="icon clickable__icon progress-indicator__icon sib-hide-loader-icon" viewBox="0 0 512 512">
    <path d="M460.116 373.846l-20.823-12.022c-5.541-3.199-7.54-10.159-4.663-15.874 30.137-59.886 28.343-131.652-5.386-189.946-33.641-58.394-94.896-95.833-161.827-99.676C261.028 55.961 256 50.751 256 44.352V20.309c0-6.904 5.808-12.337 12.703-11.982 83.556 4.306 160.163 50.864 202.11 123.677 42.063 72.696 44.079 162.316 6.031 236.832-3.14 6.148-10.75 8.461-16.728 5.01z"
    />
    </svg>
    SUBSCRIBE
    </button>
    </div>
    </div>
    <div style="padding: 16px 0;">
    <div class="sib-form-block" style="font-size:14px; text-align:center; font-family:&quot;Helvetica&quot;, sans-serif; color:#333; background-color:transparent; border-width:px;">
    <div class="sib-text-form-block">
    <p>
    <a href="https://sendinblue.com" target="_blank">Terms &amp; Privacy policy</a>
    </p>
    </div>
    </div>
    </div>

    <input type="text" name="email_address_check" value="" class="input--hidden">
    <input type="hidden" name="locale" value="en">
    </form>
    </div>
    </div>
    </div>
    <!-- END - We recommend to place the below code where you want the form in your website html  -->

    <!-- START - We recommend to place the below code in footer or bottom of your website html  -->
    <script>
    window.REQUIRED_CODE_ERROR_MESSAGE = 'Please choose a country code';

    window.EMAIL_INVALID_MESSAGE = window.SMS_INVALID_MESSAGE = "The information provided is invalid. Please review the field format and try again.";

    window.REQUIRED_ERROR_MESSAGE = "This field cannot be left blank. ";

    window.GENERIC_INVALID_MESSAGE = "The information provided is invalid. Please review the field format and try again.";




    window.translation = {
    common: {
    selectedList: '{quantity} list selected',
    selectedLists: '{quantity} lists selected'
    }
    };

    var AUTOHIDE = Boolean(1);
    </script>
    <script src="https://sibforms.com/forms/end-form/build/main.js">
    </script>
    <script src="https://www.google.com/recaptcha/api.js?hl=en"></script>
    <!-- END - We recommend to place the above code in footer or bottom of your website html  -->
    <!-- End Sendinblue Form -->
  </div>

  <div id="tab_payment_faq" class="postbox" style="padding: 10px; margin: 10px 0px;">
    <?php 
        add_filter( 'admin_footer_text', 'softsdev_product_payments_footer_text' );
        add_filter( 'update_footer', 'softsdev_product_payments_update_footer' );
        echo  '<div class="wrap wrap-mc-paid"><div id="icon-tools" class="icon32"></div></div>' ;
        echo  '<h2 class="title">' . __( 'Woocommerce Product Payments - FAQ', 'softsdev' ) . '</h2>' ;
        ?>
    <h4 class="mc4wp-title"><?php 
        echo  __( 'Looking for help?', 'Woocommerce Payment Gateway Per Product' ) ;
        ?></h4>
    <p>Below you see the link to the complete FAQ available at: <a href="https://www.dreamfoxmedia.com?utm_source=wp-plugin&utm_medium=wcpgpp-p&utm_campaign=faqall" target="_blank">dreamfoxmedia.com</a></p>
    <ul class="ul-square">
    <li><a href="https://www.dreamfoxmedia.com/woocommerce-payment-gateway-per-product?utm_source=wp-plugin&utm_medium=wcpgpp-p&utm_campaign=faqall" target="_blank">Click here to see the complete FAQ section</a></li>
    </ul>

    <p>Below you see the link to the most read FAQs available at: <a href="https://www.dreamfoxmedia.com?utm_source=wp-plugin&utm_medium=wcpgpp-p&utm_campaign=faqall" target="_blank">Dreamfoxmedia.com</a></p>
    <ul class="ul-square">
    <li><a href="https://www.dreamfoxmedia.com/knowledge-base/the-plugin-is-not-working?utm_source=wp-plugin&utm_medium=wcpgpp-p&utm_campaign=faq1" target="_blank">The plugin is not working!</a></li>
    <li><a href="https://www.dreamfoxmedia.com/knowledge-base/i-can-only-select-2-categories?utm_source=wp-plugin&utm_medium=wcpgpp-p&utm_campaign=faq2" target="_blank">I can only select 2 categories.</a></li>
    <li><a href="https://www.dreamfoxmedia.com/knowledge-base/where-can-i-set-different-payments-methods?utm_source=wp-plugin&utm_medium=wcpgpp-p&utm_campaign=faq3" target="_blank">Where can I set different payments methods?</a></li>
    <li><a href="https://www.dreamfoxmedia.com/knowledge-base/what-happens-if-i-add-more-then-one-product-in-the-shoppingcard-with-different-selected-payment-gateways?utm_source=wp-plugin&utm_medium=wcpgpp-p&utm_campaign=faq4" target="_blank">What happens if i add more then one product in the shoppingcard with different selected payment gateways?</a></li>
    <li><a href="https://www.dreamfoxmedia.com/knowledge-base/can-i-use-my-existing-wordpress-theme?utm_source=wp-plugin&utm_medium=wcpgpp-p&utm_campaign=faq4" target="_blank">Can I use my existing WordPress theme?</a></li>
    <li><a href="https://www.dreamfoxmedia.com/knowledge-base/license-is-expired?utm_source=wp-plugin&utm_medium=wcpgpp-p&utm_campaign=faq6" target="_blank">License is expired!</a></li>
    </ul>

    <p><?php 
        echo  sprintf( __( 'If your answer can not be found in the resources listed above, please use our supportsystem <a href="%s">here</a>.' ), 'https://support.dreamfoxmedia.com/index.php?pg=request' ) ;
        ?></p>
    <p>Found a bug? Please open an issue <a href="https://support.dreamfoxmedia.com/index.php?pg=request#utm_source=wp-plugin&utm_medium=wcpgpp-p&utm_campaign=issue" target="_blank">here.</a></p>
  </div>

  <div id="tab_payment_support" class="postbox <?php 
        if ( isset( $_GET['tab'] ) && $_GET['tab'] == 'payment_support' ) {
            ?>active<?php 
        }
        ?>" style="padding: 10px; margin: 10px 0px;">
    <?php 
        add_filter( 'admin_footer_text', 'softsdev_product_payments_footer_text' );
        add_filter( 'update_footer', 'softsdev_product_payments_update_footer' );
        echo  '<div class="wrap wrap-mc-paid"><div id="icon-tools" class="icon32"></div></div>' ;
        echo  '<h2 class="title">' . __( 'Woocommerce Product Payments - Support Form', 'softsdev' ) . '</h2>' ;
        $user = wp_get_current_user();
        $plugin_data = get_plugin_data( __FILE__ );
        ?>
    <div class="supportform" style="display:block; background-color:#FFF; margin:20px; padding:10px;">
    <form action="admin.php?page=dfm-pgppfw&tab=payment_support" id="support_form" method="post" class="mymail-form-2 mymail-form mymail-form-submit extern">

    <table width="100%" class="form-table">
    <tr>
    <th>
    <label><?php 
        echo  __( 'Name', 'dreamfox_dd' ) ;
        ?></label>
    <img width="16" height="16" src="<?php 
        echo  plugins_url( 'img/help.png', __FILE__ ) ;
        ?>" class="help_tip" title="<?php 
        echo  __( 'Name', 'dreamfox_dd' ) ;
        ?>">
    </th>
    <td>
    <input id="name" name="name" type="textbox" size="30" value="<?php 
        echo  $user->data->user_nicename ;
        ?>" />
    </td>
    </tr>


    <tr>
    <th>
    <label><?php 
        echo  __( 'Email', 'dreamfox_dd' ) ;
        ?></label>
    <img width="16" height="16" src="<?php 
        echo  plugins_url( 'img/help.png', __FILE__ ) ;
        ?>" class="help_tip" title="<?php 
        echo  __( 'Email', 'dreamfox_dd' ) ;
        ?>">
    </th>
    <td>
    <input id="email" name="email" type="textbox" size="30" value="<?php 
        echo  $user->data->user_email ;
        ?>" />
    </td>
    </tr>
    <tr>
    <th>
    <label><?php 
        echo  __( 'Plugin Name', 'dreamfox_dd' ) ;
        ?></label>
    <img width="16" height="16" src="<?php 
        echo  plugins_url( 'img/help.png', __FILE__ ) ;
        ?>" class="help_tip" title="<?php 
        echo  __( 'Plugin Name', 'dreamfox_dd' ) ;
        ?>">
    </th>
    <td>
    <input readonly id="plugin_name" name="plugin_name" type="textbox"  size="60" value="Woocommerce Payment Gateway Per Product Premium" />

    </td>
    </tr>
    <tr>
    <th>
    <label><?php 
        echo  __( 'Version Number', 'dreamfox_dd' ) ;
        ?></label>
    <img width="16" height="16" src="<?php 
        echo  plugins_url( 'img/help.png', __FILE__ ) ;
        ?>" class="help_tip" title="<?php 
        echo  __( 'Plugin Name', 'dreamfox_dd' ) ;
        ?>">
    </th>
    <td>
    <input readonly id="version_number" name="version_number" type="textbox"  size="30" value="<?php 
        echo  $plugin_data['Version'] ;
        ?>
    " />

    </td>
    </tr>
    <tr>
    <th>
    <label><?php 
        echo  __( 'License', 'dreamfox_dd' ) ;
        ?></label>
    <img width="16" height="16" src="<?php 
        echo  plugins_url( 'img/help.png', __FILE__ ) ;
        ?>" class="help_tip" title="<?php 
        echo  __( 'License', 'dreamfox_dd' ) ;
        ?>">
    </th>
    <td>
    <input readonly id="license" name="license" type="textbox"  size="30" value="<?php 
        echo  get_option( 'product_payments_license_key' ) ;
        ?>" />

    </td>
    </tr>

    <tr>
    <th>
    <label><?php 
        echo  __( 'Details of Problem', 'dreamfox_dd' ) ;
        ?></label>
    <img width="16" height="16" src="<?php 
        echo  plugins_url( 'img/help.png', __FILE__ ) ;
        ?>" class="help_tip" title="<?php 
        echo  __( 'Details of Problem', 'dreamfox_dd' ) ;
        ?>">
    </th>
    <td>

    <textarea name="detail_problem" id="detail_problem" style="width:300px;height:300px"></textarea>
    <input type="hidden" name="action" value="raise_product_payment_support_email" />
    </td>
    </tr>

    <tr>
    <th>&nbsp;</th>
    <td align="left">
    <button onclick="support_form();" type="button" class="button-large button-primary">Submit Support Ticket</button>
    </td>
    </tr>
    </table>

    </form>
    </div>
  </div>

  <div id="tab_payment_dfmplugins" class="postbox" style="padding: 10px; margin: 10px 0px;">
    <?php 
        add_filter( 'admin_footer_text', 'softsdev_product_payments_footer_text' );
        add_filter( 'update_footer', 'softsdev_product_payments_update_footer' );
        echo  '<div class="wrap wrap-mc-paid"><div id="icon-tools" class="icon32"></div></div>' ;
        echo  '<h2 class="title">' . __( 'Woocommerce Product Payments - Dreamfox Media Plugins', 'softsdev' ) . '</h2>' ;
        ?>
    <?php 
        $url = 'https://raw.githubusercontent.com/dreamfoxmedia/dreamfoxmedia/gh-pages/plugins/dfmplugins.json';
        $response = wp_remote_get( $url, array() );
        $response_code = wp_remote_retrieve_response_code( $response );
        $response_body = wp_remote_retrieve_body( $response );
        
        if ( $response_code != 200 || is_wp_error( $response ) ) {
            echo  '<div class="error below-h2"><p>There was an error retrieving the list from the server.</p></div>' ;
            switch ( $response_code ) {
                case '403':
                    echo  '<div class="error below-h2"><p>Seems your host is blocking <strong>' . dirname( $url ) . '</strong>. Please request to white list this domain </p></div>' ;
                    break;
            }
            wp_die();
        }
        
        $addons = json_decode( $response_body );
        // set_transient( 'mymail_addons', $addons, 3600 );
        $plugin_http_path = plugins_url();
        ?>
    <div class="wrap">
        <h3>Here you see our great Free and Premium Plugins of Dreamfox Media</h3>
        <link href="<?php 
        echo  $plugin_http_path ;
        ?>/woocommerce-delivery-date-premium/css/addons-style.min.css?ver=2.1.23" rel="stylesheet" type="text/css">

        <ul class="addons-wrap">
            <?php 
        foreach ( $addons as $addon ) {
            if ( !empty($addon->hidden) ) {
                continue;
            }
            $addon->link = ( isset( $addon->link ) ? add_query_arg( array(
                'utm_source'   => 'Dreamfox Media Plugin Page',
                'utm_medium'   => 'link',
                'utm_campaign' => 'Dreamfox Plugins Add Ons',
            ), $addon->link ) : '' );
            ?>
                <li class="mymail-addon <?php 
            if ( !empty($addon->is_free) ) {
                echo  ' is-free' ;
            }
            if ( !empty($addon->is_feature) ) {
                echo  ' is-feature' ;
            }
            
            if ( isset( $addon->image ) ) {
                $image = str_replace( 'http//', '//', $addon->image );
            } elseif ( isset( $addon->image_ ) ) {
                $image = str_replace( 'http//', '//', $addon->image_ );
            }
            
            ?>">
                <div class="bgimage" style="background-image:url(<?php 
            echo  $image ;
            ?>)">
                <?php 
            
            if ( isset( $addon->wpslug ) ) {
                ?>
                <a href="plugin-install.php?tab=plugin-information&plugin=<?php 
                echo  dirname( $addon->wpslug ) ;
                ?>&from=import&TB_iframe=true&width=745&height=745" class="thickbox">&nbsp;</a>
                <?php 
            } else {
                ?>
                <a href="<?php 
                echo  $addon->link ;
                ?>">&nbsp;</a>
                <?php 
            }
            
            ?>
                </div>
                <h4><?php 
            echo  $addon->name ;
            ?></h4>
                <p class="author">by
                <?php 
            
            if ( $addon->author_url ) {
                echo  '<a href="' . $addon->author_url . '">' . $addon->author . '</a>' ;
            } else {
                echo  $addon->author ;
            }
            
            ?>
                </p>
                <p class="description"><?php 
            echo  $addon->description ;
            ?></p>
                <div class="action-links">
                <?php 
            
            if ( !empty($addon->wpslug) ) {
                ?>
                <?php 
                
                if ( is_dir( dirname( WP_PLUGIN_DIR . '/' . $addon->wpslug ) ) ) {
                    ?>
                <?php 
                    
                    if ( is_plugin_active( $addon->wpslug ) ) {
                        ?>
                <a class="button" href="<?php 
                        echo  wp_nonce_url( 'plugins.php?action=deactivate&amp;plugin=' . $addon->wpslug, 'deactivate-plugin_' . $addon->wpslug ) ;
                        ?>"><?php 
                        _e( 'Deactivate', 'mymail' );
                        ?></a>
                <?php 
                    } elseif ( is_plugin_inactive( $addon->wpslug ) ) {
                        ?>
                <a class="button" href="<?php 
                        echo  wp_nonce_url( 'plugins.php?action=activate&amp;plugin=' . $addon->wpslug, 'activate-plugin_' . $addon->wpslug ) ;
                        ?>"><?php 
                        _e( 'Activate', 'mymail' );
                        ?></a>
                <?php 
                    }
                    
                    ?>
                <?php 
                } else {
                    ?>
                <?php 
                    
                    if ( current_user_can( 'install_plugins' ) || current_user_can( 'update_plugins' ) ) {
                        ?>
                <a class="button button-primary" href="<?php 
                        echo  wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=' . dirname( $addon->wpslug ) . '&mymail-addon' ), 'install-plugin_' . dirname( $addon->wpslug ) ) ;
                        ?>"><?php 
                        _e( 'Install', 'mymail' );
                        ?></a>
                <?php 
                    }
                    
                    ?>
                <?php 
                }
                
                ?>
                <?php 
            } else {
                ?>
                <a class="button button-primary" href="<?php 
                echo  $addon->link ;
                ?>"><?php 
                _e( 'Purchase', 'mymail' );
                ?></a>
                <?php 
            }
            
            ?>
                </div>
                </li>
            <?php 
        }
        ?>
        </ul>
    </div>
  </div>
</div>



<?php 
    }
    
    function product_payment_support_email()
    {
        global  $dreamfox_dd_version ;
        
        if ( isset( $_POST['action'] ) && $_POST['action'] == 'raise_product_payment_support_email' ) {
            $name = $_POST['name'];
            $email = $_POST['email'];
            $plugin_name = $_POST['plugin_name'];
            $license = $_POST['license'];
            $detail_problem = $_POST['detail_problem'];
            $to_email = 'dfmsup@gmail.com';
            $sep = "\n\n";
            $subject = 'Support Ticket for the Plugin : Woocommerce Payment Gateway Per Product Premium';
            $message = "Hi there," . $sep . "Here are the details of the support ticket : " . $sep . " Name : " . $name . $sep . " Email : " . $email . $sep . " Plugin : " . $plugin_name . $sep . " Version : " . $dreamfox_dd_version . $sep . " License Code : " . $license . $sep . " Details of Problem : " . $sep . $license . $sep . " Thanks," . $sep . "Admin";
            wp_mail( $to_email, $subject, $message );
            softsdev_notice( 'Support email has been sent successfully', 'updated' );
            reset( $_POST );
        }
    
    }
    
    add_action( 'init', 'product_payment_support_email' );
    /**
     * 
     * @param type $product_id
     * @return boolean
     */
    function is_product_eligible( $product_id )
    {
        // Product object
        $product_object = wc_get_product( $product_id );
        if ( !$product_object || $product_object->post_type != 'product' ) {
            return false;
        }
        $softsdev_wpp_plugin_settings = get_option( 'sdwpp_plugin_settings', array(
            'softsdev_selected_cats' => '',
        ) );
        $softsdev_selected_cats = unserialize( $softsdev_wpp_plugin_settings['softsdev_selected_cats'] );
        
        if ( $softsdev_selected_cats ) {
            $is_eligible = false;
            // Get visiblity
            $current_visibility = $product_object->get_catalog_visibility();
            // Get Category Ids
            $cat_ids = wp_get_post_terms( $product_id, 'product_cat', array(
                'fields' => 'ids',
            ) );
            // Convert saved array in to list
            $softsdev_selected_cats = ( is_array( $softsdev_selected_cats ) ? $softsdev_selected_cats : array( $softsdev_selected_cats ) );
            foreach ( $cat_ids as $cat_id ) {
                
                if ( in_array( $cat_id, $softsdev_selected_cats ) ) {
                    $is_eligible = true;
                    break;
                }
            
            }
            // check visiblity in array or now define
            
            if ( $is_eligible && in_array( $current_visibility, array( 'catalog', 'visible' ) ) ) {
                $is_eligible = true;
            } else {
                $is_eligible = false;
            }
            
            // return eligiblity
            return $is_eligible;
        }
        
        return false;
    }

}
