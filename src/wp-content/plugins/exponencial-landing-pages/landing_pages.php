<?php
/* 
Plugin Name: Exponencial Landing Pages
Plugin URI: https://www.exponencialconcursos.com.br/
Description: Plugin para criação de landing pages do portal do Exponencial
Version: 0.1
Author: André Gervásio
Author URI: https://www.exponencialconcursos.com.br/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/


class ExponencialLandingPages
{
	function __construct() {
		add_action('init', array($this, 'init'), 1);
	}
	
	function init() {
	    
	    KLoader::helper("AcessoGrupoHelper");
	    
	    /**
	     * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2588 (item 21)
	     */
	    
		if( is_admin() && AcessoGrupoHelper::wp_admin_landing_pages())
		{
			$user = new WP_User(get_current_user_id());
			$user->add_cap( 'publish_landing_pages' );
			$user->add_cap( 'edit_landing_pages' );
			$user->add_cap( 'edit_others_landing_pages' );
			$user->add_cap( 'delete_landing_pages' );
			$user->add_cap( 'delete_others_landing_pages' );
			$user->add_cap( 'read_private_landing_pages' );
			$user->add_cap( 'edit_landing_page' );
			$user->add_cap( 'delete_landing_page' );
			$user->add_cap( 'read_landing_page' );

			
			register_post_type( 'landing_pages',
				array(
					'labels' => array(
						'name' => 'Landing Pages',
						'singular_name' => 'Landing Page',
						'add_new' => 'Adicionar Nova',
						'add_new_item' => 'Adicionar Nova Landing Page',
						'edit_item' => 'Editar Landing Page',
						'new_item' => 'Nova Landing Page',
						'all_items' => 'Todas as Landing Pages',
						'view_item' => 'Ver Landing Page',
						'search_items' => 'Buscar Landing Page',
						'not_found' =>  'Nenhuma landing page encontrada',
						'not_found_in_trash' => 'Nenhuma landing page encontrada na lixeira',
						'parent_item_colon' => '',
						'menu_name' => 'Landing Page'
					),
					'public' => true,
					'supports' => array( 'title', 'editor', 'revisions' ),
					'rewrite' => array( 'slug' => 'eventos', 'with_front' => FALSE ),
					'capability_type' => 'landing_pages',
					'capabilities' => array(
						'publish_posts' => 'publish_landing_pages',
						'edit_posts' => 'edit_landing_pages',
						'edit_others_posts' => 'edit_others_landing_pages',
						'delete_posts' => 'delete_landing_pages',
						'delete_others_posts' => 'delete_others_landing_pages',
						'read_private_posts' => 'read_private_landing_pages',
						'edit_post' => 'edit_landing_page',
						'delete_post' => 'delete_landing_page',
						'read_post' => 'read_landing_page',
					),
				)
			);
			flush_rewrite_rules();
// 			add_action('admin_menu', array($this,'admin_menu'));
			
		}
	} 

} 

function elp()
{
	global $elp;
	
	if( !isset($elp) )
	{
		$elp = new ExponencialLandingPages();
	}
	
	return $elp;
}


// initialize
elp();
