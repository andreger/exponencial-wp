<?php
/* 
Plugin Name: Exponencial Detalhes de Concursos
Plugin URI: http://www.exponencialconcursos.com.br/
Description: Plugin para detalhar detalhes de um concurso do portal do Exponencial
Version: 0.1
Author: André Gervásio
Author URI: http://www.exponencialconcursos.com.br/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

// add_action('admin_menu', 'test_plugin_setup_menu');
class ExponencialDetalhesConcursos
{

	function __construct() {
		add_action('init', array($this, 'init'), 1);
	}
	
	function init() {
		// admin only
		if( is_admin() )
		{
			$user = new WP_User(get_current_user_id());
			$user->add_cap( 'publish_concursos' );
			$user->add_cap( 'edit_concursos' );
			$user->add_cap( 'edit_others_concursos' );
			$user->add_cap( 'delete_concursos' );
			$user->add_cap( 'delete_others_concursos' );
			$user->add_cap( 'read_private_concursos' );
			$user->add_cap( 'edit_concurso' );
			$user->add_cap( 'delete_concurso' );
			$user->add_cap( 'read_concurso' );

			
			register_post_type( 'concurso',
				array(
					'labels' => array(
						'name' => 'Concursos',
						'singular_name' => 'Concurso',
						'add_new' => 'Adicionar Novo',
						'add_new_item' => 'Adicionar Novo Concurso',
						'edit_item' => 'Editar Concurso',
						'new_item' => 'Novo Concurso',
						'all_items' => 'Todos os Concursos',
						'view_item' => 'Ver Concurso',
						'search_items' => 'Buscar Concurso',
						'not_found' =>  'Nenhum concurso encontrado',
						'not_found_in_trash' => 'Nenhum concurso encontrado na lixeira',
						'parent_item_colon' => '',
						'menu_name' => 'Concursos'
					),
					'public' => true,
					'supports' => array( 'title', 'editor', 'custom-fields', 'thumbnail' ),
					'rewrite' => array( 'slug' => 'concurso', 'with_front' => FALSE ),
					'capability_type' => 'concurso',
					'capabilities' => array(
						'publish_posts' => 'publish_concursos',
						'edit_posts' => 'edit_concursos',
						'edit_others_posts' => 'edit_others_concursos',
						'delete_posts' => 'delete_concursos',
						'delete_others_posts' => 'delete_others_concursos',
						'read_private_posts' => 'read_private_concursos',
						'edit_post' => 'edit_concurso',
						'delete_post' => 'delete_concurso',
						'read_post' => 'read_concurso',
					),
				)
			);
			flush_rewrite_rules();
// 			add_action('admin_menu', array($this,'admin_menu'));
			
		}
	} 

	function admin_menu() {
// 		add_menu_page('Concursos', 'Concursos', 'manage_options', 'edit.php?post_type=film', false, false, '60000');
	}
} 

function edc()
{
	global $edc;
	
	if( !isset($edc) )
	{
		$edc = new ExponencialDetalhesConcursos();
	}
	
	return $edc;
}


// initialize
edc();

function init_concurso_meta_boxes() {
	add_meta_box(
		'concurso_categoria_associada_meta_box_id', // $id
		'Categoria de Produto Associada', // $title
		'concurso_categoria_associada_meta_box', // $callback
		'concurso', // $screen
		'normal', // $context
		'high' // $priority
	);
}
add_action( 'add_meta_boxes', 'init_concurso_meta_boxes' );

function concurso_categoria_associada_meta_box() {
	include_once 'views/categoria_associada_meta_box.php';
}

function salvar_concurso_categoria_associada($post_id)
{   
	$old = get_post_meta( $post_id, 'categoria_associada_id', true );
	$new = $_POST['categoria_associada_id'];

	if ( $new && $new !== $old ) {
		update_post_meta( $post_id, 'categoria_associada_id', $new );
	} elseif ( '' === $new && $old ) {
		delete_post_meta( $post_id, 'categoria_associada_id', $old );
	}
}

add_action( 'save_post_concurso', 'salvar_concurso_categoria_associada' );