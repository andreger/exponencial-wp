<?php 
global $post;  
$meta = get_post_meta( $post->ID, 'your_fields', true ); 

KLoader::model("CategoriaModel");

$categorias = CategoriaModel::listar_filhas(CATEGORIA_CONCURSO);
$categoria_selecionada = get_post_meta( $post->ID, 'categoria_associada_id', true ) ?: null;
?>

<select name="categoria_associada_id">
	<option value="">Selecione uma categoria de produto...</option>
<?php foreach ($categorias as $categoria) : ?>
	<option value="<?= $categoria->term_id ?>" <?= $categoria->term_id == $categoria_selecionada ? " selected=selected " : "" ?>><?= $categoria->name . " (" . $categoria->slug . ")" ?></option>
<?php endforeach ?>
</select>
