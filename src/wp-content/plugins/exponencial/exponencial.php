<?php
/*
Plugin Name: Exponencial
Plugin URI: http://www.exponencialconcursos.com.br/
Description: Plugin de configurações gerais do Exponencial
Version: 0.2
Author: André Gervásio
Author URI: http://www.exponencialconcursos.com.br/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

require_once $_SERVER['DOCUMENT_ROOT'] . '/wp-content/Spout/Autoloader/autoload.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class Exponencial
{

	function __construct()
	{
		$this->plugin = new stdClass;
		$this->plugin->name = 'exponencial'; // Plugin Folder
		$this->plugin->settingsName = 'exponencial';
		$this->plugin->displayName = 'Exponencial'; // Plugin Name
		$this->plugin->version = '0.2';
		$this->plugin->folder = WP_PLUGIN_DIR.'/'.$this->plugin->name; // Full Path to Plugin Folder
		$this->plugin->url = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));

		/**
 		 * @see https://codex.wordpress.org/Plugin_API/Action_Reference/init
 		 */
		add_action('init', array($this, 'init'), 1);
	}

	public function init()
	{
		// Apenas área administrativa
		if( is_admin() )
		{
			wp_enqueue_script('jquery-ui-datepicker');
			wp_enqueue_style('jquery-ui-datepicker', '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css');

			add_action('admin_menu', array($this,'admin_menu'));
		}
	}

	function admin_menu()
	{

		add_menu_page('Exponencial', 'Exponencial', 'edit_exponencial', $this->plugin->name, array(&$this, 'geral'), false, '60000');

		add_submenu_page( $this->plugin->name, 'Afiliados', 'Afiliados', 'update_core', 'afiliados', array(&$this, 'afiliados') );

		add_submenu_page( $this->plugin->name, 'Avisos do Site', 'Avisos do Site', 'edit_exponencial', 'avisos', array(&$this, 'avisos') );

		add_submenu_page( $this->plugin->name, 'Cache - Alunos Coaching', 'Cache - Alunos Coaching', 'update_core', 'cache-alunos-coaching', array(&$this, 'cache_alunos') );

		add_submenu_page( $this->plugin->name, 'Captura Saída', 'Captura Saída', 'edit_exponencial', 'saida', array(&$this, 'saida') );

		add_submenu_page( $this->plugin->name, 'Coaching', 'Coaching', 'update_core', 'exponencial-coaching', array(&$this, 'coaching') );

		add_submenu_page( $this->plugin->name, 'Configurações', 'Configurações', 'update_core', 'exponencial-configuracoes', array(&$this, 'configuracoes') );

		add_submenu_page( $this->plugin->name, 'Coordenadores Áreas', 'Coordenadores Áreas', 'update_core', 'coordenadores-areas', array(&$this, 'coordenadores_areas') );

		add_submenu_page( $this->plugin->name, 'Coordenadores Coaching', 'Coordenadores Coaching', 'update_core', 'coordenadores-coaching', array(&$this, 'coordenadores_coaching') );

		add_submenu_page( $this->plugin->name, 'Coordenadores SQ', 'Coordenadores SQ', 'update_core', 'coordenadores-sq', array(&$this, 'coordenadores_sq') );

		add_submenu_page( $this->plugin->name, 'Cupons Bônus', 'Cupons Bônus', 'update_core', 'cupons-bonus', array(&$this, 'cupons') );

		add_submenu_page( $this->plugin->name, 'Estorno Proporcional', 'Estorno Proporcional', 'edit_exponencial_estorno', 'estorno-proporcional', array(&$this, 'estorno_proporcional') );

		add_submenu_page( $this->plugin->name, 'MailChimp', 'MailChimp', 'update_core', 'mailchimp', array(&$this, 'mailchimp') );

		add_submenu_page( $this->plugin->name, 'Mínimo Acumulado para Pagamento de Vendas', 'Mínimo Acumulado para Pagamento de Vendas', 'update_core', 'minimo-acumulado-vendas', array(&$this, 'minimo_acumulado_vendas') );

		add_submenu_page( $this->plugin->name, 'Mínimo Acumulado para Sistema de Questões', 'Mínimo Acumulado para Sistema de Questões', 'update_core', 'minimo-acumulado-sq', array(&$this, 'minimo_acumulado_sq') );

        add_submenu_page( $this->plugin->name, 'Notas Fiscais', 'Notas Fiscais', 'install_plugins', 'notas-fiscais', array(&$this, 'notas_fiscais') );

		add_submenu_page( $this->plugin->name, 'Páginas', 'Páginas', 'update_core', 'paginas', array(&$this, 'paginas') );

		add_submenu_page( $this->plugin->name, 'Painel de Controle', 'Painel de Controle', 'update_core', 'painel_controle', array(&$this, 'painel_controle') );

		add_submenu_page( $this->plugin->name, 'Parceiros', 'Parceiros', 'update_core', 'parceiros', array(&$this, 'parceiros') );

		add_submenu_page( $this->plugin->name, 'Planilhas', 'Planilhas', 'update_core', 'exponencial-planilhas', array(&$this, 'planilhas') );

		add_submenu_page( $this->plugin->name, 'Produtos Coaching', 'Produtos Coaching', 'update_core', 'produtos_coaching', array(&$this, 'produtos_coaching') );

		add_submenu_page( $this->plugin->name, 'Produtos Premium - Configurações', 'Produtos Premium - Configurações', 'update_core', 'produtos_premium', array(&$this, 'produtos_premium') );

		add_submenu_page( $this->plugin->name, 'Relatório de Vendas por Concurso - Configurações', 'Relatório de Vendas por Concurso - Configurações', 'update_core', 'config_relatorio_vendas_concurso', array(&$this, 'config_relatorio_vendas_concurso') );

		add_submenu_page( $this->plugin->name, 'Reprocessar Acumulados Premium', 'Reprocessar Acumulados Premium', 'update_core', 'reprocessar_acumulados_premium', array(&$this, 'reprocessar_acumulados_premium') );
		
		add_submenu_page( $this->plugin->name, 'Reprocessar Produtos Premium', 'Reprocessar Produtos Premium', 'update_core', 'reprocessar_produtos_premium', array(&$this, 'reprocessar_produtos_premium') );

		add_submenu_page( $this->plugin->name, 'Reprocessar Relatório de Vendas', 'Reprocessar Relatório de Vendas', 'edit_exponencial_reprocessar_vendas', 'exponencial-reprocessar', array(&$this, 'reprocessar') );

		add_submenu_page( $this->plugin->name, 'Rodapé', 'Rodapé', 'update_core', 'rodape', array(&$this, 'rodape') );

		add_submenu_page( $this->plugin->name, 'SEO', 'SEO', 'update_core', 'seo', array(&$this, 'seo') );

		add_submenu_page( $this->plugin->name, 'Sistema de Questões', 'Sistema de Questões', 'edit_exponencial_sq', 'sq', array(&$this, 'sq') );


		// Páginas que não são exibidas no menu


		add_submenu_page( null, null, null, 'update_core', 'coordenadores-areas-historico', array(&$this, 'coordenadores_areas_historico') );

		add_submenu_page( null, null, null, 'update_core', 'coordenadores-sq-historico', array(&$this, 'coordenadores_sq_historico') );

		add_submenu_page( null, null, null, 'update_core', 'coordenadores-coaching-historico', array(&$this, 'coordenadores_coaching_historico') );

		add_submenu_page( null, null, null, 'edit_exponencial', 'editar_saida', array(&$this, 'editar_saida') );

		add_submenu_page( null, null, null, 'edit_exponencial', 'editar_aviso', array(&$this, 'editar_aviso') );

		add_submenu_page( null, null, null, 'update_core', 'afiliados-relatorio', array(&$this, 'afiliados_relatorio') );

		// add_submenu_page( null, null, null, 'update_core', 'folha-dirigida-historico', array(&$this, 'folha_dirigida_historico') );

		add_submenu_page( null, null, null, 'edit_exponencial_estorno', 'estorno-proporcional-novo', array(&$this, 'estorno_proporcional_novo') );

		add_submenu_page( null, null, null, 'edit_exponencial_estorno', 'estorno-proporcional-pedido', array(&$this, 'estorno_proporcional_pedido') );

		add_submenu_page( null, null, null, 'update_core', 'editar_cronjob', array(&$this, 'editar_cronjob') );
	}

	function geral()
	{
		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/geral.php');
	}

	function configuracoes()
	{
		if(isset($_POST['submit'])) {
			salvar_valor_imposto($_POST['taxa'], $_POST['data_inicio']);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/configuracoes.php');
	}

	function coaching()
	{
		if(isset($_POST['submit'])) {
			$slug = $_POST['slug'];
			$tipo = $_POST['tipo'];

			if(empty($slug) || empty($tipo)) {
				$message = "Slug e tipo são obrigatórios.";
				$type = "error";
			}
			else {
				salvar_item_coaching($slug, $tipo);
				$message = "Item cadastrado com sucesso.";
				$type = "updated";
			}
		}

		if(isset($_GET['remove'])) {
			excluir_item_coaching($_GET['remove']);
			$message = "Item removido com sucesso.";
			$type = "updated";
		}

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/coaching.php');
	}

	function planilhas()
	{
		if(isset($_POST['submit-avaliacao'])) {

			$file_avaliacao = $_FILES['avaliacao'];
			$uploaded_avaliacao = $_SERVER['DOCUMENT_ROOT'] . get_avaliacao_estudos_url();

			if($file_avaliacao[name]) {
				if (move_uploaded_file($file_avaliacao['tmp_name'], $uploaded_avaliacao)) {
					$message = "Planilha atualizada com sucesso.";
					$type = "updated";
				}
				else {
					$message = "Ocorreu um erro ao enviar a planilha";
					$type = "error";
				}
			}
		}

		if(isset($_POST['submit-disponibilidade'])) {

			$file_disponibilidade = $_FILES['disponibilidade'];
			$uploaded_disponibilidade = $_SERVER['DOCUMENT_ROOT'] . get_disponibilidade_aluno_url();

			if($file_disponibilidade[name]) {
				if (move_uploaded_file($file_disponibilidade['tmp_name'], $uploaded_disponibilidade)) {
					$message = "Planilha atualizada com sucesso.";
					$type = "updated";
				}
				else {
					$message = "Ocorreu um erro ao enviar a planilha";
					$type = "error";
				}
			}
		}

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/planilhas.php');
	}

	/**********************************************************************
	 * Cupons Bônus
	 **********************************************************************/

	function cupons()
	{
		if(isset($_POST['submit'])) {
			$cupom = $_POST['cupom'];
			$item = $_POST['item'];
			$tipo = $_POST['tipo'];

			if(!$cupom) {
				$message = "É necessário definir um cupom.";
				$type = "error";
			}

			elseif(!$item) {
				$message = "É necessário escolher um item.";
				$type = "error";
			}

			elseif(!$tipo) {
				$message = "É necessário definir um tipo.";
				$type = "error";
			}

			else {
				if(isset($_POST['cupom_id'])) {
					atualizar_cupom_bonus($_POST['cupom_id'], $cupom, $item, $tipo);
				}
				else {
					incluir_cupom_bonus($cupom, $item, $tipo);
				}
				$cupom = null;
			}
		}

		if(isset($_GET['remove'])) {
			excluir_cupom_bonus($_GET['remove']);
			$message = "Item removido com sucesso.";
			$type = "updated";
		}

		if(isset($_GET['edit'])) {
			$cupom = get_cupom_bonus($_GET['edit']);
		}

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/cupons.php');
	}

	/**********************************************************************
	 * Reprocessar relatório de vendas
	 **********************************************************************/

	function reprocessar()
	{
		if(isset($_POST['submit-periodo'])) {
			$data_inicio = $_POST['data_inicio'];
			$data_fim = $_POST['data_fim'];

			excluir_vendas_por_periodo($data_inicio, $data_fim);
// 			processar_pedidos_nao_processados();

			$message = "Reprocessamento agendado com sucesso! O servidor levará algum tempo até que todos os dados sejam processados.";
			$type = "updated";
		}

		if(isset($_POST['submit-pedido'])) {
            try {
                $input = trim($_POST['pedido_id']);

                $pedidos_ids = explode(',', $input);

                if ($pedidos_ids) {

                    foreach ($pedidos_ids as $pedido_id) {

                        if(!filter_var($pedido_id, FILTER_VALIDATE_INT)) {
                            $message = "Número do pedido inválido: {$pedido_id}";
                            $type = "error";
                            break;
                        }
                        else {
                            excluir_vendas_por_pedido($pedido_id);
                            processar_pedido($pedido_id, true);

                            $message = "Pedido processado com sucesso!";
                            $type = "updated";
                        }
                    }

                }
            }
            catch(Exception $e) {
                $message = "Pedido inexistente";
                $type = "error";
            }
		}

		if(isset($_POST['submit-acumulado'])) {

		    if(!$_POST['data_ref']) {
		        $message = "Data a partir de é obrigatória";
		        $type = "error";
		    }

		    else {
		        $data_ref = converter_para_yyyymmdd($_POST['data_ref']);

		        KLoader::model("RelatorioVendasPorProfessorModel");
		        RelatorioVendasPorProfessorModel::calcular_acumulados($data_ref);

		        $message = "Reprocessamento realizado com sucesso!";
		        $type = "updated";
		    }
		}

		if(isset($message)) {
			add_settings_error('reprocessar-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/reprocessar.php');
	}

	/**********************************************************************
	 * Coordenadores SQ
	 **********************************************************************/
	function coordenadores_sq()
	{

		if(isset($_POST['submit'])) {
			$user_id = $_POST['user_id'];
			$percentual = $_POST['percentual'];

			if(!$user_id) {
				$message = "É necessário escolher um professor.";
				$type = "error";
			}

			elseif(!$percentual) {
				$message = "É necessário definir um percentual de participação.";
				$type = "error";
			}

			else {
				salvar_percentual_coordenador_sq($user_id, $percentual);
				$message = "Dados do coordenador salvos com sucesso.";
				$type = "updated";
			}

		}

		if($remover_id = $_GET['remover']) {
			remover_coordenador_sq($remover_id);
			$message = "Coordenador removido com sucesso.";
			$type = "updated";
		}

		if(isset($message)) {
			add_settings_error('coordenadores-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/coordenadores_sq.php');
	}

	function coordenadores_sq_historico()
	{
		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/coordenadores_sq_historico.php');
	}

	/**********************************************************************
	 * Coordenadores Areas
	 **********************************************************************/
	function coordenadores_areas()
	{

		if(isset($_POST['submit'])) {
			$user_id = $_POST['user_id'];
			$area = $_POST['area'];
			$percentual = $_POST['percentual'];

			if(!$user_id) {
				$message = "É necessário escolher um professor.";
				$type = "error";
			}

			elseif(!$area) {
				$message = "É necessário escolher uma área.";
				$type = "error";
			}

			elseif(!$percentual) {
				$message = "É necessário definir um percentual de participação.";
				$type = "error";
			}

			else {
				salvar_percentual_coordenador_area($user_id, $area, $percentual);
				$message = "Dados do coordenador salvos com sucesso.";
				$type = "updated";
			}

		}

		if($remover_id = $_GET['remover']) {
			$area_id = $_GET['area_id'];

			remover_coordenador_area($remover_id, $area_id);
			$message = "Coordenador removido com sucesso.";
			$type = "updated";
		}

		if(isset($message)) {
			add_settings_error('coordenadores-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/coordenadores_areas.php');
	}

	function coordenadores_areas_historico()
	{
		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/coordenadores_areas_historico.php');
	}

	/**********************************************************************
	 * Coordenadores Coaching
	 **********************************************************************/
	function coordenadores_coaching()
	{

		if(isset($_POST['submit'])) {
			$user_id = $_POST['user_id'];
			$percentual = $_POST['percentual'];

			if(!$user_id) {
				$message = "É necessário escolher um professor.";
				$type = "error";
			}

			elseif(!$percentual) {
				$message = "É necessário definir um percentual de participação.";
				$type = "error";
			}

			else {
				salvar_coordenador_coaching($user_id, $percentual);
				$message = "Dados do coordenador salvos com sucesso.";
				$type = "updated";
			}

		}

		if($remover_id = $_GET['remover']) {
			remover_coordenador_coaching($remover_id);
			$message = "Coordenador removido com sucesso.";
			$type = "updated";
		}

		if(isset($message)) {
			add_settings_error('coordenadores-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/coordenadores_coaching.php');
	}

	function coordenadores_coaching_historico()
	{
		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/coordenadores_coaching_historico.php');
	}

	/**********************************************************************
	 * Minimo Acumulado de Vendas
	 **********************************************************************/
	function minimo_acumulado_vendas()
	{

		if(isset($_POST['submit'])) {
			$valor = $_POST['valor'];

			if(!$valor) {
				$message = "É necessário escolher um valor.";
				$type = "error";
			}

			else {
				$data = date('Y-m-01', strtotime(date('Y-m-01') . ' +1 month'));

				salvar_minimo_valor_acumulado($data, $valor);
				$message = "Dados salvos com sucesso.";
				$type = "updated";
			}

		}

		if(isset($message)) {
			add_settings_error('coordenadores-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/minimo_acumulado_vendas.php');
	}

	/**********************************************************************
	 * Minimo Acumulado do Sistema de Questões
	 **********************************************************************/
	function minimo_acumulado_sq()
	{

		if(isset($_POST['submit'])) {
			$valor = $_POST['valor'];

			if(!$valor) {
				$message = "É necessário escolher um valor.";
				$type = "error";
			}

			else {
				$data = date('Y-m-01', strtotime(date('Y-m-01') . ' +1 month'));

				salvar_minimo_valor_acumulado($data, $valor, VMA_SQ);
				$message = "Dados salvos com sucesso.";
				$type = "updated";
			}

		}

		if(isset($message)) {
			add_settings_error('coordenadores-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/minimo_acumulado_sq.php');
	}


	/**********************************************************************
	 * Parceiros
	 **********************************************************************/

	function parceiros()
	{
		if(isset($_POST['submit'])) {
			$parceiro_id = $_POST['parceiro_id'];
			$cupom_id = $_POST['cupom_id'];
			$nivel = $_POST['nivel'];

			if(!$parceiro_id) {
				$message = "É necessário escolher um parceiro.";
				$type = "error";
			}

			elseif(!$cupom_id) {
				$message = "É necessário escolher um cupom.";
				$type = "error";
			}

			else {
				salvar_parceiro_cupom($parceiro_id, $cupom_id, $nivel);
				$message = "Associação parceiro/cupom criada com sucesso.";
				$type = "updated";
			}

		}

		if(isset($_GET['remove_p'])) {
			excluir_parceiro_cupom($_GET['remove_p'], $_GET['remove_c']);
			$message = "Item removido com sucesso.";
			$type = "updated";
		}

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		$parceiros = get_parceiros();

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/parceiros.php');
	}

	/**********************************************************************
	 * Rodapé
	 **********************************************************************/

	function rodape()
	{
		KLoader::model('RodapeItemModel');
		KLoader::api('RodapeLinkApi');

		if(isset($_POST['submit'])) {
			$titulo = $_POST['titulo'];
			$url = $_POST['url'];
			$coluna = $_POST['coluna'];

			if(!$titulo || !$url || !$coluna) {
				$message = "É necessário preencher título, url e coluna";
				$type = "error";
			}

			else {
				RodapeItemModel::salvar($titulo, $url, $coluna);
				$message = "Item Rodapé cadastrado com sucesso.";
				$type = "updated";

                RodapeLinkApi::sincronizar();
			}

		}

		if(isset($_GET['remove_roi'])) {
			RodapeItemModel::excluir($_GET['remove_roi']);
			$message = "Item de rodapé removido com sucesso.";
			$type = "updated";

            RodapeLinkApi::sincronizar();
		}

		if(isset($_POST['editar'])) {
			$id = $_POST['roi_id'];
			$titulo = $_POST['roi_titulo'];
			$url = $_POST['roi_url'];
			$idnex = $_POST['roi_index'];

			if(!$titulo || !$url) {
				$message = "É necessário preencher título e url.";
				$type = "error";
			}

			else {
				RodapeItemModel::editar($id, $titulo, $url, $idnex);
				$message = "Item de rodapé editado com sucesso.";
				$type = "updated";

				RodapeLinkApi::sincronizar();
			}

		}
		if(isset($_POST['ordenar'])) {
// 			var_dump($_POST['roi_itens']);

			$itens = json_decode(stripslashes($_POST['roi_itens']), true);
// 			var_dump($itens);
			foreach($itens as $item) {
				RodapeItemModel::editar($item['roi_id'], $item['roi_titulo'], $item['roi_url'], $item['roi_index']);
			}
			$message = "Ites de rodapé reordenado com sucesso.";
			$type = "updated";

            RodapeLinkApi::sincronizar();
// 			header('Location: '.$_SERVER['REQUEST_URI']);

		}

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		$rodapes = array();
		$rodapes[] = RodapeItemModel::list(1);
		$rodapes[] = RodapeItemModel::list(2);
		$rodapes[] = RodapeItemModel::list(3);


		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/rodape.php');
	}

	/**********************************************************************
	 * Captura Saída
	 **********************************************************************/

	function saida()
	{
	    KLoader::model('CapturaSaidaModel');
        KLoader::api('SaidaApi');

		if(isset($_POST['submit'])) {

			$html = trim(preg_replace('/\s+/', ' ', nl2br($_POST['html'])));
			$urls = trim($_POST['urls']);
			$tags = $_POST['tags'] ? trim($_POST['tags']) : '';
			$id = trim($_POST['id']);
			$prioridade = trim($_POST['prioridade']);
			$link = trim($_POST['link']);
			$titulo = trim($_POST['titulo']);
			$fundo = trim($_POST['fundo']);
			$data_inicio = trim($_POST['data_inicio']);
			$data_fim = trim($_POST['data_fim']);
			$urls_negativas = trim($_POST['urls_negativas']);
			$palavras_negativas = trim($_POST['palavras_negativas']);
			$dias = $_POST['dias'] ? trim($_POST['dias']): 0;

			$message = "";
			if(!$titulo){
				$message .= "É necessário informar o título da saída.<br/>";
				$type = "error";
			}

			if(!$html) {
				$message .= "É necessário informar a mensagem de saída.<br/>";
				$type = "error";
			}

			if(!$data_inicio){
				$message .= "É necessário informar a data de início da saída.<br/>";
				$type = "error";
			}

			if(!$data_fim){
				$message .= "É necessário informar a data fim da saída.<br/>";
				$type = "error";
			}

			if($dias && !filter_var($dias, FILTER_VALIDATE_INT)) {
				$message = "Reexibição da saída precisa ser um número inteiro.<br/>";
				$type = "error";
			}

			//Se não tem erros
			if(!$message){

			    $dados = [
			        'sai_id' => $id ?: null,
			        'sai_html' => $html,
			        'sai_urls' => $urls,
			        'sai_prioridade' => $prioridade,
			        'sai_tags' => $tags,
			        'sai_link' => $link,
			        'sai_titulo' => $titulo,
			        'sai_data_inicio' => converter_para_yyyymmdd($data_inicio) . " 00:00:00",
			        'sai_data_fim' => converter_para_yyyymmdd($data_fim) . " 23:59:59",
			        'sai_fundo' => $fundo,
			        'sai_urls_negativas' => $urls_negativas,
			        'sai_palavras_negativas' => $palavras_negativas,
			        'sai_dias_expiracao' => $dias
			    ];

			    CapturaSaidaModel::salvar($dados);
                SaidaApi::salvar($dados);

				if($id) {
				    $message = "Captura de saída atualizada com sucesso.";
					$type = "updated";
				}
				else {
					$message = "Captura de saída criada com sucesso.";
					$type = "updated";
				}
			}

		}

		if(isset($_GET['remove'])) {
		    CapturaSaidaModel::excluir($_GET['remove']);
            SaidaApi::excluir($_GET['remove']);

			$message = "Item removido com sucesso.";
			$type = "updated";
		}

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		$saidas = CapturaSaidaModel::listar_todas();

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/saida.php');
	}

	function editar_saida()
	{
	    KLoader::model('CapturaSaidaModel');

		if(isset($_GET['id'])) {
		    $saida = CapturaSaidaModel::get_by_id($_GET['id']);
		}

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		wp_enqueue_style( 'wp-color-picker' );
        wp_enqueue_script( 'custom-script-handle', plugins_url( 'custom-script.js', __FILE__ ), array( 'wp-color-picker' ), false, true );

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/editar_saida.php');
	}

	/**
	 * Controla a relação entre a turma de Coaching e o produto do WP para pagamento
	 *
	 * [JOULE][J1] UPGRADE - Cadastro automático de pagamentos do coaching no Podio
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/upgrades/items/193
	 *
	 * @since 10.0.0
	 */

	function produtos_coaching()
	{
		// Fluxo ao pressionar botão salvar
		if(isset($_POST['submit'])) {
			$produto_id = $_POST['produto_id'];
			$area_id = $_POST['area_id'];

			// produto é obrigatório
			if(!$produto_id) {
				$message = "É necessário escolher um produto.";
				$type = "error";
			}

			// área é obrigatória
			elseif(!$area_id) {
				$message = "É necessário definir uma área.";
				$type = "error";
			}

			// verifica se relação já existe
			elseif(existe_coaching_produto($produto_id)) {
				$message = "Este produto de coaching já foi cadastrado.";
				$type = "error";
			}

			// salva produto área
			else {
				salvar_produto_area($produto_id, $area_id);
				$message = "Produto do coaching salvo com sucesso.";
				$type = "updated";
			}

		}

		// Fluxo para remoção de produto área
		if(($produto_id = $_GET['r_produto_id']) && ($area_id = $_GET['r_area_id'])) {
			remover_produto_area($produto_id, $area_id);
			$message = "Produto do coaching removido com sucesso.";
			$type = "updated";
		}

		// Exibição de mensagens
		if(isset($message)) {
			add_settings_error('pagina-error', '', $message, $type);
		}

		// carrega a view
		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/produtos_coaching.php');
	}

	/**
	 * Exibe avisos estilo "toast" na homepage
	 *
	 * @see https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/318
	 * @see http://codeseven.github.io/toastr/
	 *
	 * @since Joule 2
	 */

	function avisos()
	{
	    KLoader::model("AvisoModel");
	    KLoader::api("AvisoApi");

		if(isset($_POST['submit'])) {
		    $id = $_POST['id'] ?: null;
			$titulo = trim($_POST['titulo']);
			$mensagem = trim(preg_replace('/\s+/', ' ', nl2br($_POST['mensagem'])));
			$data_inicio = trim($_POST['data_inicio']);
			$data_fim = trim($_POST['data_fim']);
			$urls = trim($_POST['urls']);
			$urls_negativas = trim($_POST['urls_negativas']);
			$palavras = trim($_POST['palavras']);
			$palavras_negativas = trim($_POST['palavras_negativas']);
			$link = trim($_POST['link']);
			$fundo = trim($_POST['fundo']);
			$dias = $_POST['dias'] ? trim($_POST['dias']) : 0;

			if(!$titulo) {
				$message = "É necessário definir o título.";
				$type = "error";
			}

			elseif(!$mensagem) {
				$message = "É necessário definir a mensagem.";
				$type = "error";
			}

			elseif(!$data_inicio) {
				$message = "É necessário definir a data do início.";
				$type = "error";
			}

			elseif(!$data_fim) {
				$message = "É necessário definir a data do fim.";
				$type = "error";
			}

			if($dias && !filter_var($dias, FILTER_VALIDATE_INT)) {
			    $message = "Tempo para reabertura precisa ser um número inteiro.<br/>";
			    $type = "error";
			}

			else {

			    $dados = [
			        'id' => $id,
			        'titulo' => $titulo,
			        'mensagem' => $mensagem,
			        'data_inicio' => converter_para_yyyymmdd($data_inicio) . " 00:00:00",
			        'data_fim' => converter_para_yyyymmdd($data_fim) . " 23:59:59",
			        'urls' => $urls,
			        'palavras' => $palavras,
			        'link' => $link,
			        'fundo' => $fundo,
			        'palavras_negativas' => $palavras_negativas,
			        'dias_expiracao' => $dias,
			        'urls_negativas' => $urls_negativas,
			    ];

			    AvisoModel::salvar($dados);
                AvisoApi::salvar($dados);

				if($id) {
					$message = "Aviso atualizado com sucesso.";
					$type = "updated";
				}
				else {
					$message = "Aviso inserido com sucesso.";
					$type = "updated";
				}

			}
		}

		if(isset($_GET['remove'])) {
		    AvisoModel::excluir($_GET['remove']);
            AvisoApi::excluir($_GET['remove']);

			$message = "Item removido com sucesso.";
			$type = "updated";
		}

		$avisos = AvisoModel::listar_todos();

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/avisos.php');
	}

	function editar_aviso()
	{
	    KLoader::model('AvisoModel');

	    if(isset($_GET['id'])) {
	        $aviso = AvisoModel::get_by_id($_GET['id']);
	    }

	    if(isset($message)) {
	        add_settings_error('coaching-error', '', $message, $type);
	    }

	    wp_enqueue_style( 'wp-color-picker' );
	    wp_enqueue_script( 'custom-script-handle', plugins_url( 'custom-script.js', __FILE__ ), array( 'wp-color-picker' ), false, true );

	    include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/editar_aviso.php');
	}

	function cache_alunos(){

		if(isset($_GET['action']) && "update" == $_GET['action']) {
			$alunos_podio = get_alunos_ativos_podio(NULL, TRUE);
			$alunos_coaching = get_alunos_ativos_turma_coaching(TRUE);
			$message = "Cache de Alunos atualizado com sucesso.";
			$type = "updated";
		}else{
			$alunos_podio = get_alunos_ativos_podio();
			$alunos_coaching = get_alunos_ativos_turma_coaching();
		}

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/cache_alunos.php');
	}

	/**
	 * Exibe avisos estilo "toast" na homepage
	 *
	 * @see https://podio.com/profinfnetedubr/lslc/apps/upgrades/items/318
	 * @see http://codeseven.github.io/toastr/
	 *
	 * @since Joule 2
	 */

	function afiliados()
	{

        /**
         * Código comentado conforme solicitação no tícket abaixo
         * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2431
        */
		/*if(isset($_POST['submit_percentual'])) {
			$percentual = trim($_POST['percentual']);

			if(!$percentual) {
				$message = "É necessário definir o percentual.";
				$type = "error";
			}
			else {
				folha_dirigida_salvar_percentual($percentual);
				$message = "Percentual atualizado com sucesso.";
				$type = "updated";
			}
		}*/
		/*
		if(isset($_POST['submit_csv'])) {
			$arquivo = $_SERVER['DOCUMENT_ROOT'] . '/wp-content/temp/folhadirigida.csv';
			move_uploaded_file($_FILES['csv']['tmp_name'], $arquivo);

			$handle = fopen($arquivo, "r");
			if ($handle) {

				$tabela = "<table>";

			    while (($line = fgets($handle)) !== false) {

			    	$tabela .= "<tr>";
			    	$tabela .= "<td>{$line}</td>";

			    	$order = new WC_Order($line);
			    	if($order) {
						if($order->status == 'completed') {
							$tabela .= "<td>Liberado</td>";

						}
						else {
							$tabela .= "<td>Não Liberado</td>";
						}

					}
					else {
						$tabela .= "<td>Não encontrado</td>";
					}

					$tabela .= "</tr>";
			    }

			    $tabela .= "</table>";

			    fclose($handle);
			} else {
			    $message = "Arquivo inválido";
				$type = "error";
			}

		}

		$percentual = ($p = folha_dirigida_atual()) ? $p->percentual : null;

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}
		*/
		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/afiliados.php');
	}

	/*
	function folha_dirigida_historico()
	{
		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/folha_dirigida_historico.php');
	}
	*/

	function afiliados_relatorio()
	{
		if(isset($_POST['submit_csv'])) {

			$arquivo = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/temp/afiliados.csv";
			move_uploaded_file($_FILES['csv']['tmp_name'], $arquivo);
			$handle = fopen($arquivo, "r");

			if ($handle) {

				$arquivo_xls = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/temp/afiliados.xls";
				$writer = WriterFactory::create(Type::XLSX);
				$writer->openToFile($arquivo_xls);

				$tabela = "<table class='wp-list-table widefat fixed striped'>";

				$linha = ['ID do Pedido', 'Percentual', 'Status', 'Data', 'Aluno'];
				$tabela .= get_tabela_thead($linha);
				$writer->addRow($linha);

				$pedidos_percentual = [];

			    while (($line = fgets($handle)) !== false) {
			    	$line_a = explode(";", $line);

			    	$key = trim($line_a[0]);
			    	$value = $line_a[1] ? trim($line_a[1]) : 0;

			    	if(isset($pedidos_percentual[$key])) {
			    		$pedidos_percentual[$key] += $value;
			    	}
			    	else {
			    		$pedidos_percentual[$key] = $value;
			    	}
			    }

			    foreach ($pedidos_percentual as $pedido_id => $percentual) {

			   		$linha = [];
			    	array_push($linha, trim($pedido_id));
			    	array_push($linha, trim($percentual));

			    	try {
			    		$order = new WC_Order((int)$pedido_id);

						if($order->status == 'completed') {
							array_push($linha, 'Liberado');

							/* Meta antigo: ref_folha_dirigida */
							update_post_meta($order->get_id(), AFILIADOS_PERCENTUAL_META, $percentual);

							excluir_vendas_por_pedido($order->get_id());
							// processar_pedidos_nao_processados();
						}
						else {
							array_push($linha, 'Não Liberado');
						}

						$aluno = get_usuario_array($order->get_user_id());

						array_push($linha, $order->get_date_created()->date('d/m/Y'));
						array_push($linha, $aluno['nome_completo']);

					}
					catch (Exception $e) {
						array_push($linha, 'Não Encontrado');
						array_push($linha, '');
						array_push($linha, '');
					}

					$tabela .= get_tabela_tr($linha);
					$writer->addRow($linha);
			    }

			    $tabela .= "</table>";
				$writer->close();
			} else {
			    $message = "Arquivo inválido";
				$type = "error";
			}

		}

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/afiliados_relatorio.php');
	}

	/**
	 * Exibe lista com todos os estornos proporcionais
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2043
	 * @see http://download.uol.com.br/pagseguro/docs/pagseguro-api-estorno-parcial.pdf
	 *
	 * @since Joule 5
	 */

	function estorno_proporcional()
	{
		$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

		$limit = 30; // number of rows in page
		$offset = ( $pagenum - 1 ) * $limit;
		$total = contar_estornos();
		$num_of_pages = ceil( $total / $limit );

		$estornos = listar_estornos($offset, $limit);

		$page_links = paginate_links( array(
            'base' => add_query_arg( 'pagenum', '%#%' ),
            'format' => '',
            'prev_text' => __( '&laquo;', 'text-domain' ),
            'next_text' => __( '&raquo;', 'text-domain' ),
            'total' => $num_of_pages,
            'current' => $pagenum
        ) );

        if(isset($_GET['sucesso']) && $_GET['sucesso']) {
        	$message = "Estorno realizado com sucesso.";
			$type = "updated";
        }

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/estorno_proporcional.php');
	}

	/**
	 * Procura por pedido para estorno proporcional de produtos
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2043
	 * @see http://download.uol.com.br/pagseguro/docs/pagseguro-api-estorno-parcial.pdf
	 *
	 * @since Joule 5
	 */

	function estorno_proporcional_novo()
	{
		if(isset($_POST['submit_pedido'])) {
			$pedido_id = trim($_POST['pedido_id']);

			// Checa se o
			if(!$pedido_id) {
				$message = "É necessário preencher o ID do pedido.";
				$type = "error";
			}
			else {
				try {
					$pedido = new WC_Order($pedido_id);

					if($pedido->get_status() != 'completed') {
						$message = "O status do pedido é diferente de CONCLUÍDO.";
						$type = "error";
					}
					else {

						$produtos = $pedido->get_items();

						if(!$produtos) {
							$message = "O pedido não possui produtos.";
							$type = "error";
						}

						else {
							wp_redirect('/wp-admin/admin.php?page=estorno-proporcional-pedido&id='. $pedido_id);
							exit;
						}
					}
				}
				catch(Exception $e) {
					$message = "Pedido inexistente.";
					$type = "error";
				}
			}
		}


		if(isset($message)) {
			add_settings_error('error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/estorno_proporcional_novo.php');
	}

	/**
	 * Realiza estorno proporcional de produtos em um pedido
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2043
	 * @see http://download.uol.com.br/pagseguro/docs/pagseguro-api-estorno-parcial.pdf
	 *
	 * @since Joule 5
	 */

	function estorno_proporcional_pedido()
	{
		try{
			$pedido = new WC_Order($_GET['id']);
		}catch(Exception $e){
			$message = "Pedido inexistente.";
			$type = "error";
		}

		if($pedido){

			$valores_produtos = get_post_meta($pedido->get_id(), 'valores-produtos', true);

			if(isset($_POST['submit_estornar'])) {
				$itens_ids = $_POST['itens_ids'];
				$motivo = isset($_POST['motivo']) ? $_POST['motivo'] : "";

				if(!$itens_ids) {
					$message = "Nenhum produto foi selecionado.";
					$type = "error";
				}
				else {
					
					log_estorno("Estornar pedido por produtos: {$pedido->get_id()}");
					
					estornar_pedido_por_produtos($pedido->get_id(), $itens_ids, $motivo);
										
					log_estorno("Excluir vendas por pedido: {$pedido->get_id()}");
					
					excluir_vendas_por_pedido($pedido->get_id());
					
					log_estorno("Processar pedido: {$pedido->get_id()}");
					
					processar_pedido($pedido->get_id(), TRUE);

					wp_redirect('/wp-admin/admin.php?page=estorno-proporcional&sucesso=1');

				}
			}

			if(isset($_POST['submit_estornar_valor'])) {

				$valor = isset($_POST['valor']) ? $_POST['valor'] : null;
				$motivo = isset($_POST['motivo']) ? $_POST['motivo'] : "";
				$itens_ids = $_POST['itens_ids'];
				$revoga_acesso = $_POST['revoga_acesso'];
				if(is_null($revoga_acesso)){
					$revoga_acesso = 0;
				}

				if(!preg_match(ER_MOEDA2, $valor)) {
					$message = "Valor inválido.";
					$type = "error";
				}else if(!$itens_ids) {
					$message = "Nenhum produto foi selecionado.";
					$type = "error";
				}else {
					$valor = numero_americano($valor);

					$items = $pedido->get_items();

					$valor_itens = 0;
					$last_bundle_id = null;
					foreach( $items as $item_id => $item ) {

						if(is_item_raiz_de_pacote($item)) {
							$last_bundle_id = $item->get_id();
						}

						if(in_array($item->get_id(), $itens_ids)) {
							if(is_item_de_pacote($item) && in_array($last_bundle_id, $itens_ids)) {
								continue;//Se o pacote estiver selecionado não adiciona o valor de seus itens
							}
							$valor_itens += $valores_produtos[$item->get_id()];
						}
					}

					if(round($valor_itens, 2) < $valor){
						$message = "O valor informado ultrapassa o valor do(s) item(ns): ".moeda($valor_itens);
						$type = "error";
					}elseif($valor > $pedido->get_remaining_refund_amount()){
						$message = "O valor informado ultrapassa o valor disponível para reembolso: ".moeda($pedido->get_remaining_refund_amount());
						$type = "error";
					}else{

						foreach( $items as $item_id => $item ) {

							if(in_array($item->get_id(), $itens_ids)) {
								$percentual = $valores_produtos[$item->get_id()] / $valor_itens;
								$valor_item = round($valor * $percentual, 2);
								if($valor_item > ( round($valores_produtos[$item->get_id()], 2) - $pedido->get_total_refunded_for_item($item->get_id()))){
									$message = "O valor proporcional de " . moeda($valor_item) . " é maior que o disponível para estorno no produto " . $item->get_name() . " de " . moeda($valores_produtos[$item->get_id()] - $pedido->get_total_refunded_for_item($item->get_id()));
									$type = "error";
									break;
								}
							}

						}

						if(!$message){
							
							log_estorno("Estorno por valor: {$pedido->get_id()}");
							
							estornar_pedido_por_valor($pedido->get_id(), $valor, $motivo, $itens_ids, $revoga_acesso);
							
							log_estorno("Excluir vendas por pedido: {$pedido->get_id()}");
							
							excluir_vendas_por_pedido($pedido->get_id());
							
							log_estorno("Processar pedido: {$pedido->get_id()}");
							
							processar_pedido($pedido->get_id(), TRUE);

							wp_redirect('/wp-admin/admin.php?page=estorno-proporcional&sucesso=1');
						}

					}
				}
			}

			$tem_estorno_por_valor = contar_estornos($pedido->get_id(), ESTORNO_POR_VALOR);
			$tem_estorno_por_produto = contar_estornos($pedido->get_id(), ESTORNO_POR_PRODUTO);
		}

		if(isset($message)) {
			add_settings_error('error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/estorno_proporcional_pedido.php');
	}

	/**
	 * Atualiza a lista do MailChimp
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2299
	 *
	 * @since Joule 5
	 */

	function mailchimp()
	{
	    KLoader::model("MailchimpModel");

	    // Recupera áreas da lista pai para view
	    $areas = MailchimpModel::listar_interesses(MC_LISTA_EXPONENCIAL_CONCURSOS, MC_INTERESSE_AREA);

		if(isset($_POST['atualizar'])) {
		    // Sincroniza a lista pai
		    MailchimpModel::sincronizar_areas_de_interesse(MC_LISTA_EXPONENCIAL_CONCURSOS, MC_INTERESSE_AREA);

		    // Itera sobre listas remotas
		    $listas_remotas = MailChimpModel::listar_listas_remotas();

		    foreach ($listas_remotas->lists as $lista_remota) {

		        $interesses_remotos = MailChimpModel::listar_interesses_remotos($lista_remota->id);

		        // se não existe interesse por áreas, cria baseado na lista pai
		        if($interesses_remotos->total_items == 0) {
		            $interest_id = MailchimpModel::criar_grupo_interesse_areas_remoto($lista_remota->id);

		            foreach ($areas as $area) {
		                MailchimpModel::criar_interesse_areas_remoto($lista_remota->id, $interest_id, $area->mca_nome);
		            }

		            // Atualiza a lista de grupos de interesses remotos
		            $interesses_remotos = MailChimpModel::listar_interesses_remotos($lista_remota->id);
		        }

		        foreach ($interesses_remotos->categories as $interesse_remoto) {
		            if($interesse_remoto->list_id == MC_LISTA_EXPONENCIAL_CONCURSOS) {
		                continue;
		            }

		            if($interesse_remoto->title == MAILCHIMP_INTERESSE_POR_AREAS) {
		                MailchimpModel::sincronizar_areas_de_interesse($lista_remota->id, $interesse_remoto->id);
		            }
		        }
		    }

		    // atualiza áreas da lista pai
		    $areas = MailchimpModel::listar_interesses(MC_LISTA_EXPONENCIAL_CONCURSOS, MC_INTERESSE_AREA);

			$message = "Lista atualizada com sucesso.";
			$type = "updated";
		}

		// Recupera áreas da lista pai para view
		$areas = MailchimpModel::listar_interesses(MC_LISTA_EXPONENCIAL_CONCURSOS, MC_INTERESSE_AREA);

		if(isset($message)) {
			add_settings_error('error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/mailchimp.php');
	}

	/**
	 * Controla a área SEO
	 *
	 * @since K1
	 *
	 */

	function seo()
	{
		if(isset($_GET['tab'])) {
			$tab = "seo_" . $_GET['tab'];
		}
		else {
			$tab = "seo_https";
		}

		$data = self::$tab();

		if(isset($data['message'])) {
			add_settings_error('error', '', $data['message'], $data['type']);
		}

		if(isset($_GET['proc']) && $_GET['proc'] == 1) {
			include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name. "/views/processando.php");
		}
		else {
			include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/seo_tab.php');
			include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name. "/views/{$tab}.php");
		}
	}


	/**
	 * Painel para substituir HTTP por HTTPS em alguns conteúdos
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2292
	 *
	 * @since K1
	 *
	 */

	function seo_https()
	{
	    KLoader::model("QuestaoModel");
	    
		$conteudo = "http://www.exponencialconcursos.com.br";

		if($_POST['corrigir']) {
		    //@see /wp-content/cronjobs/atualizar_seo_https.php
			$novo = "https://www.exponencialconcursos.com.br";

			replace_conteudo_de_posts($conteudo, $novo);
			replace_conteudo_de_usermetas($conteudo, $novo);
			QuestaoModel::replace_conteudo_de_questoes($conteudo, $novo);

			$data['message'] = "Links corrigidos com sucesso.";
			$data['type'] = "updated";
		}

		$num_blogs = contar_posts_com_conteudo($conteudo, 'post');
		$num_paginas = contar_posts_com_conteudo($conteudo, 'page');
		$num_produtos = contar_posts_com_conteudo($conteudo, 'product');
		$num_concursos = contar_posts_com_conteudo($conteudo, 'concurso');
		$num_foruns = contar_posts_com_conteudo($conteudo, 'forum');
		$num_topicos = contar_posts_com_conteudo($conteudo, 'topic');
		$num_respostas = contar_posts_com_conteudo($conteudo, 'reply');
		$num_slides = contar_posts_com_conteudo($conteudo, 'slide');
		$num_faq = contar_posts_com_conteudo($conteudo, 'questions');
		$num_historicos = contar_posts_com_conteudo($conteudo, 'revisions');
		$num_usermeta = contar_usermetas_com_conteudo($conteudo);
		$num_questoes = QuestaoModel::contar_questoes_com_conteudo($conteudo);

		$tabela = [
			['tipo' => 'Blogs', 'qtde' => $num_blogs],
			['tipo' => 'Páginas', 'qtde' => $num_paginas],
			['tipo' => 'Produtos', 'qtde' => $num_produtos],
			['tipo' => 'Concursos', 'qtde' => $num_concursos],
			['tipo' => 'Fóruns', 'qtde' => $num_foruns],
			['tipo' => 'Tópicos', 'qtde' => $num_topicos],
			['tipo' => 'Respostas', 'qtde' => $num_respostas],
			['tipo' => 'Slides', 'qtde' => $num_slides],
			['tipo' => 'FAQ', 'qtde' => $num_faq],
			['tipo' => 'Histórico', 'qtde' => $num_historicos],
			['tipo' => 'Usermeta', 'qtde' => $num_usermeta],
		    ['tipo' => 'Questões', 'qtde' => $num_questoes],
		];

		$data['tabela'] = $tabela;

		return $data;
	}
	
	/**
	 * Painel para substituir HTTP por HTTPS em alguns conteúdos externos
	 *
	 * @since L1
	 *
	 */
	
	function seo_https_externo()
	{
	    KLoader::model("SeoHTTPSExternoModel");
	    KLoader::helper("SeoHTTPSHelper");
	    
	    if($_POST['atualizar']){
	        
	        $conteudo = "http://";
	        $exclude = "http://www.exponencialconcursos.com.br";
	    
	        //Atualiza os dados que vem da tabela WP_POSTS
	        $tipos_post = ['post', 'page', 'product', 'concurso', 'forum', 'topic', 'reply', 'slides', 'questions', 'revisions'];
	        
	        foreach($tipos_post as $post_type){	        
    	        $posts = listar_posts_com_conteudo($conteudo, $post_type, $exclude);
    	        $dados = array();
    	        foreach($posts as $post)
    	        {
    	            $post_title = $post->post_title;
    	            if($post_type == 'reply'){
    	                $topic_id = bbp_get_reply_topic_id( $post->ID );
    	                $post_title = "Re: " . get_the_title( $topic_id ) ; 
    	            }
    	            
    	            array_push($dados, array(
    	                'she_tipo' => $post_type,
    	                'she_titulo' => $post_title,
    	                'she_data_atualizacao' => $post->post_modified,
    	                'she_edit_url' => "/wp-admin/post.php?post={$post->ID}&action=edit",
    	            ));
    	        }
    	        SeoHTTPSExternoModel::salvar_todos($dados, $post_type);
	        }
	        
	        
	        //Atualiza os dados que vem da tabela WP_USERMETA
    	    $usermetas = listar_usermeta_com_conteudo($conteudo, $exclude);
    	    
    	    $dados = array();
    	    foreach($usermetas as $usermeta)
    	    {
    	        $user = get_usuario($usermeta->user_id);
    	        array_push($dados, array(
    	            'she_tipo' => 'usermeta',
    	            'she_titulo' => $user->first_name . ' ' . $user->last_name,
    	            'she_data_atualizacao' => date('Y-m-d H:i:s', get_user_meta($usermeta->user_id, 'last_update', true)),
    	            'she_edit_url' => "/wp-admin/user-edit.php?user_id={$usermeta->user_id}&wp_http_referer=%2Fwp-admin%2Fusers.php",
    	        ));
    	    }
    	    SeoHTTPSExternoModel::salvar_todos($dados, 'usermeta');
    	    
    	    
    	    //Atualiza os dados que vem da tabela QUESTOES
    	    KLoader::model("QuestaoModel");
    	    $questoes = QuestaoModel::listar_questoes_com_conteudo($conteudo, $exclude);
    	    
    	    $dados = array();
    	    foreach($questoes as $questao)
    	    {
    	        array_push($dados, array(
    	            'she_tipo' => 'questao',
    	            'she_titulo' => $questao->que_id,
    	            'she_data_atualizacao' => $questao->que_data_atualizacao,
    	            'she_edit_url' => EDITAR_QUESTAO_URL . $questao->que_id,
    	        ));
    	    }
    	    SeoHTTPSExternoModel::salvar_todos($dados, 'questoes');
    	    
	    }

	    //Busca e paginação
	    $pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;
	    
	    $limit = LIMITE_SEO_HTTPS_EXTERNO; // number of rows in page
	    $offset = ( $pagenum - 1 ) * $limit;
	    $total = SeoHTTPSExternoModel::contar();
	    $num_of_pages = ceil( $total / $limit );
	    
	    $page_links = paginate_links( array(
	        'base' => add_query_arg( 'pagenum', '%#%' ),
	        'format' => '',
	        'prev_text' => __( '&laquo;', 'text-domain' ),
	        'next_text' => __( '&raquo;', 'text-domain' ),
	        'total' => $num_of_pages,
	        'current' => $pagenum
	    ) );
	    
	    $data['page_links'] = $page_links;
	    $data['resultado'] = SeoHTTPSExternoModel::listar($offset, $limit);
	    
	    return $data;
	}

	/**
	 * Painel para substituir HTTP por HTTPS em alguns conteúdos
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2292
	 *
	 * @since K1
	 *
	 */

	function seo_imagens()
	{
		if($_POST['executar']) {

			$nome = $_FILES['arquivo']['tmp_name'];
			$qualidade = $_POST['qualidade'];

			if(!isset($_FILES) || !isset($_FILES['arquivo']) || !$nome) {
				$data['message'] = "Um arquivo válido precisa ser enviado.";
				$data['type'] = "error";
			}
			else if(!isset($_POST['qualidade']) || !$qualidade) {
				$data['message'] = "A qualidade deve ser escolhida.";
				$data['type'] = "error";
			}
			else if($qualidade < 1 || $qualidade > 100) {
				$data['message'] = "A qualidade deve ser um número entre 1 e 100.";
				$data['type'] = "error";
			}
			else {
				$imagens = file($nome);

				$sie_id = salvar_seo_imagem_execucao([
					'sie_enviados' => count($imagens),
					'sie_qualidade' => $qualidade,
					'sie_data_hora' => date('Y-m-d H:i:s')
				]);

				$processados = 0;
				$com_erro = 0;

				foreach ($imagens as $imagem) {
					$data = compress_image($imagem, $imagem, $qualidade);

					salvar_seo_imagem_detalhe([
						'sid_arquivo' => $imagem,
						'sid_tamanho_antes' => $data['size_src'],
						'sid_tamanho_depois' => $data['size_dst'],
						'sid_status' => $data['status'],
						'sie_id' => $sie_id
					]);

					$processados++;

					if($data['status'] != SEO_IMAGENS_OK) {
						$com_erro++;
					}
				}

				atualizar_seo_imagem_execucao($sie_id, [
					'sie_processados' => $processados,
					'sie_com_erro' => $com_erro,
				]);

				$data['message'] = "Links corrigidos com sucesso.";
				$data['type'] = "updated";
			}

		}

		$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

		$limit = 30; // number of rows in page
		$offset = ( $pagenum - 1 ) * $limit;
		$total = contar_seo_imagens_execucoes();
		$num_of_pages = ceil( $total / $limit );

		$data['page_links'] = paginate_links( array(
            'base' => add_query_arg( 'pagenum', '%#%' ),
            'format' => '',
            'prev_text' => __( '&laquo;', 'text-domain' ),
            'next_text' => __( '&raquo;', 'text-domain' ),
            'total' => $num_of_pages,
            'current' => $pagenum
        ) );

		$data['execucoes'] = listar_seo_imagens_execucoes($limit, $offset);

		return $data;
	}

	/**
	 * Painel para substituir HTTP por HTTPS em alguns conteúdos
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2292
	 *
	 * @since K1
	 *
	 */

	function seo_imagens_detalhes()
	{
		$data['id'] = $_GET['sie'];

		$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

		$limit = 30; // number of rows in page
		$offset = ( $pagenum - 1 ) * $limit;
		$total = contar_seo_imagens_detalhes($data['id']);
		$num_of_pages = ceil( $total / $limit );

		$data['page_links'] = paginate_links( array(
            'base' => add_query_arg( 'pagenum', '%#%' ),
            'format' => '',
            'prev_text' => __( '&laquo;', 'text-domain' ),
            'next_text' => __( '&raquo;', 'text-domain' ),
            'total' => $num_of_pages,
            'current' => $pagenum
        ) );

		$data['arquivos'] = listar_seo_imagens_detalhes($data['id'], $limit, $offset);

		return $data;
	}

	/**
	 * Painel para gerenciar H1
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2252
	 *
	 * @since K2
	 *
	 */

	function seo_h1()
	{
	    KLoader::helper("UrlRequestHelper");

		if($_POST['atualizar']) {
			excluir_seo_h1();
			$posts = listar_posts_por_tipo([TIPO_POST_PAGINA, TIPO_POST_CONCURSO, TIPO_POST_POST, TIPO_POST_PRODUTO]);

			iniciar_seo_h1s($posts);

			wp_redirect('/wp-admin/admin.php?page=seo&tab=h1&proc=1');
		}

		if(isset($_GET['proc']) && $_GET['proc'] == 1) {
			$h1s = listar_seo_h1_nao_processados();

			if($h1s) {
				$percentual = porcentagem(contar_seo_h1_processados() / contar_todos_seo_h1() * 100);

				$data['message'] = "Aguarde... Atualizando informações de H1... $percentual";
				$data['type'] = "updated";

				foreach ($h1s as $item) {
					set_time_limit(300);

					$post = get_post($item['sh1_id']);
					$permalink = get_permalink($item['sh1_id']);

					$html = str_get_html(UrlRequestHelper::get_conteudo($permalink));

					$title = $html->find("title", 0);

					$titulo = "";
					if($title) {
						$titulo = $title->innertext;
					}

					$h1s = $html->find("h1");

					$h1_info = "";
					$h1_num = 0;

					if($h1s) {
						$h1_num = count($h1s);

						$h1_a = [];
						foreach ($h1s as $h1) {
							if($texto = $h1->innertext) {
								array_push($h1_a, $texto);
							}
						}

						$h1_info = implode(" ; ", $h1_a);
					}

					$edit_link = get_edit_post_link($item['sh1_id']);

					$dados = [
						'sh1_id' => $item['sh1_id'],
						'sh1_url' => $permalink,
						'sh1_titulo' => $titulo,
						'sh1_h1' => $h1_info,
						'sh1_num' => $h1_num,
						'sh1_edit_url' => $edit_link
					];

					salvar_seo_h1($dados);
				}

			}
			else {
				wp_redirect('/wp-admin/admin.php?page=seo&tab=h1&proc=2');
			}

		}

		else {
			if(isset($_GET['proc']) && $_GET['proc'] == 2) {
				$data['message'] = "Atualização realizada com sucesso. ";
				$data['type'] = "updated";
			}

			$texto = "";
			$num_tipo = isset( $_GET['num_tipo'] ) ? $_GET['num_tipo'] : "";
			$texto = isset( $_GET['texto'] ) ? $_GET['texto'] : "";

			$pagenum = isset( $_GET['pagenum'] ) ? absint( $_GET['pagenum'] ) : 1;

			$limit = 20; // number of rows in page
			$offset = ( $pagenum - 1 ) * $limit;
			$total = contar_seo_h1($texto, $num_tipo);
			$num_of_pages = ceil( $total / $limit );

			$data['page_links'] = paginate_links( array(
	            'base' => add_query_arg( 'pagenum', '%#%' ),
	            'format' => '',
	            'prev_text' => __( '&laquo;', 'text-domain' ),
	            'next_text' => __( '&raquo;', 'text-domain' ),
	            'total' => $num_of_pages,
	            'current' => $pagenum
	        ) );

			$items = listar_seo_h1($limit, $offset, $texto, $num_tipo);

			$tabela = [];
			foreach ($items as $item) {
				$linha = [
					'url' => $item['sh1_url'],
					'titulo' => $item['sh1_titulo'],
					'h1_num' => $item['sh1_num'],
					'h1_info' => $item['sh1_h1'],
					'edit_url' => $item['sh1_edit_url']
				];

				array_push($tabela, $linha);
			}

			$data['tabela'] = $tabela;

		}

		return $data;
	}

	/**
	 * Painel para configurações gerais
	 *
	 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2246
	 *
	 * @since K1
	 *
	 */

	function paginas()
	{
		if($_POST['submit']) {

			if($wa = stripslashes($_POST['wa'])) {
				update_option(GERAL_WHATSAPP, $wa);
			}

			$data['message'] = "Um arquivo válido precisa ser enviado.";
			$data['type'] = "updated";
		}

		$wa = get_option(GERAL_WHATSAPP);

		if(isset($data['message'])) {
			add_settings_error('error', '', $data['message'], $data['type']);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/paginas.php');
	}

    function notas_fiscais()
    {
        if($_POST['submit']) {

            if($nf_ativar_cadastro = $_POST['nf_ativar_cadastro']) {
                update_option(NOTA_FISCAL_ATIVAR_CADASTRO, true);
            }
            else {
                delete_option(NOTA_FISCAL_ATIVAR_CADASTRO);
            }

            if($nf_ativar_skip = $_POST['nf_ativar_skip']) {
                update_option(NOTA_FISCAL_ATIVAR_SKIP, true);
            }
            else {
                delete_option(NOTA_FISCAL_ATIVAR_SKIP);
            }

            $data['message'] = "Opções salvas com sucesso.";
            $data['type'] = "updated";
        }

        $nf_ativar_cadastro = get_option(NOTA_FISCAL_ATIVAR_CADASTRO);
        $nf_ativar_skip = get_option(NOTA_FISCAL_ATIVAR_SKIP);

        if(isset($data['message'])) {
            add_settings_error('error', '', $data['message'], $data['type']);
        }

        include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/notas_fiscais.php');
    }

	function painel_controle(){

		if($_POST['submit']) {

			$areas = listar_painel_controle();

			foreach($areas as $area){
				ativa_ou_desativa_area_painel_controle($area['pai_id'], $_POST['area'.$area['pai_id']]);
			}
			$data['message'] = "Bloqueio/Desbloqueio de áreas realizado com sucesso!";
			$data['type'] = "updated";
		}

		if($_POST['enviar_email']){

			$areas = listar_painel_controle();

			$ok = TRUE;
			foreach($areas as $area){
				if(!$area['pai_ativo']){
					$data['message'] = "Ainda existem área(s) desativada(s).";
					$data['type'] = "error";
					$ok = FALSE;
					break;
				}
			}

			if($ok == TRUE){
				enviar_email_painel_controle_aviso();
				$data['message'] = "E-mails enviados com sucesso!";
				$data['type'] = "updated";
			}

		}

		if($_POST['excluir_email']){
			limpar_painel_controle_aviso();
			$data['message'] = "E-mails removidos com sucesso!";
			$data['type'] = "updated";
		}

		if(isset($_GET['ec'])) {
			$id = $_GET['ec'];

			excluir_cronjob($id);

			$data['message'] = "Cronjob excluído com sucesso!";
			$data['type'] = "updated";
		}

		if(isset($_GET['m'])) {
			switch ($_GET['m']) {
				case CRONJOB_CRIADO: {
					$data['message'] = "Cronjob criado com sucesso!";
					$data['type'] = "updated";
					break;
				}

				case CRONJOB_EDITADO: {
					$data['message'] = "Cronjob atualizado com sucesso!";
					$data['type'] = "updated";
					break;
				}
			}
		}

		if(isset($data['message'])) {
			add_settings_error('error', '', $data['message'], $data['type']);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/painel_controle.php');
	}

	function editar_cronjob()
	{

		if(isset($_GET['id'])) {
			$cronjob = get_cronjob($_GET['id']);
		}

		if(isset($_POST['submit'])) {

			if(!isset($_POST['cro_minuto']) || !isset($_POST['cro_hora']) || !isset($_POST['cro_dia']) || !isset($_POST['cro_mes']) || !isset($_POST['cro_dia_semana']) || !isset($_POST['cro_comando']) || !isset($_POST['cro_status'])) {

				$message = "Todos os campos são obrigatórios.";
				$type = "error";
			}
			else {
			    $cronjob = [
					'cro_id' => isset($_POST['cro_id']) ? $_POST['cro_id'] : NULL,
					'cro_minuto' => $_POST['cro_minuto'],
					'cro_hora' => $_POST['cro_hora'],
					'cro_dia' => $_POST['cro_dia'],
					'cro_mes' => $_POST['cro_mes'],
					'cro_dia_semana' => $_POST['cro_dia_semana'],
					'cro_status' => $_POST['cro_status'],
					'cro_comando' => $_POST['cro_comando'],
				];

			    $cro_id = salvar_cronjob($cronjob);

				$cronjob['cro_id'] = $cro_id;
				salvar_cronjob_table($cronjob);

				$msg = $_GET['id'] ? CRONJOB_CRIADO : CRONJOB_EDITADO;
				wp_redirect("/wp-admin/admin.php?page=painel_controle&m=$msg");
			}
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/editar_cronjob.php');
	}

	/**
	 * Página de configurações gerais do Exponencial
	 *
	 * > Sistema de Questões
	 *
	 * >> Quantidade de questões diárias
	 * @see /exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/1919
	 */

	function sq()
	{
		// recupera options
		$cfg_sq_qtde_questoes_diarias = get_option(CFG_SQ_QTDE_QUESTOES_DIARIAS);

		// processa clique no botão salvar
		if(isset($_POST['submit'])) {
			// recupera valor dos campos
			$cfg_sq_qtde_questoes_diarias = $_POST['cfg_sq_qtde_questoes_diarias'];

			// verifica se qtde de questões diárias está vazio
			if(empty($cfg_sq_qtde_questoes_diarias)) {
				$message = "Informe a quantidade de questões diárias.";
				$type = "error";
			}

			// verifica se qtde de questões diárias é número inteiro
			else if(filter_var($cfg_sq_qtde_questoes_diarias, FILTER_VALIDATE_INT) == false) {
				$message = "Quantidade de questões diárias deve ser um número inteiro.";
				$type = "error";
			}

			// atualiza options
			else {
				update_option(CFG_SQ_QTDE_QUESTOES_DIARIAS, $cfg_sq_qtde_questoes_diarias);
				$message = "Configurações gerais atualizadas com sucesso.";
				$type = "updated";
			}
		}

		if(isset($message)) {
			add_settings_error('coaching-error', '', $message, $type);
		}

		include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/sq.php');
	}
	
	/**
	 * Reprocessar Produtos Premium
	 *
	 * @since L1
	 */
	
	function produtos_premium()
	{
	    if(isset($_GET['tab'])) {
	        $tab = "premium_" . $_GET['tab'];
	    }
	    else {
	        $tab = "premium_config";
	    }
	    
	    $data = self::$tab();
	    
	    if(isset($data['message'])) {
	        add_settings_error('error', '', $data['message'], $data['type']);
	    }
	    

	    include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name. "/views/{$tab}.php");
        include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/premium_config.php');
	}
	
	
	function premium_config()
	{
	    KLoader::model("PremiumModel");
	    
// 	    // processa clique no botão salvar
	    if(isset($_POST['submit'])) {
	        // recupera valor dos campos
	        $cota_sq = numero_americano($_POST['cota_sq']);
	        $cota_professor = numero_americano($_POST['cota_professor']);
	        $pgto_minimo = numero_americano($_POST['pgto_minimo']);
	             
	        // verifica se cota do sq está vindo vazio
	        if(empty($cota_sq)) {
	            $message = "Informe a Cota SQ.";
	            $type = "error";
	        }
	        
	        else if(!is_numeric($cota_sq)) {
	            $message = "Cota SQ deve ser um número.";
	            $type = "error";
	        }
	        
	        else if($cota_sq > 100) {
	            $message = "Cota SQ não pode ser maior do que 100%.";
	            $type = "error";
	        }
	        
	        else if($cota_sq < 0) {
	            $message = "Cota SQ não pode ser menor do que 0%.";
	            $type = "error";
	        }
	        
	        
	        else if(empty($cota_professor)) {
	            $message = "Informe a Cota Professor.";
	            $type = "error";
	        }
	        
	        else if(!is_numeric($cota_professor)) {
	            $message = "Cota Professor deve ser um número.";
	            $type = "error";
	        }
	        
	        else if($cota_professor > 100) {
	            $message = "Cota Professor não pode ser maior do que 100%.";
	            $type = "error";
	        }
	        
	        else if($cota_professor < 0) {
	            $message = "Cota Professor não pode ser menor do que 0%.";
	            $type = "error";
	        }
	        
	        else if($cota_professor + $cota_sq != 100) {
	            $message = "A soma de Cota SQ e Cota Professor precisa ser igual a 100%.";
	            $type = "error";
	        }
	        
	        else if(empty($pgto_minimo)) {
	            $message = "Informe o Mínimo para Pagamento.";
	            $type = "error";
	        }
	        
	        else if(!is_numeric($pgto_minimo)) {
	            $message = "Mínimo para Pagamento deve ser um número.";
	            $type = "error";
	        }
	        
	        else if($pgto_minimo < 0) {
	            $message = "Mínimo para Pagamento não pode ser menor do que 0%.";
	            $type = "error";
	        }
	        // atualiza options
	        else {
	            $dados = [
	                'ppc_data' => date('Y-m-01'),
	                'ppc_cota_sq' => $cota_sq,
	                'ppc_cota_professor' => $cota_professor,
	                'ppc_minimo_pgto' => $pgto_minimo,
	            ];
	            
	            PremiumModel::set_configs($dados);
	            
	            $message = "Configurações do Premium atualizadas com sucesso.";
	            $type = "updated";
	        }
	        
	        $data['message'] = $message;
	        $data['type'] = $type;
	    }
	    
	    // recupera dados
	    $data['configs'] = PremiumModel::listar_configs();
	    
	    return $data;
	}

	/**
	 * Reprocessar Produtos Premium
	 *
     * @since L1
	 */

	function reprocessar_produtos_premium()
	{
	    KLoader::model("PremiumModel");
	    KLoader::helper("PremiumHelper");
	    KLoader::helper("UiSelectHelper");
	    
	    if(isset($_POST['submit'])) {
	        $produtos_selecionados = $_POST['produtos_selecionados'];
	        $ano_mes_selecionado = $_POST['ano_mes_selecionado'];

	        if(!$produtos_selecionados) {
	            $message = "Selecione, pelo menos, um produto premium.";
	            $type = "error";
	        }
	        
	        if(!$ano_mes_selecionado) {
	            $message = "Selecione um período";
	            $type = "error";
	        }

	        else {
	            foreach ($produtos_selecionados as $produto_id) {
	                PremiumModel::agendar($ano_mes_selecionado, $produto_id);
	            }

	            $message = "Reprocessamento agendado com sucesso! O servidor levará algum tempo até que todos os dados sejam processados.";
	            $type = "updated";
	        }

	    }
	    
	    $produtos_combo = UiSelectHelper::get_combo_options(PremiumModel::listar_produtos(), "post_id", "post_title", false);

        $anos_meses_combo = UiSelectHelper::get_combo_options(PremiumModel::listar_anos_meses(), "chave", "valor", false);

	    $agendamentos = PremiumModel::listar_agendamentos();

	    if(isset($message)) {
	        add_settings_error('reprocessar-error', '', $message, $type);
	    }

	    include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/reprocessar_produtos_premium.php');
	}
	
	/**
	 * Reprocessar os acumulados dos Premium
	 *
	 * @since L1
	 */
	
	function reprocessar_acumulados_premium()
	{
	    KLoader::model("PremiumModel");
	    KLoader::helper("PremiumHelper");
	    KLoader::helper("UiSelectHelper");
	    
	    if(isset($_POST['submit'])) {
	        $ano_mes_i = $_POST['ano_mes_i'] ?: null;
	        $ano_mes_f = $_POST['ano_mes_f'] ?: null;

	        if(!$ano_mes_i) {
	            $message = "Selecione o período inicial";
	            $type = "error";
	        }
	        
	        elseif(!$ano_mes_f) {
	            $message = "Selecione o período final";
	            $type = "error";
	        }
	        
	        elseif($ano_mes_i > $ano_mes_f) {
	            $message = "O período inicial não pode ser maior do que o período final";
	            $type = "error";
	        }
	        
	        else {
	            set_time_limit(300);
	            PremiumHelper::atualizar_acumulados($ano_mes_i, $ano_mes_f);
	            
	            $message = "Os acumulados foram processados com sucesso";
	            $type = "updated";
	        }
	        
	    }
	    
	    $anos_meses_combo = UiSelectHelper::get_combo_options(PremiumModel::listar_anos_meses(), "chave", "valor", false, "Selecione um período...");
	    
	    if(isset($message)) {
	        add_settings_error('reprocessar-error', '', $message, $type);
	    }
	    
	    include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/reprocessar_acumulados_premium.php');
	}

	function config_relatorio_vendas_concurso()
    {
	    KLoader::model('CategoriaModel');

	    // rotina para salvar as categorias
	    if(isset($_POST['submit'])) {
	        $a_salvar = [];
            foreach($_POST['concursos'] as $id) {

                // ignora itens sem valor
                if($id) {
                    $term = get_term($id);
                    $a_salvar[$id] = $term->slug;
                }
            }

	        update_option(CATEGORIAS_AGRUPADAS, $a_salvar, false);
            $message = "A ordem de prioridade das categorias foi atualizada com sucesso.";
            $type = "updated";
        }

	    // carrega as categorias salvas
	    $categorias_salvas = get_option(CATEGORIAS_AGRUPADAS);

	    // preenchimento necessário para inicializar a interface, caso não tenha nenhuma categoria selecionada
	    if(!$categorias_salvas) {
	        $categorias_salvas = [];
	        $categorias_salvas[0] = 0;
        }

        if(isset($message)) {
            add_settings_error('reprocessar-error', '', $message, $type);
        }

        $categorias = CategoriaModel::listar([]);

        $options = [];

        foreach ($categorias as $categoria) {
            $options[$categoria->term_id] =
                $categoria->name . " - id: " . $categoria->term_id . " - slug: " . $categoria->slug;
        }

        include_once(WP_PLUGIN_DIR.'/'.$this->plugin->name.'/views/config_relatorio_vendas_concurso.php');
    }

}

function exponencial()
{
	global $exponencial;

	if( !isset($exponencial) )
	{
		$exponencial = new Exponencial();
	}

	return $exponencial;
}

function exponencial_h1_crawler()
{
    KLoader::helper("UrlRequestHelper");

	$link = get_url_atual();
	$html = str_get_html(UrlRequestHelper::get_conteudo($link));

	$title = $html->find("title", 0);

	$titulo = "";
	if($title) {
		$titulo = $title->innertext;
	}

	$h1s = $html->find("h1");

	$h1_info = "";
	$h1_num = 0;

	if($h1s) {
		$h1_num = count($h1s);

		$h1_a = [];
		foreach ($h1s as $h1) {
			if($texto = $h1->innertext) {
				array_push($h1_a, $texto);
			}
		}

		$h1_info = implode(" ; ", $h1_a);

	}

	$edit_link = get_edit_post_link($post->ID);

	$dados = [
		'sh1_url' => $link,
		'sh1_titulo' => $titulo,
		'sh1_h1' => $h1_info,
		'sh1_num' => $h1_num,
		'sh1_edit_url' => $edit_link
	];

	salvar_seo_h1($dados);
}

function get_theme_coaching_dir()
{
	return '/wp-content/themes/academy/coaching/';
}

function get_avaliacao_estudos_filename()
{
	return 'avaliacao-de-estudos-exponencial-concursos.xlsx';
}

function get_avaliacao_estudos_url()
{
	return get_theme_coaching_dir() . get_avaliacao_estudos_filename();
}

function get_disponibilidade_aluno_filename()
{
	return 'disponibilidade-do-aluno-exponencial-concursos.xlsx';
}

function get_disponibilidade_aluno_url()
{
	return get_theme_coaching_dir() . get_disponibilidade_aluno_filename();
}
// initialize
exponencial();
?>