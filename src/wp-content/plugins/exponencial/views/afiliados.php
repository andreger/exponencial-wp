<?php settings_errors() ?>
<div class="wrap">
    <h2>
    	<?= $this->plugin->displayName; ?> - Afiliados</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->



            <?php 
            /**
              * Código comentado conforme solicitação no tícket abaixo
              * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2431
              */
            ?> 
            <!--<h3>Percentual</h3>

		        <form id="post" name="post" method="post" action="admin.php?page=folha-dirigida">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                     	<div class="option">
                     		<p>
                     			<strong>Percentual da Folha Dirigida</strong>
                  		 		<input required="required" type="text" name="percentual" class="widefat" value="<?= isset($percentual) ? $percentual : '' ?>" />
                  		 	</p>  
                      </div>
                      
  		                <div class="submit">
  		                    <input type="submit" name="submit_percentual" value="Salvar" class="button button-primary" /> 
                          <a href="admin.php?page=folha-dirigida-historico" class="button button-primary">Histórico</a>
  		                </div>
                        
                    </div>
                </form>
            </div>
            -->

            <h3>CSV</h3>
            <form id="form-csv" name="form-csv" method="post" action="admin.php?page=afiliados-relatorio" enctype="multipart/form-data">
                <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                      <div class="option">
                        <p>
                          <strong>Arquivo CSV</strong>
                          <input required="required" type="file" name="csv" class="widefat" />
                        </p>  
                      </div>
                      
                      <div class="submit">
                          <input type="submit" name="submit_csv" value="Salvar" class="button button-primary" /> 
                      </div>
                        
                    </div>
                </form>
              </div>
            </div>
        </div>
<div style="clear:both"></div>