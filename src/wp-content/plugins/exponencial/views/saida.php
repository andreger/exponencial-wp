<?php settings_errors() ?>
<div class="wrap">
    <a class="button button-primary" href="admin.php?page=editar_saida">Nova Captura de Saída</a>
</div>

<?php 

if($saidas) : ?>
<div class="wrap">
    <h2>Capturas de Saída Cadastradas</h2>
    <div>
    	<table class="wp-list-table widefat fixed striped">
    		<thead>
    			<tr>
					<th>Título</th>
					<th>Data início</th>
					<th>Data fim</th>
					<th>Link</th>
					<th>Prioridade</th>
    				<th>URLs</th>    				
                    <th>Palavras-chave</th>
                    <th>URLs negativas</th>
                    <th>Palavras negativas</th>
                    <th>Tempo para reabertura</th>
    				<th>Ações</th>
    			</tr>
    		</thead>
    		<?php foreach ($saidas as $item) : ?>
    		<tr>
				<td><?= $item->sai_titulo?></td>
				<td><?= converter_para_ddmmyyyy($item->sai_data_inicio) ?></td>
				<td><?= converter_para_ddmmyyyy($item->sai_data_fim) ?></td>
				<td><?= $item->sai_link ?></td>
				<td><?= $item->sai_prioridade ?></td>
    			<td><?= $item->sai_urls ?></td>
                <td><?= $item->sai_tags ?></td>
                <td><?= $item->sai_urls_negativas ?></td>
                <td><?= $item->sai_palavras_negativas ?></td>
                <td><?= $item->sai_dias_expiracao ?></td>
    			<td>
    				<a onclick="return confirm('Deseja realmente excluir este item ?');" href="admin.php?page=saida&remove=<?= $item->sai_id ?>">Excluir</a> | 
                    <a href="admin.php?page=editar_saida&id=<?= $item->sai_id ?>">Editar</a>
    			</td>
    		</tr>
    		<?php endforeach; ?>
    	</table>
    </div>
</div>
<?php endif; ?>
