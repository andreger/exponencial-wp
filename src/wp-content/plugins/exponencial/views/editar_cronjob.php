<?php settings_errors() ?>
<div class="wrap">
    <h2>
    	<?= $this->plugin->displayName; ?> - 
    	<?= isset($cronjob) ? "Editar" : "Novo" ?>
    		Cronjob</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" name="post" method="post" action="admin.php?page=editar_cronjob">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                     	<div class="option">
                     		<p>
                     			<strong>Minuto *</strong>
                  		 		<input required="required" type="text" name="cro_minuto" class="widefat" value="<?= isset($cronjob) ? $cronjob['cro_minuto'] : '' ?>" />
                  		 	</p>  
                      </div>
                        
                     <div class="option">
                        <p>
                          <strong>Hora *</strong>
                          <input required="required" type="text" name="cro_hora" class="widefat" value="<?= isset($cronjob) ? $cronjob['cro_hora'] : '' ?>" />
                        </p>  
                      </div>

                     <div class="option">
                        <p>
                          <strong>Dia *</strong>
                          <input required="required" type="text" name="cro_dia" class="widefat" value="<?= isset($cronjob) ? $cronjob['cro_dia'] : '' ?>" />
                        </p>  
                      </div>

                     <div class="option">
                        <p>
                          <strong>Mês *</strong>
                          <input required="required" type="text" name="cro_mes" class="widefat" value="<?= isset($cronjob) ? $cronjob['cro_mes'] : '' ?>" />
                        </p>  
                      </div>

                      <div class="option">
                        <p>
                          <strong>Dia da Semana *</strong>
                          <input required="required" type="text" name="cro_dia_semana" class="widefat" value="<?= isset($cronjob) ? $cronjob['cro_dia_semana'] : '' ?>" />
                        </p>  
                      </div>

                      <div class="option">
                        <p>
                          <strong>Ativo *</strong>
                          <select name="cro_status" class="widefat">
                            <option value="1" <?= isset($cronjob) &&  $cronjob['cro_status'] == SIM ? "selected=selected" : "" ?>>Sim</option>
                            <option value="2" <?= isset($cronjob) &&  $cronjob['cro_status'] == NAO ? "selected=selected" : "" ?>>Não</option>
                          </select>
                        </p>  
                      </div>

                      <div class="option">
                        <p>
                          <strong>Comando *</strong>
                          <input required="required" type="text" name="cro_comando" class="widefat" value="<?= isset($cronjob) ? $cronjob['cro_comando'] : '' ?>" />
                        </p>  
                      </div>

                      <?php if(isset($cronjob)) : ?>
                      <input type="hidden" name="cro_id" value="<?= $cronjob['cro_id'] ?>" />
                      <?php endif ?>
                      
  		                <div class="submit">
  		                    <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
                          <a class="button button-primary" href="/wp-admin/admin.php?page=painel_controle">Voltar</a>
  		                </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>