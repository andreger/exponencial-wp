<?php settings_errors() ?>
<div class="wrap">
    <h2>Produtos Premium - Configurações</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-12">
    		<!-- Content -->
    		<div id="post-body-content">
 
                <form id="post" name="post" method="post" action="admin.php?page=produtos_premium&tab=config">
                    <div class="option">
                        <p>
                            <b>Cota SQ (%)</b>
                            <input type="text" name="cota_sq" class="widefat" value="" placeholder="(Número inteiro ou decimal entre 0 e 100. Ex: 75,00)"  />
                        </p>  
                    </div>
                    
                    <div class="option">
                        <p>
                            <b>Cota Professor</b>
                            <input type="text" name="cota_professor" class="widefat" value="" placeholder="(Número inteiro ou decimal entre 0 e 100. Ex: 75,00)" />
                        </p>  
                    </div>
                    
                    <div class="option">
                        <p>
                            <b>Mínimo para Pagamento</b>
                            <input type="text" name="pgto_minimo" class="widefat" value="" placeholder="(Número inteiro ou decimal. Ex: 75,00)" />
                        </p>  
                    </div>

                    <div class="submit">
                        <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <h2>Histórico</h2>
    <div>
        <?php if($data['configs']) :?>
        <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th style="text-align: center">Data Início</th>
                    <th style="text-align: center">Cota SQ</th>
                    <th style="text-align: center">Cota Professor</th>
                    <th style="text-align: center">Mínimo para Pagamento</th>
                </tr>
                </thead>
                
                <?php foreach ($data['configs'] as $item) : ?>
                   <tr>
                   		<td style="text-align: center"><?= converter_para_ddmmyyyy($item->ppc_data) ?></td>
                   		<td style="text-align: center"><?= porcentagem($item->ppc_cota_sq) ?></td>
                   		<td style="text-align: center"><?= porcentagem($item->ppc_cota_professor) ?></td>
                   		<td style="text-align: center"><?= moeda($item->ppc_minimo_pgto) ?></td>
                   </tr>
                <?php endforeach; ?>
               
            </table>

            <?php
            if($data['page_links']) {
                echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $data['page_links'] . '</div></div>';
            }
            ?>

            <?php else : ?>
            Histórico está vazio
            <?php endif ?>
        </div>
    </div>
    
<div style="clear:both"></div>

