<?php settings_errors() ?>

<div class="wrap">
    <h2>HTTPS</h2>

    <p>
    	Conteúdos que possuem links com HTTP em vez de HTTPS.
    </p>

    <div>
    	<table class="wp-list-table widefat fixed striped">
    		<thead>
    			<tr>
    				<th>Tipo</th>
                    <th>Quantidade</th>
    			</tr>
    		</thead>
    		<?php foreach ($data['tabela'] as $item) : ?>
    		<tr>
    			<td><?= $item['tipo'] ?></td>
                <td><?= $item['qtde'] ?></td>
    		</tr>
    		<?php endforeach; ?>
    	</table>
    </div>

    <form id="post" name="post" method="post" action="admin.php?page=seo&tab=https">
        <div class="submit">
            <input type="submit" name="corrigir" value="Corrigir Links" class="button button-primary" /> 
        </div>
    </form>
</div>