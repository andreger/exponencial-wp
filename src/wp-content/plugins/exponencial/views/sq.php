<?php settings_errors() ?>

<div class="wrap">
    <h2>Sistema de Questões</h2>

    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">

		        <form id="post" name="post" method="post" action="admin.php?page=sq">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                     	<div class="option">
                     		<p>
                     			Quantidade de questões diárias <strong>*</strong>
                  		 		<input required="required" type="text" name="cfg_sq_qtde_questoes_diarias" class="widefat" value="<?= isset($cfg_sq_qtde_questoes_diarias) ? $cfg_sq_qtde_questoes_diarias : '' ?>" />
                  		 	</p>  
                      </div>
                      
  		                <div class="submit">
  		                    <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
  		                </div>
                        
                    </div>
                </form>

                <div><strong>*</strong> Campos obrigatórios</div>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>
</div>