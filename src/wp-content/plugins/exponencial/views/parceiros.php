<?php settings_errors() ?>
<div class="wrap">
    <h2>
    	<?php echo $this->plugin->displayName; ?> - 
    		Parceiros</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" name="post" method="post" action="admin.php?page=parceiros">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">      
                        <div class="option">
                            <p>
                              <strong>Parceiro</strong>
                              <select name="parceiro_id" class="widefat">
                                <?php foreach ($parceiros as $key => $value) : ?>
                                  <option value="<?= $key ?>"><?= $value ?></option>
                                <?php endforeach ?>                            
                              </select>
                            </p>           
                        </div>

                        <div class="option">
                                <p>
                                    <strong>Nível</strong>
                                    <input required="required" type="text" name="nivel" class="widefat" />
                                </p>  
                        </div>

                        <div class="option">
                         		<p>
                         			<strong>Slug do cupom</strong>
                      		 		<input required="required" type="text" name="cupom_id" class="widefat" />
                      		 	</p>  
                        </div>
                       
		                <div class="submit">
		                    <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
		                </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>


<?php $parceiros_cupons = listar_parceiros_cupons(); if(count($parceiros_cupons)) : ?>
<div class="wrap">
    <h2>Cupons Bônus Cadastrados</h2>
    <div>
    	<table class="wp-list-table widefat fixed striped">
    		<thead>
    			<tr>
    				<th>Parceiro</th>
                    <th>Nível</th>
    				<th>Cupom</th>
    				<th>Ação</th>
    			</tr>
    		</thead>
    		<?php foreach ($parceiros_cupons as $item) : ?>
    		<tr>
    			<td><?= get_parceiro_by_id($item->par_id) ?></td>
                <td><?= $item->par_nivel ?></td>
    			<td><?= $item->cup_id ?></td>
    			<td>
    				<a onclick="return confirm('Deseja realmente excluir este item ?');" href="admin.php?page=parceiros&remove_p=<?= $item->par_id ?>&remove_c=<?= $item->cup_id ?>">Excluir</a>
    			</td>
    		</tr>
    		<?php endforeach; ?>
    	</table>
    </div>
</div>
<?php endif; ?>
