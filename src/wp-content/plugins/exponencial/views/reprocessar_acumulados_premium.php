<?php settings_errors() ?>

<link href="/questoes/assets-admin/js/plugins/select2/select2.css" rel="stylesheet">
<link href="/questoes/assets-admin/js/plugins/select2/select2-bootstrap.css" rel="stylesheet">
<script src="/questoes/assets-admin/js/plugins/select2/select2.js"></script>

<div class="wrap">
    <h2>Reprocessar Acumulados Premium</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">

		        <form id="post" name="post" method="post" action="admin.php?page=reprocessar_acumulados_premium">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                        <div class="option">
                     		<p>
                     			<div style="margin-bottom: 30px">
                     				Selecione os período para reprocessamento
                     			</div>
                     			
                        		
                        		<strong>Período Inicial</strong>
                  		 		
                  		 		<div style="margin-bottom: 20px">
                        			<?= UiSelectHelper::get_select_html("ano_mes_i", $anos_meses_combo, $ano_mes_i ?: null, false, ["class" => "filtro", "style" => "width: 100%"]) ?>
                        		</div>
                        		
                        		<strong>Final</strong>
                  		 		
                  		 		<div>
                        			<?= UiSelectHelper::get_select_html("ano_mes_f", $anos_meses_combo, $ano_mes_f ?: null, false, ["class" => "filtro", "style" => "width: 100%"]) ?>
                        		</div>
                  		 	</p>  
                        </div>
                        
		                <div class="submit">
		                    <input type="submit" name="submit" value="Reprocessar" class="button button-primary" /> 
		                </div>
                    </div>
                </form>
                
               
            </div>
        </div>
     </div>
</div>

<script>
jQuery(function() {
	jQuery(".filtro").select2();
});
</script>