<?php settings_errors() ?>
<div class="wrap">
    <h2><?php echo $this->plugin->displayName; ?> - Mínimo Acumulado para Pagamento de Vendas</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" name="post" method="post" action="admin.php?page=minimo-acumulado-vendas">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                   	<div class="option">
                   		<p>
                   			<strong>Valor Mínimo</strong> (Ex: 100)
                		 		<input type="text" name="valor" class="widefat" />
                		 	</p>  
                    </div>

		                <div class="submit">
		                    <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
		                </div>
                        
                    </div>
                </form>
                
                <table>
		     		<thead>
		     			<th>Data Início</th>
		     			<th>Valor</th>
		     		</thead>
		     	
		     		<tbody>
		     			<?php $resultado = listar_minimos_valores_acumulados(); ?>

		     			<?php foreach($resultado as $item) : ?>
		     			<tr>
		     				<td><?= converter_para_ddmmyyyy($item->vma_data) ?></td>
		     				<td><?= $item->vma_valor ?></td>
		     			</tr>
		     			<?php endforeach; ?>
		     		</tbody>
		     	</table>
            </div>
        </div>
     </div>
</div>

<style>
table {
    border-collapse: collapse;
}

th, td {
    padding: 15px;
    text-align: left;
    border-bottom: 1px solid #ddd;
}

th {
    text-align: left;
}

td {
    width: 200px;
    vertical-align: bottom;
}
</style>

<script>
jQuery(function() {
	jQuery.datepicker.setDefaults({
		dateFormat: 'dd/mm/yy',
	    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
	    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
	    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
	    nextText: 'Próximo',
	    prevText: 'Anterior'
	});
	jQuery('.campo_data').datepicker();
});
</script>