<div class="wrap">
    <h2>
    	<?php echo $this->plugin->displayName; ?> - 
    		Notas Fiscais</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" name="post" method="post" action="admin.php?page=notas-fiscais">
              <h3>Notas Fiscais</h3>
              <div class="option">
                  <p>
                    <input type="checkbox" name="nf_ativar_cadastro" value="1" <?= $nf_ativar_cadastro ? "checked=checked" : "" ?>> Habilitar cadastro de NFs
                  </p>           
              </div>

            <div class="option">
                <p>
                    <input type="checkbox" name="nf_ativar_skip" value="1" <?= $nf_ativar_skip ? "checked=checked" : "" ?>> Habilitar skip
                </p>
            </div>
                       
              <div class="submit">
                  <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
              </div>
                     
            </form>
        </div>
      </div>
    </div>
</div>
<div style="clear:both"></div>