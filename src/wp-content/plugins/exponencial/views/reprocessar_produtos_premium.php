<?php settings_errors() ?>

<link href="/questoes/assets-admin/js/plugins/select2/select2.css" rel="stylesheet">
<link href="/questoes/assets-admin/js/plugins/select2/select2-bootstrap.css" rel="stylesheet">
<script src="/questoes/assets-admin/js/plugins/select2/select2.js"></script>

<div class="wrap">
    <h2><?php echo $this->plugin->displayName; ?> - Reprocessar Produtos Premium</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">

		        <form id="post" name="post" method="post" action="admin.php?page=reprocessar_produtos_premium">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                        <div class="option">
                     		<p>
                     			<div style="margin-bottom: 30px">
                     				Selecione os produtos e o período para reprocessamento
                     			</div>
                     			
                     			<strong>Produto Premium</strong>
                  		 		
                  		 		<div style="margin-bottom: 20px">

									<?= UiSelectHelper::get_select_html("produtos_selecionados[]", $produtos_combo, null, true, ["class" => "filtro filtro-produtos", "style" => "width: 100%"]) ?>

                        			<a href="#" id="rep-pre-sel-tudo">Selecionar Tudo</a>
                        		</div>
                        		
                        		<strong>Período</strong>
                  		 		
                  		 		<div>
                        			<?= UiSelectHelper::get_select_html("ano_mes_selecionado", $anos_meses_combo, null, false, ["class" => "filtro", "style" => "width: 100%"]) ?>
                        		</div>
                  		 	</p>  
                        </div>
                        
		                <div class="submit">
		                    <input type="submit" name="submit" value="Reprocessar" class="button button-primary" /> 
		                </div>
                    </div>
                </form>

                 <div>

                	<?php if($agendamentos) : ?>
                	<table class="wp-list-table widefat fixed striped">
                		<thead>
                			<tr>
                				<th>Data da solicitação</th>
                				<th>Id do Curso</th>
                				<th>Nome do Curso</th>
                				<th>Período</th>
                				<th>Status</th>
                			</tr>
                		</thead>
                		
                		<tbody>
                			<?php foreach ($agendamentos as $agendamento) : ?>
                			<tr>
                				<td><?= converter_para_ddmmyyyy_HHiiss($agendamento->pra_data) ?></td>
                				<td><?= $agendamento->ID ?></td>
                				<td><?= $agendamento->post_title ?></td>
                				<td><?= PremiumHelper::formatar_ano_mes($agendamento->pra_ano_mes) ?></td>
                				<td><?= PremiumHelper::get_status_agendamento_nome($agendamento->pra_status) ?></td>
                			</tr>
                			<?php endforeach ?>
                		</tbody>
                	</table>
                	<?php else : ?>
                		Nenhum reprocessamento foi solicitado.
                	<?php endif ?>
				</div>
            </div>
        </div>
     </div>
</div>

<script>
jQuery(function() {
	jQuery(".filtro").select2();

	jQuery("#rep-pre-sel-tudo").click(function(e) {
		e.preventDefault();
		jQuery('.filtro-produtos option').attr('selected', true).parent().trigger('change');
	});

	setTimeout(function () { 
      location.reload();
    }, 60 * 1000);
});
</script>