<div class="wrap">
    <h2>
    	<?php echo $this->plugin->displayName; ?> - 
    		Captura de Saída</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" name="post" method="post" action="admin.php?page=saida">
                    <?php if($saida) : ?>
                        <input type="hidden" name="id" value="<?= $saida->sai_id ?>">
                    <?php endif ?>

                    <div class="option">
                        <p>
                            <strong>Título * (ATENÇÃO: O título NÃO é usado no layout da saída.)</strong>
                            <input required="required" type="text" name="titulo" class="widefat" value="<?= isset($saida) ? $saida->sai_titulo : '' ?>" />
                        </p>  
                    </div>

		            <div class="option">
                        <p>
                          <strong>Mensagem *</strong>
                          <?= wp_editor(isset($saida) ? html_entity_decode(stripcslashes($saida->sai_html)) : '', 'html') ?>
                        </p>           
                    </div>
                    
                    <div class="option">
                        <p>
                          <strong>Cor de fundo</strong>
                          <input type="text" name="fundo" class="widefat color-picker" value="<?= isset($saida) ? $saida->sai_fundo : '' ?>">
                        </p>  
                    </div>
                    
                    <div class="option">
                        <p>
                          <strong>Link de Destino</strong>
                          <input type="text" name="link" class="widefat" value="<?= isset($saida) ? $saida->sai_link : '' ?>" />
                        </p>  
                    </div>

                    <div class="option">
                        <p>
                          <strong>Data do início *</strong>
                          <input required="required" type="text" name="data_inicio" class="widefat campo_data" value="<?= isset($saida) ? converter_para_ddmmyyyy($saida->sai_data_inicio) : '' ?>" />
                        </p>  
                     </div>

                     <div class="option">
                        <p>
                          <strong>Data do fim *</strong>
                          <input required="required" type="text" name="data_fim" class="widefat campo_data" value="<?= isset($saida) ? converter_para_ddmmyyyy($saida->sai_data_fim) : '' ?>" />
                        </p>  
                    </div>

                    <div class="option">
                        <p>
                          <strong>URLs </strong> (separados por vírgula)
                          <textarea name="urls" class="widefat"><?= $saida ? trim($saida->sai_urls) : '' ?></textarea>
                        </p>           
                    </div>

                    <div class="option">
                        <p>
                          <strong>Palavras-chave</strong> (separadas por vírgula)
                          <textarea name="tags" class="widefat"><?= $saida ? trim($saida->sai_tags) : '' ?></textarea>
                        </p>           
                    </div>
                    
                    <div class="option">
                        <p>
                          <strong>URLs negativas</strong> (separadas por vírgula)
                          <textarea name="urls_negativas" class="widefat"><?= $saida ? trim($saida->sai_urls_negativas) : '' ?></textarea>
                        </p>           
                    </div>
                   
                   <div class="option">
                        <p>
                          <strong>Palavras negativas</strong> (separadas por vírgula)
                          <textarea name="palavras_negativas" class="widefat"><?= $saida ? trim($saida->sai_palavras_negativas) : '' ?></textarea>
                        </p>           
                    </div>

                    <div class="option">
                      <p>
                        <strong>Tempo para reabertura</strong> (Número de dias. Use 0 para reabrir a cada sessão do navegador.)
                        <input type="text" name="dias" class="widefat" value="<?= isset($saida) ? $saida->sai_dias_expiracao : 0 ?>" />
                      </p>   
                    </div>

                    <div class="option">
                 		<p>
                 			<strong>Prioridade</strong> (Maior valor, maior prioridade)
              		 		<input type="text" name="prioridade" class="widefat" value="<?= $saida ? $saida->sai_prioridade : '' ?>" />
              		 	</p>  
                    </div>  
                        
	                <div class="submit">
	                    <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 

                      <a href="admin.php?page=saida" class="button button-primary">Voltar</a> 
	                </div>
                        
                </form>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>
<script>
jQuery(function() {
  jQuery.datepicker.setDefaults({
    dateFormat: 'dd/mm/yy',
      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
      dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
      nextText: 'Próximo',
      prevText: 'Anterior'
  });
  jQuery('.campo_data').datepicker();
  jQuery('.color-picker').wpColorPicker();
});
</script>