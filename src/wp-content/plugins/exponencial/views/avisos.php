<?php settings_errors() ?>

<div class="wrap">
	<a class="button button-primary" href="admin.php?page=editar_aviso">Novo Aviso</a>
</div>



<?php if(!isset($aviso)) : ?>
<div class="wrap">
	<h2>Avisos Cadastrados</h2>
	<div>
		<table class="wp-list-table widefat fixed striped">
			<thead>
				<tr>
					<th>Título</th>
					<th>Link</th>
					<th>Data início</th>
					<th>Data fim</th>
					<th>Link</th>
					<th>URLs</th>
					<th>Palavras-chave</th>
					<th>URLs negativas</th>
					<th>Palavras negativas</th>
					<th>Tempo para reabertura</th>
					<th>Ação</th>
				</tr>
			</thead>
	    		<?php foreach ($avisos as $aviso) : ?>
	    		<tr>
				<td><?= $aviso['titulo']?></td>
				<td><?= $aviso['link']?></td>
				<td><?= converter_para_ddmmyyyy($aviso['data_inicio']) ?></td>
				<td><?= converter_para_ddmmyyyy($aviso['data_fim']) ?></td>
				<td><?= $aviso['link']?></td>
				<td><?= $aviso['urls']?></td>
				<td><?= $aviso['palavras']?></td>
				<td><?= $aviso['urls_negativas']?></td>
				<td><?= $aviso['palavras_negativas']?></td>
				<td><?= $aviso['dias_expiracao']?></td>
				<td><a href="admin.php?page=editar_aviso&id=<?= $aviso['id'] ?>">Editar</a>
					| <a
					onclick="return confirm('Deseja realmente excluir este item ?');"
					href="admin.php?page=avisos&remove=<?= $aviso['id'] ?>">Excluir</a>
				</td>
			</tr>
	    		<?php endforeach; ?>
	    	</table>
	</div>
</div>
<?php endif; ?>