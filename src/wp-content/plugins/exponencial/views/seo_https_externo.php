<?php settings_errors() ?>

<div class="wrap">
    <h2>HTTPS Externo</h2>

    <p>
    	Conteúdos que possuem links externos com HTTP em vez de HTTPS.
    </p>


    <form id="post" name="post" method="post" action="admin.php?page=seo&tab=https_externo">
        <div class="submit">
            <input type="submit" name="atualizar" value="Atualizar tabela" class="button button-primary" /> 
        </div>
    </form>
    <div>
    <?php if($data['resultado']): ?>
    	<table class="wp-list-table widefat fixed striped">
    		<thead>
    			<tr>
    				<th>Nome</th>
    				<th>Tipo</th>
                    <th></th>
    			</tr>
    		</thead>
    		<?php foreach ($data['resultado'] as $seo_http_externo) : ?>
        		<tr>
                    <td><?= $seo_http_externo['she_titulo'] ?></td>
        			<td><?= SeoHTTPSHelper::get_tipo_nome($seo_http_externo['she_tipo']) ?></td>
                    <td><a href="<?= $seo_http_externo['she_edit_url'] ?>" target="_blank">Editar</a></td>
        		</tr>
    		<?php endforeach; ?>
    	</table>
    	
    	<?php
            if($data['page_links']) {
                echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $data['page_links'] . '</div></div>';
            }
        ?>

    <?php else : ?>
    	Não foram encontrados HTTP Externos.
    <?php endif; ?>

    </div>
</div>