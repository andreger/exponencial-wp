<!--=== HTML para MailChimp Footer ===-->
<div style="border-top: 1px solid #eee; padding: 30px 0">
    <div class="container">
    	<form method="post" action="/" id="frm-mc-footer">
            <div class="row">
            	<div class="col-md-4">
            		<div class="font-weight-bold text-blue pt-md-1 text-center" style="font-size: 30pt; margin-bottom: 20px">Cadastro GRÁTIS</div>
            		<div class="font-weight-bold text-blue pt-md-1 text-center" style="line-height: 30px; font-size: 18pt">Acesso exclusivo a notícias atualizadas e promoções.</div>
            	</div>
            	
                <div class="col-md-4">
                	
                	<input type="text" class="form-control" placeholder="Nome" name="nome">
                	<input type="text" class="form-control mt-3" placeholder="Telefone" name="telefonemc" id="telefonemc">
                	<input type="text" class="form-control mt-3" placeholder="E-mail" name="email">
                	
                	<select class="form-control mt-3" name="area" id="area">
                		
                	</select>
        
                </div>
                <div class="col-md-4">
                	<div style="width: 304px">
                    	<div>
                    		<?= carregar_recaptcha() ?>
                    	</div>
                    	<div style="font-size: 10px; margin: 10px 0;" class="text-justify">
                    		<input type="checkbox" name="concordo" id="concordo" checked="checked"> 
                    		Você concorda com a nossa <a href="/politica-de-privacidade">Política de Privacidade</a> e aceita receber informações adicionais do Exponencial Concursos.
                    	</div>
            			<button id="mc-cadastrar" style="color: #fff !important;background-color: #0a67a9 !important;" type="submit" value="1" name="mc-submit" class="btn u-btn-darkblue font-14 font-arial font-weight-bold col-12">Cadastrar</button>
        			</div>
        		</div>
            </div>
        </form>
    </div>
</div>

<div class="remodal" data-remodal-id="mc-modal">
    <button data-remodal-action="close" class="remodal-close"></button>    
    
    <div class="mc-carregando contato form_outter" style="display: none">
        <div class="mensagem-panel">
    	   <div style="padding: 10px"><img width="80" src='/wp-content/themes/academy/images/carregando.gif'></div>
    	   <div style="padding: 10px">Aguarde só mais um momento...</div>
    	   <div style="padding: 10px">Estamos realizando seu cadastro!</div>
        </div>
    </div>

    <div class="mc-sucesso mc-response contato form_outter" style="display: none">
        <div class="mensagem-panel">
            <img src="<?= get_tema_image_url("confirm-grande.png") ?>" width="150">
            <div class="p-5 font-20">
            	Seu cadastro foi realizado com sucesso.
            </div>
            <button data-remodal-action="cancel" class="btn btn-primary">Fechar</button>
        </div>
    </div>

    <div class="mc-erro mc-response contato form_outter" style="display: none">
        <div class="mensagem-panel">
            <img src="<?= get_tema_image_url("erro-grande.png") ?>" width="150">
            <div class="p-5 font-20">
            	Você precisa selecionar o campo<br>"Não sou um robô".
            </div>
            <button data-remodal-action="cancel" class="btn btn-danger">Fechar</button>
        </div>
    </div>
</div>
<!--=== Fim HTML para MailChimp Footer ===-->