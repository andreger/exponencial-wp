<?php settings_errors() ?>
<div class="wrap">
    <h2><?= $this->plugin->displayName; ?> - Painel de Controle</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" name="post" method="post" action="admin.php?page=painel_controle">
                    <div class="wrap">
                        <h2>Áreas do Sistema</h2>
                        <div>
                            <?php $areas = listar_painel_controle(); if(count($areas) > 0) : ?>
                                <table class="wp-list-table widefat fixed striped">
                                    <thead>
                                        <tr>
                                            <th>Área</th>
                                            <th>Ativo</th>
                                        </tr>
                                    </thead>
                                    <?php foreach ($areas as $area) : ?>
                                    <tr>
                                        <td><?= $area['pai_area'] ?></td>
                                        <td><input type="checkbox" name="area<?= $area['pai_id'] ?>" value="1" <?= $area['pai_ativo']?"checked":"" ?> /></td>
                                    </tr>
                                    <?php endforeach; ?>
                                </table>
                                <div class="submit">
                                    <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
                                    <br/>
                                    <div class="wrap">
                                        Total de e-mails aguardando o aviso de fim da manutenção: <?= contar_email_painel_controle_aviso(); ?>.<br/>
                                        <input type="submit" name="enviar_email" value="Enviar e-mail com aviso de fim da manutenção" class="button button-primary" /> 
                                        <input type="submit" name="excluir_email" value="Excluir e-mails aguardando fim da manutenção" class="button button-primary" /> 
                                    </div>
                                </div>
                            <?php else : ?>
                                <div style="margin: 20px">
                                    Não existem áreas do sistema cadastradas
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </form>

                <hr>

                <form id="post" name="post" method="post" action="admin.php?page=editar_cronjob">
                    <div class="wrap">
                        <h2>Cronjobs</h2>
                        <div>
                            <?php $cronjobs = listar_cronjobs(); if(count($cronjobs) > 0) : ?>
                                <table class="wp-list-table widefat fixed striped">
                                    <thead>
                                        <tr>
                                            <th>Minuto</th>
                                            <th>Hora</th>
                                            <th>Dia</th>
                                            <th>Mês</th>
                                            <th>Dia da Semana</th>
                                            <th>Comando</th>
                                            <th>Ativo</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <?php foreach ($cronjobs as $item) : ?>
                                    <tr>
                                        <td><?= $item['cro_minuto'] ?></td>
                                        <td><?= $item['cro_hora'] ?></td>
                                        <td><?= $item['cro_dia'] ?></td>
                                        <td><?= $item['cro_mes'] ?></td>
                                        <td><?= $item['cro_dia_semana'] ?></td>
                                        <td><?= $item['cro_comando'] ?></td>
                                        <td><?= $item['cro_status'] == SIM ? "Sim" : "Não" ?></td>
                                        <td>
                                            <a href="admin.php?page=editar_cronjob&id=<?= $item['cro_id'] ?>">Editar</a> | 
                                            <a onclick="return confirm('Deseja realmente excluir este item ?');"  href="/wp-admin/admin.php?page=painel_controle&ec=<?= $item['cro_id'] ?>">Excluir</a>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </table>
                            <?php else : ?>
                                <div style="margin: 20px">
                                    Não existem cronjobs cadastrados.
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="submit">
                            <a href="/wp-admin/admin.php?page=editar_cronjob" class="button button-primary">Novo Cronjob</a> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>
