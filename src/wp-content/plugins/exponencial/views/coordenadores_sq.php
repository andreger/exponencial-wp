<?php settings_errors() ?>
<div class="wrap">
    <h2>
    	<?php echo $this->plugin->displayName; ?> - 
    		Coordenador SQ</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" name="post" method="post" action="admin.php?page=coordenadores-sq">
  	            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">      <div class="option">
                        <p>
                          <b>Professor</b>
                          <select class="widefat" name="user_id" >
                          <?php foreach (get_professores_combo_options("Selecione um professor", 0, FALSE) as $key => $value) : ?>
                            <option value="<?= $key ?>" > <?= $value ?> </option>
                          <?php endforeach ?>
                          </select>
                      </div>

                     	<div class="option">
                     		<p>
                     			<b>% de participacao (Ex: 4.5 para 4,5%) </b>
                  		 		<input required="required" type="text" name="percentual" class="widefat" value="" />
                  		 	</p>  
                      </div>

                       
		                <div class="submit">
		                    <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
		                </div>
                        
                    </div>
                </form>
            </div>

            <?php $coordenadores = listar_coordenadores_sq_ativos(); ?>

            <?php if($coordenadores) : ?>
            <div>
             <h2>Participação dos Coordenadores</h2>

              <table class="widefat fixed">
                <thead>
                  <th>Coordenador</th>
                  <th>Participação (%)</th>
                  <th></th>
                </thead>
              
                <tbody>

                  <?php foreach($coordenadores as $coordenador) : ?>
                  <tr>
                    <td><?= $coordenador->display_name ?></td>
                    <td><?= $coordenador->percentual ?></td>
                    <td>
                      <div class="">
                        <a href="/wp-admin/admin.php?page=coordenadores-sq-historico&user_id=<?= $coordenador->user_id ?>">Histórico</a> | 
                        <a href="/wp-admin/admin.php?page=coordenadores-sq&remover=<?= $coordenador->user_id ?>" onclick="return confirm('Confirma exclusão do item?');">Remover</a>
                      </div>
                    </td>
                </div>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>
            </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<div style="clear:both"></div>

