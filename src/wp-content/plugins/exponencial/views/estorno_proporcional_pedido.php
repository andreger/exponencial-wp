<?php settings_errors() ?>

<?php
$aluno = get_usuario_array($pedido->get_user_id());
$total = $pedido->get_total() + $pedido->get_total_discount();
?>

<div class="wrap">
  <h2><?php echo $this->plugin->displayName; ?> - Novo Estorno Proporcional</h2>
  <div id="poststuff">
  	<div id="post-body" class="metabox-holder columns-2">
  		<!-- Content -->
  		<div id="post-body-content">
          <h2>Pedido <?= $pedido->get_id() ?></h2>
          
          <?php if($pedido->get_date_completed()) : ?>
          <div style="margin: 10px 0">
            <strong>Data:</strong> <?= $pedido->get_date_completed()->format('d/m/Y') ?>
          </div>
          <?php endif ?>
          
          <div style="margin: 10px 0">
            <strong>Aluno:</strong> <?= $aluno['nome_completo'] ?>
          </div>
          
          <div style="margin: 10px 0">
            <strong>E-mail:</strong> <?= $aluno['email'] ?>
          </div>
          
          <div style="margin: 10px 0">
            <strong>Valor:</strong> <?= moeda($pedido->get_total()) ?>
          </div>

          <div style="margin: 10px 0">
            <strong>Valor reembolsado:</strong> <?= moeda($pedido->get_total_refunded()) ?>
          </div>

          <div style="margin: 10px 0">
            <strong>Valor restante:</strong> <?= moeda($pedido->get_remaining_refund_amount()) ?>
          </div>

          <hr style="margin: 30px 0">

          <h2>Estorno por Produtos</h2>
  			  
          <!-- Form Start -->
          <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">
            <?php if($tem_estorno_por_valor): ?>     
              <p>Indisponível. Pedido já foi estornado por valor</p>       
              <a href="/wp-admin/admin.php?page=estorno-proporcional-novo" class="button button-primary">Voltar</a>
            <?php else: ?>            
  	        <form id="post" name="post" method="post" action="admin.php?page=estorno-proporcional-pedido&id=<?= $pedido->get_id() ?>">
  	          <?php if($pedido) : ?>
  	          <?php foreach ($pedido->get_items() as $item) : ?>
              <?php 
                $is_item_pacote = is_item_de_pacote($item); 
                $is_item_raiz_de_pacote = is_item_raiz_de_pacote($item);
                if($is_item_raiz_de_pacote){
                  $pai_id = $item->get_id();
                }
              ?>
              <div class="option">
                  <p>
                    <?php if(is_item_pedido_estornado($pedido->get_id(), $item->get_id())) : ?>
                      <span style="<?= $is_item_pacote?'margin-left: 20px;':'' ?> text-decoration: line-through;"> <?= $item->get_name() ?>
                    <?php else : ?>
                      <input type="checkbox" name="itens_ids[]" value="<?= $item->get_id() ?>" style="<?= $is_item_pacote?'margin-left: 20px;':'' ?>" data-is-pai="<?= $is_item_raiz_de_pacote?$pai_id:'' ?>" data-pai-id="<?= $is_item_pacote?$pai_id:'' ?>" class="check_produto" > <?= $item->get_name() ." (Disponível " . moeda($valores_produtos[$item->get_id()] - $pedido->get_total_refunded_for_item($item->get_id())) . " de " . moeda($valores_produtos[$item->get_id()]) . ")" ?>
                    <?php endif; ?>
                  </p>
              </div>  
              <?php endforeach; ?>
              <?php endif ?>
              <div class="option">
                        <p>
                          <strong>Motivo</strong>
              <textarea name="motivo" class="widefat"></textarea>
              </p>
            </div>  
          
            <div class="submit">
                <input type="submit" name="submit_estornar" value="Estornar Selecionados" class="button button-primary" /> 
                <a href="/wp-admin/admin.php?page=estorno-proporcional-novo" class="button button-primary">Voltar</a>
            </form>
            <?php endif; ?>            
          </div>

        <div style="margin-bottom: 40px;"></div>
         

        <hr style="margin: 30px 0">

          <h2>Estorno por Valor</h2>
          
          <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
            <?php if($tem_estorno_por_produto): ?>     
              <p>Indisponível. Pedido já foi estornado por produto</p>  
              <a href="/wp-admin/admin.php?page=estorno-proporcional-novo" class="button button-primary">Voltar</a>     
            <?php else: ?> 
            <form id="post" name="post" method="post" action="admin.php?page=estorno-proporcional-pedido&id=<?= $pedido->get_id() ?>">
              <div class="option">
                  <p>
                    <strong>Valor</strong> <small> (Formato: X.XXX,XX)</small>
                    <input type="text" name="valor" class="widefat valor" value="<?= isset($_POST['valor']) ? $_POST['valor'] : '' ?>">
                </p>
              </div>  
              <div class="option">
                  <p>
                    <strong>Produtos do estorno:</strong>
                  </p>
              </div> 
              <?php if($pedido) : ?>
              <?php foreach ($pedido->get_items() as $item) : ?>
                <?php 
                  $is_item_pacote = is_item_de_pacote($item); 
                  $is_item_raiz_de_pacote = is_item_raiz_de_pacote($item);
                  if($is_item_raiz_de_pacote){
                    $pai_id = $item->get_id();
                  }
                ?>
                <div class="option">
                  <p>
                      <?php 
                      //if(is_item_pedido_estornado($pedido->get_id(), $item->get_id(), TRUE)) :
                      $disponivel_str = " (Disponível " . moeda(round($valores_produtos[$item->get_id()], 2) - $pedido->get_total_refunded_for_item($item->get_id())) . " de " . moeda($valores_produtos[$item->get_id()]) . ")";
                      
                      if( ( round($valores_produtos[$item->get_id()], 2) - $pedido->get_total_refunded_for_item($item->get_id()) ) == 0 ) :
                      ?>
                        <span style="<?= $is_item_pacote?'margin-left: 20px;':'' ?> text-decoration: line-through;"> <?= $item->get_name() . $disponivel_str  ?>
                      <?php else : ?>
                        <input type="checkbox" name="itens_ids[]" value="<?= $item->get_id() ?>" <?= isset($itens_ids)?(in_array($item->get_id(), $itens_ids)?"checked":""):"checked" ?> style="<?= $is_item_pacote?'margin-left: 20px;':'' ?>" data-is-pai="<?= $is_item_raiz_de_pacote?$pai_id:'' ?>" data-pai-id="<?= $is_item_pacote?$pai_id:'' ?>" class="check_produto" /> <?= $item->get_name() . $disponivel_str  ?>
                      <?php endif; ?>
                  </p>
                </div>
              <?php endforeach ?>
              <?php endif ?>
              <div class="option">
                  <p>
                    <strong>Cortar acesso do aluno: </strong> <input type="checkbox" name="revoga_acesso" value="1" <?= isset($revoga_acesso)&&$revoga_acesso?"checked":"" ?> />
                  </p>
              </div>
              <div class="option">
                          <p>
                            <strong>Motivo</strong>
                <textarea name="motivo" class="widefat"><?= isset($motivo)?$motivo:"" ?></textarea>
                </p>
              </div>  
            
              <div class="submit">
                  <input type="submit" name="submit_estornar_valor" value="Estornar Valor" class="button button-primary" /> 
                  <a href="/wp-admin/admin.php?page=estorno-proporcional-novo" class="button button-primary">Voltar</a>
          </form>
        <?php endif; ?>
      </div>
       
    </div>
  </div>
</div>
<div style="clear:both"></div>
<script>
jQuery(function()
{
	jQuery(".valor").attr('maxlength','10');

	jQuery(".check_produto").change(function(e){
      if(jQuery(this).data("is-pai")){

    	  jQuery(this).closest("form")
            .find("[data-pai-id='" + jQuery(this).val() + "']")
              .prop( "checked", jQuery(this).is(":checked") );

      }else if(jQuery(this).data("pai-id")){

        if(jQuery(this).is(":checked")){
          var todos =  jQuery(this).closest("form")
                        .find("[data-pai-id='" + jQuery(this).data("pai-id") + "']")
                        .size();
          var selecionados = jQuery(this).closest("form")
                              .find("[data-pai-id='" + jQuery(this).data("pai-id") + "']:checked")
                              .size();
          if(todos == selecionados){
            jQuery(this).closest("form")
              .find("[data-is-pai='" + jQuery(this).data("pai-id") + "']")
                .prop( "checked", true );
          }
        }else{
          jQuery(this).closest("form")
            .find("[data-is-pai='" + jQuery(this).data("pai-id") + "']")
              .prop( "checked", false );
        }
        
      }
    });

});
</script>

