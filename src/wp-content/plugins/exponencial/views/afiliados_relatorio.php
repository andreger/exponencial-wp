<?php settings_errors() ?>
<div class="wrap">
    <h2>
    	<?= $this->plugin->displayName; ?> - Afiliados - Relatório</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->

            <h3>CSV</h3>
            <form id="form-csv" name="form-csv" method="post" action="admin.php?page=afiliados-relatorio" enctype="multipart/form-data">
                <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                      <div class="option">
                        <p>
                          <strong>Arquivo CSV</strong>
                          <label>
                            <span class="button button-primary">Enviar Arquivo </span> 
                            <input required="required" type="file" name="csv" class="widefat" style="display: none" />
                          </label>
                        </p>  
                      </div>
                      
                      <div style="margin-top: 20px">
                          <input type="submit" name="submit_csv" value="Enviar" class="button button-primary" /> 
                          <a href="/wp-admin/admin.php?page=folha-dirigida" class="button button-primary">Voltar </a> 
                      </div>
                        
                    </div>
                </form>

                <?php if(isset($_POST['submit_csv'])) : ?>
                  <h3>Resultado do Processamento</h3>
                  
                  <div><?= $tabela ?></div>
                  
                  <div style="margin-top: 20px">
                      <a href="/wp-content/temp/afiliados.xls" class="button button-primary">Exportar XLS </a> 
                      <a href="/wp-admin/admin.php?page=afiliados" class="button button-primary">Voltar </a> 
                  </div>
                  
                <?php endif ?>
              </div>
            </div>
        </div>
<div style="clear:both"></div>