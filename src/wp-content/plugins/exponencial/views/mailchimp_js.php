<!--=== JS para MailChimp Footer ===-->
<link rel='stylesheet' id='remodal.min-css'  href='/wp-content/themes/academy/css/remodal.css' type='text/css' media='all' />
<link rel='stylesheet' id='remodal-default-theme-css'  href='/wp-content/themes/academy/css/remodal-default-theme.css' type='text/css' media='all' />
<script type='text/javascript' src='/wp-content/themes/academy/js/remodal.min.js'></script>
<script>
$(function() {
     $("#frm-mc-footer").submit( function(e) {
			e.preventDefault();
         }).validate({
         rules: {
             email: {
                 required: true,
                 email: true,
             },
             nome: {
                 required: true,
                 onlychar: true
             },
             telefonemc: {
                 required: true,
                 brphone: true
             },
             area: {
				 required: true,
             },
             concordo: {
				 required: true,
             }
         },
         messages: {
             email: {
                 required: "E-mail é obrigatório",
                 email: "Formato do e-mail inválido",
             },
             nome: {
                 required: "Nome é obrigatório",
                 onlychar: "O nome deve conter apenas letras e espaços"
             },
             telefonemc: {
                 required: "Telefone é obrigatório",
                 brphone: "Formato do telefone inválido"
             },
             area: {
				 required: "Área de interesse é obrigatória",
             },
             concordo: {
				 required: "Você precisa concordar com a política de privacidade",
             }
         },
         errorPlacement: function(error, element) {
            error.insertAfter(element);
         },
         submitHandler: function( form ){
        	 	$(".mc-response").hide();
        	 	$(".mc-carregando").show();
             
        	 	var inst = $('[data-remodal-id=mc-modal]').remodal();
        	 	inst.open();
        	 	
				var dados = $( form ).serialize();

				$.ajax({
					type: "POST",
					url: "/wp-content/themes/academy/ajax/enviar_mc_footer.php",
					data: dados,
					success: function( data )
					{
						$(".mc-carregando").hide();
						
						if(data == "1") {
							$(".mc-sucesso").show();
						}
						else {
							$(".mc-erro").show();
						}

						grecaptcha.reset();
					}
				});

				return false;
			}
     });
 
     var SPMaskBehavior = function (val) {
     	return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
     },
     spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
          }
     };
 
     $('#telefonemc').mask(SPMaskBehavior, spOptions); 

     $.get('/wp-content/themes/academy/ajax/mailchimp_areas.php', function(data) {
		$('#area').html(data);
     });
});
</script>
<!--=== JS para MailChimp Footer ===-->