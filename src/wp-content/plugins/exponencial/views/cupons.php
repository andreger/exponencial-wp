<?php settings_errors() ?>
<div class="wrap">
    <h2>
    	<?php echo $this->plugin->displayName; ?> - 
    	<?php echo isset($cupom) ? "Editar" : "Novo" ?>
    		Cupons Bônus</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" name="post" method="post" action="admin.php?page=cupons-bonus">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                     	<div class="option">
                     		<p>
                     			<strong>Slug do cupom</strong>
                  		 		<input required="required" type="text" name="cupom" class="widefat" value="<?php echo isset($cupom) ? $cupom->cupom_slug : '' ?>" />
                  		 	</p>  
                        </div>
                        <div class="option">
                     		<p>
                     			<strong>Slug do item</strong>
                  		 		<input required="required" type="text" name="item" class="widefat" value="<?php echo isset($cupom) ? $cupom->item_slug : '' ?>" />
                  		 	</p>  
                        </div>
                        <div class="option">
                     		<p>
                     			<strong>Tipo do item</strong>
                  		 		<input type="radio" name="tipo" value="1" class="widefat"  <?php echo (isset($cupom) && $cupom->item_tipo == 1) ? 'checked=checked' : '' ?>/> Produto
                  		 		<input type="radio" name="tipo" value="2" class="widefat"  <?php echo (isset($cupom) && $cupom->item_tipo == 2) ? 'checked=checked' : '' ?>/> Categoria de Produtos
                  		 		<?php if(isset($cupom)) : ?>
	                  		 		<input type="hidden" name="cupom_id" value="<?php echo $cupom->cb_id?>" />
                  		 		<?php endif ?>
                  		 	</p>  
                        </div>
		                <div class="submit">
		                    <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
		                </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>

<?php if(!isset($cupom)) : ?>
	<?php $cupons = listar_cupons_bonus(); if(count($cupons)) : ?>
	<div class="wrap">
	    <h2>Cupons Bônus Cadastrados</h2>
	    <div>
	    	<table class="wp-list-table widefat fixed striped">
	    		<thead>
	    			<tr>
	    				<th>Cupom</th>
	    				<th>Item</th>
	    				<th>Tipo</th>
	    				<th>Ação</th>
	    			</tr>
	    		</thead>
	    		<?php foreach ($cupons as $cupom) : ?>
	    		<tr>
	    			<td><?php echo $cupom['cupom_slug']?></td>
	    			<td><?php echo $cupom['item_slug']?></td>
	    			<td><?php echo $cupom['item_tipo'] == 1 ? 'Produto' : 'Categoria' ?></td>
	    			<td>
	    				<a href="admin.php?page=cupons-bonus&edit=<?php echo $cupom['cb_id'] ?>">Editar</a> | 
	    				<a onclick="return confirm('Deseja realmente excluir este item ?');" href="admin.php?page=cupons-bonus&remove=<?php echo $cupom['cb_id'] ?>">Excluir</a>
	    			</td>
	    		</tr>
	    		<?php endforeach; ?>
	    	</table>
	    </div>
	</div>
	<?php endif; ?>
<?php endif; ?>