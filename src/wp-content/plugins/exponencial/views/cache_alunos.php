<?php settings_errors() ?>
<div class="wrap">
    <h2>
    	<?= $this->plugin->displayName; ?> - Cache - Alunos Coaching</h2>
</div>
<div style="clear:both"></div>

<?php 
    
  $max = count($alunos_podio);

  if($max < count($alunos_coaching)){
    $max = count($alunos_coaching);
  }

  if($max) : 
?>
<div class="wrap">
    <div class="submit">
        <a href="admin.php?page=cache-alunos-coaching&action=update" class="button button-primary">Atualizar Cache</a>
    </div>
    <div>
      <table class="wp-list-table widefat fixed striped">
        <thead>
          <tr>
            <th>Coaching Individual (<?= count($alunos_podio) ?>)</th>
            <th>Turma de Coaching (<?= count($alunos_coaching) ?>)</th>
          </tr>
        </thead>
        <?php for ($i = 0; $i < $max; $i++) : ?>
        <tr>
          <td><?= count($alunos_podio) > $i ? $alunos_podio[$i]['nome_completo']." - ".$alunos_podio[$i]['email'] : "" ?></td>
          <td><?= count($alunos_coaching) > $i ? $alunos_coaching[$i]['nome_completo']." - ".$alunos_coaching[$i]['email'] : "" ?></td>
        </tr>
        <?php endfor; ?>
      </table>
    </div>
</div>
<?php endif; ?>
