<div class="wrap">
	<h2>
    	<?= $this->plugin->displayName; ?> - 
    	<?= isset($aviso) ? "Editar" : "Novo" ?>
    		Aviso do Site</h2>
	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">
			<!-- Content -->
			<div id="post-body-content">
				<!-- Form Start -->
				<form id="post" name="post" method="post"
					action="admin.php?page=avisos">

					<div id="normal-sortables"
						class="meta-box-sortables ui-sortable publishing-defaults">
						<div class="option">
							<p>
								<strong>Título * (ATENÇÃO: O título NÃO é usado no layout do
									aviso.)</strong> <input required="required" type="text"
									name="titulo" class="widefat"
									value="<?= isset($aviso) ? $aviso->titulo : '' ?>" />
							</p>
						</div>

						<div class="option">
							<p>
								<strong>Mensagem *</strong>
                  		 		<?= wp_editor(isset($aviso) ? html_entity_decode(stripcslashes($aviso->mensagem)) : '', 'mensagem') ?>
                  		 	</p>
						</div>

						<div class="option">
							<p>
								<strong>Cor de fundo</strong> <input type="text" name="fundo"
									class="widefat color-picker"
									value="<?= isset($aviso) ? $aviso->fundo : '' ?>">
							</p>
						</div>

						<div class="option">
							<p>
								<strong>Link de Destino</strong> <input type="text" name="link"
									class="widefat"
									value="<?= isset($aviso) ? $aviso->link : '' ?>" />
							</p>
						</div>

						<div class="option">
							<p>
								<strong>Data do início *</strong> <input required="required"
									type="text" name="data_inicio" class="widefat campo_data"
									value="<?= isset($aviso) ? converter_para_ddmmyyyy($aviso->data_inicio) : '' ?>" />
							</p>
						</div>

						<div class="option">
							<p>
								<strong>Data do fim *</strong> <input required="required"
									type="text" name="data_fim" class="widefat campo_data"
									value="<?= isset($aviso) ? converter_para_ddmmyyyy($aviso->data_fim) : '' ?>" />
							</p>
						</div>

						<div class="option">
							<p>
								<strong>URLs</strong> (Separadas por vírgula. Ex:
								https://www.exponencialconcursos.com.br/concursos,https://www.exponencialconcursos.com.br/cursos-online)
								<textarea name="urls" class="widefat"><?= isset($aviso) ? $aviso->urls : '' ?></textarea>
							</p>
						</div>

						<div class="option">
							<p>
								<strong>Palavras-chave</strong> (Separadas por vírgula. Ex:
								concurso, icms, abin)
								<textarea name="palavras" class="widefat"><?= isset($aviso) ? $aviso->palavras : '' ?></textarea>
							</p>
						</div>

						<div class="option">
							<p>
								<strong>URLs negativas</strong> (separadas por vírgula)
								<textarea name="urls_negativas" class="widefat"><?= $aviso ? trim($aviso->urls_negativas) : '' ?></textarea>
							</p>
						</div>

						<div class="option">
							<p>
								<strong>Palavras negativas</strong> (separadas por vírgula)
								<textarea name="palavras_negativas" class="widefat"><?= $aviso ? trim($aviso->palavras_negativas) : '' ?></textarea>
							</p>
						</div>

						<div class="option">
							<p>
								<strong>Tempo para reabertura</strong> (Número de dias. Use 0 para reabrir a cada sessão do navegador.) 
								<input
									type="text" name="dias" class="widefat"
									value="<?= isset($aviso) ? $aviso->dias_expiracao : 0 ?>" />
							</p>
						</div>

                          <?php if(isset($aviso)) : ?>
                          <input type="hidden" name="id"
							value="<?= $aviso->id?>" />
                          <?php endif ?>
                      
  		                <div class="submit">
							<input type="submit" name="submit" value="Salvar"
								class="button button-primary" />
							<a href="admin.php?page=avisos" class="button button-primary">Voltar</a>
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div style="clear: both"></div>

<script>
jQuery(function() {
  jQuery.datepicker.setDefaults({
    dateFormat: 'dd/mm/yy',
      dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
      dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
      dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
      monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
      monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
      nextText: 'Próximo',
      prevText: 'Anterior'
  });
  jQuery('.campo_data').datepicker();
  jQuery('.color-picker').wpColorPicker();
});
</script>