<?php settings_errors() ?>
<div class="wrap">
    <h2><?php echo $this->plugin->displayName; ?> - Reprocessar Relatório de Vendas</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<h3>Reprocessar vendas por PERÍODO</h3>
		        <form id="post" name="post" method="post" action="admin.php?page=exponencial-reprocessar">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                        <div class="option">
                     		<p>
                     			<strong>Data de início</strong>
                  		 		<input type="text" name="data_inicio" class="campo_data widefat" required="required" />
                  		 	</p>  
                        </div>
                        <div class="option">
                     		<p>
                     			<strong>Data de fim</strong> (se não for informada o sistema considerará somente a data de início) 
                  		 		<input type="text" name="data_fim" class="campo_data widefat" />
                  		 	</p>  
                        </div>
		                <div class="submit">
		                    <input type="submit" name="submit-periodo" value="Reprocessar" class="button button-primary" /> 
		                </div>
                    </div>
                </form>
                
                <h3>Reprocessar vendas por PEDIDO</h3>
		        <form id="post" name="post" method="post" action="admin.php?page=exponencial-reprocessar">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                        <div class="option">
                     		<p>
                     			<strong>Id do pedido</strong> (order id)
                  		 		<input type="text" name="pedido_id" class="widefat" required="required" />
                  		 	</p>  
                        </div>
		                <div class="submit">
		                    <input type="submit" name="submit-pedido" value="Reprocessar" class="button button-primary" /> 
		                </div>
                    </div>
                </form>
                
                <h3>Reprocessar ACUMULADOS de professores</h3>
		        <form id="post" name="post" method="post" action="admin.php?page=exponencial-reprocessar">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                        <div class="option">
                     		<p>
                     			<strong>Data a partir de</strong> (os meses seguintes serão reprocessados)
                  		 		<input type="text" name="data_ref" class="campo_data widefat" required="required" />
                  		 	</p>  
                        </div>
		                <div class="submit">
		                    <input type="submit" name="submit-acumulado" value="Reprocessar" class="button button-primary" /> 
		                </div>
                    </div>
                </form>
            </div>
        </div>
     </div>
</div>

<script>
jQuery(function() {
	jQuery.datepicker.setDefaults({
		dateFormat: 'dd/mm/yy',
	    dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
	    dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
	    dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
	    monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
	    monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
	    nextText: 'Próximo',
	    prevText: 'Anterior'
	});
	jQuery('.campo_data').datepicker();
});
</script>