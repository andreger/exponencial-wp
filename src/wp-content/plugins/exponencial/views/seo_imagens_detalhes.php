<?php settings_errors() ?>
<div class="wrap">
    <h2>Imagens - Detalhes da Execução #<?= $data['id'] ?></h2>

    <div class="wrap">
        <div>
            <?php if($data['arquivos']) :?>
            <table class="wp-list-table widefat fixed striped">
                <thead>
                <tr>
                    <th>Nome</th>
                    <th style="text-align: center">Tamanho Anterior (KB)</th>
                    <th style="text-align: center">Novo Tamanho (KB)</th>
                    <th style="text-align: center">Redução</th>
                    <th style="text-align: center">Status</th>
                </tr>
                </thead>
                <?php foreach ($data['arquivos'] as $item) : ?>
                <?php 
                    $p = "-";

                    if($item['sid_tamanho_antes'] && ($item['sid_tamanho_antes'] != $item['sid_tamanho_depois'])) {    

                        $p = porcentagem(100 - ($item['sid_tamanho_depois'] / $item['sid_tamanho_antes'] * 100));
                    }
                ?>
                <tr>
                    <td><?= $item['sid_arquivo'] ?></td>
                    <td style="text-align: center"><?= decimal($item['sid_tamanho_antes'] / 1024) ?></td>
                    <td style="text-align: center"><?= decimal($item['sid_tamanho_depois'] / 1024) ?></td>
                    <td style="text-align: center"><?= $p ?></td>
                    <td style="text-align: center"><?= seo_imagens_status_str($item['sid_status']) ?></td>
                </tr>
                <?php endforeach; ?>
            </table>

            <?php
            if($data['page_links']) {
                echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $data['page_links'] . '</div></div>';
            }
            ?>

            <?php else : ?>
            Ainda não foram realizadas execucoes.
            <?php endif ?>

            <a class="button button-primary" style="margin-top: 20px" href="/wp-admin/admin.php?page=seo&tab=imagens">&lt;&lt; Voltar</a>
        </div>
    </div>
</div>
<div style="clear:both"></div>

