<?php settings_errors() ?>

<div class="wrap">
    <h2>H1</h2>
    
    <form id="post" method="get" action="admin.php">
         <div class="option">
            <p>
                <b>Texto</b>
                <input type="text" name="texto" class="widefat" value="<?= isset($_GET['texto']) && $_GET['texto'] ? $_GET['texto'] : "" ?>" />
            </p>  
        </div>

         <div class="option">
            <p>
                <b>Número de H1 encontrados:</b> <br>
                <input type="hidden" name="page" value="seo">
                <input type="hidden" name="tab" value="h1">
                <input type="radio" name="num_tipo" class="widefat" value="0" <?= !isset($_GET['num_tipo']) || !$_GET['num_tipo'] ? "checked=checked" : "" ?> /> Qualquer número <br>
                <input type="radio" name="num_tipo" class="widefat" value="1" <?= isset($_GET['num_tipo']) && $_GET['num_tipo'] == 1 ? "checked=checked" : "" ?> /> Nenhum H1 <br>
                <input type="radio" name="num_tipo" class="widefat" value="2" <?= isset($_GET['num_tipo']) && $_GET['num_tipo'] == 2 ? "checked=checked" : "" ?> /> Mais de 1 H1
            </p>  
        </div>

        <div class="submit">
            <input type="submit" name="filtrar" value="Filtrar" class="button button-primary" /> 
        </div>

    </form>

    <div>
    	<?php if($data['tabela']) : ?>
    	<table class="wp-list-table widefat fixed striped">
    		<thead>
    			<tr>
    				<th>URL</th>
                    <th>Título</th>
                    <th>Qtde H1</th>
                    <th>H1</th>
                    <th>Ações</th>
    			</tr>
    		</thead>
    		
            <?php foreach ($data['tabela'] as $item) : ?>
    		<tr>
    			<td><a href="<?= $item['url'] ?>"><?= $item['url'] ?></a></td>
                <td><?= $item['titulo'] ?></td>
                <td><?= $item['h1_num'] ?></td>
                <td><?= $item['h1_info'] ?></td>
                <td><a href="<?= $item['edit_url'] ?>">Editar</a></td>
    		</tr>
    		<?php endforeach; ?>
    	</table>
    	<?php else : ?>
			<div>Nenhuma página encontrada com esse filtro.</div>
		<?php endif; ?>
		
        <?php
        if($data['page_links']) {
            echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $data['page_links'] . '</div></div>';
        }
        ?>
    </div>
 
</div>