<?php settings_errors() ?>
<div class="wrap">
    <h2>Imagens</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-12">
    		<!-- Content -->
    		<div id="post-body-content">
 
                <form id="post" name="post" method="post" enctype="multipart/form-data" action="admin.php?page=seo&tab=imagens">
            
                    <div class="option">
                        <p>
                            <b>Arquivo CSV</b>
                            <input type="file" name="arquivo" class="widefat" />
                            <i><b>Instruções:</b> O arquivo deve conter os nomes completos das imagens. Cada linha do arquivo deverá ter uma imagem. Exemplo abaixo:<br>
                            https://www.exponencialconcursos.com.br/questoes/uploads/simulados/img_simulado_222.jpg<br>
                            https://www.exponencialconcursos.com.br/questoes/uploads/simulados/img_simulado_223.jpg<br>
                            https://www.exponencialconcursos.com.br/questoes/uploads/simulados/img_simulado_224.jpg</i>
                        </p>  
                    </div>

                    <div class="option">
                        <p>
                            <b>Qualidade</b>
                            <input type="text" name="qualidade" class="widefat" value="70" />
                        </p>  
                    </div>

                    <div class="submit">
                        <input type="submit" name="executar" value="Executar" class="button button-primary" /> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <h2>Histórico</h2>
    <div>
        <?php if($data['execucoes']) :?>
        <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th style="text-align: center">#</th>
                    <th style="text-align: center">Data</th>
                    <th style="text-align: center">Qualidade</th>
                    <th style="text-align: center">Imagens Enviadas</th>
                    <th style="text-align: center">Imagens Processadas</th>
                    <th style="text-align: center">Imagens Com Erro</th>
                    <th>Ações</th>
                </tr>
                </thead>
                <?php foreach ($data['execucoes'] as $execucao) : ?>
                <tr>
                    <td style="text-align: center"><?= $execucao['sie_id'] ?></td>
                    <td style="text-align: center"><?= converter_para_ddmmyyyy_HHii($execucao['sie_data_hora']) ?></td>
                    <td style="text-align: center"><?= $execucao['sie_qualidade'] ?></td>
                    <td style="text-align: center"><?= $execucao['sie_enviados'] ?></td>
                    <td style="text-align: center"><?= $execucao['sie_processados'] ?></td>
                    <td style="text-align: center"><?= $execucao['sie_com_erro'] ?></td>
                    <td><a href="/wp-admin/admin.php?page=seo&tab=imagens_detalhes&sie=<?= $execucao['sie_id'] ?>">Ver Detalhes</a></td>
                </tr>
                <?php endforeach; ?>
            </table>

            <?php
            if($data['page_links']) {
                echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $data['page_links'] . '</div></div>';
            }
            ?>

            <?php else : ?>
            Ainda não foram realizadas execucoes.
            <?php endif ?>
        </div>
    </div>
    
<div style="clear:both"></div>

