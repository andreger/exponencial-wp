<?php settings_errors() ?>
<div class="wrap">
    <h2>
    	<?= $this->plugin->displayName; ?> - 
    	<?= "MailChimp" ?>
    		</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" name="post" method="post" action="admin.php?page=mailchimp">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
  		                <div class="submit">
  		                    <input type="submit" name="atualizar" value="Atualizar Lista" class="button button-primary" /> 
  		                </div>
                        
                    </div>
                </form>
                
                <h2>Interesses por Áreas</h2>
                
                <table class="wp-list-table widefat fixed striped">
                	<thead>
                	<tr>
                		<th>Id</th>
                		<th>Valor</th>
                	</tr>
                	</thead>
                	
                	<tbody>
                	<?php foreach ($areas as $item) : ?>
                	<tr>
                		<td><?= $item->mca_id ?></td>
                		<td><?= $item->mca_nome ?></td>
                	</tr>
                	<?php endforeach ?>
                	</tbody>
                </table>
                
                <div style="margin-top: 50px"></div>
                <!--
                <h2>Snippet para Landing Pages - HTML</h2>
                <div style="color: #ff0000"><i>Inserir esta parte antes do footer</i></div>
                
                <textarea rows="10" style="width: 100%"><?php // include "mailchimp_html.php" ?></textarea>
                
                <div style="margin-top: 50px"></div>
                <h2>Snippet para Landing Pages - Javascript</h2>
                <div style="color: #ff0000"><i>Inserir esta parte antes do PostAffiliatePro</i></div>
                
                <textarea rows="10" style="width: 100%"><?php // include "mailchimp_js.php" ?></textarea>
                -->
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>