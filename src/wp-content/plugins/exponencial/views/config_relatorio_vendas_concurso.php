<?php settings_errors() ?>

<div class="wrap">
    <h2>Relatório de Vendas por Concurso - Configurações</h2>

    <h3>Ordem de prioridade das categorias</h3>

    <p>Para não duplicar contagem de curso com mais de uma categoria, é necessário definir a ordem de prioridade das categorias.</p>

    <p>
        Mais informações em: <a href="https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/kb/items/54">
            https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/kb/items/54</a>
    </p>

    <form name="post" method="post" action="admin.php?page=config_relatorio_vendas_concurso">
        <div class="options">

            <?php foreach ($categorias_salvas as $categoria_salva_id => $categoria_salva_slug) : ?>
                <div class="option">
                    <p>
                        <select name="concursos[]" class="concurso-dropdown">
                            <option value="0">Selecione...</option>
                            <?php foreach ($options as $id => $nome) : ?>
                                <option value="<?= $id ?>" <?= $id == $categoria_salva_id ? 'selected' : '' ?>><?= $nome ?></option>
                            <?php endforeach; ?>
                        </select>
                        <input type="button" value="Remover" class="button button-primary remover-btn">
                        <input type="button" value="Adicionar" class="button button-primary adicionar-btn">
                    </p>
                </div>
            <?php endforeach; ?>
        </div>

        <div class="">
            <input type="submit" name="submit" value="Salvar" class="button button-primary">
        </div>
    </form>
</div>

<link href="/wp-content/themes/academy/css/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="/wp-content/themes/academy/js/select2.min.js"></script>
<script>
    jQuery(function() {
        jQuery('.concurso-dropdown').select2();

        jQuery('body').on('click', '.adicionar-btn', function() {
            var optionPaiEl = jQuery(this).closest('.option'),
                optionCloneEl;

            optionPaiEl.find('.concurso-dropdown').select2('destroy');
            optionCloneEl = optionPaiEl.clone();
            optionCloneEl.find('.concurso-dropdown').val(0);

            optionCloneEl.insertAfter(optionPaiEl);

            optionPaiEl.find('.concurso-dropdown').select2();
            optionCloneEl.find('.concurso-dropdown').select2();
        });

        jQuery('body').on('click', '.remover-btn', function() {
            var optionPaiEl = jQuery(this).closest('.option');
            optionPaiEl.remove();
        });
    });
</script>
