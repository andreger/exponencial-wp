<?php settings_errors() ?>
<div class="wrap">
    <h2><?php echo $this->plugin->displayName; ?> - Planilhas</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" enctype="multipart/form-data" name="post" method="post" action="admin.php?page=exponencial-planilhas">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                    <div class="option">
                     		<p>
                     			<h2>Avaliação de Estudos</h2>
                  		 		<input type="file" name="avaliacao" class="widefat" />
                  		 	</p>  
                  		 	<div class="submit">
		                    	<input type="submit" name="submit-avaliacao" value="Enviar" class="button button-primary" /> 

                          <a href="<?= get_avaliacao_estudos_url() ?>" class="button button-primary">Ver Atual</a>
		                	  </div>

                        </div>
                    </div>
		            </div>
		        </form>
		        
		        <div style="margin-top: 40px"></div>
		        
		        <form id="post" enctype="multipart/form-data" name="post" method="post" action="admin.php?page=exponencial-planilhas">
					<div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                        
                        <div class="option">
                     		<p>
                     			<h2>Disponibilidade do Aluno</h2>
                  		 		<input type="file" name="disponibilidade" class="widefat" />
                  		 	</p>  
                  		 	<div class="submit">
		                    	<input type="submit" name="submit-disponibilidade" value="Enviar" class="button button-primary" /> 

                          <a href="<?= get_disponibilidade_aluno_url() ?>" class="button button-primary">Ver Atual</a>
		                	</div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>