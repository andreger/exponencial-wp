<?php settings_errors() ?>

<?php 
$usuario = get_usuario($_GET['user_id']);
$historico = get_coordenador_sq_historico($_GET['user_id']);
?>

<div class="wrap">
    <h2>
    	<?php echo $this->plugin->displayName; ?> - 
    		Coordenador SQ - Histórico</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		
            <div>


             <h2><?= $usuario->display_name ?></h2>

              <table class="widefat fixed">
                <thead>
                  <th>Data Início</th>
                  <th>Data Fim</th>
                  <th>Participação (%)</th>
                </thead>
              
                <tbody>

                  <?php foreach($historico as $item) : ?>
                  <tr>
                    <td><?= converter_para_ddmmyyyy($item->data_inicio) ?></td>
                    <td><?= converter_para_ddmmyyyy($item->data_fim) ?></td>
                    <td><?= $item->percentual ?></td>
                </div>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>

            </div>

           <div class="submit">
              <a href="/wp-admin/admin.php?page=coordenadores-sq">
                <input type="button" value="Voltar" class="button button-primary" /> 
              </a>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>


