<?php settings_errors() ?>
<div class="wrap">
    <h2>
    	<?php echo $this->plugin->displayName; ?> - 
    		Estorno Proporcional</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
 
                <div>
                    <a href="/wp-admin/admin.php?page=estorno-proporcional-novo" class="button button-primary">Novo Estorno</a> 
                </div>
            </div>
        </div>
    </div>
</div>

<div class="wrap">
    <h2>Estornos</h2>
    <div>
        <?php if($estornos) :?>
        <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th>Pedido</th>
                    <th>Data Pedido</th>
                    <th>Aluno</th>
                    <th>E-mail</th>
                    <th>Data Estorno</th>
                    <th>Tipo</th>
                    <th>Valor</th>
                    <th>Produtos</th>
                    <th>Revogou acesso?</th>
                    <th>Status</th>
                    <th>Motivo</th>
                </tr>
                </thead>
                <?php foreach ($estornos as $estorno) : ?>
                <?php $aluno = get_usuario_array($estorno['usu_id']); ?>
                <?php $status = $estorno['est_status'] ?: "OK" ?>
                <tr>
                    <td><?= $estorno['pedido_id']?></td>
                    <td><?php 
                            if($data_criacao = get_pedido_data_criacao($estorno['pedido_id'])){
                                echo converter_para_ddmmyyyy_HHiiss($data_criacao);
                            }else{
                                echo "-";
                            }
                        ?>
                    </td>
                    <td><?= $aluno['nome_completo']?></td>
                    <td><?= $aluno['email']?></td>
                    <td><?= converter_para_ddmmyyyy_HHiiss($estorno['est_data_hora']) ?></td>
                    <td><?= $estorno['est_tipo'] == ESTORNO_POR_VALOR?"Valor":($estorno['est_tipo'] == ESTORNO_POR_PRODUTO?"Produto":"") ?></td>
                    <td><?= moeda($estorno['est_valor']) ?></td>
                    <td><?= $estorno['est_produtos_ids'] ?></td>
                    <td><?= is_null($estorno['est_revoga_acesso'])?"-":($estorno['est_revoga_acesso']?"Sim":"Não") ?></td>
                    <td><?= $status ?></td>
                    <td><?= $estorno['est_motivo'] ?></td>
                </tr>
                <?php endforeach; ?>
            </table>

            <?php
            if($page_links) {
                echo '<div class="tablenav"><div class="tablenav-pages" style="margin: 1em 0">' . $page_links . '</div></div>';
            }
            ?>

            <?php else : ?>
            Ainda não foram realizados estornos.
            <?php endif ?>
        </div>
    </div>
    
<div style="clear:both"></div>

