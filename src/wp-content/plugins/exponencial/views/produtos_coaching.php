<?php settings_errors() ?>

<div class="wrap">

  <h2><?php echo $this->plugin->displayName; ?> - Produtos do Coaching</h2>

  <div id="poststuff">
  	
    <div id="post-body" class="metabox-holder columns-2">
  		
      <!-- Content -->
  		<div id="post-body-content">
  		
      	<!-- Form Start -->
        <form id="post" name="post" method="post" action="admin.php?page=produtos_coaching">
            
          <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">     
          
            <div class="option">
                <b>Produto WP</b>
                
                <select class="widefat" name="produto_id" >

                  <option value="">Escolha um produto</option>
                  
                  <?php foreach (get_produtos_coaching_combo("Selecione um produto") as $key => $value) : ?>
                    
                    <?php $selecionado = isset($_POST['produto_id']) && $_POST['produto_id'] == $key ? "selected=selected" : "" ?>
                    
                    <option value="<?= $key ?>" <?= $selecionado ?>> <?= $value ?> </option>
                  <?php endforeach ?>
                </select>

            </div>

            <div class="option">
                <b>Áreas</b>

                <?php get_turmas_coaching_combo() ?>

                <select class="widefat" name="area_id" >
                  
                  <option value="">Escolha uma área</option>
                  
                  <?php foreach (get_turmas_coaching_combo() as $key => $value) : ?>

                    <?php $selecionado = isset($_POST['area_id']) && $_POST['area_id'] == $key ? "selected=selected" : "" ?>
                    
                    <option value="<?= $key ?>" <?= $selecionado ?>> <?= $value ?> </option>
                  <?php endforeach ?>
                </select>
            </div>

            <div class="submit">
                <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
            </div>
          </div>    

        </form>
      
        <?php $items = listar_produtos_areas(); ?>
           
        <?php if($items) : ?>
        <div>
          <h2>Produtos do coaching existentes</h2>

          <table class="widefat fixed">
            <thead>
              <th>Produto</th>
              <th>Área</th>
              <th width="20%"></th>
            </thead>
          
            <tbody>

              <?php foreach($items as $item) : ?>
              <tr>
                <td><?= $item['produto_nome'] ?></td>
                <td><?= $item['area_nome'] ?></td>
                <td>
                  <div class="">
                    <a href="/wp-admin/admin.php?page=produtos_coaching&r_produto_id=<?= $item['produto_id'] ?>&r_area_id=<?= $item['area_id'] ?>" onclick="return confirm('Confirma exclusão do item?');">Remover</a>
                  </div>
                </td>
              </tr>
              <?php endforeach; ?>

            </tbody>

          </table>

        </div>

        <?php endif; ?>

    </div>

  </div>

<div style="clear:both"></div>

