<?php settings_errors() ?>
<div class="wrap">
    <h2><?php echo $this->plugin->displayName; ?> - Coaching</h2>
    <div id="poststuff">
    	<div id="post-body" class="metabox-holder columns-2">
    		<!-- Content -->
    		<div id="post-body-content">
    			<!-- Form Start -->
		        <form id="post" name="post" method="post" action="admin.php?page=exponencial-coaching">
		            <div id="normal-sortables" class="meta-box-sortables ui-sortable publishing-defaults">                        
                     	<div class="option">
                     		<p>
                     			<strong>Slug do item</strong>
                  		 		<input type="text" name="slug" class="widefat" />
                  		 	</p>  
                        </div>
                        <div class="option">
                     		<p>
                     			<strong>Tipo do item</strong>
                  		 		<input type="radio" name="tipo" value="1" class="widefat" /> Cupom
                  		 		<input type="radio" name="tipo" value="2" class="widefat" /> Produto
                  		 	</p>  
                        </div>
		                <div class="submit">
		                    <input type="submit" name="submit" value="Salvar" class="button button-primary" /> 
		                </div>
                        
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>
<?php $items = listar_items_coaching(); if(count($items)) : ?>
<div class="wrap">
    <h2>Itens Cadastrados</h2>
    <div>
    	<table class="wp-list-table widefat fixed striped">
    		<thead>
    			<tr>
    				<th>Item</th>
    				<th>Tipo</th>
    				<th>Ação</th>
    			</tr>
    		</thead>
    		<?php foreach ($items as $item) : ?>
    		<tr>
    			<td><?php echo $item['ite_slug']?></td>
    			<td><?php echo $item['ite_tipo'] == 1 ? 'Cupom' : 'Produto' ?></td>
    			<td><a onclick="return confirm('Deseja realmente excluir este item ?');" href="admin.php?page=exponencial-coaching&remove=<?php echo $item['ite_slug'] ?>">Excluir</a></td>
    		</tr>
    		<?php endforeach; ?>
    	</table>
    </div>
</div>    
<?php endif; ?>