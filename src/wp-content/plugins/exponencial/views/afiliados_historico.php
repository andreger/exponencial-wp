<?php settings_errors() ?>

<?php 
$historico = folha_dirigida_listar_historico();
?>

<div class="wrap">
    <h2>
      <?php echo $this->plugin->displayName; ?> - 
        Folha Dirigida - Histórico</h2>
    <div id="poststuff">
      <div id="post-body" class="metabox-holder columns-2">
        <!-- Content -->
        
            <div>
              <table class="widefat fixed">
                <thead>
                  <th>Data Início</th>
                  <th>Data Fim</th>
                  <th>Percentual (%)</th>
                </thead>
              
                <tbody>

                  <?php foreach($historico as $item) : ?>
                  <tr>
                    <td><?= converter_para_ddmmyyyy($item->data_inicio) ?></td>
                    <td><?= converter_para_ddmmyyyy($item->data_fim) ?></td>
                    <td><?= $item->percentual ?></td>
                </div>
                  </tr>
                  <?php endforeach; ?>

                </tbody>
              </table>

            </div>

           <div class="submit">
              <a href="/wp-admin/admin.php?page=folha-dirigida">
                <input type="button" value="Voltar" class="button button-primary" /> 
              </a>
            </div>
        </div>
    </div>
</div>
<div style="clear:both"></div>


