<?php settings_errors() ?>
<?php
wp_enqueue_script(
		array(
				'jquery-ui-core',
				'jquery-ui-sortable'
		)
)
?>
<div class="wrap">
	<h2>
		<?php echo $this->plugin->displayName; ?> -
			Rodapé</h2>
	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">
			<!-- Content -->
			<div id="post-body-content">
				<!-- Form Start -->
				<form id="post" name="post" method="post"
					action="admin.php?page=rodape">
					<div id="normal-sortables"
						class="meta-box-sortables ui-sortable publishing-defaults">

						<div class="option">
							<p>
								<strong>Título</strong> <input required="required" type="text"
									name="titulo" class="widefat" />
							</p>
						</div>

						<div class="option">
							<p>
								<strong>URL</strong> <input required="required"
									type="text" name="url" class="widefat" />
							</p>
						</div>

						<div class="option">
							<p>
								<strong>Coluna</strong>
								<select name="coluna" class="widefat">
									<option selected>Selecione...</option>
									<option value="1">Coluna 1</option>
									<option value="2">Coluna 2</option>
									<option value="3">Coluna 3</option>
								</select>
							</p>
						</div>

						<div class="submit">
							<input type="submit" name="submit" value="Salvar"
								class="button button-primary" />
						</div>

					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div style="clear: both"></div>


<?php if(count($rodapes)) : ?>
<div class="wrap" style="width: 1400px;">
	<h2>Itens de Rodapé Cadastrados</h2>
	<div style="display: flex; justify-content: space-between;">
		<?php foreach ($rodapes as $itens) : ?>
			<div style="max-width: 33%;">
			<ul id="sortable-<?= $itens[0]->roi_coluna; ?>" class="sortable">
				<?php foreach ($itens as $item) : ?>
				<li class="roi-itens ui-state-default" data-id="<?= $item->roi_id; ?>">
						<span class="ui-icon ui-icon-arrowthick-2-n-s"></span>
					<form id="edit" name="edit" method="post"
						action="admin.php?page=rodape" style="display: flex;">
						<input type="hidden" name="roi_id" class="roi_id" value="<?= $item->roi_id; ?>">
						<input type="hidden" name="roi_index" class="roi_index" value="<?= $item->roi_index; ?>">
						<input type="text" class="widefat roi_titulo" name="roi_titulo" value="<?= $item->roi_titulo; ?>" required>
						<input type="text" class="widefat roi_url" name="roi_url" value="<?= $item->roi_url; ?>" required>
						<button type="submit" class="button button-primary" name="editar">Editar</button>
						<a class="button button-primary" onclick="return confirm('Deseja realmente excluir este item ?');" href="admin.php?page=rodape&remove_roi=<?= $item->roi_id ?>">Excluir</a>
					</form>
				</li>

				<?php endforeach; ?>
			</ul>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<form id="ordenar-itens" name="ordenar_itens" method="post" action="admin.php?page=rodape">
	<input type="hidden" name="roi_itens" id="roi_itens">
	<button class="button button-primary" id="salvar-ordem" name="ordenar" type="submit">Salvar Ordem</button>
</form>
<script>
  $( function() {
    $( ".sortable" ).sortable();
    $( ".sortable" ).disableSelection();

    $("#ordenar-itens").submit(function() {
		array_envio = [];
		$( ".sortable" ).each(function() {
			$(".roi-itens", this).each(function(i) {
				array_envio.push({
					roi_id: $(this).find(".roi_id").val(),
					roi_titulo: $(this).find(".roi_titulo").val(),
					roi_url: $(this).find(".roi_url").val(),
					roi_index: i
				});
			});

		});

		$("#roi_itens").val(JSON.stringify(array_envio));
		return;
  });
  } );



// 	$(".editar").on('click', function() {
// 		_li = $(this).parents('li');
// 		id = $(this).data('id');

// 	});

  </script>
<?php endif; ?>
