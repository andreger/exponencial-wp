<?php
/* 
Plugin Name: Exponencial Depoimentos
Plugin URI: http://www.exponencialconcursos.com.br/
Description: Plugin para gerenciar um depoimento do portal do Exponencial
Version: 0.1
Author: João Paulo Ferreira
Author URI: http://www.exponencialconcursos.com.br/
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/
class ExponencialDepoimentos
{

	function __construct() {
		add_action('init', array($this, 'init'), 1);
	}
	
	function init() {
		// admin only
		if( is_admin() )
		{
			$user = new WP_User(get_current_user_id());
			$user->add_cap( 'publish_depoimentos' );
			$user->add_cap( 'edit_depoimentos' );
			$user->add_cap( 'edit_others_depoimentos' );
			$user->add_cap( 'delete_depoimentos' );
			$user->add_cap( 'delete_others_depoimentos' );
			$user->add_cap( 'read_private_depoimentos' );
			$user->add_cap( 'edit_depoimento' );
			$user->add_cap( 'delete_depoimento' );
			$user->add_cap( 'read_depoimento' );

			
			register_post_type( 'depoimentos',
				array(
					'labels' => array(
						'name' => 'Depoimentos',
						'singular_name' => 'Depoimento',
						'add_new' => 'Adicionar Novo',
						'add_new_item' => 'Adicionar Novo Depoimento',
						'edit_item' => 'Editar Depoimento',
						'new_item' => 'Novo Depoimento',
						'all_items' => 'Todos os Depoimentos',
						'view_item' => 'Ver Depoimento',
						'search_items' => 'Buscar Depoimento',
						'not_found' =>  'Nenhum depoimento encontrado',
						'not_found_in_trash' => 'Nenhum depoimento encontrado na lixeira',
						'parent_item_colon' => '',
						'menu_name' => 'Depoimentos'
					),
					'public' => true,
					'supports' => array( 'title', 'editor', 'custom-fields', 'thumbnail' ),
					'rewrite' => array( 'slug' => 'depoimento', 'with_front' => FALSE ),
					'capability_type' => 'depoimento',
					'capabilities' => array(
						'publish_posts' => 'publish_depoimentos',
						'edit_posts' => 'edit_depoimentos',
						'edit_others_posts' => 'edit_others_depoimentos',
						'delete_posts' => 'delete_depoimentos',
						'delete_others_posts' => 'delete_others_depoimentos',
						'read_private_posts' => 'read_private_depoimentos',
						'edit_post' => 'edit_depoimento',
						'delete_post' => 'delete_depoimento',
						'read_post' => 'read_depoimento',
					),
				)
			);
            flush_rewrite_rules();
 			//add_action('admin_menu', array($this,'admin_menu'));
			
		}
	} 

	function admin_menu() {
        //add_menu_page('Depoimentos', 'Depoimentos', 'manage_options', 'edit.php?post_type=depoimentos', false, false, '60000');
	}
}

function edd()
{
	global $edd;
	
	if( !isset($edd) )
	{
		$edd = new ExponencialDepoimentos();
	}
	
	return $edd;
}

// initialize
edd();