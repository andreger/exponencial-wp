<?php  
require_once '../../wp-load.php';

global $wpdb;

//OrderItemId 416348 originário do estorno 360370 realizado no pedido 357227 gerou o item com subtotal e total igual a -1, estamos corrigindo e reprocessando
$sql = "update wp_woocommerce_order_itemmeta set meta_value = -1428 where order_item_id = 416348 and meta_key in ('_line_subtotal', '_line_total');";

$wpdb->query($sql);

$pedido_id = 357227;
excluir_vendas_por_pedido($pedido_id);
processar_pedido($pedido_id, true, true);

echo "Acabou";