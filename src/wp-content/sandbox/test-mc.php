<?php 
$lista = $_GET['list_id'];
"14611d639c";

// cria a representação do contato
$membro = [
	'email' => $_POST['EMAIL'],
	'nome' => $_POST['NOME'],
	'telefone' => $_POST['TELEFONE'],
];

// cria um array para armazenar segmentos
$segmentos_a = [];

// se já existe atualiza o seguimento do contato
if($mc_contato = get_mc_contato($lista, $membro['email'])) {

	// alimenta array de segmentos com os já existentes
	if($segs = $mc_contato->merge_fields->SEG) {
		$segmentos_a = explode(",", $segs);
	};
}

// se novos segmentos forem passados adiciona-os no array
if($_POST['SEG']) {

	if($post_segs = explode(",", $_POST['SEG'])) {
		foreach ($post_segs as $item) {
			array_push($segmentos_a, trim($item));
		}
	}
	
}

// transforma array em string para gravação no MC
$segmentos_a = array_unique($segmentos_a);
$membro['segmento'] = implode(",", $segmentos_a);

// salva ou atualiza o contato
salvar_mc_contato($lista, $membro);

// redireciona para página de agradecimento
header("Location: /agradecimento");

function get_mc_contato($lista, $email) 
{
	$busca = json_decode(mc_request("GET", "/search-members?query={$email}&list_id={$lista}"));

	if($busca && $busca->exact_matches && $busca->exact_matches->members)
	{
		return $busca->exact_matches->members[0];
	}

	else {
		return null;
	}
}

function salvar_mc_contato($lista, $membro)
{
	$data = [
		'email_address' => $membro['email'],
		'status_if_new' => "subscribed",
		'merge_fields' => [
			'NOME' => $membro['nome'],
			'SEG' => $membro['segmento'],
			'TELEFONE' => $membro['telefone']
		]
	];

	$subscriber_hash = md5(strtolower($membro['email']));

	mc_request("PUT", "/lists/{$lista}/members/{$subscriber_hash}", $data);
}

function mc_request($type, $target, $data = false )
{   
	$ch = curl_init('https://us1.api.mailchimp.com/3.0/' . $target );

	curl_setopt( $ch, CURLOPT_HTTPHEADER, array
	(
		'Content-Type: application/json', 
		'Authorization: WP 9a8bb3e1c8eba8bb248fffcc29a842d1-us1',
	) );
 
	curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $type );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 5 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLOPT_USERAGENT, 'YOUR-USER-AGENT' );
 
	if( $data )
		curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $data ) );
 
	$response = curl_exec( $ch );
	curl_close( $ch );
 
	return $response;
}