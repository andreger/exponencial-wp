<?php
require_once '../../wp-load.php';

if(!tem_acesso([ADMINISTRADOR])) exit; 

global $wpdb;

KLoader::model("PostModel");

KLoader::model("PedidoModel");

set_time_limit(300);

$data_hora = date('d/m/Y h:i:sa');
echo "Início: " . $data_hora . "<br/>";

//PedidoModel::alterar_pedido_status_usuario(393126);
//PedidoModel::alterar_pedido_status_usuario(393125);
//PedidoModel::alterar_pedido_status_usuario(204314);
//PedidoModel::alterar_pedido_status_usuario(393210);

$sql = "SELECT post_id 
        FROM wp_postmeta 
        WHERE meta_key='_customer_user' 
            AND meta_value IN ('1', '201')
            AND post_id NOT IN (SELECT pedido_oculto_id FROM premium_log)  
        ORDER BY meta_id DESC";

$result = $wpdb->get_results($sql);

foreach($result as $row){
    PedidoModel::alterar_pedido_status_usuario($row->post_id);
}

echo "<br/>DADOS GERADOS COM SUCESSO PARA OS USER_ID 1 e 201 <br/>";
$data_hora = date('d/m/Y h:i:sa');
echo "Fim: " . $data_hora . "<br/>";

?>