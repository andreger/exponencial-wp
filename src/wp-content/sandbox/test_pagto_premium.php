<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once '../../wp-load.php';

global $wpdb;

KLoader::model("ProdutoModel");
KLoader::model("PedidoModel");
KLoader::model("PremiumModel");
KLoader::helper("PagarMeGatewayHelper");
KLoader::helper("PagSeguroGatewayHelper");

$id = isset($_GET['o']) ? $_GET['o'] : 1;

$sql= "SELECT v.*
    	    FROM wp_vendas v
    	       JOIN produtos p ON p.post_id = v.ven_curso_id
    	    WHERE
    	       pro_is_premium = 1 AND v.ven_data >= '2019-09-01 00:00:00' AND ven_data <= '2019-09-30 23:59:59' 
               AND v.ven_order_id > $id
    	    ORDER BY v.ven_order_id ASC LIMIT 1";


$row = $wpdb->get_row($sql);

echo "<pre>";
if(!$row) {
    echo "Script finalizado";
    exit;
}

$order = new WC_Order($row->ven_order_id);

if($order) {
    
    if(!$order->get_date_created()) {
        log_premium($row->ven_order_id, "ERROR");
    }
    else {
        $data_pedido = $order->get_date_created()->date('Y-m-d H:i:s');
        
        
        $total_desconto = PedidoModel::get_pedido_desconto($order);
        $total = $order->get_total() + $total_desconto;
        
        $taxas_pagseguro = 0;
        if($total > 0) {
            // Taxas caso pedido tenha sido no Pagar.me
            if(PagarMeGatewayHelper::is_gateway_pedido_pagarme($order->get_id())) {
                $taxas_pagseguro = PagarMeGatewayHelper::get_taxas($order);
            }
            // Taxas caso pedido tenha sido no PagSeguro
            else {
                $taxas_pagseguro = PagSeguroGatewayHelper::get_taxas($order, $force_pagseguro);
            }
        }
        
        $parceiro = identifica_parceria_do_pedido($order);
        $valores_produtos = get_post_meta($order->get_id(), 'valores-produtos', true);
        $aluno = get_usuario_array( $order->get_user_id() );
        $aliquota = get_imposto_por_data($data_pedido);
        
        echo $aliquota;
        
        
        // Hack para reembolso total. Se reembolso total então valor_venda - desconto = reembolso.
        // Necessário, pois reembolsos diretos no PS não adicionam informações referentes aos produtos
        $is_reembolso_total = (($order->get_total_refunded() > 0) && ($order->get_total_refunded() == $total)) ? true : false;
        
        $items = $order->get_items();
        
        if($items) {
            foreach ($items as $item) {
                
                $produto_id = $item['product_id'];
    
                $produto = ProdutoModel::get_by_id($item['product_id']);
                
                if($produto->pro_is_premium) {
                    
                    // recupera valor estornado
                    $reembolso = 0;
                    
                    if(is_produto_estornado($order->get_id(), $item->get_product_id())) {
                        $reembolso = $order->get_total_refunded_for_item($item->get_id());
                    }
                    
                    if(count($items) == 1) {
                        $percentual_item = 1;
                    }
                    else {
                            
                        $percentual_item = ($total > 0) ? ( ($item->get_subtotal() - $reembolso) / $total ) : 0;
                    }
                    
                    $desconto = $total_desconto * $percentual_item;
                    
                    $afiliados_percentual = get_post_meta($order->get_id(), AFILIADOS_PERCENTUAL_META, true);
                    $folha_dirigida = $afiliados_percentual ? $afiliados_percentual / 100 : 0;
                    
                    // Hack para reembolso total. Se reembolso total então valor_venda - desconto = reembolso.
                    // Necessário, pois reembolsos diretos no PS não adicionam informações referentes aos produtos
                    if($is_reembolso_total) {
                        $reembolso = $item['line_subtotal'] - $desconto;
                    }
                    
                    
                    $desconto = $total_desconto * $percentual_item;
                    $valor_venda = $item['line_subtotal'];
                    $valor_pago = $valor_venda - $desconto - $reembolso;
                    
                    $gateway = $valor_pago == 0 ? 0 : $taxas_pagseguro * $percentual_item;
                    $valor_imposto = $valor_pago == 0 ? 0 : $aliquota / 100 * $valor_pago;
                    $afiliados = $valor_pago == 0 ? 0 : $folha_dirigida * $valor_pago;
                    $liquido = $valor_pago - $valor_imposto - $gateway - $afiliados;
                    $expo_parte = 0.2 * $liquido;
                    $prof_parte = 0.8 * $liquido;
                    
                    
                    
                    
                    
                    
                    // prepara os dados dos pedidos de produtos premiums
                    $ano_mes = '201909';
                    $professor_id = null;
                    $confere_professor = 0;
                    
                    $pedidos_premiums = [];
                    $retorno_pedidos = PremiumModel::listar_pedidos_premium_agrupados_por_produtos(null, null, $order->get_id());
                    foreach ($retorno_pedidos as $item) {
                        $pedidos_premiums[$item->ven_curso_id] = $item;
                    }
    
                    
                    if($retorno_pedidos) {
                        // prepara os dados dos professores
                        $retorno = PremiumModel::listar_premium_item_professores_por_periodo($ano_mes, $professor_id);            
                   
                        $professores_premiums = [];
                        
                        foreach ($retorno as $item) {
                            
                            if($item->pro_id != $produto_id) continue;
                            
                            if(!isset($professores_premiums[$item->display_name]['produtos'][$item->pro_id])) {
                                $cota_sq_percentual = 0.2;
                                $cota_professor_percentual = 0.8;
                                
                                
                                $total_premium_mcache_key = "premium_total_{$item->pro_id}_{$ano_mes}";
                                $total_premium = wp_cache_get($total_premium_mcache_key);
                                if(!$total_premium) {
                                    $total_premium = PremiumModel::get_valor_total_produtos($item->pro_id, $ano_mes);
                                    wp_cache_set($total_premium_mcache_key, $total_premium);
                                }
                                
                                $parcial_professor_mcache_key = "parcial_professor_{$item->pro_id}_{$ano_mes}_{$item->user_id}";
                                $parcial_professor = wp_cache_get($parcial_professor_mcache_key);
                                
                                if(!$parcial_professor) {
                                    $parcial_professor = PremiumModel::somar_valor_parcial_por_professor($item->pro_id, $ano_mes, $item->user_id);
                                    wp_cache_set($parcial_professor_mcache_key, $parcial_professor);
                                }
                                
                                $cota = $parcial_professor / $total_premium;
                                                            
                                $total_vendas = $pedidos_premiums[$item->pro_id]->total_valor_venda;
                                
                                if($total_vendas <= 0) continue;
                                
                                $total_descontos = $pedidos_premiums[$item->pro_id]->total_desconto;
                                $total_reembolsado = $pedidos_premiums[$item->pro_id]->total_reembolso;
                                $total_pago = $total_vendas - $total_descontos - $total_reembolsado;
                                $total_imposto = $pedidos_premiums[$item->pro_id]->total_imposto;
                                $total_pagseguro = $pedidos_premiums[$item->pro_id]->total_pagseguro;
                                $total_afiliados = $pedidos_premiums[$item->pro_id]->total_folha_dirigida;
                                $total_liquido = $total_pago - $total_pagseguro - $total_imposto - $total_afiliados;
                                $cota_professor = $cota_professor_percentual * $total_liquido;
                                $total_professor = $cota * $cota_professor;
                                
                                $minha_cota = $cota * 100;
                                
                                $linha = [
                                    'id' => $item->pro_id,
                                    'produto' => $item->post_title,
                                    'periodo' => periodo_mes_ano($ano_mes),
                                    'total_vendas' => moeda($total_vendas),
                                    'total_descontos' => moeda($total_descontos),
                                    'total_reembolsado' => moeda($total_reembolsado),
                                    'total_pago' => moeda($total_pago),
                                    'total_imposto' => moeda($total_imposto),
                                    'total_pagseguro' => moeda($total_pagseguro),
                                    'total_afiliados' => moeda($total_afiliados),
                                    'total_liquido' => moeda($total_liquido),
                                    'cota_sq' => moeda($cota_sq_percentual * $total_liquido),
                                    'cota_professor' => moeda($cota_professor),
                                    'minha_cota' => porcentagem($minha_cota, 4),
                                    'total_professor' => $total_professor,
                                    'm_cota' => $minha_cota
                                ];
                                
                                $professores_premiums[$item->display_name] = $linha;
                            }
                            
                        }
                    }
                    
//                     print_r($professores_premiums);
                    
                    
                    $confere_cota = 0;
                    $confere_professor = 0;
                    foreach ($professores_premiums as $prof) {
                        $confere_professor += $prof['total_professor'];
                        $confere_cota += $prof['m_cota'];
                    }
                    
                    echo "<br><br> confere cota: $confere_cota <br><br>";

    
                    
                    $confere_diff = $prof_parte - $confere_professor;
                        
                    
                    $csv_linha = [
                        $row->ven_order_id,
                        numero_brasileiro($valor_venda),
                        numero_brasileiro($desconto),
                        numero_brasileiro($reembolso),
                        numero_brasileiro($valor_pago),
                        numero_brasileiro($gateway),
                        numero_brasileiro($valor_imposto),
                        numero_brasileiro($afiliados),
                        numero_brasileiro($liquido),
                        numero_brasileiro($expo_parte),
                        numero_brasileiro($prof_parte),
                        numero_brasileiro($confere_professor),
                        numero_brasileiro($confere_diff),
                        numero_brasileiro($confere_cota, 4),
                    ];
                    
                    print_r($csv_linha);
//                     exit;
                    
                    $linha = implode(";", $csv_linha);
                    
                    
                    log_premium($linha);
                    
                    
                }
                
            }
        }
    }
}
?>
<html>
<head></head>
<body>
<script>
setTimeout(function () {
	window.location.href="/wp-content/sandbox/test_pagto_premium.php?o=<?= $row->ven_order_id ?>";
}, 1000);
</script>
</body>
</html>