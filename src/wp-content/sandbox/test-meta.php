<?php
require_once '../../wp-load.php';

global $wpdb;

set_time_limit(300);

// KLoader::model("ProdutoModel");
// ProdutoModel::atualizar_metadados(203237);
// exit;

// ProdutoModel::atualizar_metadados_basicos(115335);

$op = $_GET['op'];
$todos = $_GET['todos'];
$incremental = $_GET['inc'] ?: 0;
// exit;



if($op == 999) {
	KLoader::model("ConcursoModel");

	$resultado = $wpdb->get_results('select ID, post_name, post_title, term_id FROM wp_posts LEFT JOIN wp_terms ON post_name = slug WHERE post_type = "concurso" AND term_id IS NULL AND post_name != "" ');

	foreach ($resultado as $linha) {
		echo "Atualizando concurso: " . $linha->ID . "<br>";
		ConcursoModel::atualizar_metadados($linha->ID);
	}
}



if($op == 1 || $todos == 1) {


	KLoader::model("UsuarioModel");

	$wpdb->query("DELETE FROM colaboradores WHERE 1");
	$colaboradores = listar_professores_e_consultores();

	foreach ($colaboradores as $item) {
	   UsuarioModel::atualizar_metadados($item['ID']);
	}


	echo "Dados de colaboradores atualizados com sucesso";
}




if($op == 2 || $todos == 1) {


	KLoader::model("CategoriaModel");
	$wpdb->query("DELETE FROM materias WHERE 1");

	$materias = CategoriaModel::listar_filhas(CATEGORIA_MATERIA);
	if($materias) {
	    foreach($materias as $item) {
	        $qtde = contar_produtos_por_categoria($item->slug, TRUE);

	        $dados = [
	            'mat_id' => $item->term_id,
	            'mat_qtde_cursos' => $qtde,
	            'mat_slug' => $item->slug
	        ];

	        $wpdb->replace('materias', $dados);
	    }
	}

	echo "Dados de materias atualizados com sucesso"; 

}




if($op == 3 || $todos == 1) {

	// $posts = get_posts(['post_type' => 'concurso']);

	$wpdb->query("DELETE FROM concursos WHERE 1");

	$concursos = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type = 'concurso' ");

	if($concursos) {
	    foreach($concursos as $item) {

	    	$categoria_id = get_post_meta($item->ID, 'categoria_associada_id', true);


	    	if(!$categoria_id) {
				$sql = "SELECT * FROM wp_terms where slug = '{$item->post_name}'";
				$categoria = $wpdb->get_row($sql);


				if($categoria) {
					$categoria_id = $categoria->term_id;

					update_post_meta($item->ID, 'categoria_associada_id', $categoria_id);
				}
			}

		

	    	if($categoria_id) {
	    		$qtde = contar_produtos_por_categoria($item->post_name, TRUE);
	    	}

			$status = get_post_meta($item->ID, 'status_do_concurso', TRUE);
			$thumb = get_the_post_thumbnail_url($item->ID);

	        $dados = [
	            'con_id' => $item->ID,
	            'con_qtde_cursos' => $qtde,
	            'con_slug' => $item->post_name,
	            'con_status' => $status,
	            'con_thumb' => $thumb,
	            'cat_id' => $categoria_id
	        ];

	        $wpdb->replace('concursos', $dados);
	    }
	}

	echo "Dados de concursos atualizados com sucesso";

}




if($op == 4 || $todos == 1) {

	KLoader::model("BlogModel");
	$wpdb->query("DELETE FROM blogs WHERE 1");

	$blogs = listar_todos_blog_posts();
	if($blogs) {
	    foreach($blogs as $item) {
	        BlogModel::atualizar_metadados($item->ID);
	    }
	}

	echo "Dados de blogs atualizados com sucesso";

}




if($op == 5 || $todos == 1) {

	KLoader::model("ProdutoModel");

	if(!$incremental) {
		$wpdb->query("DELETE FROM produtos WHERE 1");
	}
	else {
		$produtos_ids = $wpdb->get_col("SELECT post_id FROM produtos");	
	}

	$posts = get_todos_produtos_posts(FALSE, 'any');

	KLoader::model("ProdutoModel");

	foreach ($posts as $item) {
	    set_time_limit(300);


	    if($incremental) {
	    	if(in_array($item->ID, $produtos_ids)) {
	    		continue;
	    	}
	    }

	    ProdutoModel::atualizar_metadados_basicos($item->ID);
	}

	echo "Dados de produtos-básicos atualizados com sucesso";

}



if($op == 6 || $todos == 1) { 

	KLoader::model("ProdutoModel");

	$posts = get_todos_produtos_posts(FALSE, 'any');
	
	if($incremental) {
	    $produtos_ids = $wpdb->get_col("SELECT DISTINCT post_id FROM produtos_exames");	
	}

	foreach ($posts as $item) {
	    set_time_limit(300);
	    
	    if($incremental) {
	        if(in_array($item->ID, $produtos_ids)) {
	            continue;
	        }
	    }
	    
	    ProdutoModel::atualizar_metadados_concursos($item->ID, false);
	}

	echo "Dados de produtos-exames atualizados com sucesso";

}



if($op == 7 || $todos == 1) { 

	KLoader::model("ProdutoModel");

	$posts = get_todos_produtos_posts(FALSE, 'any');
	
	if($incremental) {
	    $produtos_ids = $wpdb->get_col("SELECT DISTINCT post_id FROM produtos_materias");
	}

	foreach ($posts as $item) {
	    set_time_limit(300);
	    
	    if($incremental) {
	        if(in_array($item->ID, $produtos_ids)) {
	            continue;
	        }
	    }
	    
	    ProdutoModel::atualizar_metadados_materias($item->ID);
	}

	echo "Dados de produtos-materias atualizados com sucesso";

}




if($op == 8 || $todos == 1) { 

	KLoader::model("ProdutoModel");

	$posts = get_todos_produtos_posts(FALSE, 'any');

	if($incremental) {
	    $produtos_ids = $wpdb->get_col("SELECT DISTINCT post_id FROM produtos_professores");
	}
	
	foreach ($posts as $item) {
	    set_time_limit(300);
	    
	    if($incremental) {
	        if(in_array($item->ID, $produtos_ids)) {
	            continue;
	        }
	    }
	    
	    ProdutoModel::atualizar_metadados_professores($item->ID, TRUE);
	}

	echo "Dados de produtos-professores atualizados com sucesso";

}



if($op == 9 || $todos == 1) { 

	KLoader::model("ProdutoModel");

	$posts = get_todos_produtos_posts(FALSE, 'any');
	
	if($incremental) {
	    $produtos_ids = $wpdb->get_col("SELECT DISTINCT post_id FROM produtos_relacionados");
	}

	foreach ($posts as $item) {
	    set_time_limit(300);
	    
	    if($incremental) {
	        if(in_array($item->ID, $produtos_ids)) {
	            continue;
	        }
	    }
	    
	    ProdutoModel::atualizar_produtos_relacionados($item->ID);
	}

	echo "Dados de produtos-relacionados atualizados com sucesso";

}



if($op == 10 || $todos == 1) { 

	KLoader::model("ProdutoModel");

	$posts = get_todos_produtos_posts(FALSE, 'any');
	
	if($incremental) {
	    $produtos_ids = $wpdb->get_col("SELECT DISTINCT post_id FROM produtos_areas");
	}

	foreach ($posts as $item) {
	    set_time_limit(300);
	    
	    if($incremental) {
	        if(in_array($item->ID, $produtos_ids)) {
	            continue;
	        }
	    }
	    
	    ProdutoModel::atualizar_metadados_areas($item->ID);
	}

	echo "Dados de produtos-areas atualizados com sucesso";

}

if($op == 11 || $todos == 1) {
    
    KLoader::model("ProdutoModel");
    
    $posts = get_todos_produtos_posts(FALSE, 'any');
    
    if($incremental) {
        $produtos_ids = $wpdb->get_col("SELECT DISTINCT post_id FROM produtos_aulas");
    }
    
    foreach ($posts as $item) {
        set_time_limit(300);
        
        if($incremental) {
            if(in_array($item->ID, $produtos_ids)) {
                continue;
            }
        }
        
        ProdutoModel::atualizar_metadados_aulas($item->ID);
    }
    
    echo "Dados de produtos-areas atualizados com sucesso";
    
}

if($op == 12 || $todos == 1) {
    
    KLoader::model("DepoimentoModel");
    
    $posts_ids = $wpdb->get_col("SELECT DISTINCT ID FROM wp_posts WHERE post_type = 'depoimentos'");
        
    foreach ($posts_ids as $ID) {
        set_time_limit(300);
        
        DepoimentoModel::atualizar_metadados($ID, TRUE);
    }
    
    echo "Dados de depoimentos atualizados com sucesso";
    
}

if($op == 50) {
    $wpdb->query("UPDATE produtos SET pro_is_premium = 0 WHERE post_id > 0");
    $wpdb->query("UPDATE produtos SET pro_is_assinatura_sq = 0 WHERE post_id > 0");
    
    KLoader::model("ProdutoModel");
    $posts = get_todos_produtos_posts(FALSE, 'any');
    
    foreach ($posts as $item) {
            
        if(ProdutoModel::is_produto_premium($item->ID)) {
            $wpdb->query("UPDATE produtos SET pro_is_premium = 1 WHERE post_id = {$item->ID}");
        }
        
        if(is_produto_assinatura($item->ID)) {
            $wpdb->query("UPDATE produtos SET pro_is_assinatura_sq = 1 WHERE post_id = {$item->ID}");
        }
    }
}