<?php
require_once '../../wp-load.php';

global $wpdb;

KLoader::model("RodapeItemModel");

$params = [];

for($i = 1; $i <= 3; $i++) {
    $items = RodapeItemModel::list($i);

    foreach ($items as $item) {

        $url_a = parse_url($item->roi_url);

        $link = [
            'linha' => $item->roi_index,
            'coluna' => $item->roi_coluna - 1,
            'texto' => $item->roi_titulo,
            'url' => $url_a['path']
        ];

        $params[] = $link;
    }
}

$ch = curl_init('http://localhost:3000/api/rodape-links/sync');

curl_setopt( $ch, CURLOPT_HTTPHEADER, array
(
    'Content-Type: application/json',
//    'Authorization: WP 9a8bb3e1c8eba8bb248fffcc29a842d1-us1',
) );

curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
curl_setopt( $ch, CURLOPT_TIMEOUT, 30 );
curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $params ) );

$response = curl_exec( $ch );
curl_close( $ch );

echo $response;