<?php
require_once '../../wp-load.php';

KLoader::model("ProdutoModel");

// $ids = [319271];
// $ids = [210074];

global $wpdb;

$sql = "SELECT ID from wp_posts where post_type = 'product' ";

$ids = $wpdb->get_col($sql);


$out = [];
foreach ($ids as $id) {
   set_time_limit(300);
   
   $autores = get_post_meta($id, '_authors', true);
   $percentuais = get_post_meta($id, '_percentuais', true);
   
   $autores = array_diff($autores, array ("", -1));
   $percentuais = array_diff($percentuais, array ("", -1));
   
   $num_autores = count($autores);
   if($num_autores == 1) {
       
       $percentuais = array_diff($percentuais, array ("", -1));

       if(!$percentuais) {
           $meta_value = [];
           $meta_value[0] = 100;
           
           $out[] = $id;
           
           update_post_meta($id, '_percentuais', $meta_value);
           ProdutoModel::atualizar_metadados_professores($id, false);
       }
   }
   
}

echo implode(",", $out);