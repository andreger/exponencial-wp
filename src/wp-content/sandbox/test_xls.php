<?php
require_once '../../wp-load.php';

$newFilePath = "http://exponencial/wp-content/temp/test.xlsx";

header('Content-Description: File Transfer');
header('Content-Disposition: attachment; filename="test.xlsx"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($newFilePath));
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
header('Pragma: public');
header('Expires: 0');
	
//clean all levels of output buffering
// while (ob_get_level()) {
//     ob_end_clean();
// }
readfile($newFilePath);