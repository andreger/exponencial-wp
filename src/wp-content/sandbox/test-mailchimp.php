<?php
require_once '../../wp-load.php';
// expo_mailchimp([]);

// $args = array(
//     'timeout'     => 5,
//     'redirection' => 5,
//     'httpversion' => '1.1',
//     'user-agent'  => 'MailChimp WordPress Plugin/' . get_bloginfo('url'),
//     'headers'     => array("Authorization" => 'apikey ' . MAILCHIMP_API_KEY)
// );

// $url = "https://us1.api.mailchimp.com/3.0/lists/7746555c27/interest-categories/cd3440d75a/interests";
// #$url = "https://us1.api.mailchimp.com/3.0/lists/7746555c27/interest-categories";
// $request = wp_remote_get($url, $args);

// echo "<pre>";
// print_r(json_decode($request['body']));

$doc = file_get_html("http://eepurl.com/cViiW9");

$areas = $doc->find('#interestTable .groups', 0)->find('.checkbox');

$campos = [];
foreach ($areas as $area) {
	$nome = $area->find('span', 0)->plaintext;
	$id_a = explode("_", $area->find('input', 0)->id);
	$id = $id_a[1];

    array_push($campos, ['nome' => $nome, 'id' => $id]);
}

set_transient(TRANSIENT_MAILCHIMP, serialize($campos));

