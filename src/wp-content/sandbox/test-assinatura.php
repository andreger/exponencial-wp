<?php
require_once '../../wp-load.php';

global $wpdb;

$ultimo = $wpdb->get_row("select * from wp_posts where post_type = 'shop_order' and post_status = 'wc-completed' order by ID desc limit 1 ");

if($ultimo->ID < $min_id) {
	echo "Script finalizado";
}

$min_id = isset($_GET['min_id']) ? $_GET['min_id'] : 0;
$step = 1000;


$resultado = $wpdb->get_results("select * from wp_posts where post_type = 'shop_order' and post_status = 'wc-completed' and ID > {$min_id} order by ID limit {$step}");

$min_id += $step;

foreach ($resultado as $row) {
	processar_assinaturas($row->ID);
}
?>

<html>
<head>
</head>
<body>
	<script>
		 window.location.href= "/wp-content/sandbox/test-assinatura.php?min_id=<?= $min_id ?>";
	</script>
</body>
</html>
