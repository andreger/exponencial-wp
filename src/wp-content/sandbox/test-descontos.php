<?php
require_once '../../wp-load.php';

get_header();
?>
<div class="descontos">
	<div class="row">
		<div class="texto-azul desconto-titulo"></div>
		<div class="texto-azul desconto-titulo"><img src="/wp-content/themes/academy/images/desconto-geral.png" />Aqui vão algumas vantagens que você obtém como aluno do Exponencial Concursos:</div>
	</div>
	<div class="row">
		<div class="threecol column">
			<div class="descontos-pre">Se você contratou o serviço de coaching, durante a sua duração, você terá os seguintes descontos:</div>

			<div class="descontos-coluna">
				<div class="desconto-img-sup"><img src="/wp-content/themes/academy/images/aluno-coaching.png" /></div>
				<div class="descontos-coluna-interna">
					<div class="desconto-texto-normal">Indicação de alunos</div>
					<div class="desconto-texto-azul">50% em 1 mensalidade<sup>1</sup></div>
					<div class="desconto-texto-normal">Pacotes</div>
					<div class="desconto-texto-azul">25%</div>
					<div class="desconto-texto-normal">Cursos Avulsos</div>
					<div class="desconto-texto-azul">15%</div>
					<div class="desconto-texto-normal">Mapas Mentais</div>
					<div class="desconto-texto-azul">50%</div>
					<div class="desconto-texto-normal">Cadernos de Questões</div>
					<div class="desconto-texto-azul">50%</div>
					<div class="desconto-texto-normal">Sistema de Questões</div>
					<div class="desconto-texto-azul">100%</div>
					<div class="desconto-texto-normal">Simulados</div>
					<div class="desconto-texto-azul">100%<sup>2</sup></div>
				</div>
				<div class="desconto-img-inf"><a href="/painel-coaching/inscricao"><img src="/wp-content/themes/academy/images/ser-um-aluno-coaching.png" /></a></div>
			</div>
		</div>
		<div class="threecol column">
			<div class="descontos-pre">Agora, se você adquiriu algum pacote <b>de cursos</b>, então terá direito aos seguintes descontos:</div>

			<div class="descontos-coluna">
				<div class="desconto-img-sup"><img src="/wp-content/themes/academy/images/aluno-pacote.png" /></div>
				<div class="descontos-coluna-interna">
					<div class="desconto-texto-normal">Pacotes</div>
					<div class="desconto-texto-azul">15%</div>
					<div class="desconto-texto-normal">Cursos Avulsos</div>
					<div class="desconto-texto-azul">10%<sup>3</sup></div>
					<div class="desconto-texto-normal">Mapas Mentais</div>
					<div class="desconto-texto-azul">25%</div>
					<div class="desconto-texto-normal">Cadernos de Questões</div>
					<div class="desconto-texto-azul">50%</div>
					<div class="desconto-texto-normal">Sistema de Questões</div>
					<div class="desconto-texto-azul">6 meses gratuitos</div>
					<div class="desconto-texto-normal">Simulados</div>
					<div class="desconto-texto-azul">50%</div>
				</div>
				<div class="desconto-img-inf"><a href="/cursos-por-concurso"><img src="/wp-content/themes/academy/images/adquirir-um-pacote.png" /></a></div>
			</div>
		</div>
		<div class="threecol column" >
			<div class="descontos-pre">Ahhhh, e claro que não esquecemos de dar desconto para você que é nosso aluno de curso avulso ou assinante do sistema de questões. Confira:</div>
			<div class="descontos-coluna">
				<div class="desconto-img-sup"><img src="/wp-content/themes/academy/images/aluno-curso.png" /></div>
				<div class="descontos-coluna-interna">
					<div class="desconto-texto-normal">Cursos Avulsos</div>
					<div class="desconto-texto-azul">10%<sup>4</sup></div>
					<div class="desconto-texto-normal">Cadernos de Questões</div>
					<div class="desconto-texto-azul">25%</div>
				</div>
				<div class="desconto-img-inf"><a href="/cursos-por-concurso"><img src="/wp-content/themes/academy/images/escolher-um-curso.png" /></a></div>
			</div>
		</div>
		<div class="threecol column last">
			<div class="descontos-pre"></div>
			<div class="descontos-coluna">
				<div class="desconto-img-sup"><img src="/wp-content/themes/academy/images/aluno-sistema.png" /></div>
				<div class="descontos-coluna-interna">
					<div class="desconto-texto-normal">Mapas Mentais</div>
					<div class="desconto-texto-azul">25%</div>
					<div class="desconto-texto-normal">Cadernos de Questões</div>
					<div class="desconto-texto-azul">25%</div>
					<div class="desconto-texto-normal">Simulados</div>
					<div class="desconto-texto-azul">50%</div>
				</div>
				<div class="desconto-img-inf"><a href="/sistema-de-questoes/#assinatura"><img src="/wp-content/themes/academy/images/aderir-sistema-questoes.png" /></a></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="desconto-banner"><a href="/fale-conosco"><img src="/wp-content/themes/academy/images/banner-cupom.jpg" /></a></div>
	</div>
	<div class="row">
		<div class="texto-azul desconto-subtitulo">Legenda:</div>
		<div class="desconto-legenda"><span class="desconto-num">1 -</span> Válido apenas após o período mínimo definido no programa.</div>
		<div class="desconto-legenda"><span class="desconto-num">2 -</span> Conforme designação feita pelo seu Coach, ao longo do programa de coaching.</div>
		<div class="desconto-legenda"><span class="desconto-num">3 -</span> Se o aluno comprou um pacote de cursos e ele teve, após a compra, a inclusão de novo curso que não existia anteriormente, ele poderá solicitar o cupom do % do Pacote para compra do curso avulso.</div>
		<div class="desconto-legenda"><span class="desconto-num">4 -</span> Para o aluno que já comprou algum curso e deseja comprar o restante de forma a completar o Pacote existente, será dado o mesmo valor de desconto do pacote para os cursos restantes.</div>
	</div>
	<div class="desconto-info">
		<div class="row">
			<div class="twelvecol column">
				<div class="texto-azul desconto-subtitulo">Mais informações:</div>
			</div>
			<div class="sixcol column">
				<div><img src="/wp-content/themes/academy/images/check-tick.png" width="12" height="12" /> Todos os descontos são de uso exclusivo, pessoal, intransferível, não-conversível em moeda ou qualquer outro valor e não-cumulativos entre si, de uso restrito no sitio do Exponencial Concursos, respeitando as condições, regras, regulamentos e disponibilidades das ofertas.</div>
				<div><img src="/wp-content/themes/academy/images/check-tick.png" width="12" height="12" /> Os descontos para os alunos Coaching e assinantes do Sistema de Questões são válidos apenas enquanto durarem os serviços. Interrupções, postergações, e/ou cancelamentos interrompem a validade dos descontos.</div>
			</div>
			<div class="sixcol column last">
				<div><img src="/wp-content/themes/academy/images/check-tick.png" width="12" height="12" /> Os descontos para os alunos de pacote e cursos avulsos são válidos para solicitações feitas até 12 meses após o pagamento dos serviços, não se aplicando aos cursos gratuitos.</div>
				<div><img src="/wp-content/themes/academy/images/check-tick.png" width="12" height="12" /> Caso você seja aluno em mais de um serviço, você poderá usar apenas 1 dos descontos, não sendo cumulativo. Exemplo: você é aluno do Coaching e do pacote de cursos. Caso você resolva comprar um curso de mapa mental, seu desconto será de 50% (apenas o desconto para o perfil aluno coaching), sem acumular os 25% do perfil aluno pacote.</div>
				<div><img src="/wp-content/themes/academy/images/check-tick.png" width="12" height="12" /> Os descontos devem ser solicitados através do nosso "fale conosco".</div>
			</div>
		</div>
	</div>
</div>
<?php get_footer() ?>