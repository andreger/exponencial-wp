<?php
require_once '../../wp-load.php';

global $wpdb;

$dir = '../themes/academy/paginas/wp';
$files = scandir($dir);

if($files) {
	foreach ($files as $file) {
		if($file != "." && $file != "..") {
			echo "{$file}<br>";

			$filename = $_SERVER['DOCUMENT_ROOT'] . "/wp-content/themes/academy/paginas/wp/" . $file;

			$content = file_get_contents($filename);

			$file_a = explode(".", $file);

			$id = get_post_id_by_slug($file_a[0], "page");

			$post = [
				'ID' => $id,
				'post_content' => $content //novo conteúdo
			];

			wp_update_post($post);
		}
	}
}