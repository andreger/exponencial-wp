<?php
require_once '../../wp-load.php';

global $wpdb;

$order = new WC_Order($_GET['o']);
$usuario = get_userdata($order->user_id);
$usuario_array = get_usuario_array($order->user_id); 

$discriminacao = 'Curso(s) online: ';

logger_info("Compra finalizada: " . $order_id);

$data_validade = get_user_meta($order->user_id, 'assinante-sq-validade', true) ? : 'now';
$adicionar_validade = null;

foreach ($order->get_items() as $item) {

	$item_id = $item['product_id'];
	$product_name = $item['name'];

	echo $product_name . "<br>";

	$post = get_post($item_id);
	
	$notificar_vendas = get_post_meta($item_id, 'notificar_vendas', true) == 'yes';

	echo "Notificar vendas: ";
	echo $notificar_vendas ? "Sim" : "Nao";
	echo "<br>";

	if($notificar_vendas) {
		$autores = get_autores($post);
	
		$discriminacao .= $product_name.', ';
	
		echo "Preparando para notificar " . count($autores) . " professor(es) <br>";
		
		foreach ($autores as $author)
		{
			$author_email = $author->user_email;
			
			echo "E-mail de notificação de novo aluno enviado para professor " . $author_email . " <br>";
		}
		
	}
	else {
		echo "Pulando envio de e-mail de aviso de novo aluno (opção Notificar Vendas desmarcada: " . $product_name . "<br>";
	}

	echo 'Buscando cupom pelo item slug: ' . $post->post_name . '<br>';

	$cupom = get_cupom_bonus_by_item_slug($post->post_name, ITEM_BONUS_PRODUTO);

	if(!$cupom) {
		echo 'Nao encontramos cupom com item slug: ' . $post->post_name . '<br>';
		echo 'Listando categorias do post: ' . $post->ID . '<br>';

		$categorias = listar_categorias($post->ID);
		
		echo 'Achamos '. count($categorias) . ' categorias do post: ' . $post->ID . '<br>';

		foreach ($categorias as $categoria) {

			echo 'verificando categorias com slug: ' . $categoria->slug . '<br>';

			if($cupom = get_cupom_bonus_by_item_slug($categoria->slug, ITEM_BONUS_CATEGORIA)) {
				echo 'Achou cupom de categoria: ' . $categoria->slug . '<br>';
				break;
			}
		}
	}
	
	if($cupom) {
		echo 'Temos cupom para item: ' . $post->post_name . '<br>';
		echo 'Adicionando restricoes para item: ' . $post->post_name . ' usuario: ' . $order->user_id. ' cupom: ' . $cupom->post_name . '<br>';
	}
	else {
		echo 'nao ha cupons para esse item: ' . $post->post_name . '<br>';
	}
	
	$categorias = listar_categorias($item_id);
	
	foreach ($categorias as $categoria) {
		echo "verificando se produto é assinatura: {$item_id}, categorias: " . serialize($categoria) . "<br>";
		
		if(is_categoria_assinatura_sq_mensal($categoria)) {
			echo 'É assinatura mensal<br>';
			$adicionar_validade += 1;
		}
		elseif(is_categoria_assinatura_sq_trimestral($categoria)) {
			echo 'É assinatura trimenstral<br>';
			$adicionar_validade += 3;
		}
		elseif(is_categoria_assinatura_sq_semestral($categoria)) {
			echo 'É assinatura semestral<br>';
			$adicionar_validade += 6;
		}
		elseif(is_categoria_assinatura_sq_anual($categoria)) {
			echo 'É assinatura anual<br>';
			$adicionar_validade += 12;
		}
		
	}
	
}

if($adicionar_validade) {
	$nova_validade = date('Y-m-d', strtotime($data_validade . " +" . $adicionar_validade . " months"));

	echo "Validade: $nova_validade<br>";
}
