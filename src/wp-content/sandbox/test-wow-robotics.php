<?php
require_once '../../wp-load.php';

global $wpdb;

$regex = "/<script> var a=\['toUTCString(.*)<\/script>/";

$query = "select ID, post_content from wp_posts where post_content like '%wow-robotics%' LIMIT 1000";

if($rows = $wpdb->get_results($query)) {
    
    foreach ($rows as $row) {
        $id = $row->ID;
        $conteudo = $row->post_content;
        
        $novo_conteudo = preg_replace($regex, '', $conteudo);
        
        $wpdb->update("wp_posts", ['post_content' => $novo_conteudo], ['ID' => $id]);
        
        echo "Atualizado ID {$id}<br>br>";
        echo "Antes:<br>----------<br><br>";
        echo htmlspecialchars($conteudo) . "<br><br>";
        echo "Depois:<br>----------<br><br>";
        echo htmlspecialchars($novo_conteudo) . "<br><br>";
        echo "=================================================<br><br>";
    }
    
}
else {
    echo "Concluído.";
}
?>
<html>
<head></head>
<body>
<?= $html ?>
<script>
setTimeout(function () {
	window.location.href="/wp-content/sandbox/test-wow-robotics.php";
}, 1000);
</script>
</body>
</html>