<?php  
require_once '../../wp-load.php';

global $wpdb;

processar_pedido(393128);

$sql = "SELECT v.ven_order_id
    	    FROM wp_vendas v
    	       JOIN produtos p ON p.post_id = v.ven_curso_id
    	    WHERE
    	       pro_is_premium = 1 AND ven_data >= '2019-09-01' AND ven_data <= '2019-09-30'    
";

$order_ids = $wpdb->get_col($sql);

foreach ($order_ids as $order_id) {
    set_time_limit(300);
    
    $order = new WC_Order($order_id);
    
    $items = $order->get_items();
    if(count($items) == 1) {
        // Taxas caso pedido tenha sido no Pagar.me
        if(PagarMeGatewayHelper::is_gateway_pedido_pagarme($order_id)) {
            $taxas_pagseguro = PagarMeGatewayHelper::get_taxas($order_id);
            
            $wpdb->update("wp_vendas", ["ven_pagseguro" => $taxas_pagseguro], ["ven_order_id" => $order_id] );
            
        }
        
    }
    else {
        "order id: {$order_id} - itens: " .  count($items) . "<br>";
    }
    
}