<?php
use MatthiasMullie\Minify\Exception;

require_once '../../wp-load.php';

global $wpdb;

$min_id = isset($_GET['min_id']) ? $_GET['min_id'] : 0;
$user_id = isset($_GET['user_id']) ? $_GET['user_id'] : 0;
$step = 1000;

if($user_id){
	$user_where = " and user_id = {$user_id} ";
}else{
	$user_where = "";
}
$ultimo = $wpdb->get_row("select * from assinaturas where preco is null {$user_where} order by id desc limit 1 ");

if($ultimo->id < $min_id) {
	echo "Script finalizado";
	exit();
}


$resultado = $wpdb->get_results("select * from assinaturas where preco is null {$user_where} and id > {$min_id} order by id limit {$step}");

$min_id += $step;

foreach ($resultado as $row) {

	$order = new WC_Order($row->order_id);

	if($items = $order->get_items()) {

		foreach ($items as $item) {

			$product_id = $item['product_id'];

			if($tempo = get_produto_assinatura_tipo($product_id)) {
				
				$valores_produtos = get_post_meta($order->get_id(), 'valores-produtos', true);
				
				$wpdb->update("assinaturas", ['preco' => $valores_produtos[$item->get_id()]], ['id' => $row->id]);
				
			}	
		}
	}
	
}
?>

<html>
<head>
</head>
<body>
	<script>
		 window.location.href= "/wp-content/sandbox/test-assinatura-preco.php?min_id=<?= $min_id ?>&user_id=<?= $user_id ?>";
	</script>
</body>
</html>
