<?php
include_once '../../wp-load.php';

KLoader::model("PremiumModel");

$premiums = PremiumModel::listar_produtos();

KLoader::model("ProdutoModel");

if($premiums) {
    
    $data = $_GET['d'];
    
    $mes = substr($data, 4, 2);
    $ano = substr($data, 0, 4);
    
    echo "-------------------------------------------------------------------------------------------------<br>";
    echo "Processamento de Itens de produto premium - $mes / $ano <br>";
    echo "-------------------------------------------------------------------------------------------------<br>";
    echo "<br>";
    
    foreach ($premiums as $item) {
        set_time_limit(300);
        
        echo "-------------------------------------------------------------------------------------------------<br>";
        echo "Premium: " . $item->post_title . "<br>";
        echo "ID: " . $item->ID . "<br>";
        echo "-------------------------------------------------------------------------------------------------<br>";
        
        echo "<br>";
        
        PremiumModel::processar_info($data, $item->post_id);
        
        
    }
    
}


echo "Produtos premiums";