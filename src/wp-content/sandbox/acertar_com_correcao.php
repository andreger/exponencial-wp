<?php
require_once '../../wp-load.php';

global $wpdb;

KLoader::model("ProdutoModel");

$posts = get_todos_produtos_posts(FALSE, 'any');


foreach ($posts as $item) {
    set_time_limit(300);
    
    $is_com_correcao = is_com_correcao($item->ID) ? 1 : 0;
    $is_coaching = is_produto_coaching($item->ID) ? 1 : 0;
    
    $dados = [
        'pro_com_correcao' => $is_com_correcao,
        'pro_is_coaching' => $is_coaching
    ];
    
    if($dados) {
        $wpdb->update('produtos', $dados, ['post_id' => $item->ID]);
    }
}

echo "Dados 'com correcao' e coaching atualizados no produto com sucesso";