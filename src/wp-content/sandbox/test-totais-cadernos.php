<?php
require_once '../../wp-load.php';

if(!tem_acesso([ADMINISTRADOR])) exit; 

KLoader::helper("AcessoGrupoHelper");

global $wpdb;

set_time_limit(300);

$limite = 100;

$offset = $_GET['offset']?:0;

$usuario_id = $_GET['usu_id']?:0;

$sql_usuario = "";
if($usuario_id > 0){
    $sql_usuario = " AND usu_id = {$usuario_id}";
}

$sql = "SELECT COUNT(*) FROM exponenc_corp.cadernos WHERE 1=1 {$sql_usuario}";

//echo "<br>{$sql}";

$total = $wpdb->get_var($sql);

$sql = "SELECT cad_id, usu_id FROM exponenc_corp.cadernos WHERE 1=1 {$sql_usuario} ORDER BY cad_id DESC LIMIT {$limite} OFFSET {$offset}";

//echo "<br>{$sql}";

$result = $wpdb->get_results($sql);


foreach($result as $cad){

    $cad_id = $cad->cad_id;
    $usu_id = $cad->usu_id;

    $sql_totais = 
        "SELECT COUNT(*) 
            FROM exponenc_corp.cadernos_questoes cq
                INNER JOIN exponenc_corp.questoes_resolvidas qr ON qr.que_id = cq.que_id
                INNER JOIN exponenc_corp.questoes q ON qr.que_id = q.que_id
            WHERE qr.cad_id = {$cad_id}
                AND cq.cad_id = {$cad_id}
                AND qr.usu_id = {$usu_id}
         ";
    
    $incluir_inativas = is_administrador($usu_id) || is_coordenador_sq($usu_id) || is_professor($usu_id) ;
    
    //echo "<br>Incluir inativas: {$incluir_inativas}";
    
    if(!$incluir_inativas){
        $sql_totais .= " AND q.que_ativo = " . SIM;
    }

    //echo "<br>{$sql_totais}";
    
    $certas = $wpdb->get_var($sql_totais . " AND qre_selecao = q.que_resposta ");
    $erradas = $wpdb->get_var($sql_totais . " AND qre_selecao != q.que_resposta ");		
    
    $sql_total_questoes = 
        "SELECT COUNT(*) 
            FROM exponenc_corp.cadernos_questoes cq 
                INNER JOIN exponenc_corp.questoes q ON q.que_id = cq.que_id
            WHERE cq.cad_id = {$cad_id}
        ";

    if(!$incluir_inativas){
        $sql_total_questoes .= " AND q.que_ativo = " . SIM;
    }
    
    //echo "<br>{$sql_total_questoes}";

    $total_questoes = $wpdb->get_var($sql_total_questoes);

    $sql_update = 
        "UPDATE exponenc_corp.cadernos 
            SET cad_qtd_erros = {$erradas}
            , cad_qtd_acertos = {$certas}
            , cad_qtd_questoes_usuario = {$total_questoes} 
        WHERE cad_id = {$cad_id}";
    
    //echo "<br>{$sql_update}";

    $wpdb->query($sql_update);

}

$offset += $limite;

if($offset <= $total) : ?>
<html>
<head>
<meta http-equiv="Refresh" content="1">
</head>
<body>
	<?php echo("GERANDO totais de cadernos COM OFFSET {$offset} PARA O TOTAL DE ".$total." cadernos<br/>"); ?>
	<script>
        window.location.replace('/wp-content/sandbox/test-totais-cadernos.php?offset=<?= $offset ?>&usu_id=<?= $usuario_id ?>');
     </script>
</body>
</html>
<?php else : ?>
Script finalizado.
<?php endif ?>
