<?php
require_once '../../wp-load.php';

$product_id = $_GET['p'];
echo $product_id;
$relacionados = array();

foreach (get_produtos_relacionados($product_id, 20) as $relacionado) {
	
	$preco = is_pacote($relacionado) ? $relacionado->max_price : $relacionado->get_price();
	if($preco == 0) continue;
	
	if(count($relacionados) < 4) {
		if(!in_array($relacionado, $relacionados)) {
			array_push($relacionados, $relacionado);
		}	
	}													
	
}

echo "<pre>";
print_r($relacionados);