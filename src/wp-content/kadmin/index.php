<?php
require_once '../../wp-load.php';

if(!tem_acesso([ADMINISTRADOR, ATENDENTE])) exit; 
?>

<html>

<body>
    <h1>KAdmin</h1>
    
    Hora do servidor: <?= date("d-m-Y H:i") ?>
    
    <?php if(tem_acesso([ADMINISTRADOR])) : ?>
    <ul>
    	<li><a href="/wp-content/kadmin/usuarios.php">Login de Usuários</a></li>
    	<li><a href="/wp-content/kadmin/pagseguro.php">PagSeguro</a></li>
    	<li><a href="/wp-content/kadmin/pagarme.php">Pagar.me</a></li>
    	<li><a href="/wp-content/kadmin/phpinfo.php">PHP Info</a></li>
    </ul>
    
    <h2>Scripts</h2>
     <ul>
    	<li><a href="/wp-content/scripts/atualizar_vendas.php">Atualizar Vendas</a></li>
    	<li><a href="/wp-content/kadmin/scripts/atualizar_status_concursos.php">Atualizar Status do Concursos</a> - Atualiza o status de todos os concursos</li>
    	<li><a href="/wp-content/kadmin/scripts/atualizar_qtde_cursos_concursos.php">Atualizar Qtde de Cursos do Concursos</a> - Atualiza a quantidade de cursos de todos os concursos</li>
    </ul>
    <?php endif ?>
    
    <h2>Integridade de Base</h2>
     <ul>
    	<li><a href="/wp-content/kadmin/pedidos_usuario.php">Atualizar Pedidos do Usuário</a> - Recria os pedidos de um usuário na tabela pedido_produto </li>
    </ul>  
    
</body>

</html>
