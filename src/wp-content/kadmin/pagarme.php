<?php
require_once '../../wp-load.php';

KLoader::helper("PagarMeGatewayHelper");

if(!tem_acesso([ADMINISTRADOR])) exit; 

if($_POST['submit']) {
 
    $id = trim($_POST['id']);

    if(PagarMeGatewayHelper::is_gateway_pedido_pagarme($id)) {
        $taxas = PagarMeGatewayHelper::get_taxas($id);
    }
}
?>

<html>

<body>

    <h1>Pagar.me</h1>
    
    <div>
    	<h2>Pedido Id</h2>
    	<form method="post">
    		<input type="text" name="id" id="txb-id" value="<?= isset($_POST['id']) ? $_POST['id'] : '' ?>">
    		<input type="submit" name="submit" value="Consultar" id="submit">
    	</form>
    </div>
    
    <?php if($taxas) : ?>
    <div>
        <h2>XML</h2>
        
        <pre><?= $taxas ?></pre>
    </div>
    <?php endif ?>
</body>

</html>