<?php
require_once '../../wp-load.php';

global $wpdb;

if(!tem_acesso([ADMINISTRADOR, ATENDENTE])) exit; 

KLoader::model("PedidoModel");

$id = isset($_GET['id']) ? $_GET['id'] : null;

$login = isset($_GET['login']) ? $_GET['login'] : null;
if($login){
    $user = get_user_by('login', $login );
    $id = $user->ID;
}

$msg = "";
if($id){
    $sql = "SELECT post_id
			FROM wp_postmeta
			WHERE meta_key='_customer_user'
				AND meta_value='{$id}'
				AND post_id NOT IN (SELECT pedido_oculto_id FROM premium_log)
			ORDER BY meta_id DESC";
    
    $result = $wpdb->get_results($sql);
    
    
    
    foreach ( $result as $row ) {
        $post_id = $row->post_id;
        PedidoModel::alterar_pedido_status_usuario($post_id);
    }

    $msg = "Pedidos atualizados com sucesso";
}
else {
    $msg = "Um usuário precisa ser escolhido";
}

?>

<html>

<body>

    <h1>Atualizar Pedidos do Usuário</h1>
    
    <div><?= $msg ?></div>
    
    <div>
    	<h2>ID do Usuário</h2>
    	<form method="get">
    		<input type="text" name="id" id="txb-id">
    		<input type="submit" value="Atualizar" id="submit">
    	</form>
    	
    	<h2>E-mail do Usuário</h2>
    	<form method="get">
    		<input type="text" name="login" id="txb-login">
    		<input type="submit" value="Atualizar" id="sbm-login">
    	</form>
    </div>
    

</body>

</html>