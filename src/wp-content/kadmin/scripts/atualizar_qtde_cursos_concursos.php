<?php
require_once '../../../wp-load.php';

if(!tem_acesso([ADMINISTRADOR])) exit;

KLoader::model("ConcursoModel");

$concursos = ConcursoModel::listar_todos();

foreach ($concursos as $concurso) {    
    ConcursoModel::atualizar_metadados_qtde_produtos($concurso->con_id);
}
