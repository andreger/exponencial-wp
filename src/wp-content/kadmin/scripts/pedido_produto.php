<?php
require_once '../../../wp-load.php';

global $wpdb;

KLoader::model("PostModel");

KLoader::model("PedidoModel");

set_time_limit(300);

$limite = 100;

$offset = $_GET['offset']?:0;

$total = $_GET['total'];
if(!$total){
    $sql = 
    "SELECT COUNT(DISTINCT p.ID) FROM wp_posts p, wp_woocommerce_order_items oi, wp_woocommerce_order_itemmeta oim 
        WHERE oi.order_id = p.ID 
            AND oim.order_item_id = oi.order_item_id 
            AND oim.meta_key = '_product_id' 
            AND p.post_type = 'shop_order' 
            AND NOT EXISTS(SELECT 1 FROM pedido_produto pp WHERE pp.pedido_id = p.ID) 
            AND EXISTS(SELECT 1 FROM wp_posts prd WHERE prd.post_type = 'product' AND prd.ID = oim.meta_value)";
    $total = $wpdb->get_var($sql);
}


$sql = 
    "SELECT DISTINCT p.ID FROM wp_posts p, wp_woocommerce_order_items oi, wp_woocommerce_order_itemmeta oim 
        WHERE oi.order_id = p.ID 
            AND oim.order_item_id = oi.order_item_id 
            AND oim.meta_key = '_product_id' 
            AND p.post_type = 'shop_order' 
            AND NOT EXISTS(SELECT 1 FROM pedido_produto pp WHERE pp.pedido_id = p.ID) 
            AND EXISTS(SELECT 1 FROM wp_posts prd WHERE prd.post_type = 'product' AND prd.ID = oim.meta_value) 
        ORDER BY p.ID DESC LIMIT $limite";

$result = $wpdb->get_results($sql);

foreach($result as $pedido){

    $pedido_id = $pedido->ID;

    PedidoModel::alterar_pedido_status_usuario($pedido_id);

}

$offset += $limite;

if($offset <= $total) : ?>
<html>
<head>
<meta http-equiv="Refresh" content="1">
</head>
<body>
	<?php echo("<br/>GERANDO PEDIDO_PRODUTO COM OFFSET {$offset} PARA O TOTAL DE ".$total." PEDIDOS<br/>"); ?>
	<script>
        window.location.replace('/wp-content/kadmin/scripts/pedido_produto.php?offset=<?= $offset ?>&total=<?= $total ?>');
     </script>
</body>
</html>
<?php else : ?>
Script finalizado.
<?php endif ?>