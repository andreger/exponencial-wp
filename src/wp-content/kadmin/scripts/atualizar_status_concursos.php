<?php
require_once '../../../wp-load.php';

global $wpdb;

if(!tem_acesso([ADMINISTRADOR])) exit;


KLoader::model("ConcursoModel");

$concursos = $wpdb->get_results("SELECT * FROM wp_posts WHERE post_type = 'concurso' ");

$divulgado = [];
$proximo = [];
foreach ($concursos as $concurso) {
    
    if(ConcursoModel::get_by_id($concurso->ID)) {
        $status = get_post_meta($concurso->ID, 'status_do_concurso', true);
        
        echo "Status {$status} no Concurso {$concurso->ID} {$concurso->post_title}...<br>";
        
        if($status == EDITAL_DIVULGADO) {
            $divulgado[] = "{$concurso->ID} {$concurso->post_title}";
        }
        
        if($status == EDITAL_PROXIMO) {
            $proximo[] = "{$concurso->ID} {$concurso->post_title}";
        }
        
        $wpdb->update("concursos", ["con_status" => $status], ['con_id' => $concurso->ID]);
    }
}
?>
<br><br>
Atualizados <?= count($concursos) ?> concursos.
<br><br>
=====================
Edital Divulgado
=====================
<br>
<?php 
$i = 1; 
foreach ($divulgado as $item) {
    echo $i . " - " . $item . "<br>";
    $i++;
}
?> 
<br><br>
=====================
Edital Próximo
=====================
<br>
<?php 
$i = 1; 
foreach ($proximo as $item) {
    echo $i . " - " . $item . "<br>";
    $i++;
}
?> 
