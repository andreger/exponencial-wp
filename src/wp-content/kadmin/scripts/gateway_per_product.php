<?php
require_once '../../../wp-load.php';

global $wpdb;

if(!tem_acesso([ADMINISTRADOR])) exit;

$offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
$limit = 100;   

$total = $wpdb->get_var("SELECT COUNT(*) as qtde FROM wp_posts WHERE post_type = 'product'");
$sql = "SELECT * FROM wp_posts WHERE post_type = 'product' ORDER BY ID ASC LIMIT {$offset}, {$limit} ";

$restantes = $total - $offset;
$html = "Produtos restantes: {$restantes}<br><br>";

$produtos = $wpdb->get_results($sql);

foreach ($produtos as $produto) {
    $categorias_produto = wp_get_object_terms($produto->ID, 'product_cat');
    $categorias = [TERM_GATEWAY_PER_PRODUCT];
    
    foreach ($categorias_produto as $categoria_produto) {
        array_push($categorias, $categoria_produto->term_id);
    }
    
    $html .= "Atualizando produto {$produto->ID}<br>";
    
    wp_set_post_terms($produto->ID, $categorias, 'product_cat');
    update_post_meta($produto->ID, 'sd_payments', ['pagseguro']);
}

$offset += $limit;
?> 
<html>
<head></head>
<body>
<?= $html ?>
<script>
setTimeout(function () {
	window.location.href="/wp-content/kadmin/scripts/gateway_per_product.php?offset=<?= $offset ?>"
}, 3000);
</script>
</body>
</html>