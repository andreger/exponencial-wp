<?php
require_once '../../../wp-load.php';

global $wpdb;

KLoader::model("PostModel");

KLoader::model("PedidoModel");

set_time_limit(300);

$limite = 100;

$offset = $_GET['offset']?:0;

$total = $_GET['total'];
if(!$total){
    $sql = "SELECT COUNT(*) FROM wp_posts WHERE post_type = 'shop_subscription'";
    $total = $wpdb->get_var($sql);
}


$sql = "SELECT post_parent FROM wp_posts WHERE post_type = 'shop_subscription' ORDER BY ID ASC LIMIT $limite";

$result = $wpdb->get_results($sql);
foreach($result as $pedido){
    
    $pedido_id = $pedido->post_parent;

    PedidoModel::alterar_pedido_status_usuario($pedido_id);

}

$offset += $limite;

if($offset <= $total) : ?>
<html>
<head>
<meta http-equiv="Refresh" content="1">
</head>
<body>
	<?php echo("<br/>GERANDO PEDIDO_PRODUTO para assinaturas recorrentes COM OFFSET {$offset} PARA O TOTAL DE ".$total." PEDIDOS<br/>"); ?>
	<script>
        window.location.replace('/wp-content/kadmin/scripts/atualizar_pedido_produto_assinatura_recorrente.php?offset=<?= $offset ?>&total=<?= $total ?>');
     </script>
</body>
</html>
<?php else : ?>
Script finalizado.
<?php endif ?>