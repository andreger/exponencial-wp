<?php
require_once '../../wp-load.php';

if(!tem_acesso([ADMINISTRADOR])) exit; 

if($_POST['submit']) {
    
    $pagseguro_id = get_pagseguro_transaction_id($_POST['id']);
    $base = "https://ws.pagseguro.uol.com.br/v3/transactions/". $pagseguro_id ;
    $data = "email=leonardo.coelho@exponencialconcursos.com.br&token=67E47689AEB74EF685A9B47B2FCB1E4A";
    
    $url = $base . '?' . $data;
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_URL, $url );
    $return = curl_exec($ch);
    curl_close($ch);
 
    
}
?>

<html>

<body>

    <h1>PagSeguro</h1>
    
    <div>
    	<h2>Pedido Id</h2>
    	<form method="post">
    		<input type="text" name="id" id="txb-id" value="<?= isset($_POST['id']) ? $_POST['id'] : '' ?>">
    		<input type="submit" name="submit" value="Consultar" id="submit">
    	</form>
    </div>
    
    <?php if($return) : ?>
    <div>
        <h2>XML</h2>
        
        <pre><?= $return ?></pre>
    </div>
    <?php endif ?>
</body>

</html>