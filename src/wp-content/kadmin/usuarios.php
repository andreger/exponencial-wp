<?php
require_once '../../wp-load.php';

if(!tem_acesso([ADMINISTRADOR])) exit; 

KLoader::model("ColaboradorModel");

$id = isset($_GET['id']) ? $_GET['id'] : null;
$user = null;

if($id){
    $user = get_user_by('ID', $id ); 
}

$login = isset($_GET['login']) ? $_GET['login'] : null;
if($login){
    $user = get_user_by('login', $login );
}

$email = isset($_GET['email']) ? $_GET['email'] : null;
if($email){
    $user = get_user_by('email', $email );
}

if($user) {
    // Redirect URL //
    if ( !is_wp_error( $user ) )
    {
        wp_clear_auth_cookie();
        wp_set_current_user ( $user->ID );
        wp_set_auth_cookie  ( $user->ID );
        
        $redirect_to = user_admin_url();
        wp_safe_redirect( $redirect_to );
        exit();
    }
}


// Lista de Professores
$professores = ColaboradorModel::listar_por_tipo(COLABORADOR_PROFESSOR);
$consultores = ColaboradorModel::listar_por_tipo(COLABORADOR_CONSULTOR);
$coordenadores_sq = listar_coordenadores_sq_ativos();
$coordenadores_areas = listar_coordenadores_areas_ativos();
$coordenadores_coaching = listar_coordenadores_coaching_ativos();

?>

<html>

<body>

    <h1>Login de Usuários</h1>
    
    <div>
    	<h2>Por ID</h2>
    	<form method="get">
    		<input type="text" name="id" id="txb-id">
    		<input type="submit" value="Logar" id="submit">
    	</form>
    </div>
    
    <div>
    	<h2>Por Login</h2>
    	<form method="get">
    		<input type="text" name="login" id="txb-login">
    		<input type="submit" value="Logar" id="sbm-login">
    	</form>
    </div>
    
    <div>
    	<h2>Por E-mail</h2>
    	<form method="get">
    		<input type="text" name="email" id="txb-email">
    		<input type="submit" value="Logar" id="sbm-email">
    	</form>
    </div>
    
    <div>
    	<h2>Administradores</h2>	
    	<a href="?id=1">Leonardo Coelho</a> | <a href="?id=192">Lucas Salvetti</a>
    </div>
    
    <div>
    	<h2>Coordenadores SQ</h2>	
    	<?php foreach ($coordenadores_sq as $item) : ?>
    	<a href="?id=<?= $item->ID ?>"><?= $item->display_name ?></a> | 
    	<?php endforeach; ?>
    </div>
    
    <div>
    	<h2>Coordenadores Áreas</h2>	
    	<?php foreach ($coordenadores_areas as $item) : ?>
    	<a href="?id=<?= $item->ID ?>"><?= $item->display_name ?></a> | 
    	<?php endforeach; ?>
    </div>
    
    <div>
    	<h2>Coordenadores Coaching</h2>	
    	<?php foreach ($coordenadores_coaching as $item) : ?>
    	<a href="?id=<?= $item->ID ?>"><?= $item->display_name ?></a> | 
    	<?php endforeach; ?>
    </div>
    
    <div>
    	<h2>Professores</h2>	
    	<?php foreach ($professores as $item) : ?>
    	<a href="?id=<?= $item->ID ?>"><?= $item->display_name ?></a> | 
    	<?php endforeach; ?>
    </div>
    
    <div>
    	<h2>Consultores</h2>	
    	<?php foreach ($consultores as $item) : ?>
    	<a href="?id=<?= $item->ID ?>"><?= $item->display_name ?></a> | 
    	<?php endforeach; ?>
    </div>

</body>

</html>