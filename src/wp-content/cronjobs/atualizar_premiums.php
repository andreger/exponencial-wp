<?php
/**
 * Atualiza tabela de produtos diariamente informando se há aulas pendentes e cursos expirados
 *
 * @package cronjobs
 *
 * @since K5
 *
 * --------------------------------------------------------
 *
 * CRONJOB:
 *
 * 4	3	*	*	*
 *
 * wget -O /dev/null
 *
 * https://www.exponencialconcursos.com.br/wp-content/cronjobs/atualiza_status_produto_aulas.php
 *
 */

include_once '../../wp-load.php';

KLoader::model("PremiumModel");

$premiums = PremiumModel::listar_produtos();

KLoader::model("ProdutoModel");

if($premiums) {
    
    foreach ($premiums as $item) {
        set_time_limit(300);
        $data = date("Ym");
        echo $data . " " . $item->post_id . "<br>";
        PremiumModel::processar_info($data, $item->post_id);
    }
    
}


echo "Produtos premiums";

