<?php

/**
 * Envia e-mail para usuários que tiveram pedidos cancelados pelo PagSeguro
 * 
 * @package cronjobs
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2192
 *
 * @since j4
 * 
 * --------------------------------------------------------
 * 
 * CRONJOB:
 * 
 * 13	*	*	*	*
 * 
 * wget -O /dev/null 
 * 
 * https://www.exponencialconcursos.com.br/wp-content/cronjobs/notificacao_pedidos_cancelados_pagseguro.php
 * 
 */

include_once '../../wp-load.php';

global $wpdb;

log_wp('debug', "[Cancelamento de Pedido] Iniciando rotina de notificacao de pedido cancelado");

$pedidos = listar_pedidos_cancelados_pagseguro();

if($pedidos) {

	foreach ($pedidos as $pedido) {

		$pedido_id = $pedido->id;

		log_wp('debug', "[Cancelamento de Pedido] Processando pedido: {$pedido->id}");

		if(get_post_meta($pedido->id, NOTIFICACAO_CANCELAMENTO_PAGSEGURO, TRUE)) {
			log_wp('debug', "[Cancelamento de Pedido] Notificacao ja enviada");
			continue;
		}
		else {
			$usuario = get_usuario_array($pedido->get_user_id());

			$email = get_template_email('pedido-cancelado.php', array('nome' => $usuario['nome_completo'], 'pedido_id' => $pedido->id));
			enviar_email($usuario['email'], 'Exponencial Concursos - Pedido Cancelado', $email, EMAIL_WP_CONTATO, [EMAIL_WP_CONTATO, EMAIL_LEONARDO]);
			log_wp("debug", "[Cancelamento de Pedido] Enviado e-mail de cancelamento do pedido {$pedido->id} feito pelo aluno {$usuario['nome_completo']}");

			update_post_meta($pedido->id, NOTIFICACAO_CANCELAMENTO_PAGSEGURO, 1);
		}	
	}

}
else {
	log_wp("debug", "[Cancelamento de Pedido] Não foram encontrados pedidos cancelados para serem notificados");	
}
