<?php

/**
 * Reprocessa produtos premiums
 *
 * @package cronjobs
 *
 *
 * @since L1
 *
 * --------------------------------------------------------
 *
 * CRONJOB:
 *
 *  *	*	*	*	*
 *
 * wget -O /dev/null
 *
 * https://www.exponencialconcursos.com.br/wp-content/cronjobs/reprocessar_premiums.php
 *
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

set_time_limit(360);

include_once '../../wp-load.php';

KLoader::model("PremiumModel");

// exclui downloads expirados
$filtros = [
    'status' => PREMIUM_STATUS_PENDENTE,
    'limit' => 1
];

$agendamentos = PremiumModel::listar_agendamentos($filtros);

if($agendamentos) {
    foreach ($agendamentos as $agendamento) {
        $info = PremiumModel::processar_info($agendamento->pra_ano_mes, $agendamento->pro_id);
        $info_professores = PremiumModel::processar_info_professores($agendamento->pra_ano_mes, $agendamento->pro_id);

        if($info && $info_professores) {
            PremiumModel::atualizar_status_agendamento($agendamento->pra_id, PREMIUM_STATUS_CONCLUIDO);
        }
    }
}
?>
<script>
    setTimeout(function(){
        window.location.reload(1);
    }, 1000);
</script>