<?php
/**
 * Altera posts e questões que contenham http://www.exponencialconcursos.com.br para HTTPS://www.exponencialconcursos.com.br
 *
 * @package cronjobs
 *
 * @since L1
 *
 * --------------------------------------------------------
 *
 * CRONJOB:
 *
 * 4	3	*	*	*
 *
 * wget -O /dev/null
 *
 * https://www.exponencialconcursos.com.br/wp-content/cronjobs/atualizar_seo_https.php
 *
 */

include_once '../../wp-load.php';

KLoader::model("QuestaoModel");

$antigo = "http://www.exponencialconcursos.com.br";
$novo = "https://www.exponencialconcursos.com.br";

replace_conteudo_de_posts($antigo, $novo);
replace_conteudo_de_usermetas($antigo, $novo);
QuestaoModel::replace_conteudo_de_questoes($antigo, $novo);

echo "Links corrigidos com sucesso.";