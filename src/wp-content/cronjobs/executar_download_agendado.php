<?php

/**
 * Executa downloads agendados
 *
 * @package cronjobs
 *
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2235
 *
 * @since K5
 *
 * --------------------------------------------------------
 *
 * CRONJOB:
 *
 *  *	*	*	*	*
 *
 * wget -O /dev/null
 *
 * https://www.exponencialconcursos.com.br/wp-content/cronjobs/executar_download_agendado.php
 *
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

set_time_limit(360);

include_once '../../wp-load.php';

KLoader::model("DownloadModel");
KLoader::helper("DownloadHelper");

// exclui downloads expirados
DownloadModel::excluir_expirados();

// lista primeiro download pendente, com menos de 10 tentativas
$download = DownloadModel::get_primeiro();

// caso não tenha download agendado, encerra script
if(!$download) {
    exit;
}

// Incrementa número de tentativas
DownloadModel::incrementa_tentativas($download->dow_id);

// verifica tipo do relatório
switch ($download->dow_relatorio_id) {
    
    case DOWNLOAD_RELATORIO_VENDAS: {
        $arquivo = exportar_relatorio_vendas($download);
        break;
    }
    
    case DOWNLOAD_BAIXAR_TODAS: {
        $arquivo = baixar_todas_aulas($download);
        break;
    }
}

// Atualiza o status e arquivo
if($arquivo) {

    $dados = [
        'dow_id' => $download->dow_id,
        'dow_arquivo' => $arquivo,
        'dow_status' => DOWNLOAD_STATUS_CONCLUIDO
    ];
    
    DownloadModel::atualizar($dados);
    
    update_option(DownloadHelper::get_notificacao_id($download->user_id), "1", false);
}

function exportar_relatorio_vendas($download)
{
    KLoader::model("RelatorioVendasModel");
    KLoader::helper("RelatorioVendasHelper");
    
    $filtros = unserialize($download->dow_filtros);
    
    // tratamento dos filtros
    $filtros['start_date'] = converter_para_yyyymmdd($filtros['start_date']);
    $filtros['end_date'] = converter_para_yyyymmdd($filtros['end_date']); 
    $filtros['user_id'] = $download->user_id;
    
    $lines = RelatorioVendasModel::get_dados($filtros);
    return RelatorioVendasHelper::exportar_xls($lines, $download->user_id);
}

function baixar_todas_aulas($download)
{
    KLoader::model("PedidoModel");
    KLoader::helper("PedidoHelper");
    
    log_zip("INFO", "Iniciando zip cron");
    
    $filtros = unserialize($download->dow_filtros);
    $produto_id = $filtros['produto_id'];
    $pedido_id = $filtros['pedido_id'];
    
    $produto = wc_get_product($produto_id);   
    $nome = sanitize_title($produto->get_title()) . "-" . time();
    
    // tratamento dos filtros
    $arquivos = PedidoModel::listar_arquivos_aulas_disponiveis_agrupados_por_aula($produto_id, $pedido_id);

    $lista = PedidoHelper::nivelar_arquivos_aulas_disponiveis($arquivos);
    
    if(!$lista) {
        DownloadModel::remover($download->dow_id);
    }
    
    log_zip("INFO", "Contabilizando downloads");
    PedidoModel::registrar_download_curso($pedido_id, $produto_id);
    
    log_zip("INFO", "Removendo atualizações");
    remover_todas_atualizacoes($produto_id, $download->user_id);
    
    return download_all_pdf_produto_zip($lista, $nome);
}