<?php

/**
 * Envia e-mail para usuários que tiveram pedidos cancelados pelo PagSeguro
 * 
 * @package cronjobs
 * 
 * @see https://podio.com/exponencialconcursoscombr/exponencial-concursos/apps/bug-tracker/items/2192
 *
 * @since j4
 * 
 * --------------------------------------------------------
 * 
 * CRONJOB:
 * 
 * 13	*	*	*	*
 * 
 * wget -O /dev/null 
 * 
 * https://www.exponencialconcursos.com.br/wp-content/cronjobs/notificacao_pedidos_cancelados_pagseguro.php
 * 
 */

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once '../../wp-load.php';

global $wpdb;

excluir_seo_h1();

$posts = listar_posts_por_tipo([TIPO_POST_PAGINA]);



// print_r($posts);

foreach ($posts as $post) {
	set_time_limit(300);

	$post = get_post($post->ID);

	$permalink = get_permalink($post->ID);

	echo "<br>";
	echo $permalink;

	$html =file_get_contents($permalink);

	// $html = file_get_html($permalink);

	print_r($html);exit;
	// exit;

	// print_r($html->find("head",0)->innertext);
	// exit;

	$title = $html->find("title", 0);

	echo "<br>";
	print_r($title);
	exit;

	$titulo = "";
	if($title) {
		$titulo = $title->innertext;
	}


	$h1s = $html->find("h1");
	
	$h1_info = "";
	$h1_num = 0;

	if($h1s) {
		$h1_num = count($h1s);

		$h1_a = [];
		foreach ($h1s as $h1) {
			if($texto = $h1->innertext) {
				array_push($h1_a, $texto);
			}
		}

		$h1_info = implode(" ; ", $h1_a);

	}

	$edit_link = get_edit_post_link($post->ID);
	
	$dados = [
		'sh1_url' => $permalink,
		'sh1_titulo' => $titulo,
		'sh1_h1' => $h1_info,
		'sh1_num' => $h1_num,
		'sh1_edit_url' => $edit_link
	];

	salvar_seo_h1($dados);
}

$data['message'] = "Informações de H1 atualizados com sucesso.";
$data['type'] = "updated";