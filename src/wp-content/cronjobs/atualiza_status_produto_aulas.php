<?php
/**
 * Atualiza tabela de produtos diariamente informando se há aulas pendentes e cursos expirados
 * 
 * @package cronjobs
 * 
 * @since K5
 * 
 * --------------------------------------------------------
 * 
 * CRONJOB:
 * 
 * 4	3	*	*	*
 * 
 * wget -O /dev/null 
 * 
 * https://www.exponencialconcursos.com.br/wp-content/cronjobs/atualiza_status_produto_aulas.php
 * 
 */

include_once '../../wp-load.php';

global $wpdb;

KLoader::model("ProdutoModel");

$posts = get_todos_produtos_posts(FALSE, 'any');

KLoader::model("ProdutoModel");

foreach ($posts as $item) {
    set_time_limit(300);
    ProdutoModel::atualizar_metadados_status_aulas($item->ID);
}

echo "Dados de produtos-status-aula atualizados com sucesso";