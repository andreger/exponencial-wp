#!/bin/bash
DIR="../wp-content/temp"

echo "Removendo arquivos antigos"
rm -f $DIR/exponenc_db.sql
rm -f $DIR/exponenc_db.sql.gz
rm -f $DIR/exponenc_corp.sql
rm -f $DIR/exponenc_corp.sql.gz

echo "Backup do banco exponenc_db"
mysqldump -h exponencial.c5wpnyvtqf9c.us-west-2.rds.amazonaws.com -uexponencial -p'QeRwJmULAM63rYbw' --single-transaction --quick --lock-tables=false exponenc_db | gzip > $DIR/exponenc_db.sql.gz

echo "Backup do banco exponenc_corp"
mysqldump -h exponencial.c5wpnyvtqf9c.us-west-2.rds.amazonaws.com -uexponencial -p'QeRwJmULAM63rYbw' --single-transaction --quick --lock-tables=false exponenc_corp | gzip > $DIR/exponenc_corp.sql.gz

echo "Descompactando arquivos"
gzip -dc < $DIR/exponenc_db.sql.gz > $DIR/exponenc_db.sql
gzip -dc < $DIR/exponenc_corp.sql.gz > $DIR/exponenc_corp.sql

echo "Recriando bancos de dados"
mysql -uroot -prootpass -e "DROP DATABASE exponenc_db_dev;DROP DATABASE exponenc_corp_dev;CREATE DATABASE exponenc_db_dev;CREATE DATABASE exponenc_corp_dev;"

echo "Importando o banco exponenc_db"
mysql -uroot -prootpass exponenc_db_dev < $DIR/exponenc_db.sql

echo "Importando o banco exponenc_corp"
mysql -uroot -prootpass exponenc_corp_dev < $DIR/exponenc_corp.sql

echo "Preparando wp-config-cli"
rm ../wp-config.php
cp ../wp-config-cli-dev.php ../wp-config.php

echo "Alterando senhas de usuarios"
php restore_db.php

echo "Search replace"
wp search-replace "https://www.exponencialconcursos.com.br" "http://exponencial" --allow-root

echo "Restaurando wp-config"
rm ../wp-config.php
cp ../wp-config-exponencial.php ../wp-config.php

echo "Restore concluido com sucesso"