#!/bin/bash
rm ../wp-config.php
cp ../wp-config-cli.php ../wp-config.php
wp search-replace "https://www.exponencialconcursos.com.br" "http://homol.exponencialconcursos.com.br" --allow-root
rm ../wp-config.php
cp ../wp-config-exponencial.php ../wp-config.php