#!/bin/bash
mysqlcheck -uexponenc -pexpo2014 -o exponenc_db
mysqlcheck -uexponenc -pexpo2014 -o exponenc_db --auto-repair
mysqlcheck -uexponenc -pexpo2014 -o exponenc_db --analyze
mysqlcheck -uexponenc -pexpo2014 -o exponenc_corp
mysqlcheck -uexponenc -pexpo2014 -o exponenc_corp --auto-repair
mysqlcheck -uexponenc -pexpo2014 -o exponenc_corp --analyze