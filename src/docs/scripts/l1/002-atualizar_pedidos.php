<?php
session_start();

require_once '../../../wp-load.php';

KLoader::model("PedidoModel");

global $wpdb;
?>
<html>
<head>
<meta http-equiv="Refresh" content="1">
</head>
<body>
<?php 
$antigo_ref =  $_SESSION["antigo_ref"] ?: 444515;

$sql = "SELECT COUNT(*) FROM wp_posts WHERE post_type = 'shop_order' and ID < $antigo_ref 
    and ID not in (SELECT pedido_id FROM pedido_produto)";

$num = $wpdb->get_var($sql);

echo "Restando $num pedidos<br><br>";

if($num > 0) {

    $sql = "SELECT ID FROM wp_posts WHERE post_type = 'shop_order' and ID < $antigo_ref 
        and ID NOT IN (SELECT pedido_id FROM pedido_produto) ORDER BY ID DESC LIMIT 50";
    
    $ids = $wpdb->get_col($sql);
    
    
    echo "Antigo Ref: {$antigo_ref}<br><br>";
    
    foreach ($ids as $id) {
        $negrito = false;
        if($id >= $antigo_ref) {
            $negrito = true;
        }
        
        $msg = "Alterando $id...";
        PedidoModel::alterar_pedido_status_usuario($id);
        $msg .= "OK<br>";  
        
        echo $negrito ? "<b>$msg</b>" : $msg;
        
    }
    
    $_SESSION["antigo_ref"] = $id;
}
?>
	
</body>
</html>