<?php
require_once '../../../wp-load.php';

$posts = get_todos_produtos_posts();

$num = count($posts);
foreach ($posts as $post) {
    update_post_meta($post->ID, 'mostrar_garantia', 'yes');
}

echo "Script finalizado. Foram atualizados {$num} produtos";