<?php
require_once '../../../wp-load.php';

get_header();
?>
<div>
	<img class="img-fluid" alt="Capa" src="/wp-content/themes/academy/assets/img/assinaturas/capa.jpg">
</div>

<!-- === Inicio Vendas até === -->
<div class="container-fluid p-0 bg-vermelhao text-white text-center pt-4 pb-4">
	<div class="text-uppercase font-weight-bold display-4">Vendas apenas até xx/xx/xxxx</div>
	<div class="h3">Depois disto, as assinaturas não serão comercializadas</div>
</div>
<!-- === Fim Vendas até === -->

<!-- === Inicio O que está incluso === -->
<div class="container-fluid bg-cinza-inverno p-0 pb-4">
	<div class="row text-uppercase text-center text-azul-denim">
		<div class="col-12 display-4 pt-4 font-weight-bold">
			O que está incluso na
		</div>
		<div class="col-12 display-4 font-weight-bold">
			assinatura ilimitada?
		</div>
	</div>
</div>

<div class="container-fluid bg-cinza-inverno p-0 pb-4">	
	<div class="row assinatura-incluso h3 text-muted">
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Cursos em PDF <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-pdf.png" class="d-none d-sm-inline">
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-atualizacao.png"  class="d-none d-sm-inline"> Atualização pós-edital
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Cursos em videoaulas <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-video.png" class="d-none d-sm-inline">
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-mapa.png" class="d-none d-sm-inline"> Mapas Mentais
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Resumos <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-resumo.png" class="d-none d-sm-inline">
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-600mil.png" class="d-none d-sm-inline"> +600 mil questões cadastradas
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			+300 mil questões comentadas por professores <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-forum.png" class="d-none d-sm-inline"> 
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-download.png" class="d-none d-sm-inline"> Download ilimitado do PDF
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Questões e Simulados <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-simulados.png" class="d-none d-sm-inline"> 
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-visualizacao.png" class="d-none d-sm-inline"> Visualização ilimitada dos vídeos 
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Conteúdos exclusivos <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-conteudo.png" class="d-none d-sm-inline"> 
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			 <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-serie.png" class="d-none d-sm-inline"> Série #CrescimentoExponencial
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Acesso ilimitado por 1 ou 2 anos <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-ilimitado.png" class="d-none d-sm-inline"> 
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			 <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-forum.png" class="d-none d-sm-inline"> Fórum tira-dúvidas
		</div>
	</div>
</div>
<!-- === Fim O que está incluso === -->

<!-- === Inicio Areas === -->
<div class="container-fluid bg-cinza-inverno p-0 pb-4">	
	<div class="container">
    	<div class="row">
    	
    		<!-- === Inicio Area === -->
    		<div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
    			<div class="rounded-top bg-azul-denim pb-2 pt-3 lp-aslan-titulo">
    				<img height="40" src="/wp-content/themes/academy/assets/img/assinaturas/area-fiscal.png">
    				<div class="h2 font-weight-bold text-verde-menta mt-1">Área Fiscal</div>
    			</div>
    		
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-azul-denim font-weight-bold">Assinatura 1 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
					</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-verde-menta font-weight-bold">Assinatura 2 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
    				</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2 h6 muted-text">
    				* pagamento recorrente não ocupa limite de cartão sobre o valor total
    			</div>
    		</div>
    		<!-- === Fim Area === -->
    		
    		<!-- === Inicio Area === -->
    		<div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
    			<div class="rounded-top bg-azul-denim pb-2 pt-3 lp-aslan-titulo">
    				<img height="40" src="/wp-content/themes/academy/assets/img/assinaturas/area-controle.png">
    				<div class="h2 font-weight-bold text-verde-menta mt-1">Área de Controle</div>
    			</div>
    		
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-azul-denim font-weight-bold">Assinatura 1 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
					</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-verde-menta font-weight-bold">Assinatura 2 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
    				</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2 h6 muted-text">
    				* pagamento recorrente não ocupa limite de cartão sobre o valor total
    			</div>
    		</div>
    		<!-- === Fim Area === -->
    		
    		<!-- === Inicio Area === -->
    		<div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
    			<div class="rounded-top bg-azul-denim pb-2 pt-3 lp-aslan-titulo">
    				<img height="40" src="/wp-content/themes/academy/assets/img/assinaturas/area-policial.png">
    				<div class="h2 font-weight-bold text-verde-menta mt-1">Área Policial</div>
    			</div>
    		
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-azul-denim font-weight-bold">Assinatura 1 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
					</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-verde-menta font-weight-bold">Assinatura 2 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
    				</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2 h6 muted-text">
    				* pagamento recorrente não ocupa limite de cartão sobre o valor total
    			</div>
    		</div>
    		<!-- === Fim Area === -->
    		
    		<!-- === Inicio Area === -->
    		<div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
    			<div class="rounded-top bg-azul-denim pb-2 pt-3 lp-aslan-titulo">
    				<img height="40" src="/wp-content/themes/academy/assets/img/assinaturas/area-tribunais.png">
    				<div class="h2 font-weight-bold text-verde-menta mt-1">Área de Tribunais, MPs e Afins</div>
    			</div>
    		
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-azul-denim font-weight-bold">Assinatura 1 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
					</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-verde-menta font-weight-bold">Assinatura 2 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
    				</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2 h6 muted-text">
    				* pagamento recorrente não ocupa limite de cartão sobre o valor total
    			</div>
    		</div>
    		<!-- === Fim Area === -->
		</div>
	</div>
</div>
<!-- === Fim Areas === -->

<!-- === Inicio Satisfacao Garantida === -->
<div class="container-fluid p-0 bg-dark text-center pt-4 pb-5">
	<div class="text-uppercase font-weight-bold display-4 text-verde-menta">Satifação Garantida</div>
	<div class="text-uppercase font-weight-bold display-4 text-vermelhao">Ou seu dinheiro de volta!</div>
	<div class="h3 text-white">
		Você tem até 30 dias do pagamento para solicitar o reembolso integral.<br>
		Confira as regras no termo de uso.
	</div>
</div>
<!-- === Fim Satisfacao Garantida === -->

<!-- === Inicio Motivos === -->
<div class="container-fluid bg-white p-0 pb-4">
	<div class="row text-uppercase text-center text-azul-denim">
		<div class="col-12 display-4 pt-4 font-weight-bold">
			Motivos para assinar agora!
		</div>
	</div>
</div>

<div class="container-fluid p-0 pb-4">	
	<div class="container">
    	<div class="row">
    	
    		<div class="col-12 col-sm-6 mb-5">
    			<img height="140" src="/wp-content/themes/academy/assets/img/assinaturas/motivo-custo.png">
    			<div class="h2 font-weight-bold text-azul-denim mt-1">Melhor Custo<br>benefício</div>
    			<div class="h3 muted-text">Com menos de 2 reais por dia você tem acesso à TUDO que precisa para ser aprovado.</div>
    		</div>
    		
    		<div class="col-12 col-sm-6 mb-5">
    			<img height="140" src="/wp-content/themes/academy/assets/img/assinaturas/motivo-masterclass.png">
    			<div class="h2 font-weight-bold text-azul-denim mt-1">MasterClass<br>#CrescimentoExponencial</div>
    			<div class="h3 muted-text">Conteúdos exclusivos para assinante, aprenda a evitar erros e como ser aprovado efetivamente, sem papinho de "senta a bunda e estuda" ou "é preciso 5 anos pra ser aprovado".</div>
    		</div>
    		
    		<div class="col-12 col-sm-6 mb-5">
    			<img height="140" src="/wp-content/themes/academy/assets/img/assinaturas/motivo-metodologia.png">
    			<div class="h2 font-weight-bold text-azul-denim mt-1">Metodologia</div>
    			<div class="h3 muted-text">Você não tem tempo pra jogar fora, e é por isto que nossa metodologia é a sua salvação! Com muitas esquematizações e mapas mentais, fica Simples de aprender, Rápido para Revisar e Fácil de Memorizar. Fuja dos materiais "gigantes" que só consomem o seu tempo e tornam o processo impossível.<br>
    			#CrescimentoExponencial, mais conhecimento em menos tempo, tudo MASTIGADO pra você.</div>
    		</div>
    		
    		<div class="col-12 col-sm-6 mb-5">
    			<img height="140" src="/wp-content/themes/academy/assets/img/assinaturas/motivo-indices.png">
    			<div class="h2 font-weight-bold text-azul-denim mt-1">Maiores índices de<br>aprovação</div>
    			<div class="h3 muted-text">Temos índices ELEVADÍSSIMOS de aprovação, como 100% dos aprovados no ISS São Luís, 89% SEFAZ Goiás, 83% ICMS SC, 45% CAGE-RS dentre outros. Mas isto não é milagre, é o resultado de muitos anos de aperfeiçoamento da metodologia, e os resultados estão aí.</div>
				<a href="#">
					<div class="h3 text-white text-center bg-verde-menta font-weight-bold rounded-5 pl-2 pl-2 pt-2 pb-2" style="width: 340px; margin: 20px 0">Confira mais resultados aqui</div>
				</a>
    		</div>
    	</div>
    </div>
</div>
    		

<!-- === Inicio Alguma Dúvida === -->
<div class="container-fluid p-0 bg-azul-denim text-center pt-4 pb-5">
	<div class="text-uppercase font-weight-bold display-4 text-white">Alguma Dúvida</div>
	<div class="font-weight-bold display-4 text-verde-menta">Chama o Exponencial no WhatsApp!</div>
	
	<a href="#">
		<div class="h3 text-white bg-verde-menta font-weight-bold rounded-5 pl-4 pl-4 pt-2 pb-2" style="width: 360px; margin: 20px auto">Falar agora com atendente</div>
	</a>
	
	<div class="h3 text-white">
		Você também pode enviar um e-mail pela nossa página de contato.<br>
		<a class="link-branco" href="/fale-conosco">Clique aqui!</a>
	</div>
</div>
<!-- === Fim Alguma Dúvida === -->

<!-- === Inicio O que está incluso === -->
<div class="container-fluid bg-cinza-inverno p-0 pb-4">
	<div class="row text-uppercase text-center text-azul-denim">
		<div class="col-12 display-4 pt-4 font-weight-bold">
			O que está incluso na
		</div>
		<div class="col-12 display-4 font-weight-bold">
			assinatura ilimitada?
		</div>
	</div>
</div>

<div class="container-fluid bg-cinza-inverno p-0 pb-4">	
	<div class="row assinatura-incluso h3 text-muted">
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Cursos em PDF <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-pdf.png" class="d-none d-sm-inline">
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-atualizacao.png"  class="d-none d-sm-inline"> Atualização pós-edital
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Cursos em videoaulas <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-video.png" class="d-none d-sm-inline">
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-mapa.png" class="d-none d-sm-inline"> Mapas Mentais
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Resumos <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-resumo.png" class="d-none d-sm-inline">
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-600mil.png" class="d-none d-sm-inline"> +600 mil questões cadastradas
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			+300 mil questões comentadas por professores <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-forum.png" class="d-none d-sm-inline"> 
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-download.png" class="d-none d-sm-inline"> Download ilimitado do PDF
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Questões e Simulados <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-simulados.png" class="d-none d-sm-inline"> 
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-visualizacao.png" class="d-none d-sm-inline"> Visualização ilimitada dos vídeos 
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Conteúdos exclusivos <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-conteudo.png" class="d-none d-sm-inline"> 
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			 <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-serie.png" class="d-none d-sm-inline"> Série #CrescimentoExponencial
		</div>
		
		<div class="col-12 col-md-6 pr-md-5 text-center text-md-right mb-3">
			Acesso ilimitado por 1 ou 2 anos <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-ilimitado.png" class="d-none d-sm-inline"> 
		</div>
		
		<div class="col-12 col-md-6 pl-md-5 text-center text-md-left mb-3">
			 <img src="/wp-content/themes/academy/assets/img/assinaturas/ico-forum.png" class="d-none d-sm-inline"> Fórum tira-dúvidas
		</div>
	</div>
</div>
<!-- === Fim O que está incluso === -->

<!-- === Inicio Areas === -->
<div class="container-fluid bg-cinza-inverno p-0 pb-4">	
	<div class="container">
    	<div class="row">
    	
    		<!-- === Inicio Area === -->
    		<div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
    			<div class="rounded-top bg-azul-denim pb-2 pt-3 lp-aslan-titulo">
    				<img height="40" src="/wp-content/themes/academy/assets/img/assinaturas/area-fiscal.png">
    				<div class="h2 font-weight-bold text-verde-menta mt-1">Área Fiscal</div>
    			</div>
    		
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-azul-denim font-weight-bold">Assinatura 1 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
					</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-verde-menta font-weight-bold">Assinatura 2 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
    				</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2 h6 muted-text">
    				* pagamento recorrente não ocupa limite de cartão sobre o valor total
    			</div>
    		</div>
    		<!-- === Fim Area === -->
    		
    		<!-- === Inicio Area === -->
    		<div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
    			<div class="rounded-top bg-azul-denim pb-2 pt-3 lp-aslan-titulo">
    				<img height="40" src="/wp-content/themes/academy/assets/img/assinaturas/area-controle.png">
    				<div class="h2 font-weight-bold text-verde-menta mt-1">Área de Controle</div>
    			</div>
    		
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-azul-denim font-weight-bold">Assinatura 1 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
					</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-verde-menta font-weight-bold">Assinatura 2 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
    				</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2 h6 muted-text">
    				* pagamento recorrente não ocupa limite de cartão sobre o valor total
    			</div>
    		</div>
    		<!-- === Fim Area === -->
    		
    		<!-- === Inicio Area === -->
    		<div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
    			<div class="rounded-top bg-azul-denim pb-2 pt-3 lp-aslan-titulo">
    				<img height="40" src="/wp-content/themes/academy/assets/img/assinaturas/area-policial.png">
    				<div class="h2 font-weight-bold text-verde-menta mt-1">Área Policial</div>
    			</div>
    		
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-azul-denim font-weight-bold">Assinatura 1 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
					</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-verde-menta font-weight-bold">Assinatura 2 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
    				</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2 h6 muted-text">
    				* pagamento recorrente não ocupa limite de cartão sobre o valor total
    			</div>
    		</div>
    		<!-- === Fim Area === -->
    		
    		<!-- === Inicio Area === -->
    		<div class="col-12 col-sm-6 col-md-4 col-lg-3 text-center">
    			<div class="rounded-top bg-azul-denim pb-2 pt-3 lp-aslan-titulo">
    				<img height="40" src="/wp-content/themes/academy/assets/img/assinaturas/area-tribunais.png">
    				<div class="h2 font-weight-bold text-verde-menta mt-1">Área de Tribunais, MPs e Afins</div>
    			</div>
    		
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-azul-denim font-weight-bold">Assinatura 1 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-white bg-azul-denim rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
					</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2">
    				<div class="h3 text-verde-menta font-weight-bold">Assinatura 2 ano</div>
    				<div class=""><del>Lote 1: 12 x R$ X (até XX/XX)</del></div>
    				<div class="h4 text-white bg-vermelhao font-weight-bold text-uppercase rounded-5">Esgotado</div>
    				<div class="mb-2">Lote 2: 12 x R$ X (até XX/XX)</div>
    				<br>
    				<div class="mb-2">Lote 3: 12 x R$ X (até XX/XX)</div>
    				
    				<a href="#">
        				<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Cartão</div>
    						<div class="">12x sem juros (recorrente)*</div>
    					</div>
					</a>
					
					<a href="#">
    					<div class="text-azul-denim bg-verde-menta rounded-6 pt-2 pb-2 mb-2">
    						<div class="h3 font-weight-bold mb-0">Assinar com Boleto</div>
    						<div class="">10% à vista</div>
    					</div>
    				</a>
    			</div>
    			
    			<div class="bg-white pl-2 pr-2 pt-2 pb-2 h6 muted-text">
    				* pagamento recorrente não ocupa limite de cartão sobre o valor total
    			</div>
    		</div>
    		<!-- === Fim Area === -->
		</div>
	</div>
</div>
<!-- === Fim Areas === -->

<!-- === Inicio Vendas até === -->
<div class="container-fluid p-0 bg-vermelhao text-white text-center pt-4 pb-4">
	<div class="text-uppercase font-weight-bold display-4">Vendas apenas até xx/xx/xxxx</div>
	<div class="h3">Depois disto, as assinaturas não serão comercializadas</div>
</div>
<!-- === Fim Vendas até === -->

<!-- === Inicio Depoimentos === -->
<div class="container-fluid p-0 pb-5">
	<div class="row text-center text-azul-denim">
		<div class="col-12 display-4 pt-4 font-weight-bold">
			Veja o que nossos alunos tem a dizer
		</div>
	</div>
</div>

<div class="container-fluid p-0 pb-4">
	<div class="container">
    	<div class="row text-center mb-5">	
    		<div class="col-12 col-sm-3 text-right mr-3">
    			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-visualizacao.png">
    		</div>
    		<div class="col-12 col-sm-8">
    			<div class="h2 font-italic text-left">
    				Adoro os esquemas dos cursos do Exponencial, facilita muito a vida do concursando.
    			</div>
    			<div class="h5 font-italic text-muted text-left">
    				Antônia C.
    			</div>
    		</div>
    	</div>
    	
    	<div class="row text-center mb-5">	
    		<div class="col-12 col-sm-3 text-right mr-3">
    			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-visualizacao.png">
    		</div>
    		<div class="col-12 col-sm-8">
    			<div class="h2 font-italic text-left">
    				Adoro os esquemas dos cursos do Exponencial, facilita muito a vida do concursando.
    			</div>
    			<div class="h5 font-italic text-muted text-left">
    				Antônia C.
    			</div>
    		</div>
    	</div>
    	
    	<div class="row text-center mb-5">	
    		<div class="col-12 col-sm-3 text-right mr-3">
    			<img src="/wp-content/themes/academy/assets/img/assinaturas/ico-visualizacao.png">
    		</div>
    		<div class="col-12 col-sm-8">
    			<div class="h2 font-italic text-left">
    				Adoro os esquemas dos cursos do Exponencial, facilita muito a vida do concursando.
    			</div>
    			<div class="h5 font-italic text-muted text-left">
    				Antônia C.
    			</div>
    		</div>
    	</div>
	</div>
</div>
<!-- === Fim Depoimentos === -->

<?php get_footer() ?>