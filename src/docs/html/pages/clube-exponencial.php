<!DOCTYPE html>
<html>
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" href="https://www.exponencialconcursos.com.br/wp-content/uploads/2019/01/favicon-exponencial.png" />
      <!--=== TITLE ===-->
      <title>Clube Exponencial | Exponencial Concursos</title>
      <!--=== Fim TITLE ===-->
      <!--=== CSS e JS necessários. NÃO MEXER ===-->
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      <link rel="stylesheet" href="/wp-content/themes/academy/css/custom.css">
      <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Poppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2Cregular%2Citalic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;ver=4.9.5#038;subset=latin,devanagari,latin-ext' type='text/css' media='all' />
      <link rel='stylesheet' href='/wp-content/themes/academy/css/jquery.countdown.css' type='text/css' media='all' />
      <style type="text/css">
         .bg-img-1 {
         background-image: url(/wp-content/uploads/2019/02/banner1.jpg);
         background-repeat: no-repeat;
         background-size: cover;
         }
         .bg-img-2 {
         background-image: url(/wp-content/uploads/2019/02/tcu-banner1.jpg);
         background-repeat: no-repeat;
         background-size: cover;
         }
         body {
         line-height: 1.1!important;
         color: #666;
         background-color: #fff;
         font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif!important;
         font-size: 12px;
         font-weight: 500;
         -webkit-font-smoothing: antialiased;
         -moz-osx-font-smoothing: grayscale;
         margin-top: 2rem;
         }
         b {
         color: #3a3a3a!important;
         }
         h1 {
         color: #fff!important;
         }
         .text-orange {
         color: #fc851b;
         }
         .btn-color-orange {
         color: 
         }
         .btn-orange {
         color:black;
         background-color:#fc851b;
         border-color:#fc851b;
         }
         .text-dark {
         color:black!important;
         }
         .font-pop {
         font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif;
         }
         .rounded-2 {
         border-radius: 1.5rem!important;
         }
         .rounded-3 {
         border-radius: 0.8rem!important;
         }
         .video-responsive-expo {
         width: 100%;
         height: 315px;
         }
         .line-height-3 {
         line-height: 1.8;
         }
         .nav-top {
         position: fixed;
         width: 100%;
         top : 0;
         background-color: 
         }
         .container-2 {
         width: 100%;
         padding-right: 14%;
         padding-left: 14%;
         margin-right: auto;
         margin-left: auto;
         }
         .ancora {
         transition: all 2s ease-in-out;
         -webkit-transition: all 2s ease-in-out;
         -moz-transition: all 2s ease-in-out;
         }
         /* -- backgrounds -- */ 
         .bg-tropa-blue {
         background-color: #0a67aa;
         }
         .bg-gray {
         background-color: #e5e6e7;
         }
         /* -- textos -- */ 
         .text-white {
         color: white!important;
         }
         .border-color-orange {
         border-color: #fc851b; 
         }
         .t-d-none {
         text-decoration: none!important;
         }
         /* ---- fonts ---- */ 
         .font-11 {font-size: 11px;}
         .font-12 {font-size: 12px;}
         .font-14 {font-size: 14px;}
         .font-15 {font-size: 15px;}
         .font-16 {font-size: 16px;}
         .font-18 {font-size: 18px;}
         .font-20 {font-size: 20px;}
         .font-22 {font-size: 22px;}
         .font-24 {font-size: 24px;}
         .font-26 {font-size: 26px;}
         .font-28 {font-size: 28px;}
         .font-30 {font-size: 30px;}
         .font-34 {font-size: 34px;}
         .font-36 {font-size: 36px;}
         .font-38 {font-size: 38px;}
         .font-40 {font-size: 40px;}
         .font-44 {font-size: 44px;}
         .font-46 {font-size: 46px;}
         .font-50 {font-size: 50px;}
         /* ---- form ----*/
         .error {
         color: #ff0000;
         }
         /* ---- countdown ----*/
         ul#countdown {
         list-style: none;
         margin: 50px 0;
         padding: 0;
         display: block;
         text-align: center;
         }
         ul#countdown li { display: inline-block; }
         ul#countdown li span {
         font-size: 80px;
         font-weight: 300;
         line-height: 80px;
         }
         ul#countdown li.seperator {
         font-size: 80px;
         line-height: 70px;
         vertical-align: top;
         }
         ul#countdown li p {
         color: #a7abb1;
         font-size: 25px;
         }
         @media screen and (max-width: 2000px) and (min-width: 1600px) {
         .container-2 {   
         padding-right: 25%;
         padding-left: 25%;   
         }
         } 
         @media (max-width: 567px) {
         .border-top-1 {
         border-top: 1px solid;
         }    
         .video-responsive-expo {
         width: 100%;
         height: 200px;
         }
         .container-2 {   
         padding-right: 5%;
         padding-left: 5%;   
         }    
         .font-11 {font-size: 11px;}
         .font-12 {font-size: 12px!important;}
         .font-14 {font-size: 14px!important;}
         .font-15 {font-size: 15px;}
         .font-16 {font-size: 16px!important;}
         .font-18 {font-size: 22px!important;}
         .font-20 {font-size: 20px!important;}
         .font-22 {font-size: 22px!important;}
         .font-24 {font-size: 24px!important;}
         .font-26 {font-size: 30px!important;}
         .font-28 {font-size: 28px!important;}
         .font-30 {font-size: 30px!important;}
         .font-34 {font-size: 34px!important;}
         .font-36 {font-size: 36px!important;}
         .font-38 {font-size: 38px!important;}
         .font-40 {font-size: 40px!important;}
         .font-44 {font-size: 44px!important;}
         .font-46 {font-size: 46px!important;}
         .font-50 {font-size: 40px!important;} 
         ul#countdown li span {
         font-size: 20px;
         line-height: 20px;
         }
         ul#countdown li p {
         font-size: 10px;
         }
         ul#countdown li.seperator {
         font-size: 20px;
         line-height: 50px;
         }
         #inscreva-se {
         font-size: 13px;
         }
         }
      </style>
      <script
         src="https://code.jquery.com/jquery-3.3.1.js"
         integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
         crossorigin="anonymous"></script>
      <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      <!--=== Fim CSS e JS necessários. NÃO MEXER ===-->
   </head>
   <body>
        <!--=== Header ===--> 
        <header>	
        	<div class="container pt-5 pb-5">
        		<div class="row">
        			<div class="col-md-4 text-left">
        				<a title="Programa de Afiliados - Exponencial Concursos" href="https://exponencialconcursos.postaffiliatepro.com/affiliates/">
        					<img class="img-fluid" src="/wp-content/themes/academy/images/logo2018.png"/>
        				</a>
        			</div>
        
        			<div class="col-md-8 nav-clube">	
        				<ul class="nav justify-content-end mt-4">
          				<li class="nav-item">
            				<a class="nav-link bg-white font-14 font-weight-bold" href="https://exponencialconcursos.postaffiliatepro.com/affiliates" style="color: #0c1f4f!important;">Início</a>
          				</li>
          				<li class="nav-item">
            				<a class="nav-link bg-white font-14 font-weight-bold" href="https://exponencialconcursos.postaffiliatepro.com/affiliates/signup.php#SignupForm" style="color: #0c1f4f!important;">Sign up</a>
          				</li>
          				<li class="nav-item">
            				<a class="text-white font-weight-bold font-13 nav-link btn u-btn-blue" href="https://exponencialconcursos.postaffiliatepro.com/affiliates/login.php" style="background-color: #0c1f4f!important;">Entrar</a>
          				</li>
        				</ul>
        
        			</div>
        		</div>
        	</div>
        </header>
        
        <div class="container-fluid p-0">
        	<img class="img-fluid" src="/wp-content/themes/academy/images/clubeexponencial/afiliados-home.jpg">
        </div>
        
        <div class="container-fluid bg-white pt-5 pb-5">
        	<div class="container">
        		<div class="row">
        			<div class="col-12 col-lg-6">
        				<div class="row">
        					<div class="col-12 col-md-3 text-center text-md-left">
        						<img class="img-fluid" src="/wp-content/themes/academy/images/clubeexponencial/como-funciona.png">
        					</div>	
        					<div class="text-azulescuro font-40 font-lg-33 font-weight-bold col-12 col-md-9 text-center text-md-left pt-4">
        						Como funciona?
        					</div>
        					<div class="col-12 mt-5 font-26 line-height-1-2 text-center text-md-left">
        						Adquira um voucher do Clube Exponencial e faça parte do Clube.
        					</div>
        				</div>
        			</div>
        			<div class="col-12 col-lg-6 mt-4 mt-lg-0">
        				<div class="embed-responsive embed-responsive-16by9">
         					 <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/5CwDOSNZ-6U"></iframe>
        				</div>				
        			</div>
        			<div class="col-12 font-18 line-height-1-2 mt-4 text-justify">
        				Você adquire o Voucher do Clube Exponencial pelo valor de R$ XX,XX e passa a fazer parte do Clube. O valor que você pagou pelo Voucher pode ser ultilizado para adquirir qualquer produto do Exponencial. Além disso, participando do Clube você ganha 1 ano de ultilização	do Sistema de Questões, Descontos Proquessivos na aquisição de produtos do Exponencial e ainda pode ganhar comissões indicando o Exponencial Concursos.
        			</div>
        		</div>
        	</div>
        </div>
        
        <div class="container-fluid bg-gray pt-5 pb-5">	
        		<div class="col-12 col-md-2 col-lg-1 ml-auto mr-auto text-center p-0">
        			<img class="img-fluid" src="/wp-content/themes/academy/images/clubeexponencial/como-funciona.png">
        		</div>
        		<div class="text-azulescuro font-46 font-weight-bold col-12 text-center p-0">
        			Descontos Exclusivos
        		</div>
        		<div class="col-12 text-center font-20 line-height-1-2">
        			Participantes do Clube tem descontos de 20% na aquisição de qualquer<br> produto do Exponencial. Este desconto pode chegar até a 80%!
        		</div>	
        </div>
        <div class="container-fluid bg-white pt-5 pb-5">
        		<div class="col-12 col-md-2 col-lg-1 ml-auto mr-auto text-center p-0">
        			<img class="img-fluid" src="/wp-content/themes/academy/images/clubeexponencial/receba.png">
        		</div>
        		<div class="text-azulescuro font-46 font-weight-bold col-12 text-center p-0 line-height-1">
        			Receba comissões pela sua indicação!
        		</div>
        		<div class="col-12 col-md-11 col-lg-10 ml-auto mr-auto text-center font-20 line-height-1-2 mt-3">
        			Você divulga excelentes conteúdos do Exponencial Concursos e é recompensado por isso! Nosso Sistema registra o tráfego oriundo de sua campanha e nós pagaremos comissoẽs sempre que o seu púplico efetivar compras no nosso site.
        		</div>
        </div>
        <div class="container-fluid bg-gray pt-5 pb-5">
        		<div class="col-12 col-md-2 col-lg-1 ml-auto mr-auto text-center p-0">
        			<img class="img-fluid" src="/wp-content/themes/academy/images/clubeexponencial/receba.png">
        		</div>
        		<div class="text-azulescuro font-46 font-weight-bold col-12 text-center p-0 line-height-1">
        			Crie a sua rede e lucre
        		</div>
        		<div class="col-12 col-md-11 col-lg-10 ml-auto mr-auto text-center font-20 line-height-1-2 mt-3">
        			Sempre que alguém se cadastrar no programa por sua indicação, ele passa a compor a sua rede. Comissões geradas pelos integrantes da sua rede também lhe bonifica!
        		</div>
        </div>

      <!--== Footer ===-->
      <footer>
         <div class="bg-tropa-blue pt-4 pb-5">
         <div class="container">
         <div class="row text-uppercase font-weight-bold">
            <div class="col-12 col-md-3 text-center text-md-right">
               <img src="https://www.exponencialconcursos.com.br/wp-content/themes/academy/images/logo-branco.png" class="img-fluid">                           
            </div>
            <div class="col-12 col-md-4 text-center text-md-left mt-3">
               <a class="t-d-none text-white" style="color: white; text-decoration: none;" href="">Resultados de Aprovação</a><br>
               <a class="t-d-none text-white" href="https://exponencialconcursos.postaffiliatepro.com/affiliates/ ">Programa de Afiliados</a><br/>
               <a class="t-d-none text-white" href="/metodologia-exponencial/">Nossa Metodologia</a><br/>  
               <a class="t-d-none text-white" href="/como-funciona/">Como Funciona</a><br/>
               <a class="t-d-none text-white" href="/quem-somos/">Quem Somos</a><br/>
            </div>
            <div class="col-12 col-md-5 text-center text-md-right mt-3">
               <a class="t-d-none text-white" href="/politica-de-privacidade/">Política de Privacidade</a><br/>
               <a class="t-d-none text-white" href="/perguntas-frequentes-portal/">Perguntas Frequentes</a><br/>
               <a class="t-d-none text-white" href="/termos-de-uso/">Termos do Serviço</a><br/>
               <a class="t-d-none text-white" href="/parceiros/">Parceiros</a>
            </div>
         </div>
      </footer>
      <!--=== Fim Footer ===-->
      <!--=== CSS e JS necessários. NÃO MEXER ===-->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
      <script>
         $(function() {
         
         var $doc = $('html, body');
         $('.ancora').click(function() {
           $doc.animate({
               scrollTop: $( $.attr(this, 'href') ).offset().top
           }, 500);
           return false;
         });
         
      </script>
      <!--=== Fim CSS e JS necessários. NÃO MEXER ===-->
   </body>
</html>