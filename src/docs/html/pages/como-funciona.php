<?php
require_once '../../../wp-load.php';

get_header();
?>

<div class="container pt-1 pt-md-5 pb-5">
    <div class="col-12 mt-3">[expo_h1]</div>
    <div class="row mt-5">
        <div class="col-12 col-md-5 col-lg-4"><img class="img-fluid" src="/wp-content/themes/academy/images/cf-estudar-certo-exponencial.png" alt="" /></div>
        <div class="col-12 col-md-7 col-lg-8">
            <p class="text-blue font-16 font-weight-bold">Aprenda a estudar certo e seja aprovado mais rapidamente.</p>
            <p>Aqui no Exponencial você terá o direcionamento para estudar da melhor forma possível, além de materiais completos e totalmente focados na sua prova.</p>
            <p class="text-blue font-16 font-weight-bold">Quer saber mais sobre nossa metodologia? <a class="text-success" href="/metodologia-exponencial">Veja aqui!</a></p>
            <p class="text-blue font-16 font-weight-bold">Quer fazer questões online para treinar? <a class="text-success" href="/questoes/main/resolver_questoes">Acesse aqui!</a></p>
            <p class="text-blue font-16 font-weight-bold">Quer fazer simulados focados no seu concurso? <a class="text-success" href="/simulados/">Escolha aqui!</a></p>
            <p class="text-blue font-16 font-weight-bold">Quer saber como encontrar o curso que deseja?</p>
            <p class="font-13"><a class="text-success" href="/cursos-por-professor">Procure pelo nome do professor aqui</a>
                <br /><a class="text-success" href="/cursos-por-concurso">Ou procure pelo concurso que deseja fazer</a>
                <br /><a class="text-success" href="/cursos-por-materia">Ou procure pela matéria que deseja estudar</a></p>
        </div>
    </div>
    <div class="row mt-2 p-4 bg-light">
        <div class="col-12 col-md-3 col-lg-2 pt-2 text-center text-md-left"><img class="img-fluid" src="/wp-content/themes/academy/assets/img/como-funciona/30dias.png" alt="30 dias" /></div>
        <div class="col-12 col-md-4 col-lg-4 text-blue font-16 font-weight-bold pt-2">30 dias de satisfação ou seu dinheiro de volta</div>
        <div class="col-12 col-md-5 col-lg-6 pt-3 text-justify">
            <p>Envie um e-mail para <strong>cancelamento@exponencialconcursos.com.br</strong> mencionando: nome do curso, número do pedido (fica na área do aluno) data de compra e motivos da insatisfação.</p>
            <p>O prazo para solicitação é de 30 dias após a compra, para produtos com vigência maior que este prazo. O reembolso é feito pelas plataformas de pagamento utilizadas, sendo processadas normalmente em até 2 meses do pedido, conforme política das mesmas. Condição válida apenas para produtos selecionados</p>
        </div>
    </div>
    <div class="row mt-2 p-4">
        <div class="col-12 col-md-3 col-lg-2 pt-2 text-center text-md-left"><img class="img-fluid" src="/wp-content/themes/academy/assets/img/como-funciona/acesso-ilimitado.png" alt="Acesso Ilimitado" /></div>
        <div class="col-12 col-md-4 col-lg-4 text-blue font-16 font-weight-bold pt-4">Download liberado + Acesso Ilimitado</div>
        <div class="col-12 col-md-5 col-lg-6 pt-3 text-justify">
            <p>Você pode fazer o download do material em PDF e assistir quantas vezes quiser as videoaulas.</p>
        </div>
    </div>
    <div class="row mt-2 p-4 bg-light">
        <div class="col-12 col-md-3 col-lg-2 pt-2 text-center text-md-left">
            <img class="img-fluid" src="/wp-content/themes/academy/images/cf-pacote-cursos-exponencial.png" alt="" />
        </div>
        <div class="col-12 col-md-4 col-lg-4 text-blue font-16 font-weight-bold pt-2">
            Quer um pacote com várias matérias para estudar?
        </div>
        <div class="col-12 col-md-5 col-lg-6 pt-3 text-justify">
            <p>Aproveite nossos pacotes com <b>descontos de até 30%</b>.</p>
            <p>Nosso site é 100% seguro e toda vez que efetuar a compra de um produto, você poderá fazer o download de suas aulas no link <a class="text-success" href="/minha-conta">Área do aluno</a>, visível no canto superior direito de nosso site, logo após efetuar seu login.</p>
        </div>
    </div>

    <div class="row mt-2 p-4">
        <div class="col-12 col-md-3 col-lg-2 pt-2 text-center text-md-left">
            <img class="img-fluid" src="/wp-content/themes/academy/images/cf-cronograma-aulas-exponencial.png" alt="" />
        </div>
        <div class="col-12 col-md-4 col-lg-4 text-blue font-16 font-weight-bold pt-4">Cronograma das aulas
        </div>
        <div class="col-12 col-md-5 col-lg-6 pt-3 text-justify">
            <p>Cursos muito recentes podem não estar 100% disponíveis no momento de sua compra. Verifique sempre o cronograma de disponibilização das aulas para saber quando elas estarão disponíveis para você.</p>
            Na data prevista, o link para a aula irá aparecer no menu <a class="text-success" href="/minha-conta">Área do aluno</a>. Basta clicar e fazer o download de sua aula!
        </div>
    </div>

    <div class="row mt-2 p-4 bg-light">
        <div class="col-12 col-md-3 col-lg-2 pt-2 text-center text-md-left">
            <img class="img-fluid" src="/wp-content/themes/academy/images/cf-forum-exponencial.png" alt="" />
        </div>
        <div class="col-12 col-md-4 col-lg-4 text-blue font-16 font-weight-bold pt-4">Fórum tira-dúvidas
        </div>
        <div class="col-12 col-md-5 col-lg-6 pt-3 text-justify">
            <p>Não deixe de se conectar com seu professor e outros colegas que estudam como você. Tire suas dúvidas, interaja, seja ativo na hora de estudar, pois isto o tornará mais preparado para sua prova.</p>
            <p>Para acessar o fórum, basta fazer o login e clicar no menu <a class="text-success" href="/minha-conta">Área do aluno</a>. Lá estará disponível o link para acessar os fóruns dos cursos que adquiriu. Nossos professores estão disponíveis para ajudá-lo em sua trajetória!</p>
        </div>
    </div>

    <div class="row mt-2 p-4">
        <div class="col-12 col-md-3 col-lg-2 pt-2 text-center text-md-left">
            <img class="img-fluid" src="/wp-content/themes/academy/images/cf-coaching-concurso-exponencial.png" alt="" />
        </div>
        <div class="col-12 col-md-4 col-lg-4 pt-4 text-center">
            <a class="text-blue font-16 font-weight-bold" href="/coaching">Coaching</a>
        </div>
        <div class="col-12 col-md-5 col-lg-6 pt-3 text-justify">
            <p>Nosso serviço de coaching é 100% personalizado e desenvolvido com base nas especificidades de cada aluno. Nosso foco é fazer você estudar melhor e obter melhores resultados com a dedicação de que pode dispor. <a class="text-success" href="/coaching">Clique aqui para saber mais.</a></p>
        </div>
    </div>

    <div class="row mt-2 p-4 bg-light">
        <div class="col-12 col-md-3 col-lg-2 pt-2 text-center text-md-left">
            <img class="img-fluid" src="/wp-content/uploads/2016/09/questoes-oline.png" alt="" />
        </div>
        <div class="col-12 col-md-4 col-lg-4 pt-4 text-center">
            <a class="text-blue font-16 font-weight-bold" href="/sistema-de-questoes/">Questões Online</a>
        </div>
        <div class="col-12 col-md-5 col-lg-6 pt-3 text-justify">
            <p>Nosso <a class="text-success" href="/sistema-de-questoes/">Sistema de Questões Online</a> possui mais de 350 mil questões dos principais concursos para que você estude, pratique e avance nos seus resultados. Você pode resolver até 50 questões por dia, gratuitamente!</p>
            E se quiser acompanhar seu desempenho, fazer simulados focados e ver os comentários de nossos professores para cada questão não gastará mais que R$ 0,40 por dia. <a class="text-success" href="/sistema-de-questoes/#assinatura">Clique aqui para assinar.</a>
        </div>
    </div>

    <div class="row mt-2 p-4">
        <div class="col-12 col-md-3 col-lg-2 pt-2 text-center text-md-left">
            <img class="img-fluid" src="/wp-content/uploads/2016/09/simulados-online.png" alt="" />
        </div>
        <div class="col-12 col-md-4 col-lg-4 pt-4 text-center">
            <a class="text-blue font-16 font-weight-bold" href="/simulados/">Simulados Online</a>
        </div>
        <div class="col-12 col-md-5 col-lg-6 pt-3 text-justify">
            <p>Os simulados do Exponencial são 100% revisados e comentados por nossos professores, sempre focados no estilo da banca e no concurso a que se destina. Vários deles são gratuitos e você pode fazê-los offline ou online, como preferir!</p>
            Para acessar seu simulado, basta clicar no link <a class="text-success" href="/minha-conta">Área do aluno</a> na parte superior direita de nosso site. Você verá um botão chamado 'Acessar o Simulado'. Clique nele e terá acesso a todos os dados do seu simulado! Participe ainda do Ranking Online para saber como andam seus estudos e analise seu desempenho em cada disciplina. <a class="text-success" href="/simulados/">Veja mais aqui.</a>
        </div>
    </div>

    <div class="row mt-2 p-4 bg-light">
        <div class="col-12 col-md-3 col-lg-3 pt-2 text-center text-md-left">
            <img class="img-fluid" src="/wp-content/themes/academy/images/cf-blog-novidades-exponencial.png" alt="" />
        </div>
        <div class="col-12 col-md-4 col-lg-3 pt-4 text-center">
            <a class="text-blue font-16 font-weight-bold" href="/blog-artigos">Blog</a>
        </div>
        <div class="col-12 col-md-5 col-lg-6 pt-3 text-justify">
            <p>Nosso blog é dividido em duas partes: artigos e notícias. Artigos são textos produzidos por nossos professores a respeito de temas relevantes da área de concursos. As notícias são informações sobre novos editais, novidades no mundo dos concursos e técnicas de estudo que divulgamos para ajudá-lo a se orientar em sua jornada! <a class="text-success" href="/blog-artigos">Acesse nosso blog!</a></p>
        </div>
    </div>

    <div class="row mt-5">
        <div class="col-3 text-right">
            <img src="/wp-content/themes/academy/images/cf-orientacao-exponencial.png" alt="" />
        </div>
        <div class="col-9">
            <p class="text-blue font-16 font-weight-bold">Quer falar conosco sobre alguma dúvida ou orientação de que precisa?</p>
            <p><a class="text-success font-14" href="/fale-conosco">Clique aqui e envie-nos sua mensagem!</a></p>
        </div>
    </div>
</div>

<?php get_footer() ?>