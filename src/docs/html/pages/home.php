<section>
    <div class="row midia-filtro">Filtrar por:
      <a id="filtro-todos-concursos" href="#">Todos os Concursos</a>
      <a id="filtro-edital-divulgado" href="#">Edital Divulgado</a>
      <a id="filtro-edital-proximo" href="#">Edital Próximo</a>
    </div>
    
    <div class="row woocommerce home_midia">
      <ul id="concursos" class="products">[home_concursos]</ul>
    </div>

    <div style="text-align: center; margin-bottom: 30px;">
      <a id="carregar-mais" href="#">CARREGAR MAIS</a>
    </div>
</section>
<div style="clear: both;"></div>

<section>
  <div class="section_head">
    <div class="row">
      <div class="section_head_img"><img class="alignnone" src="/wp-content/themes/academy/images/icone-depoimentos.png" alt="" />
    </div>
    <div class="header_text">Depoimentos</div>
</section>

[rev_slider depoimentos-home]

<section>
  <div class="section_head">
    <div class="row">
      <div class="section_head_img">
        <img class="alignnone" src="/wp-content/uploads/2014/04/badeira-brasil.png" alt="" />
      </div>
      
      <div class="header_text">Saiba o que está rolando no mundo dos concursos.</div>
    </div>
  </div>
</section>

<div class="row midia-filtro">
  Filtrar por: <a id="filtro-noticias" href="#">Notícias</a> <a id="filtro-artigos" href="#">Artigos</a> <a id="filtro-videos" href="#">Vídeos</a>
</div>

<div class="row woocommerce home_midia">
  <ul id="midias" class="products">[home_midia]</ul>
</div>

<div style="text-align: center; margin-bottom: 30px;">
  <a href="/blog-noticias"><img src="/wp-content/themes/academy/images/mais-noticias.png" /></a> <a href="/blog-artigos"><img src="/wp-content/themes/academy/images/mais-artigos.png" /></a> <a href="/blog-videos"><img src="/wp-content/themes/academy/images/mais-videos.png" /></a>
</div>

<script> 
jQuery(function() { jQuery('#filtro-noticias').click(function(e) { e.preventDefault(); jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=news', function(data) { jQuery('#midias').html(data); }); });
jQuery('#filtro-artigos').click(function(e) {
    e.preventDefault();
    jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>');     jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=articles', function(data) {
        jQuery('#midias').html(data); 
    });
});

jQuery('#filtro-videos').click(function(e) {
    e.preventDefault();
    jQuery('#midias').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
    jQuery.get('/wp-content/themes/academy/xhr-filtrar-midias.php?midia=videos', function(data) {
        jQuery('#midias').html(data); 
    });
});

jQuery('#filtro-edital-proximo').click(function(e) { 
    e.preventDefault(); 
    jQuery('#concursos').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
    jQuery.get('/wp-content/themes/academy/ajax/filtrar_concursos.php?status=2', function(data) { 
        jQuery('#concursos').html(data); 
    });
});

jQuery('#filtro-edital-divulgado').click(function(e) { 
    e.preventDefault(); 
    jQuery('#concursos').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
    jQuery.get('/wp-content/themes/academy/ajax/filtrar_concursos.php?status=1', function(data) { 
        jQuery('#concursos').html(data); 
    });
});

jQuery('#filtro-todos-concursos').click(function(e) { 
    e.preventDefault(); 
    jQuery('#concursos').html('</p><div style="margin-bottom: 30px">Carregando...</div><p>'); 
    jQuery.get('/wp-content/themes/academy/ajax/filtrar_concursos.php', function(data) { 
        jQuery('#concursos').html(data); 
    });
});

jQuery('#carregar-mais').click(function(e) {
  	e.preventDefault();
    	jQuery('#concursos li:hidden').slice(0,6).show();
        exibir_esconder_carregar_mais();
});
}); 
</script>