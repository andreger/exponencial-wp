<?php
require_once '../../../wp-load.php';

get_header();
?>
    <div class="container-fluid p-0">
        <div class="pt-1 pt-md-5"></div>
        <div class="pb-3 col-12 font-coaching">[expo_h1]</div>
        [rev_slider coaching]
        <div class="col-12 pt-5 text-center bg-gray">
            <div class="font-pop">O que os alunos falam?</div>
        </div>
        [rev_slider depoimentos-coaching]
        <div class="col-12 mt-4 text-center">

            <img src="/wp-content/themes/academy/images/coaching-icon.png" width="45" />
            <div class="font-pop mb-3">Vagas Abertas</div>
        </div>

        <div id="accordion" class="container p-2 p-md-0">

            <!-- === Área Fiscal, Controle... === -->
            <div class="card mt-3">

                <!-- === Título === -->
                <div id="headingOne" class="card-header">
                    <h5 class="mb-0">
						<a class="font-xs-16 font-20 text-blue" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">&gt; Área Fiscal, Controle, Legislativa, BACEN, Agências Reguladoras e demais</a>
					</h5>
                </div>
                <!-- ===  Fim Título === -->
                
                <!-- === Corpo === -->
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row bg-gray rounded p-3">

                            <!-- === Ícone === -->
                            <div class="col-12 col-lg-1 text-center">
                                <img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" />
                            </div>
                            <!-- === Fim Ícone === -->

                            <!-- === Descrição === -->
                            <div class="ml-3 ml-md-5 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-8 coaching-concurso line-height-2 font-11 text-dark">
                                <ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-12">
                                    <li>Receita Federal, Fiscal do Trabalho, Fiscos Estaduais e Municipais
                                    </li>
                                    <li>Câmara dos Deputados, Senado Federal, Assembleias Legislativas
                                    </li>
                                    <li>TCU, CGU, Tribunais de Contas e Controladorias Estaduais e Municipais, TCM RJ</li>
                                    <li>BACEN</li>
                                    <li>CVM, SUSEP, ANS, ANTT, ANVISA, ANEEL, ANAC, ANTAQ, ANA, ANP, ANATEL, ANCINE</li>
                                </ul>
                            </div>
                            <!-- === Fim Descrição === -->

                            <!-- === Box Preço === -->
                            <div class="p-0 col-12 col-lg-3">
                                <a class="coaching-link" href="https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-fiscal-area-de-controle-area-legislativa-banco-central-e-agencias-reguladoras">
                                    <div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" />
                                            </div>
                                            <div class="pl-1 pl-lg-3 col-6">

                                                <img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
                                                <p class="text-white mb-2 font-12">COACHING</p>

                                            </div>
                                        </div>
                                        <div class="bg-aqua-box text-white font-22 text-center p-1">
                                            MENSAL
                                        </div>
                                        <div class="mt-3 text-white font-18 text-center p-0">

                                            <span style="text-decoration: line-through;">499,90</span> R$ 399,90

                                            <span class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase">COMPRAR</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <!-- === Fim Box Preço === -->
						</div>
                    </div>
                </div>
                <!-- === Fim Corpo === -->
            </div>
            <!-- === Fim Área Fiscal, Controle... === -->
                
            <!-- === INSS, Policial, Tribunais... === --> 
            <div class="card mt-3">
            
                <!-- === Título === -->
                <div id="headingTwo" class="card-header">
                    <h5 class="mb-0">
						<a
						class="font-xs-16 font-20 text-blue collapsed"
						data-toggle="collapse" data-target="#collapseTwo"
						aria-expanded="false" aria-controls="collapseTwo">&gt; INSS,
						Policial, Tribunais (TRE, TRT, TRF e TJ) e Bancária (exceto
						BACEN)</a>
					</h5>
                </div>
                <!-- === Fim Título === -->
                
                <!-- === Corpo === -->
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row bg-gray rounded p-3">
                            
                            <!-- === Ícone === -->
                            <div class="col-12 col-lg-1 text-center">
                                <img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" />
                            </div>
                            
                            <!-- === Fim Ícone === -->

                       		<!-- === Descrição === -->
                            <div class="ml-3 ml-md-5 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-8 coaching-concurso line-height-2 font-11 text-dark">
                                <ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-12">
                                    <li>Analista e Técnico Judiciário e Administrativo dos Tribunais TRE, TRT, TRF e TJ</li>
                                    <li>Polícia Federal, Polícia Rodoviária Federal, Polícias Civis e Militares, Bombeiros</li>
                                    <li>Banco do Brasil, Caixa Econômica Federal, Bancos Regionais</li>
                                    <li>Analista e Técnico do INSS</li>
                                </ul>
                            </div>
                            <!-- === Fim Descrição === -->

                            <!-- === Box Preço === -->
                            <div class="p-0 col-12 col-lg-3">
                                 <a class="coaching-link" href="https://www.exponencialconcursos.com.br/produto/coaching-tribunais-tre-trt-trf-e-tj-area-policial-area-bancaria-e-inss">
                                    <div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" />
                                            </div>
                                            <div class="pl-1 pl-lg-3 col-6">
    
                                                <img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
                                                <p class="text-white mb-2 font-12">COACHING</p>
    
                                            </div>
                                        </div>
                                        <div class="bg-aqua-box text-white font-22 text-center p-1">
                                           MENSAL
                                        </div>
                                        <div class="mt-3 text-white font-18 text-center p-0">
                                            <span style="text-decoration: line-through;">399,90</span> R$ 299,90 <span class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="https://www.exponencialconcursos.com.br/produto/coaching-tribunais-tre-trt-trf-e-tj-area-policial-area-bancaria-e-inss">COMPRAR</span>
                                        </div>
                               		</div>
                               	</a>
                            </div>
                            <!-- === Fim Box Preço === -->
                        </div>
                    </div>
                </div>
                <!-- === Fim Corpo === -->  
            </div>
            <!-- === Fim INSS, Policial, Tribunais... === -->
                
            <!-- === Área Jurídica... === --> 
            <div class="card mt-3 mb-3">
            
                <!-- === Título === -->
                <div id="headingThree" class="card-header">
                    <h5 class="mb-0">
						<a class="font-xs-16 font-20 text-blue collapsed" data-toggle="collapse" data-target="#collapseThree"
						aria-expanded="false" aria-controls="collapseThree">&gt; Área
						Jurídica (Procuradoria, MP, Defensorias, Advocacias, Delegados
						de Policias)</a>
					</h5>
                </div>
                <!-- === Fim Título === -->
                 
                <!-- === Corpo === -->
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                    <div class="card-body">
                        <div class="row bg-gray rounded p-3">
                            
                            <!-- === Ícone === -->
                            <div class="col-12 col-lg-1 text-center">
                                <img src="/wp-content/themes/academy/images/coaching2-area-fiscal.png" width="50" />
                            </div>
                            <!-- === Fim Ícone === -->
                            
                            <!-- === Descrição === -->
                            <div class="ml-0 ml-md-4 ml-lg-0 p-0 mt-3 mt-lg-0 col-12 col-lg-8 coaching-concurso line-height-2 font-11 text-dark">
                                <ul class="ml-auto mr-auto ml-lg-0 mr-lg-0 col-12">
                                    <li>Procuradorias, Defensorias Públicas</li>
                                    <li>Ministérios Públicos, Advocacia Pública</li>
                                    <li>Delegados de Polícia</li>
                                </ul>
                            </div>
                            <!-- === Fim Descrição === -->
                             
                            <!-- === Box Preço === -->
                            <div class="p-0 col-12 col-lg-3">
                            	<a class="coaching-link" href="https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-juridica-2">
                                    <div class="ml-auto mr-auto col-12 col-sm-6 col-md-11 col-lg-12 bg-blue-box pt-3 pb-3 p-0">
                                        <div class="row">
                                            <div class="col-6">
                                                <img class="img-fluid ml-3" src="/wp-content/themes/academy/images/c1-box.png" width="45" />
                                            </div>
                                            <div class="pl-1 pl-lg-3 col-6">
    
                                                <img class="img-fluid ml-3" src="/wp-content/themes/academy/images/coaching-box.png" width="70" />
                                                <p class="text-white mb-2 font-12">COACHING</p>
    
                                            </div>
                                        </div>
                                        <div class="bg-aqua-box text-white font-22 text-center p-1">
                                            MENSAL
                                        </div>
                                        <div class="mt-3 text-white font-18 text-center p-0">
                                            <span style="text-decoration: line-through;">559,90</span> R$ 459,90 <span class="text-center t-d-none font-weight-bold font-12 text-green text-uppercase" href="https://www.exponencialconcursos.com.br/produto/coaching-exponencial-concursos-area-juridica-2">COMPRAR</span>
                                        </div>
                                    </div>
                                 </a>
                            </div>
                            <!-- === Fim Box Preço === -->
                        </div>
                    </div>                      
                </div>
                <!-- === Fim Corpo === -->
            </div>
            <!-- === Fim Área Jurídica... === --> 
        </div>
    </div>

    <div class="bg-gray container-fluid pt-3 pb-2">
        <div class="col-12 text-center mt-4 mb-3">

            <img src="/wp-content/themes/academy/images/coaching2-modulos.png" width="55" />
            <div class="font-pop">Módulos do programa</div>
        </div>
        <div class="container">
            <div class="row ml-auto mr-auto">
                <div class="col-12 col-md-6 pr-0 text-center border-coaching">
                    <div class="col-12 col-md-10 ml-auto">
                        <div class="col-12 col-md-6 ml-auto">
                            <img class="mt-1" src="/wp-content/themes/academy/images/coaching2-raio-x.png" width="55" />
                        </div>
                        <h2 class="text-center text-md-right line-height-1 mt-1">Raio-x do
						Aluno</h2>
                        <p class="text-center text-md-right line-height-1-5">Nosso coaching é 100% personalizado e para isto ser efetivo, coletamos as principais informações sobre VOCÊ para poder direcionar seu esforço com a maior efetividade possível.</p>

                        <div class="col-12 col-md-6 ml-auto">
                            <img class="mt-1" src="/wp-content/themes/academy/images/coaching2-material.png" width="55" />
                        </div>
                        <h2 class="text-center text-md-right line-height-1 mt-1">Material
						de estudo</h2>
                        <p class="text-center text-md-right line-height-1-5">A indicação de materiais de estudo é feita pela EQUIPE DE COACHING com 100% de autonomia para indicar QUALQUER fonte de estudo (não só do Exponencial) para você, com base na sua necessidade e perfil.</p>

                        <div class="col-12 col-md-6 ml-auto">
                            <img class="mt-1" src="/wp-content/themes/academy/images/coaching2-planejamento.png" width="55" />
                        </div>
                        <h2 class="text-center text-md-right line-height-1 mt-1">Planejamento,
						Ciclos e Metas</h2>
                        <p class="text-center text-md-right line-height-1-5">Dentro dos ciclos de estudos, serão definidas as metas base e extras para serem cumpridas pelos alunos. O resultado (aprovação no concurso desejado) está extremamente ligado ao cumprimento destas metas e ao feedback com o coach.</p>

                        <div class="col-12 col-md-6 ml-auto">
                            <img class="mt-1" src="/wp-content/themes/academy/images/coaching2-questoes.png" width="55" />
                        </div>
                        <h2 class="text-center text-md-right line-height-1 mt-1">Questões
						online</h2>
                        <p class="text-center text-md-right line-height-1-5">Temos uma plataforma própria de questões com +400mil questões sendo +180mil comentadas. Mas não se preocupe, seu coach irá te ENCAMINHAR cadernos com as questões que você deve fazer em cada momento do seu estudo.</p>

                    </div>
                </div>
                <div class="col-12 col-md-6 pl-0 mt-0 mt-md-5">
                    <div class="col-12 col-md-10 text-center mt-5 pb-5">
                        <div class="col-12 col-md-6">
                            <img class="mt-1 mt-lg-4" src="/wp-content/themes/academy/images/coaching2-simulados.png" width="55" />
                        </div>
                        <h2 class="text-center text-md-left line-height-1 mt-1">Simulados
						online</h2>
                        <p class="text-center text-md-left line-height-1-5">Com uma plataforma inédita e inovadora, você realizará simulados com base na prova, podendo imprimir, fazer no notebook, celular, destkop, e ainda acompanhar o resultado no nosso ranking detalhado.</p>

                        <div class="col-12 col-md-6">
                            <img class="mt-1" src="/wp-content/themes/academy/images/coaching2-tecnicas.png" width="55" />
                        </div>
                        <h2 class="text-center text-md-left line-height-1 mt-1">Técnicas</h2>
                        <p class="text-center text-md-left line-height-1-5">Não basta mais "estudar, estudar, estudar", é preciso ESTUDAR CERTO. Por isto, iremos te ensinar diversas técnicas como: "estudo de lei seca", "esquematizações e mapas mentais" e muito mais.</p>

                        <div class="col-12 col-md-6">
                            <img class="mt-1" src="/wp-content/themes/academy/images/coaching2-acompanhamento.png" width="55" />
                        </div>
                        <h2 class="text-center text-md-left line-height-1 mt-1">Acompanhamento
						e feedback</h2>
                        <p class="text-center text-md-left line-height-1-5">Seu coach e você devem ser UNHA e CARNE. O compartilhamento das dificuldades e desafios é muito importante para encontrarmos TODOS OS DIAS novos ATALHOS para sua aprovação.</p>

                        <div class="col-12 col-md-6">
                            <img class="mt-1" src="/wp-content/themes/academy/images/coaching2-posedital.png" width="55" />
                        </div>
                        <h2 class="text-center text-md-left line-height-1 mt-1">Pós Edital</h2>
                        <p class="text-center text-md-left line-height-1-5">Saiu edital, bateu desespero? Fique tranquilo, seu coach irá fazer o raio-x do edital, modelar seu plano de estudo e otimizar seu tempo até a prova para você conquistar a SUA VAGA!</p>

                    </div>
                </div>
                <div class="col-12 text-center mt-2">

                    <img src="/wp-content/themes/academy/images/coaching2-aprovacao.png" width="55" />
                    <h2 class="line-height-1 mt-1">Aprovação</h2>
                    <p class="line-height-1-5 col-12 col-md-5 col-lg-4 ml-auto mr-auto">Fácil não será, mas nossa missão é tornar isto uma REALIDADE para você no MENOR TEMPO possível.</p>

                </div>
                <div class="col-12 text-center mt-5">
                    <h5>
					Veja o índice Completo do Coaching <img class="ml-1"
						src="/wp-content/themes/academy/images/coaching2-pdf.png"
						width="30" /> <img
						src="/wp-content/themes/academy/images/coaching2-video.png"
						width="35" />
				</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 text-center mt-5">

        <img src="/wp-content/themes/academy/images/coaching2-equipe.png" width="55" />
        <div class="font-pop">Equipe</div>
    </div>
    <div class="container">[carousel_equipe_coaching]</div>
    <div class="col-12 text-center mt-5">

        <img src="/wp-content/themes/academy/images/coaching2-desconto.png" width="55" />
        <div class="font-pop">Use cupons de descontos</div>
    </div>
    <div class="container pl-0 pr-0">
        <div class="pl-0 pr-0 pddl-xs-0 col-12 ml-auto mr-auto mt-5">
            <div class="pddl-xs-0 row ml-auto mr-auto text-white">
                <div class="col-12 col-lg-4 pl-0 pl-md-5 pl-lg-0 pr-0">
                    <div class="ml-auto mr-auto col-12 col-md-6 col-lg-12 bg-img-desc-1">
                        <div class="line-height-1-4 font-55 font-arial mt-2 mb-0 font-weight-bold">100%</div>
                        <p class="ml-1 mt-0 pb-4">
                            Técnicas de Estudos,
                            <br> Caderno de Questões,
                            <br> Simulados, Sistema de Questões
                        </p>
                    </div>
                </div>
                <div class="col-12 col-lg-4 pl-0 pl-md-5 pl-lg-0 pr-0">
                    <div class="ml-auto mr-auto col-12 col-md-6 col-lg-12 bg-img-desc-2">
                        <div class="font-55 font-arial mt-2 mb-0 font-weight-bold">50%</div>
                        <p class="ml-1 mt-0 pb-4">
                            Pacotes de Cursos,
                            <br> Mapas Mentais
                            <br>
                            <br>
                        </p>
                    </div>
                </div>
                <div class="col-12 col-lg-4 pl-0 pl-md-5 pl-lg-0 pr-0">
                    <div class="ml-auto mr-auto col-12 col-md-6 col-lg-12 bg-img-desc-3">
                        <div class="font-55 font-arial mt-2 mb-0 font-weight-bold">25%</div>
                        <p class="ml-1 mt-0 pb-5">
                            Cursos Avulsos
                            <br>
                        </p>
                        <p>
                            <br>
                        </p>
                    </div>
                </div>
            </div>
            <div class="text-center mt-3">
                <a href="/descontos_promocoes" class="btn u-btn-darkblue mr-2 pl-4 pr-4"><h5 class="mb-0">ACESSAR
					CUPONS</h5></a>
            </div>
        </div>
    </div>
    <div class="bg-gray pb-3 mt-5 pl-2 pr-2">
        <div class="col-12 text-center">

            <img class="mt-4" src="/wp-content/themes/academy/images/perguntas.png" width="50" />
            <h2 class="font-pop">Perguntas frequentes</h2>
        </div>
        <div class="container">[faq faq_topic="coaching-2" limit=-1]</div>
    </div>

    <div class="container">
        <div class="col-12 text-center">

            <img class="mt-4" src="/wp-content/themes/academy/images/coaching2-turma.png" width="50" />

            <div class="font-pop">Turma de coaching</div>
        </div>
        <div class="mt-3 font-12">
            <div class="text-justify p-0">

                O Programa de Turma de Coaching nasceu com o intuito de direcionar os alunos com base nos seus objetivos (por áreas) e nivel (básico e avançada) para permitir que cada um consiga implementar as Metas dentro das suas especificidades. </br>
                </br> Este programa PODE ser para a RETA FINAL, ou seja, para o PÓS-EDITAL confira a opção para cada CONCURSO que lançamos as turmas e você pode encontrar elas <a href="https://www.exponencialconcursos.com.br/concurso/turma-de-coaching">AQUI!</a>
                </br>
                </br>As principais características do programa são:</br>
                </br>
                <ul class="coaching-concurso-3 text-gray">
                    <li><span class="text-blue font-weight-bold">Até 6 pessoas por
						turma:</span> isto significa que você estará em contato direto com as pessoas com mesmo interesse, objetivo e seriedade que você.</li>
                    <li><span class="text-blue font-weight-bold">Interações:</span> Além das interações por whatsapp (contato diário e ilimitado), o Coach/Mentor passa um pente fino regularmente por Skype.</li>
                    <li><span class="text-blue font-weight-bold">Metas:</span> 100% focadas nos concursos da sua área, com direcionamento no nível dos ASSUNTOS das disciplinas (extremamente detalhado), verdadeiro "MAPA DA MINA" para você focar no mais importante e ter o melhor resultado no menor tempo possível.</li>
                    <li><span class="text-blue font-weight-bold">Material de Estudo:</span> 2 a 3 opções por matéria, com total liberdade de escolha pro aluno.</li>
                    <li><span class="text-blue font-weight-bold">Descontos Exclusivos:</span> Cursos do Exponencial com <span class="text-danger">25%</span> (avulsos) e <span class="text-danger">50%</span> (pacote), conforme
                        <span class="text-blue">Políticas de Descontos</span>, além do acesso <span class="text-danger font-weight-bold">GRATUITO</span> ao Sistema de Questões Online.</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-12 text-center">
        <div class="font-16">
            <a href="https://www.exponencialconcursos.com.br/concurso/turma-de-coaching">Procurando
			pelas turmas de coaching?</a>
        </div>
    </div>
    <div class="text-center mt-3 mb-5">
        <h5 class="mb-0">SAIBA MAIS</h5> &nbsp;

    </div>
    </div>

    <?php get_footer() ?>