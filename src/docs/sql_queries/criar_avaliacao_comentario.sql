CREATE TABLE exponenc_corp.comentarios_avaliacoes (
  `cav_id` int(11) NOT NULL AUTO_INCREMENT,
  `usu_id` int(11) unsigned NOT NULL,
  `com_id` int(11) unsigned NOT NULL,
  `cav_data_avaliacao` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `cav_avaliacao` int(2) unsigned NOT NULL,
  PRIMARY KEY (`cav_id`),
  UNIQUE KEY `usu_com_id_UNIQUE` (`usu_id`, `com_id`)
);


alter table exponenc_corp.comentarios add column com_media_avaliacao decimal(3,2);

alter table exponenc_corp.comentarios add column com_qtd_avaliacoes integer(11);

alter table exponenc_corp.comentarios add column com_media_qtd_avaliacoes integer(11);
