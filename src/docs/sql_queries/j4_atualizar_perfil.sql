update wp_usermeta set meta_key = "estado" where meta_key = "billing_state";
update wp_usermeta set meta_key = "endereco_1" where meta_key = "billing_address_1";
update wp_usermeta set meta_key = "endereco_2" where meta_key = "billing_address_2";
update wp_usermeta set meta_key = "cidade_nome" where meta_key = "billing_city";
update wp_usermeta set meta_key = "telefone" where meta_key = "billing_phone";

update wp_usermeta set meta_key = "billing_state" where meta_key = "estado";
update wp_usermeta set meta_key = "billing_address_1" where meta_key = "endereco_1";
update wp_usermeta set meta_key = "billing_address_2" where meta_key = "endereco_2";
update wp_usermeta set meta_key = "billing_city" where meta_key = "cidade_nome";
update wp_usermeta set meta_key = "billing_phone" where meta_key = "telefone";