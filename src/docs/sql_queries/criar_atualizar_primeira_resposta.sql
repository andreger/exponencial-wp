--cria coluna para salvar a primeira seleção
ALTER TABLE `exponenc_corp`.`questoes_resolvidas` 
ADD COLUMN `qre_primeira_selecao` INT(11) NULL AFTER `qre_atualizacao_pendente`;

--atualiza as questoes livres (sim_id = null e cad_id = null) com o valor da resposta atual

update exponenc_corp.questoes_resolvidas set qre_primeira_selecao = qre_selecao where sim_id is null and cad_id is null;
