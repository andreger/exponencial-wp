alter table comentarios add column com_media_simples_avaliacoes decimal(3,2);

update exponenc_corp.comentarios com 
	set com.com_media_simples_avaliacoes = (select round(sum(cav.cav_avaliacao)/count(cav.com_id), 2) from exponenc_corp.comentarios_avaliacoes cav where cav.com_id = com.com_id)
where com.com_qtd_avaliacoes is not null;
