CREATE TABLE IF NOT EXISTS `exponenc_corp`.`questoes_provas` (
  `que_id` INT(11) NOT NULL,
  `pro_id` INT(11) NOT NULL,
  PRIMARY KEY (`que_id`, `pro_id`),
  INDEX `fk_questoes_provas_provas1_idx` (`pro_id` ASC),
  CONSTRAINT `fk_questoes_provas_questoes1`
    FOREIGN KEY (`que_id`)
    REFERENCES `exponenc_corp`.`questoes` (`que_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_questoes_provas_provas1`
    FOREIGN KEY (`pro_id`)
    REFERENCES `exponenc_corp`.`provas` (`pro_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB