update exponenc_corp.questoes q
	left join (
		select que.que_id
			, truncate(sum(case when que.que_resposta = qre.qre_primeira_selecao then 1 else 0 end)/count(*), 4) as media
	from exponenc_corp.questoes_resolvidas qre, exponenc_corp.questoes que
	where qre.que_id = que.que_id 
	and qre.cad_id is null 
	and qre.sim_id is null
    and qre.qre_primeira_selecao is not null
    group by que.que_id
    ) as tot on tot.que_id = q.que_id
 set q.que_dificuldade = (
	case when tot.media is null then null
		when tot.media <= 0.25 then 5 
		when  tot.media <= 0.5 then 4
		when  tot.media <= 0.7 then 3
		when  tot.media <= 0.85 then 2
		else 1 end
	)
where 1=1;
