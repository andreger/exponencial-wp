#select meta_value, count(*) from wp_usermeta where meta_key = "billing_phone" group by meta_value order by count(*) desc;

delete from wp_usermeta where meta_key = "billing_phone" and meta_value = "(11) 12345-6789";
delete from wp_usermeta where meta_key = "billing_phone" and meta_value = "(11) 92345-6789";
delete from wp_usermeta where meta_key = "billing_phone" and meta_value = "(00) 00000-0000";
delete from wp_usermeta where meta_key = "billing_phone" and meta_value = "(11) 11111-1111";
delete from wp_usermeta where meta_key = "billing_phone" and meta_value = "(21) 99999-9999";
delete from wp_usermeta where meta_key = "billing_phone" and meta_value = "(31) 99999-9999";
delete from wp_usermeta where meta_key = "billing_phone" and meta_value = "(99) 99999-9999";
delete from wp_usermeta where meta_key = "billing_phone" and meta_value = "555-666-0606";