alter table exponenc_corp.assuntos add column ass_qtd_questoes int(11);

update exponenc_corp.assuntos ass
set ass.ass_qtd_questoes = (
	select count(*) from exponenc_corp.questoes_assuntos qa where qa.ass_id = ass.ass_id
);
