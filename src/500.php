<?php 
include 'wp-load.php';
date_default_timezone_set('America/Sao_Paulo');

$code = $_GET['code'];
if($code) {
	$data = array(
		'lea_data_hora'	=> date("Y-m-d H:i:s"),
		'lea_tipo_erro'	=> $code
	);
	salvar_log_erro($data);
}

$salvou_email = FALSE;
if($_POST['submit']){
	$email = $_POST['email'];
	if(!empty($email)){
		salva_painel_controle_aviso($email);
		$salvou_email = TRUE;
	}
}

?>
<html>	
<head>
	<link rel="stylesheet" type="text/css" href="/wp-content/themes/academy/css/bootstrap.min.css">
<meta charset="UTF-8">
<style>
body {
    margin: 0;
    padding: 0;
    direction: ltr;
    font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-style: normal;
	font-weight: bold;
	line-height: 1.4;
}

.texto {
    position: relative;
    top: 170px;
    margin-right: 100px;
    left: 100px;
}

#fundo {
    background: url("/wp-content/themes/academy/images/erro-500-fundo.jpg") no-repeat;
    width: 100%;
    min-height: 100%;
    background-size: cover;
}

a:link, a:visited {
	color: #ccc;
	text-decoration: none;
	font-size: 20px;
	font-style: italic;
	font-weight: normal;
}
.barra-email{
	font-size: 14px;
	padding-left: 15px;
}
.form-control{
	display: block;
	width: 100%;
	padding: .375rem .75rem;
	font-size: 1rem;
	line-height: 1.5;
	background-color: #fff;
	background-clip: padding-box;
	border: 1px solid #ced4da;
}
.rouded{
	border-radius: .25rem !important;
}
.btn{
	display: inline-block;
	font-weight: 400;
	text-align: center;
	white-space: nowrap;
	vertical-align: middle;
	border: 1px solid transparent;
	padding: .175rem .75rem;
	font-size: 1rem;
	line-height: 1.5;
	border-radius: .25rem;
	background-color: #72c02c !important;
	color: rgb(255, 255, 255);
}
h1 {
    font-family: 'Raleway',serif!important;
    font-size: 26px!important;
    font-weight: bold;
}
</style>
</head>
<body marginheight="">




<div id="fundo"><?php if($salvou_email == FALSE) : ?>
<div class="fixed pt-2 pb-5" style="background-color: #e7e8ea !important; height: 40px;">
	<form method="post">
		<div class="row toolbar" style="text-align: left">
			<div class="barra-email pl-5 pt-2">
				Informe seu e-mail para ser notificado quando a manutenção terminar: 
			</div>
			<div class="col-4">
				<input class="form-control form-control-lg" type="text" name="email"/>
			</div>
				<input class="btn" type="submit" name="submit" value="Enviar"/>
		</div>
	</form>
</div>
<?php endif; ?>
	<div class="texto" style="color:#fff">
		<div><h1>Desculpe-nos,<br>
		nossos programadores estão<br>
		trabalhando pesado para<br>
		deixar o Exponencial ainda melhor.<br>
		Voltamos já já :)<br></h1>
		</div>
		<div style="margin-top:30px;">
			<a href="#" onclick="history.go(-1)"><img style="vertical-align: middle;display:inline-block;" src="/wp-content/themes/academy/images/voltar.png" vspace=""> Voltar para tela anterior</span>
		</div>
	</div>
</div>
</body>
</html>

