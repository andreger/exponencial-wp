<?php
include_once '../wp-load.php';

$exclusoes = array(
    '/acesso-',
    '/activation',
    '/aula-demonstrativa-download',
    '/cadastro_2',
    '/carrinho',
    '/categoria',
    '/checkout',
    '/coaching-etapa-',
    '/concurso',
    '/confirmacao-',
    '/courses-',
    '/cronograma-',
    '/curso-expirado',
    '/cursos/',
    '/cursos-online',
    '/depoimentos-old',
    '/descontos_promocoes2',
    '/download-curso',
    '/home',
    '/forum-expirado',
    '/lancamento-',
    '/landingpage',
    '/mailchimp',
    '/minha-conta',
    '/metodologia',
    '/pagseguro',
    '/produto-expirado',
    '/questao/',
    '/questoes-de-concurso',
    '/ranking',
    '/reenviar-',
    '/relatorio-',
    '/sem-forum',
    '/senha-reenviada',
    '/sg-test',
    '/simular-erro-500',
    '/upload_course',
    '/xhr-'
);

$links = array();

array_push($links, array(
	'loc' => 'https://www.exponencialconcursos.com.br',
	'changefreq' => 'daily',
	'priority' => '1.0'
));

foreach (get_midias() as $item) {
	$link = get_permalink($item->ID);
	$pular = false;
	foreach ($exclusoes as $exclusao) {
	    
	    if(strpos($link, $exclusao) !== false) {
	        $pular = true;
	        break;
	    }
	}
	
	if($pular) continue;

	array_push($links, array(
		'loc' => $link,
		'changefreq' => 'daily',
		'priority' => '0.7'
	));
}

foreach (get_pages() as $item) {
	

	$link = get_permalink($item->ID);
	$pular = false;
	foreach ($exclusoes as $exclusao) {

		if(strpos($link, $exclusao) !== false) {
			$pular = true; 
			break;
		}
	}

	if($pular) continue;
	
	array_push($links, array(
		'loc' => $link,
		'changefreq' => 'daily',
		'priority' => '0.8'
	));
}

ob_clean();

header('Content-type: text/xml');
header('Pragma: public');
header('Cache-control: private');
header('Expires: -1');
?>
<?php echo '<?xml version="1.0" encoding="UTF-8"?>' ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	<?php foreach ($links as $link) :?>
   <url>
      <loc><?= $link['loc'] ?></loc>
      <changefreq><?= $link['changefreq'] ?></changefreq>
      <priority><?= $link['priority'] ?></priority>
   </url>
   <?php endforeach; ?>
</urlset>