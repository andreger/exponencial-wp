<html>	
<head>
<meta charset="UTF-8">
<style>
body {
    margin: 0;
    padding: 0;
    direction: ltr;
    font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 24px;
	font-style: normal;
	font-weight: bold;
	line-height: 1.4;
}

.texto {
    position: relative;
    top: 170px;
    margin-right: 100px;
    left: 100px;
}

#fundo {
    background: url("/wp-content/themes/academy/images/erro-500-fundo.jpg") no-repeat;
    width: 100%;
    min-height: 100%;
    background-size: cover;
}

a:link, a:visited {
	color: #ccc;
	text-decoration: none;
	font-size: 20px;
	font-style: italic;
	font-weight: normal;
}
</style>
</head>
<body marginheight="">
<div id="fundo">
	<div class="texto" style="color:#fff; font-size:24px; font-weight:bold;">
		<div>Desculpe-nos,<br>
		nossos programadores estão<br>
		trabalhando pesado para<br>
		deixar o Exponencial ainda melhor.<br>
		Voltamos já já :)<br>
		</div>
		<div style="margin-top:30px;">
			<img style="vertical-align: middle;display:inline-block;" src="/wp-content/themes/academy/images/voltar.png" vspace=""> <a href="#" onclick="history.go(-1)">Voltar para tela anterior</span>
		</div>
	</div>
</div>
</body>
</html>

