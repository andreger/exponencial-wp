<?php
// Handle reverse proxy, passing the IP to the server.
// This is used by some plugins to fetch the user's IP.
if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ips = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
    $_SERVER['REMOTE_ADDR'] = $ips[0];
}

define('WP_CACHE', false); // Added by WP Rocket

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/** MySQL hostname */

set_time_limit(300);

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2E6w a+$=8aK*dKPcUg0');
define('SECURE_AUTH_KEY',  'IQ-MjZ4Ivw#D@mxpEXmx');
define('LOGGED_IN_KEY',    'm&bDQm_g9az--W@-9PFt');
define('NONCE_KEY',        ')jLOpOEHKV43Pyf*/51A');
define('AUTH_SALT',        'QCq2f WTs1 FYwP49=k)');
define('SECURE_AUTH_SALT', '=)=%(Z6j1GkB6F45p 7t');
define('LOGGED_IN_SALT',   '@hv)&I+8-frjZyChWjNa');
define('NONCE_SALT',       'I=L!ZHUkTCyT(wxw*9v8');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define ('WPLANG', 'pt_BR');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define('WP_DEBUG_LOG', false);
define('WP_DEBUG_DISPLAY', false);

define('WP_MEMORY_LIMIT', '2048M');

/* Disable WordPress automatic updates */
define('WP_AUTO_UPDATE_CORE', false);

/* Executa código específico de cada ambiente */
$env = trim(file_get_contents(__DIR__ . "/../../.env"));
require_once "wp-config-{$env}.php";

/* That's all, stop editing! Happy blogging. */
/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

date_default_timezone_set('America/Sao_Paulo');
setlocale(LC_COLLATE, 'pt_BR.utf8');

